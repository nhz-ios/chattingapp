//
//  Constant.swift
//  ChattingApp
//
//  Created by Deepak iOS on 22/02/18.
//  Copyright © 2018 Nine Hertz India. All rights reserved.
//

import Foundation
import UIKit
import Alamofire



//MARK:-
//MARK:- UrlApi
let kAPISocialSearchUser                = "search-user.json"
let kAPISocialGetOrherUserProfile       = "other-user.json"
let kAPISocialFollowUser                = "follow-user.json"
let kAPISocialAddPost                   = "add-feeds.json"
let kAPISocialGetAllPost                = "get-feeds.json"
let kAPISocialLikePost                  = "like-feed.json"
let kAPISocialFollowList                = "follow-list.json"
let kAPISocialSavePost                  = "bookmark-api.json"
let kAPISocialPostDetails               = "feed-details.json"
let kAPISocialLikesUserList             = "feed-like-user-list.json"
let kAPISocialDislikesUserList          = "feed-like-user-list.json"
let kAPISocialBlockUser                 = "block-user.json"
let kAPISocialReportUser                = "report-user.json"
let kAPISocialBlockedUsersList          = "block-user-list.json"
let kAPISocialPostComment               = "feed-comment.json"
let kAPISocialCommentsList              = "feed-comment-list.json"
let kAPISocialDeleteComment             = "delete-comment.json"
let kAPISocialSavedPostList             = "bookmark-feed-list.json"
let kAPISocialCollectionList            = "collection-list.json"
let kAPISocialCreateCollection          = "create-collection.json"
let kAPISocialEditCollection            = "edit-collection.json"
let kAPISocialDeleteCollection          = "delete-collection.json"

let kAPISocialReportPost                = "report-feed.json"
let kAPISocialDeletePost                = "delete-feed.json"
let kAPISocialTaggedPeopleList          = "feed-tag-user-list.json"
let kAPISocialTaggedPostsList           = "tag-feed-list.json"
let kAPISocialMyActivitiesList          = "activity-list.json"


let kKeySocialKeyword                   = "keyword"
let kKeySocialOtherUserID               = "other_user_id"
let kKeySocialStatus                    = "status"
let kKeySocialType                      = "type"
let kKeySocialReportMessage             = "report_message"
let kKeySocialIsMediaFeeds              = "mediaFeed"
let kKeySocialPage                      = "pagenumber"
let kKeySocialTotalData                 = "totalData"
let kKeySocialPageLimit                 = "dataLimit"
let kKeySocialMessage                   = "message"


let kKeySocialCaption                   = "caption"
let kKeySocialMultiMedia                = "media_"
let kKeySocialMultiMediaThumb           = "media_thumb_"
let kKeySocialTaggedPeople              = "tagUsers"

let kKeySocialMediaCount                = "media_count"


let kKeySocialFeedDetails               = "feedDetails"
let kKeySocialUserInfo                  = "userInfo"

let kKeySocialFeedId                    = "feedId"
let kKeySocialIsLiked                   = "is_liked"
let kKeySocialLikeCount                 = "like_count"
let kKeySocialIsDisliked                = "is_dis_liked"
let kKeySocialDislikeCount              = "dislike_count"
let kKeySocialCommentsCount             = "comments_count"
let kKeySocialIsSaved                   = "is_saved"
let kKeySocialMedia                     = "image"
let kKeySocialCreatedAt                 = "created_at"
let kKeySocialCreatedAtTimeStamp        = "created_at_timestamp"
let kKeySocialMediaType                 = "media_type"
let kKeySocialMediaURL                  = "media_url"
let kKeySocialMediaThumbURL             = "thumb_url"
let kKeySocialMediaTypeImage            = "Image"
let kKeySocialComment                   = "feed_comment"
let kKeySocialCommentId                 = "comment_id"

let kKeySocialCollectionId              = "collectionId"
let kKeySocialCollectionName            = "collectionName"
let kKeySocialCollectionImage           = "collectionImage"
let kKeySocialCollectionFeeds           = "feedInfo"
let kKeySocialUsersData                 = "UserData"
let kKeySocialOtherUsersData            = "otherUserData"

let kKeySocialActivityTypeFollow        = "follow"
let kKeySocialActivityTypeLike          = "like_post"
let kKeySocialActivityTypeDislike       = "dislike_post"
let kKeySocialActivityTypeComment       = "comment_on_post"
let kKeySocialActivityTypeAddPost       = "add_feed"

let kKeySocialFollowedBy                = "follow_msg"






let kMessageSocialEnterFeed             = "Please write feed message or post at least 1 picture/video"
let kMessageSocialBlock                 = "Are you sure, you want to block this user?"
let kMessageSocialUnblock               = "Are you sure, you want to unblock this user?"
let kMessageSocialReport                = "Are you sure, you want to report this user?"
let kMessageSocialEnterReport           = "Please enter your message"
let kMessageSocialDeleteComment         = "Are you sure, you want to delete this comment?"
let kMessageSocialDeleteCollection      = "Are you sure, you want to delete your collection?"
let kMessageSocialSelectPicture         = "Please select at least 1 picture/video first!!"

let kMessageSocialEnterCollection       = "Please enter collection name"
let kMessageSocialSelectCollectionFeed  = "No feed selected"

let kMessageSocialSelectTagPeople       = "No user selected"
let kMessageSocialReportPost            = "Are you sure, you want to report this post?"
let kMessageSocialDeletePost            = "Are you sure, you want to delete this post?"


//MARK:-
//MARK:- Notification
let kNotificationSocialUpdateFeeds          = "updateFeedsNotification"
let kNotificationSocialAddNewFeed           = "addNewFeedNotification"
let kNotificationSocialUpdateCollections    = "updateCollectionsNotification"
let kNotificationSocialAddNewCollection     = "addNewCollectionNotification"

let kNotificationSocialAddNewComment        = "addNewCommentNotification"
let kNotificationSocialDeleteComment        = "deleteCommentNotification"

let kNotificationSocialUpdateTaggedPeople   = "updateTaggedPeopleNotification"

let kNotificationSocialRefreshData          = "refreshDataNotification"
