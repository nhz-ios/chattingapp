import Foundation
import UIKit

class CollectionModel: NSObject, NSCoding {
    
    var strCollectionId            = ""
    var strCollectionName          = ""
    var strCollectionImage         = ""
    var arrFeeds                   = NSMutableArray()
    
    override init() {
        
    }
    
    //MARK: NSCoding Methods
    func encode(with aCoder: NSCoder) {
        aCoder.encode(strCollectionId, forKey: kKeySocialCollectionId)
        aCoder.encode(strCollectionName, forKey: kKeySocialCollectionName)
        aCoder.encode(strCollectionImage, forKey: kKeySocialCollectionImage)
        aCoder.encode(arrFeeds, forKey: kKeySocialCollectionFeeds)

    }
    required init?(coder aDecoder: NSCoder) {
        strCollectionId = aDecoder.decodeObject(forKey: kKeySocialCollectionId) as? String  ??  ""
        strCollectionName = aDecoder.decodeObject(forKey: kKeySocialCollectionName) as? String  ??  ""
        strCollectionImage = aDecoder.decodeObject(forKey: kKeySocialCollectionImage) as? String  ??  ""
        arrFeeds = aDecoder.decodeObject(forKey: kKeySocialCollectionFeeds) as? NSMutableArray  ??  NSMutableArray()

    }
    init(dict : NSDictionary)
    {
        strCollectionId = dict.decryptedValueForKey(key: kKeySocialCollectionId)
        strCollectionName = dict.decryptedValueForKey(key: kKeySocialCollectionName)
        strCollectionImage = dict.decryptedValueForKey(key: kKeySocialCollectionImage)

        if dict.object(forKey: kKeySocialCollectionFeeds) != nil  &&  (dict.object(forKey: kKeySocialCollectionFeeds) is NSArray) && (dict.object(forKey: kKeySocialCollectionFeeds) as! NSArray).count > 0
        {
            arrFeeds = getFeedsObjectArray(arrResult: dict.object(forKey: kKeySocialCollectionFeeds) as? NSArray ?? NSArray())
        }
    }
    
    
}
