import Foundation
import UIKit

class CommentModel: NSObject, NSCoding {
    
    var strFeedId               = ""
    var strCommentId            = ""
    var strComment              = ""
    var UserInfo: UserModel     = UserModel()

    
    override init() {
        
    }
    
    //MARK: NSCoding Methods
    func encode(with aCoder: NSCoder) {
        aCoder.encode(strFeedId, forKey: kKeySocialFeedId)
        aCoder.encode(strCommentId, forKey: kKeySocialCommentId)
        aCoder.encode(strComment, forKey: kKeySocialComment)
        aCoder.encode(UserInfo, forKey: kKeySocialUserInfo)

    }
    required init?(coder aDecoder: NSCoder) {
        strFeedId = aDecoder.decodeObject(forKey: kKeySocialFeedId) as? String  ??  ""
        strCommentId = aDecoder.decodeObject(forKey: kKeySocialCommentId) as? String  ??  ""
        strComment = aDecoder.decodeObject(forKey: kKeySocialComment) as? String  ??  ""
        UserInfo = aDecoder.decodeObject(forKey: kKeySocialUserInfo) as? UserModel  ??  UserModel()

    }
    init(dict : NSDictionary)
    {
        strFeedId = dict.decryptedValueForKey(key: kKeySocialFeedId)
        strCommentId = dict.decryptedValueForKey(key: kKeySocialCommentId)
        strComment = dict.decryptedValueForKey(key: kKeySocialComment)

        if dict.object(forKey: kKeySocialUserInfo) != nil  &&  (dict.object(forKey: kKeySocialUserInfo) is NSDictionary) && (dict.object(forKey: kKeySocialUserInfo) as! NSDictionary).count > 0
        {
            UserInfo = UserModel.init(dict :dict.object(forKey: kKeySocialUserInfo) as? NSDictionary ?? NSDictionary())
        }
    }
    
    
}
