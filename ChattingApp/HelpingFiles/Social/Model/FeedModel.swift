import Foundation
import UIKit

class FeedModel: NSObject, NSCoding {
    
    var strFeedId               = ""
    var strCaption              = ""
    var strIsLiked              = ""
    var strIsDisliked           = ""
    var strIsSaved              = ""
    var strLikeCount            = ""
    var strDislikeCount         = ""
    var strCommentsCount        = ""
    var strCreatedAt            = ""
    var strCreatedAtTimeStamp   = ""

    var arrMedia                = NSMutableArray()
    var UserInfo: UserModel     = UserModel()
    var arrLast3Comments        = NSMutableArray()
    var strTaggedPeople         = ""

    
    override init() {
        
    }
    
    //MARK: NSCoding Methods
    func encode(with aCoder: NSCoder) {
        aCoder.encode(strFeedId, forKey: kKeySocialFeedId)
        aCoder.encode(strCaption, forKey: kKeySocialCaption)
        aCoder.encode(strIsLiked, forKey: kKeySocialIsLiked)
        aCoder.encode(strIsDisliked, forKey: kKeySocialIsDisliked)
        aCoder.encode(strIsSaved, forKey: kKeySocialIsSaved)
        aCoder.encode(strLikeCount, forKey: kKeySocialLikeCount)
        aCoder.encode(strDislikeCount, forKey: kKeySocialDislikeCount)
        aCoder.encode(strCommentsCount, forKey: kKeySocialCommentsCount)
        aCoder.encode(strCreatedAt, forKey: kKeySocialCreatedAt)
        aCoder.encode(strCreatedAtTimeStamp, forKey: kKeySocialCreatedAtTimeStamp)
        aCoder.encode(arrMedia, forKey: kKeySocialMedia)
        aCoder.encode(UserInfo, forKey: kKeySocialUserInfo)
        aCoder.encode(arrLast3Comments, forKey: kKeySocialComment)
        aCoder.encode(strTaggedPeople, forKey: kKeySocialTaggedPeople)
        

    }
    required init?(coder aDecoder: NSCoder) {
        strFeedId = aDecoder.decodeObject(forKey: kKeySocialFeedId) as? String  ??  ""
        strCaption = aDecoder.decodeObject(forKey: kKeySocialCaption) as? String  ??  ""
        strIsLiked = aDecoder.decodeObject(forKey: kKeySocialIsLiked) as? String  ??  ""
        strIsDisliked = aDecoder.decodeObject(forKey: kKeySocialIsDisliked) as? String  ??  ""
        strIsSaved = aDecoder.decodeObject(forKey: kKeySocialIsSaved) as? String  ??  ""
        strLikeCount = aDecoder.decodeObject(forKey: kKeySocialLikeCount) as? String  ??  ""
        strDislikeCount = aDecoder.decodeObject(forKey: kKeySocialDislikeCount) as? String  ??  ""
        strCommentsCount = aDecoder.decodeObject(forKey: kKeySocialCommentsCount) as? String  ??  ""
        strCreatedAt = aDecoder.decodeObject(forKey: kKeySocialCreatedAt) as? String  ??  ""
        strCreatedAtTimeStamp = aDecoder.decodeObject(forKey: kKeySocialCreatedAtTimeStamp) as? String  ??  ""
        arrMedia = aDecoder.decodeObject(forKey: kKeySocialMedia) as? NSMutableArray  ??  NSMutableArray()
        UserInfo = aDecoder.decodeObject(forKey: kKeySocialUserInfo) as? UserModel  ??  UserModel()
        arrLast3Comments = aDecoder.decodeObject(forKey: kKeySocialComment) as? NSMutableArray  ??  NSMutableArray()
        strTaggedPeople = aDecoder.decodeObject(forKey: kKeySocialTaggedPeople) as? String  ??  ""

    }
    init(dict : NSDictionary)
    {
        strFeedId = dict.decryptedValueForKey(key: kKeySocialFeedId)
        strCaption = dict.decryptedValueForKey(key: kKeySocialCaption)
        strIsLiked = dict.decryptedValueForKey(key: kKeySocialIsLiked)
        strIsDisliked = dict.decryptedValueForKey(key: kKeySocialIsDisliked)
        strIsSaved = dict.decryptedValueForKey(key: kKeySocialIsSaved)
        strLikeCount = dict.decryptedValueForKey(key: kKeySocialLikeCount)
        strDislikeCount = dict.decryptedValueForKey(key: kKeySocialDislikeCount)
        strCommentsCount = dict.decryptedValueForKey(key: kKeySocialCommentsCount)
        strCreatedAt = dict.decryptedValueForKey(key: kKeySocialCreatedAt)
        strCreatedAtTimeStamp = dict.decryptedValueForKey(key: kKeySocialCreatedAtTimeStamp)

        if dict.object(forKey: kKeySocialMedia) != nil  &&  (dict.object(forKey: kKeySocialMedia) is NSArray) && (dict.object(forKey: kKeySocialMedia) as! NSArray).count > 0
        {
            arrMedia = getMediaObjectArray(arrResult: dict.object(forKey: kKeySocialMedia) as? NSArray ?? NSArray())
        }
        if dict.object(forKey: kKeySocialUserInfo) != nil  &&  (dict.object(forKey: kKeySocialUserInfo) is NSDictionary) && (dict.object(forKey: kKeySocialUserInfo) as! NSDictionary).count > 0
        {
            UserInfo = UserModel.init(dict :dict.object(forKey: kKeySocialUserInfo) as? NSDictionary ?? NSDictionary())
        }
        if dict.object(forKey: kKeySocialComment) != nil  &&  (dict.object(forKey: kKeySocialComment) is NSArray) && (dict.object(forKey: kKeySocialComment) as! NSArray).count > 0
        {
            arrLast3Comments = getCommentsObjectArray(arrResult: dict.object(forKey: kKeySocialComment) as? NSArray ?? NSArray())
        }
        strTaggedPeople = dict.decryptedValueForKey(key: kKeySocialTaggedPeople)
    }
    
    
}
