import Foundation
import UIKit

class ActivityModel: NSObject, NSCoding {
    
//    var strActivityId           = ""
    var strActivityMessage      = ""
    var strActivityType         = ""

    var feedData: FeedModel     = FeedModel()
    var arrUsersData            = NSMutableArray()
    var userInfo: UserModel     = UserModel()
    var otherUserData: UserModel     = UserModel()

    
    override init() {
        
    }
    
    //MARK: NSCoding Methods
    func encode(with aCoder: NSCoder) {
//        aCoder.encode(strActivityId, forKey: kKeySocialActivityId)
        aCoder.encode(strActivityMessage, forKey: kKeySocialMessage)
        aCoder.encode(strActivityType, forKey: kKeySocialType)
        aCoder.encode(feedData, forKey: kKeySocialCollectionFeeds)
        aCoder.encode(arrUsersData, forKey: kKeySocialUsersData)
        aCoder.encode(userInfo, forKey: kKeySocialUserInfo)
        aCoder.encode(otherUserData, forKey: kKeySocialOtherUsersData)


    }
    required init?(coder aDecoder: NSCoder) {
//        strActivityId = aDecoder.decodeObject(forKey: kKeySocialActivityId) as? String  ??  ""
        strActivityMessage = aDecoder.decodeObject(forKey: kKeySocialMessage) as? String  ??  ""
        strActivityType = aDecoder.decodeObject(forKey: kKeySocialType) as? String  ??  ""
        feedData = aDecoder.decodeObject(forKey: kKeySocialCollectionFeeds) as? FeedModel  ??  FeedModel()
        arrUsersData = aDecoder.decodeObject(forKey: kKeySocialUsersData) as? NSMutableArray  ??  NSMutableArray()
        userInfo = aDecoder.decodeObject(forKey: kKeySocialUserInfo) as? UserModel  ??  UserModel()
        otherUserData = aDecoder.decodeObject(forKey: kKeySocialOtherUsersData) as? UserModel  ??  UserModel()

    }
    init(dict : NSDictionary)
    {
//        strActivityId = dict.decryptedValueForKey(key: kKeySocialActivityId)
        strActivityMessage = dict.decryptedValueForKey(key: kKeySocialMessage)
        strActivityType = dict.decryptedValueForKey(key: kKeySocialType)

        if dict.object(forKey: kKeySocialCollectionFeeds) != nil  &&  (dict.object(forKey: kKeySocialCollectionFeeds) is NSDictionary) && (dict.object(forKey: kKeySocialCollectionFeeds) as! NSDictionary).count > 0
        {
            feedData = FeedModel.init(dict :dict.object(forKey: kKeySocialCollectionFeeds) as? NSDictionary ?? NSDictionary())
        }
        if dict.object(forKey: kKeySocialUsersData) != nil  &&  (dict.object(forKey: kKeySocialUsersData) is NSArray) && (dict.object(forKey: kKeySocialUsersData) as! NSArray).count > 0
        {
            arrUsersData = getUsersObjectArray(arrResult: dict.object(forKey: kKeySocialUsersData) as? NSArray ?? NSArray())
        }
        if dict.object(forKey: kKeySocialUserInfo) != nil  &&  (dict.object(forKey: kKeySocialUserInfo) is NSDictionary) && (dict.object(forKey: kKeySocialUserInfo) as! NSDictionary).count > 0
        {
            userInfo = UserModel.init(dict :dict.object(forKey: kKeySocialUserInfo) as? NSDictionary ?? NSDictionary())
        }
        if dict.object(forKey: kKeySocialOtherUsersData) != nil  &&  (dict.object(forKey: kKeySocialOtherUsersData) is NSDictionary) && (dict.object(forKey: kKeySocialOtherUsersData) as! NSDictionary).count > 0
        {
            otherUserData = UserModel.init(dict :dict.object(forKey: kKeySocialOtherUsersData) as? NSDictionary ?? NSDictionary())
        }
    }
    
    
}
