import Foundation
import UIKit

class FeedMediaModel: NSObject, NSCoding {
    
    var strMediaType               = ""
    var strMediaURL              = ""
    var strMediaThumbURL              = ""

    
    override init() {
        
    }
    
    //MARK: NSCoding Methods
    func encode(with aCoder: NSCoder) {
        aCoder.encode(strMediaType, forKey: kKeySocialMediaType)
        aCoder.encode(strMediaURL, forKey: kKeySocialMediaURL)
        aCoder.encode(strMediaThumbURL, forKey: kKeySocialMediaThumbURL)
    }

    required init?(coder aDecoder: NSCoder) {
        strMediaType = aDecoder.decodeObject(forKey: kKeySocialMediaType) as? String  ??  ""
        strMediaURL = aDecoder.decodeObject(forKey: kKeySocialMediaURL) as? String  ??  ""
        strMediaThumbURL = aDecoder.decodeObject(forKey: kKeySocialMediaThumbURL) as? String  ??  ""
    }

    init(dict : NSDictionary)
    {
        strMediaType = dict.decryptedValueForKey(key: kKeySocialMediaType)
        strMediaURL = dict.decryptedValueForKey(key: kKeySocialMediaURL)
        strMediaThumbURL = dict.decryptedValueForKey(key: kKeySocialMediaThumbURL)
        print("strMediaThumbURL", strMediaThumbURL)
    }
}
