//
//  YPPhotoFiltersVC.swift
//  photoTaking
//
//  Created by Sacha Durand Saint Omer on 21/10/16.
//  Copyright © 2016 octopepper. All rights reserved.
//

import UIKit

protocol IsMediaFilterVC: class {
    var didSave: ((YPMediaItem) -> Void)? { get set }
    var didCancel: (() -> Void)? { get set }
}

open class YPPhotoFiltersVC: UIViewController, IsMediaFilterVC, UIGestureRecognizerDelegate {
    
   
    required public init(inputPhoto: YPMediaPhoto, isFromSelectionVC: Bool, iscomefromgallery: Bool) {
        super.init(nibName: nil, bundle: nil)
        
        self.inputPhoto = inputPhoto
        self.isFromSelectionVC = isFromSelectionVC
        self.iscomefromgallery = iscomefromgallery
    }

    
    public var inputPhoto: YPMediaPhoto!
    public var isFromSelectionVC = false
    public var iscomefromgallery = false

    public var didSave: ((YPMediaItem) -> Void)?
    public var didCancel: (() -> Void)?


    fileprivate let filters: [YPFilter] = YPConfig.filters

    fileprivate var selectedFilter: YPFilter?
    
    fileprivate var filteredThumbnailImagesArray: [UIImage] = []
    fileprivate var thumbnailImageForFiltering: CIImage? // Small image for creating filters thumbnails
    fileprivate var currentlySelectedImageThumbnail: UIImage? // Used for comparing with original image when tapped

    fileprivate var v = YPFiltersView()

    override open var prefersStatusBarHidden: Bool { return YPConfig.hidesStatusBar }
    override open func loadView() { view = v }
    required public init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    // MARK: - Life Cycle ♻️

    override open func viewDidLoad() {
        super.viewDidLoad()
        v.isComefromGallery = self.iscomefromgallery

        // Setup of main image an thumbnail images
        v.imageView.image = inputPhoto.image
        thumbnailImageForFiltering = thumbFromImage(inputPhoto.image)
        DispatchQueue.global().async {
            self.filteredThumbnailImagesArray = self.filters.map { filter -> UIImage in
                if let applier = filter.applier,
                    let thumbnailImage = self.thumbnailImageForFiltering,
                    let outputImage = applier(thumbnailImage) {
                    return outputImage.toUIImage()
                } else {
                    return self.inputPhoto.originalImage
                }
            }
            DispatchQueue.main.async {
                self.v.collectionView.reloadData()
                self.v.collectionView.selectItem(at: IndexPath(row: 0, section: 0),
                                            animated: false,
                                            scrollPosition: UICollectionView.ScrollPosition.bottom)
                self.v.filtersLoader.stopAnimating()
            }
        }
        
        // Setup of Collection View
        v.collectionView.register(YPFilterCollectionViewCell.self, forCellWithReuseIdentifier: "FilterCell")
        v.collectionView.dataSource = self
        v.collectionView.delegate = self
        
        // Setup of Navigation Bar
        title = "SAVE PHOTO"
        if isFromSelectionVC {
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(back))
            
            //navigationItem.leftBarButtonItem = UIBarButtonItem(title: YPConfig.wordings.done, style: .plain, target: self, action: #selector(cancel))
        }
        setupRightBarButton()
        
        YPHelper.changeBackButtonIcon(self)
        YPHelper.changeBackButtonTitle(self)
        
        // Touch preview to see original image.
        let touchDownGR = UILongPressGestureRecognizer(target: self,
                                                       action: #selector(handleTouchDown))
        touchDownGR.minimumPressDuration = 0
        touchDownGR.delegate = self
        v.imageView.addGestureRecognizer(touchDownGR)
        v.imageView.isUserInteractionEnabled = true
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPixelImageEditComplete), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateEditPixelIamgeNotification), name: NSNotification.Name(rawValue: kPixelImageEditComplete), object: nil)
    }
    @objc func UpdateEditPixelIamgeNotification(data : NSNotification)
    {
        let image = data.object as! UIImage
        v.imageView.image = image
        thumbnailImageForFiltering = thumbFromImage(image)
        inputPhoto.originalImage = image
        inputPhoto.modifiedImage = nil
        guard let didSave = didSave else { return print("Don't have saveCallback") }
        didSave(YPMediaItem.photo(p: inputPhoto))
        
        DispatchQueue.global().async {
            self.filteredThumbnailImagesArray = self.filters.map { filter -> UIImage in
                if let applier = filter.applier,
                    let thumbnailImage = self.thumbnailImageForFiltering,
                    let outputImage = applier(thumbnailImage) {
                    return outputImage.toUIImage()
                } else {
                    return self.inputPhoto.originalImage
                }
            }
            DispatchQueue.main.async {
                self.v.collectionView.reloadData()
                self.v.collectionView.selectItem(at: IndexPath(row: 0, section: 0),
                                                 animated: false,
                                                 scrollPosition: UICollectionView.ScrollPosition.bottom)
                self.v.filtersLoader.stopAnimating()
            }
        }

        
    }
    // MARK: Setup - ⚙️
    
    fileprivate func setupRightBarButton() {
        let rightBarButtonTitle = isFromSelectionVC ? YPConfig.wordings.done : YPConfig.wordings.next
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: rightBarButtonTitle,
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(save))
        //navigationItem.rightBarButtonItem?.tintColor = YPConfig.colors.tintColor
    }
    
    // MARK: - Methods 🏓

    @objc
    fileprivate func handleTouchDown(sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            v.imageView.image = inputPhoto.originalImage
        case .ended:
            v.imageView.image = currentlySelectedImageThumbnail ?? inputPhoto.originalImage
        default: ()
        }
    }
    
    fileprivate func thumbFromImage(_ img: UIImage) -> CIImage {
        let k = img.size.width / img.size.height
        let scale = UIScreen.main.scale
        let thumbnailHeight: CGFloat = 300 * scale
        let thumbnailWidth = thumbnailHeight * k
        let thumbnailSize = CGSize(width: thumbnailWidth, height: thumbnailHeight)
        UIGraphicsBeginImageContext(thumbnailSize)
        img.draw(in: CGRect(x: 0, y: 0, width: thumbnailSize.width, height: thumbnailSize.height))
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return smallImage!.toCIImage()!
    }
    
    // MARK: - Actions 🥂

    @objc
    func cancel() {
        didCancel?()
    }
    
    @objc
    func back()
    {
        
        let nav = appDelegate.TabLogin.selectedViewController  as! UINavigationController
        if iscomefromgallery == true
        {
            nav.popViewController(animated: false)
            return
        }
        
        let arrVC : NSArray = (nav.viewControllers) as NSArray
        for VC in arrVC
        {
            if VC is PostCreateViewController
            {
                nav.popToViewController(VC as! UIViewController, animated: true)
                return
            }
        }
    }
    
    @objc
    func save() {
        guard let didSave = didSave else { return print("Don't have saveCallback") }
        self.navigationItem.rightBarButtonItem = YPLoaders.defaultLoader
        DispatchQueue.global().async {
            if let f = self.selectedFilter,
                let applier = f.applier,
                let ciImage = self.inputPhoto.originalImage.toCIImage(),
                let modifiedFullSizeImage = applier(ciImage) {
                self.inputPhoto.modifiedImage = modifiedFullSizeImage.toUIImage()
            } else {
                self.inputPhoto.modifiedImage = nil
            }
            DispatchQueue.main.async {

                self.setupRightBarButton()

                didSave(YPMediaItem.photo(p: self.inputPhoto))
                
                let nav = self.appDelegate.TabLogin.selectedViewController  as! UINavigationController
                if self.iscomefromgallery == true
                {
                    nav.popViewController(animated: false)
                    return
                }
                let tempimage = (self.inputPhoto.modifiedImage == nil) ? self.inputPhoto.originalImage :  self.inputPhoto.modifiedImage
                let strPath = "\(self.appDelegate.strVideoFolderPath)/Image1.jpg"
                self.RemoveDirectoryUrls(strPath)
                self.addImageTODirectory(strPath, tempimage!)
                self.appDelegate.arrPostUrls.add(strPath)
                self.appDelegate.arrPostthumbUrls.add("")

                self.navigationItem.rightBarButtonItem = YPLoaders.defaultLoader
                self.navigationItem.leftBarButtonItem = YPLoaders.defaultLoader
                self.view.isUserInteractionEnabled = false
                NotificationCenter.default.post(name: NSNotification.Name(klibCloseButtonClick), object: nil)

                let arrVC : NSArray = (nav.viewControllers) as NSArray
                for VC in arrVC
                {
                    if VC is WriteFeedVC
                    {
                        self.view.isUserInteractionEnabled = true
                        nav.popToViewController(VC as! UIViewController, animated: true)
                        return
                    }
                }
                
            }
        }
    }
}

extension YPPhotoFiltersVC: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredThumbnailImagesArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filter = filters[indexPath.row]
        let image = filteredThumbnailImagesArray[indexPath.row]
        if let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "FilterCell",
                                 for: indexPath) as? YPFilterCollectionViewCell {
            cell.name.text = filter.name
            cell.imageView.image = image
            return cell
        }
        return UICollectionViewCell()
    }
}

extension YPPhotoFiltersVC: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedFilter = filters[indexPath.row]
        currentlySelectedImageThumbnail = filteredThumbnailImagesArray[indexPath.row]
        self.v.imageView.image = currentlySelectedImageThumbnail
        self.isFromSelectionVC = true
        setupRightBarButton()
    }
}
