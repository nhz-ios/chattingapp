//
//  VideoFiltersVC.swift
//  YPImagePicker
//
//  Created by Nik Kov || nik-kov.com on 18.04.2018.
//  Copyright © 2018 Yummypets. All rights reserved.
//

import UIKit
import Photos
import PryntTrimmerView
import AVKit
public protocol FilterVideoViewControllerDelegate {
    func filterVideoViewControllerVideoDidFilter(video: AVURLAsset)
    func filterVideoViewControllerDidCancel()
}
public class YPVideoFiltersVC: UIViewController, IsMediaFilterVC {
    
    @IBOutlet weak var collectionViewContainer: UIView!
    public var delegate: FilterVideoViewControllerDelegate?
//    @IBOutlet weak var filterBottomItem: YPMenuItem!
//    @IBOutlet weak var trimBottomItem: YPMenuItem!
//    @IBOutlet weak var coverBottomItem: YPMenuItem!
    
    @IBOutlet weak var videoView: YPVideoView!
    @IBOutlet weak var trimmerView: TrimmerView!
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var coverThumbSelectorView: ThumbSelectorView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnTrim: UIButton!
    @IBOutlet weak var btnCover: UIButton!

    public var inputVideo: YPMediaVideo!
    public var inputAsset: AVAsset { return AVAsset(url: inputVideo.url) }
    
    private var playbackTimeCheckerTimer: Timer?
    private var imageGenerator: AVAssetImageGenerator?
    private var isFromSelectionVC = false
    private var videoNumber = 0

    var didSave: ((YPMediaItem) -> Void)?
    var didCancel: (() -> Void)?

    var collectionView: UICollectionView!
    var filtersLoader: UIActivityIndicatorView!
    
    fileprivate let filters: [YPFilter] = YPConfig.filters
    fileprivate var selectedFilter: YPFilter?
    fileprivate var filteredThumbnailImagesArray: [UIImage] = []
    fileprivate var thumbnailImageForFiltering: CIImage?
    fileprivate var currentlySelectedImageThumbnail: UIImage?
    
    //video player vishal change video filter
    @IBOutlet weak var videoAVView: UIView!

    fileprivate var avpController: AVPlayerViewController!
    fileprivate var avVideoComposition: AVVideoComposition!
    fileprivate var playerItem: AVPlayerItem!
    fileprivate var videoPlayer:AVPlayer!
    fileprivate var videoAv: AVURLAsset?
    fileprivate var originalImage: UIImage?

    
    /// Designated initializer
    public class func initWith(video: YPMediaVideo,
                               isFromSelectionVC: Bool , videoNumber : Int) -> YPVideoFiltersVC
    {
        let vc = YPVideoFiltersVC(nibName: "YPVideoFiltersVC", bundle: Bundle(for: YPVideoFiltersVC.self))
        vc.inputVideo = video
        vc.isFromSelectionVC = isFromSelectionVC
        vc.videoNumber =  videoNumber
        return vc
    }
    
    // MARK: - Live cycle
    override public func viewDidLoad() {

        super.viewDidLoad()

        trimmerView.mainColor = YPConfig.colors.trimmerMainColor
        trimmerView.handleColor = YPConfig.colors.trimmerHandleColor
        trimmerView.positionBarColor = YPConfig.colors.positionLineColor
        trimmerView.maxDuration = YPConfig.video.trimmerMaxDuration
        trimmerView.minDuration = YPConfig.video.trimmerMinDuration
        
        coverThumbSelectorView.thumbBorderColor = YPConfig.colors.coverSelectorBorderColor
        
//        filterBottomItem.textLabel.text = YPConfig.wordings.filter
//        trimBottomItem.textLabel.text = YPConfig.wordings.trim
//        coverBottomItem.textLabel.text = YPConfig.wordings.cover
//
//        filterBottomItem.button.addTarget(self, action: #selector(selectFilter), for: .touchUpInside)
//        trimBottomItem.button.addTarget(self, action: #selector(selectTrim), for: .touchUpInside)
//        coverBottomItem.button.addTarget(self, action: #selector(selectCover), for: .touchUpInside)
        
        // Remove the default and add a notification to repeat playback from the start
      //  videoView.removeReachEndObserver()

        
        // Set initial video cover
        imageGenerator = AVAssetImageGenerator(asset: self.inputAsset)
        imageGenerator?.appliesPreferredTrackTransform = true
        didChangeThumbPosition(CMTime(seconds: 1, preferredTimescale: 1))
        
        // Navigation bar setup
        title = YPConfig.wordings.filter
        
        if isFromSelectionVC {
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(back))
            
            //navigationItem.leftBarButtonItem = UIBarButtonItem(title: YPConfig.wordings.cancel, style: .plain, target: self, action: #selector(cancel))
        }
        setupRightBarButtonItem()
    
        // Setup of Collection View
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout())
        collectionView.register(YPFilterCollectionViewCell.self, forCellWithReuseIdentifier: "FilterCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        self.collectionViewContainer.addSubview(self.collectionView)
        
        filtersLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        filtersLoader.hidesWhenStopped = true
        filtersLoader.startAnimating()
        filtersLoader.color = YPConfig.colors.tintColor
        self.collectionViewContainer.addSubview(self.filtersLoader)
        
        filtersLoader.centerInContainer()
        
        self.filtersLoader.startAnimating()
        
        thumbnailImageForFiltering = thumbFromImage(inputVideo.thumbnail)
        
        DispatchQueue.global().async {
            self.filteredThumbnailImagesArray = self.filters.map { filter -> UIImage in
                if let applier = filter.applier,
                    let thumbnailImage = self.thumbnailImageForFiltering,
                    let outputImage = applier(thumbnailImage) {
                    return outputImage.toUIImage()
                } else {
                
                    return self.inputVideo.thumbnail
                }
            }
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.collectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.bottom)
                self.filtersLoader.stopAnimating()
            }
        }
    }
    override public func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        print("inputAsset",inputAsset)
        trimmerView.asset = inputAsset
        trimmerView.delegate = self
        
        coverThumbSelectorView.asset = inputAsset
        coverThumbSelectorView.delegate = self
        
        ResetButtons()
        btnFilter.backgroundColor = UIColor(r: 126, g: 139, b: 153)
        btnFilter.isSelected = true
        selectFilter()
       // videoView.loadVideo(inputVideo)
        
        videoAv = AVURLAsset(url: inputVideo.url as URL, options: nil)
        
        let filterName = appDelegate.dicFilterNameSave.valueForNullableKey(key: "\(videoNumber)")
        if filterName != ""
        {
            playVideo(video: videoAv!, filterName: getColorFilterNameCode(filterName))
        }
        else
        {
            playVideo(video: videoAv!, filterName: "Normal")
        }
        

        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: self.collectionView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.collectionViewContainer, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        
        let verticalConstraint = NSLayoutConstraint(item: self.collectionView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.collectionViewContainer, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        
        let widthConstraint = NSLayoutConstraint(item: self.collectionView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.width)
        
        let heightConstraint = NSLayoutConstraint(item: self.collectionView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 150)
        
        let leadingConstraint = NSLayoutConstraint(item: self.collectionView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.collectionViewContainer, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
        
        let trailingConstraint = NSLayoutConstraint(item: self.collectionView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.collectionViewContainer, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
        
        self.collectionViewContainer.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint, leadingConstraint, trailingConstraint])
        
        let horizontalFiltersLoaderConstraint = NSLayoutConstraint(item: self.filtersLoader, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.collectionViewContainer, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        
        let verticalFiltersLoaderConstraint = NSLayoutConstraint(item: self.filtersLoader, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.collectionViewContainer, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        
        self.collectionViewContainer.addConstraints([horizontalFiltersLoaderConstraint, verticalFiltersLoaderConstraint])
        
        NotificationCenter.default
            .addObserver(self, selector: #selector(itemDidFinishPlaying(_:)), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopPlaybackTimeChecker()
        //videoView.stop()
        
        if videoPlayer != nil
        {
            videoPlayer.pause()
            videoPlayer = nil
            avpController = nil
            avVideoComposition = nil
        }
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)

    }
    
    func setupRightBarButtonItem() {
        let rightBarButtonTitle = isFromSelectionVC ? YPConfig.wordings.done : YPConfig.wordings.next
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: rightBarButtonTitle,
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(saveVideoMethod))
        //navigationItem.rightBarButtonItem?.tintColor = YPConfig.colors.tintColor
    }
    
    // MARK: - Top buttons

    
    @objc func back() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func cancel() {
        didCancel?()
    }
    
    // MARK: - Bottom buttons
    @objc public func selectFilter() {
        title = YPConfig.wordings.filter
        
//        filterBottomItem.select()
//        trimBottomItem.deselect()
//        coverBottomItem.deselect()
        
        collectionView.isHidden = false
        trimmerView.isHidden = true
       // videoView.isHidden = false
        videoAVView.isHidden = false
        coverImageView.isHidden = true
        coverThumbSelectorView.isHidden = true
    }
    
    @objc public func selectTrim() {
        title = YPConfig.wordings.trim
        
//        filterBottomItem.deselect()
//        trimBottomItem.select()
//        coverBottomItem.deselect()

        collectionView.isHidden = true
        trimmerView.isHidden = false
        //videoView.isHidden = false
        videoAVView.isHidden = false
        coverImageView.isHidden = true
        coverThumbSelectorView.isHidden = true
    }
    
    @objc public func selectCover() {
        title = YPConfig.wordings.cover
        
//        filterBottomItem.deselect()
//        trimBottomItem.deselect()
//       coverBottomItem.select()
        
        collectionView.isHidden = true
        trimmerView.isHidden = true
        //videoView.isHidden = true
        videoAVView.isHidden = true
        coverImageView.isHidden = false
        coverThumbSelectorView.isHidden = false
        
        stopPlaybackTimeChecker()
        
        if videoPlayer != nil
        {
            videoPlayer.pause()
        }
       // videoView.stop()
    }
    
    // MARK: - Various Methods

    // Updates the bounds of the cover picker if the video is trimmed
    // TODO: Now the trimmer framework doesn't support an easy way to do this.
    // Need to rethink a flow or search other ways.
    func updateCoverPickerBounds() {
        if let startTime = trimmerView.startTime,
            let endTime = trimmerView.endTime {
            if let selectedCoverTime = coverThumbSelectorView.selectedTime {
                let range = CMTimeRange(start: startTime, end: endTime)
                if !range.containsTime(selectedCoverTime) {
                    // If the selected before cover range is not in new trimeed range,
                    // than reset the cover to start time of the trimmed video
                }
            } else {
                // If none cover time selected yet, than set the cover to the start time of the trimmed video
            }
        }
    }
    
    func layout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 2
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 100, height: 120)
        return layout
    }
    
    fileprivate func thumbFromImage(_ img: UIImage) -> CIImage {
        
        let k = img.size.width / img.size.height
        let scale = UIScreen.main.scale
        let thumbnailHeight: CGFloat = 300 * scale
        let thumbnailWidth = thumbnailHeight * k
        let thumbnailSize = CGSize(width: thumbnailWidth, height: thumbnailHeight)
        UIGraphicsBeginImageContext(thumbnailSize)
        img.draw(in: CGRect(x: 0, y: 0, width: thumbnailSize.width, height: thumbnailSize.height))
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return smallImage!.toCIImage()!
    }
    
    // MARK: - Trimmer playback
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = trimmerView.startTime {
            if videoPlayer != nil
            {
                videoPlayer.seek(to: startTime)
                videoPlayer.play()
            }
            //videoView.player.seek(to: startTime)
        }
    }
    
    func startPlaybackTimeChecker() {
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer
            .scheduledTimer(timeInterval: 0.05, target: self,
                            selector: #selector(onPlaybackTimeChecker),
                            userInfo: nil,
                            repeats: true)
    }
    
    func stopPlaybackTimeChecker() {
        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
    @objc func onPlaybackTimeChecker() {
        guard let startTime = trimmerView.startTime,
            let endTime = trimmerView.endTime else {
            return
        }
        
        if videoPlayer == nil
        {
            return
        }
        
        let playBackTime = videoPlayer.currentTime()
        trimmerView.seek(to: playBackTime)
        
        if playBackTime >= endTime {
            videoPlayer.seek(to: startTime,
                                  toleranceBefore: kCMTimeZero,
                                  toleranceAfter: kCMTimeZero)
            trimmerView.seek(to: startTime)
        }
    }
    
     // MARK: -  VISHAl Video Player Methods  
    
//    public init(video: AVURLAsset) {
//        super.init(nibName: nil, bundle: nil)
//        self.video = video
////        self.image = video.videoToUIImage()
////        self.originalImage = self.image
//
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    func playVideo(video:AVURLAsset, filterName:String)
    {
        print("filterName******",filterName)
        let avPlayerItem = AVPlayerItem(asset: video)
        if(filterName != "Normal")
        {
//            avVideoComposition = AVVideoComposition(asset: self.videoAv!, applyingCIFiltersWithHandler: { request in
//                let source = request.sourceImage.clampedToExtent()
//                let filter = CIFilter(name:filterName)!
//                filter.setDefaults()
//                filter.setValue(source, forKey: kCIInputImageKey)
//                let output = filter.outputImage!
//                request.finish(with:output, context: nil)
//            })
            avVideoComposition = createVideoComposition(filterName, self.videoAv!)
            avPlayerItem.videoComposition = avVideoComposition
        }
        if self.videoPlayer == nil {
            self.videoPlayer = AVPlayer(playerItem: avPlayerItem)
            self.avpController = AVPlayerViewController()
            self.avpController.player = self.videoPlayer
            self.avpController.view.frame = self.videoAVView.bounds
            self.avpController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
            self.avpController.removeFromParentViewController()
            self.addChildViewController(avpController)
            self.videoAVView.addSubview(avpController.view)
            videoPlayer.play()
        }
        else {
            videoPlayer.replaceCurrentItem(with: avPlayerItem)
            videoPlayer.play()
        }
    }
    
//     func applyFilter() {
////        let filterName = filterNameList[filterIndex]
////        if let image = self.image {
////            self.originalImage = createFilteredImage(filterName: filterName, image: image)
////        }
//        if let video = self.video {
//            //self.playVideo(video:video, filterName:filterNameList[filterIndex])
//        }
    //}
    
//    override func createFilteredImage(filterName: String, image: UIImage) -> UIImage {
//        if(filterName == filterNameList[0]){
//            return self.image!
//        }
//        // 1 - create source image
//        let sourceImage = CIImage(image: image)
//
//        // 2 - create filter using name
//        let filter = CIFilter(name: filterName)
//        filter?.setDefaults()
//
//        // 3 - set source image
//        filter?.setValue(sourceImage, forKey: kCIInputImageKey)
//
//        // 4 - output filtered image as cgImage with dimension.
//        let outputCGImage = context.createCGImage((filter?.outputImage!)!, from: (filter?.outputImage!.extent)!)
//
//        // 5 - convert filtered CGImage to UIImage
//        let filteredImage = UIImage(cgImage: outputCGImage!, scale: image.scale, orientation: image.imageOrientation)
//
//        return filteredImage
//    }
    func CreateCoverImageWithFilter(tempCoverImage : UIImage , filterEffect : String) -> UIImage
    {
        guard let filter = CIFilter(name: filterEffect) else{
            return tempCoverImage
        }
        let aCGImage = tempCoverImage.cgImage
        let tempCG = CIImage.init(cgImage: aCGImage!)
        filter.setDefaults()
        filter.setValue(tempCG, forKey: kCIInputImageKey)
        return UIImage(ciImage: filter.outputImage!)
    }
    func createVideoComposition(_ filterEffect : String , _ videoAsset : AVURLAsset) -> AVVideoComposition {
        let playerItem = AVPlayerItem(asset: videoAsset)
        let composition = AVVideoComposition(asset: playerItem.asset) { (request) in
            let sourceImage = request.sourceImage.clampedToExtent()
            guard let filter = CIFilter(name: filterEffect) else{
                return request.finish(with: NSError())
            }
            filter.setDefaults()
            filter.setValue(sourceImage, forKey: kCIInputImageKey)
            return request.finish(with: filter.outputImage!, context: nil)
        }
        return composition }
    
    func videoExportMethod(strPath : String)
    {
//        let strPath = "\(appDelegate.strVideoFolderPath)/finalfilterVideo.mp4"
        
        videoAv?.exportFilterVideo(videoComposition: avVideoComposition , pathUrl : strPath, completion: { (url) in
//            if let delegate = self.delegate {
//                let convertedVideo = AVURLAsset(url: url as URL!)
//                print("convertedVideo",convertedVideo)
//                delegate.filterVideoViewControllerVideoDidFilter(video: convertedVideo)
//            }
            if (url == nil)
            {
                    self.onShowAlertController(title: "", message: "Video Export fails.")
                return
            }
            let convertedVideo = AVURLAsset(url: url as URL!)
            print("convertedVideo",convertedVideo)
            
            NotificationCenter.default.post(name: NSNotification.Name(klibCloseButtonClick), object: nil)
            
            let nav = self.appDelegate.TabLogin.selectedViewController  as! UINavigationController
            
            let arrVC : NSArray = (nav.viewControllers) as NSArray
            for VC in arrVC
            {
                if VC is WriteFeedVC
                {
                    nav.popToViewController(VC as! UIViewController, animated: true)
                    return
                }
            }
        })
    }
    func ResetButtons()
    {
        btnFilter.isSelected = false
        btnTrim.isSelected = false
        btnCover.isSelected = false
        
        btnFilter.backgroundColor = UIColor(r: 247, g: 247, b: 247)
        btnTrim.backgroundColor = UIColor(r: 247, g: 247, b: 247)
        btnCover.backgroundColor = UIColor(r: 247, g: 247, b: 247)
    }
    @IBAction func btnFilterTrimCoverClick(_ sender : UIButton)
    {
        ResetButtons()
        sender.isSelected = !sender.isSelected
        sender.backgroundColor = UIColor(r: 126, g: 139, b: 153)
        if btnFilter.isSelected == true
        {
            selectFilter()
        }
        else if btnTrim.isSelected == true
        {
            selectTrim()
        }
        else
        {
            selectCover()
        }
    }
    @objc public func saveVideoMethod()
    {
        guard let didSave = didSave else { return print("Don't have saveCallback") }
        navigationItem.rightBarButtonItem = YPLoaders.defaultLoader
        navigationItem.leftBarButtonItem = YPLoaders.defaultLoader
        self.view.isUserInteractionEnabled = false
        
        var sendCoverImage = (coverImageView != nil) ? coverImageView.image! : UIImage()
        
        if isFromSelectionVC == true
        {
            if (self.selectedFilter != nil) && (self.selectedFilter?.name != "Normal")
            {
                appDelegate.dicFilterNameSave.setObject(self.selectedFilter!.name, forKey: "\(videoNumber)" as NSCopying)
                sendCoverImage = CreateCoverImageWithFilter(tempCoverImage: sendCoverImage, filterEffect: getColorFilterNameCode(self.selectedFilter!.name))
            }
                
            if (trimmerView == nil) || ((trimmerView.startTime == kCMTimeZero) && (trimmerView.endTime == inputAsset.duration ))
            {
                let resultVideo = YPMediaVideo(thumbnail: sendCoverImage,
                                               videoURL: inputVideo.url, asset: inputVideo.asset)
                didSave(YPMediaItem.video(v: resultVideo))
                setupRightBarButtonItem()
                navigationController?.popViewController(animated: true)
            }
            else{
                do {
                    let asset = AVURLAsset(url: inputVideo.url)
                    let trimmedAsset = try asset
                        .assetByTrimming(startTime: trimmerView.startTime ?? kCMTimeZero,
                                         endTime: trimmerView.endTime ?? inputAsset.duration)
                    let strPath = "\(appDelegate.strVideoFolderPath)/trimmerVideo\(videoNumber).mp4"
                    RemoveDirectoryUrls(strPath)
                    let destinationURL =  URL(fileURLWithPath: strPath)
                    try trimmedAsset.export(to: destinationURL) { [weak self] in
                         guard let strongSelf = self else { return }
                        DispatchQueue.main.async {
                            let resultVideo = YPMediaVideo(thumbnail: sendCoverImage,
                                                           videoURL: destinationURL, asset: strongSelf.inputVideo.asset)
                            didSave(YPMediaItem.video(v: resultVideo))
                            strongSelf.setupRightBarButtonItem()
                            self!.navigationController?.popViewController(animated: true)
                        }
                    }
                } catch let error {
                    print("💩 \(error)")
                }
                
            }
        }
        else
        {
            SaveThumbImage()
            
            if (self.selectedFilter != nil) && (self.selectedFilter?.name != "Normal")
            {
                let strPath = "\(appDelegate.strVideoFolderPath)/Video1.mp4"
                RemoveDirectoryUrls(strPath)
                
                appDelegate.arrPostUrls.add(strPath)

                
                
                if (trimmerView == nil) || ((trimmerView.startTime == kCMTimeZero) && (trimmerView.endTime == inputAsset.duration ))
                {
                    self.videoExportMethod(strPath: strPath)
                }
                else
                {
                    do {
                        let asset = AVURLAsset(url: inputVideo.url)
                        let trimmedAsset = try asset
                            .assetByTrimming(startTime: trimmerView.startTime ?? kCMTimeZero,
                                             endTime: trimmerView.endTime ?? inputAsset.duration)
                        let strtempPath = "\(appDelegate.strVideoFolderPath)/trimmerVideo.mp4"
                        RemoveDirectoryUrls(strtempPath)
                        
                        let destinationURL =  URL(fileURLWithPath: strtempPath)
                        
                        //                    let destinationURL = URL(fileURLWithPath: NSTemporaryDirectory())
                        //                        .appendingUniquePathComponent(pathExtension: YPConfig.video.fileType.fileExtension)
                        try trimmedAsset.export(to: destinationURL) { [weak self] in
                            // guard let strongSelf = self else { return }
                            DispatchQueue.main.async {
                                self?.videoAv = AVURLAsset(url: destinationURL as URL, options: nil)
                                self?.videoExportMethod(strPath: strPath)
                            }
                        }
                    } catch let error {
                        print("💩 \(error)")
                    }
                }
            }
            else
            {
                let strPath = "\(self.appDelegate.strVideoFolderPath)/Video1.mp4"

                
                if (trimmerView == nil) || ((trimmerView.startTime == kCMTimeZero) && (trimmerView.endTime == inputAsset.duration ))
                {
                    self.RemoveDirectoryUrls(strPath)
                    
                    let videoData = NSData(contentsOf: inputVideo.url)
                    videoData?.write(toFile: strPath, atomically: false)
//                    self.appDelegate.arrPostUrls.add("\(inputVideo.url)")
                    self.appDelegate.arrPostUrls.add(strPath)
                    NotificationCenter.default.post(name: NSNotification.Name(klibCloseButtonClick), object: nil)
                    let nav = self.appDelegate.TabLogin.selectedViewController  as! UINavigationController

                    let arrVC : NSArray = (nav.viewControllers) as NSArray
                    for VC in arrVC
                    {
                        if VC is WriteFeedVC
                        {
                            nav.popToViewController(VC as! UIViewController, animated: true)
                            return
                        }
                    }
                }
                else
                {
                    do {
                        let asset = AVURLAsset(url: inputVideo.url)
                        let trimmedAsset = try asset
                            .assetByTrimming(startTime: trimmerView.startTime ?? kCMTimeZero,
                                             endTime: trimmerView.endTime ?? inputAsset.duration)
                        RemoveDirectoryUrls(strPath)
                        let croppedOutputFileUrl =  URL(fileURLWithPath: strPath)
                        try trimmedAsset.export(to: croppedOutputFileUrl) { [weak self] in
                            // guard let strongSelf = self else { return }
                            
                            DispatchQueue.main.async {
                                
                                self?.appDelegate.arrPostUrls.add(strPath)
                                NotificationCenter.default.post(name: NSNotification.Name(klibCloseButtonClick), object: nil)
                                let nav = self?.appDelegate.TabLogin.selectedViewController  as! UINavigationController
                                
                                let arrVC : NSArray = (nav.viewControllers) as NSArray
                                for VC in arrVC
                                {
                                    if VC is WriteFeedVC
                                    {
                                        nav.popToViewController(VC as! UIViewController, animated: true)
                                        return
                                    }
                                }
                            }

                        }
                    } catch let error {
                        print("💩 \(error)")
                    }
                }
                
            }
            
        }
        
        /*
        
        if (self.selectedFilter != nil) && (self.selectedFilter?.name != "Normal")
        {
            if (trimmerView == nil) || ((trimmerView.startTime == kCMTimeZero) && (trimmerView.endTime == inputAsset.duration ))
            {
                self.videoExportMethod()
            }
            else
            {
                do {
                    let asset = AVURLAsset(url: inputVideo.url)
                    let trimmedAsset = try asset
                        .assetByTrimming(startTime: trimmerView.startTime ?? kCMTimeZero,
                                         endTime: trimmerView.endTime ?? inputAsset.duration)
                    let strPath = "\(appDelegate.strVideoFolderPath)/trimmerVideo.mp4"
                    RemoveDirectoryUrls(strPath)
                    let destinationURL =  URL(fileURLWithPath: strPath)

//                    let destinationURL = URL(fileURLWithPath: NSTemporaryDirectory())
//                        .appendingUniquePathComponent(pathExtension: YPConfig.video.fileType.fileExtension)
                    try trimmedAsset.export(to: destinationURL) { [weak self] in
                        // guard let strongSelf = self else { return }
                        DispatchQueue.main.async {
                            self?.videoAv = AVURLAsset(url: destinationURL as URL, options: nil)
                            self?.videoExportMethod()
                        }
                    }
                } catch let error {
                    print("💩 \(error)")
                }
            }
        }
        else
        {
            do {
                let asset = AVURLAsset(url: inputVideo.url)
                let trimmedAsset = try asset
                    .assetByTrimming(startTime: trimmerView.startTime ?? kCMTimeZero,
                                     endTime: trimmerView.endTime ?? inputAsset.duration)
                RemoveDirectoryUrls("\(appDelegate.strVideoFolderPath)/finalfilterVideo.mp4")
                // let destinationURL = URL(fileURLWithPath: NSTemporaryDirectory())
                //                    .appendingUniquePathComponent(pathExtension: YPConfig.video.fileType.fileExtension)
                let croppedOutputFileUrl =  URL(fileURLWithPath: "\(appDelegate.strVideoFolderPath)/finalfilterVideo.mp4")
                try trimmedAsset.export(to: croppedOutputFileUrl) { [weak self] in
                    // guard let strongSelf = self else { return }
                    DispatchQueue.main.async {
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            } catch let error {
                print("💩 \(error)")
            }
        }
        */
        
        
        //                    let resultVideo = YPMediaVideo(thumbnail: strongSelf.coverImageView.image!,
        //                                                   videoURL: destinationURL, asset: strongSelf.inputVideo.asset)
        //                    didSave(YPMediaItem.video(v: resultVideo))
        //                    strongSelf.setupRightBarButtonItem()
        //                    self!.navigationController?.popViewController(animated: true)
        
    }
    
    func SaveThumbImage()
    {
        let strThumbPath = "\(appDelegate.strVideoFolderPath)/ThumbImageVideo1.jpg"
        appDelegate.arrPostthumbUrls.add(strThumbPath)
        self.RemoveDirectoryUrls(strThumbPath)
        var tempimage = (coverImageView != nil) ? coverImageView.image! : currentlySelectedImageThumbnail
            
        if (self.selectedFilter != nil) && (self.selectedFilter?.name != "Normal")
        {
            tempimage = CreateCoverImageWithFilter(tempCoverImage: tempimage ?? UIImage(), filterEffect: getColorFilterNameCode(self.selectedFilter!.name))
        }
        self.addImageTODirectory(strThumbPath, tempimage ?? UIImage())
    }
    
    
}

// MARK: - TrimmerViewDelegate
extension YPVideoFiltersVC: TrimmerViewDelegate {
    public func positionBarStoppedMoving(_ playerTime: CMTime) {
        
        if videoPlayer == nil
        {
            return
        }
        videoPlayer.seek(to: playerTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        videoPlayer.play()
        startPlaybackTimeChecker()
        updateCoverPickerBounds()
    }
    
    public func didChangePositionBar(_ playerTime: CMTime) {
        if videoPlayer == nil
        {
            return
        }
        stopPlaybackTimeChecker()
        videoPlayer.pause()
        videoPlayer.seek(to: playerTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
}

// MARK: - ThumbSelectorViewDelegate
extension YPVideoFiltersVC: ThumbSelectorViewDelegate {
    public func didChangeThumbPosition(_ imageTime: CMTime) {
        if let imageGenerator = imageGenerator,
            let imageRef = try? imageGenerator.copyCGImage(at: imageTime, actualTime: nil) {
            coverImageView.image = UIImage(cgImage: imageRef)
        }
    }
}

extension YPVideoFiltersVC: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredThumbnailImagesArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filter = filters[indexPath.row]
        let image = filteredThumbnailImagesArray[indexPath.row]
        if let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "FilterCell",
                                 for: indexPath) as? YPFilterCollectionViewCell {
            cell.name.text = filter.name
            cell.imageView.image = image
            return cell
        }
        return UICollectionViewCell()
    }
}

extension YPVideoFiltersVC: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedFilter = filters[indexPath.row]
        currentlySelectedImageThumbnail = filteredThumbnailImagesArray[indexPath.row]
        
        if videoPlayer != nil
        {
            videoPlayer.pause()
        }
        playVideo(video: videoAv!, filterName: getColorFilterNameCode(selectedFilter?.name ?? "Normal"))
        
        // for multi
//        let filterData:[String: YPFilter] = ["filter": selectedFilter!]
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: klibApplySelectionGallaryFilterReload), object: nil, userInfo: filterData)
    }
}
