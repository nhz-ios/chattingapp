//
//  YPFiltersView.swift
//  photoTaking
//
//  Created by Sacha Durand Saint Omer on 21/10/16.
//  Copyright © 2016 octopepper. All rights reserved.
//

import Stevia

class YPFiltersView: UIView , PixelEditViewControllerDelegate {
    func pixelEditViewController(_ controller: PixelEditViewController, didEndEditing editingStack: SquareEditingStack , _ isFinised : Bool) {
        if isFinised == false
        {
            let tempimage = editingStack.makeRenderer().render(resolution: .full)
            imageView.image = tempimage
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kPixelImageEditComplete), object: tempimage)
        }
    }
    
    func pixelEditViewControllerDidCancelEditing(in controller: PixelEditViewController) {
        
    }
    
    
    let imageView = UIImageView()
    var collectionView: UICollectionView!
    var filtersLoader: UIActivityIndicatorView!
    fileprivate let collectionViewContainer: UIView = UIView()
    let sepraterView = UIView()
    var filterBottomItem = YPMenuItem()
    var editBottomItem = YPMenuItem()
    
    var stackView = ControlStackView()
    var editingStack: SquareEditingStack!
    var colorCubeStorage : ColorCubeStorage = .default
    var isComefromGallery : Bool = false
    convenience init() {
        
        self.init(frame: CGRect.zero)
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout())
        filtersLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        filtersLoader.hidesWhenStopped = true
        filtersLoader.startAnimating()
        filtersLoader.color = YPConfig.colors.tintColor
        filtersLoader.left( UIScreen.main.bounds.width/2)

        
        filterBottomItem.textLabel.text = "Filter"
        filterBottomItem.textLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        filterBottomItem.button.addTarget(self, action: #selector(selectFilter), for: .touchUpInside)
        filterBottomItem.isUserInteractionEnabled = true
        
        editBottomItem.textLabel.text = "Edit"
        editBottomItem.textLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        editBottomItem.button.addTarget(self, action: #selector(selectEdit), for: .touchUpInside)
        editBottomItem.isUserInteractionEnabled = true
        
        sv(
            imageView,
            collectionViewContainer.sv(
                stackView,
                collectionView.sv(
                    filtersLoader
                ),
                filterBottomItem,
                editBottomItem,
                sepraterView
            )
        )
        
        stackView.backgroundColor = YPConfig.colors.tintColor
        
        let isIphone4 = UIScreen.main.bounds.height == 480
        let sideMargin: CGFloat = isIphone4 ? 20 : 0
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        |-sideMargin-imageView.top(0)-sideMargin-|
        
        //        if #available(iOS 11.0, *) {
        //
        //            let window = UIApplication.shared.keyWindow
        //            let topPadding = window?.safeAreaInsets.top
        //
        //            if (topPadding == 44) {
        //
        //                |-sideMargin-imageView.top(44 + 44)-sideMargin-|
        //            } else if (topPadding == 20) {
        //
        //                |-sideMargin-imageView.top(44 + 20)-sideMargin-|
        //            }
        //        }
        
        imageView.Bottom == collectionViewContainer.Top
        
        if #available(iOS 11.0, *) {
            
            imageView.Height == screenHeight - 310
            collectionViewContainer.Bottom == safeAreaLayoutGuide.Bottom + 50
        } else {
            
            imageView.Height == screenHeight - 251
            collectionViewContainer.bottom(0)
        }
        
        |-sideMargin-collectionViewContainer-sideMargin-|
        
        imageView.Bottom == collectionViewContainer.Top
        |collectionView.height(150)|
        |stackView.height(150)|
        
        
        backgroundColor = UIColor(r: 247, g: 247, b: 247)
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        
        |-sideMargin-sepraterView-sideMargin-|
        sepraterView.backgroundColor = UIColor(r: 126, g: 139, b: 153)
        |sepraterView.height(1)|
        sepraterView.width(screenWidth)
        collectionView.Bottom == sepraterView.Top
        
        collectionView.Bottom == filterBottomItem.Top
        if #available(iOS 11.0, *) {
            
            filterBottomItem.Bottom == safeAreaLayoutGuide.Bottom
        } else {
            
            filterBottomItem.bottom(0)
        }
        |filterBottomItem.height(45)|
        filterBottomItem.width(screenWidth / 2)
        filterBottomItem.backgroundColor = UIColor(r: 126, g: 139, b: 153)
        filterBottomItem.textLabel.textColor = UIColor(r: 274, g: 247, b: 247)
        collectionView.Bottom == editBottomItem.Top
        if #available(iOS 11.0, *) {
            
            editBottomItem.Bottom == safeAreaLayoutGuide.Bottom
        } else {
            
            editBottomItem.bottom(0)
        }
        |editBottomItem.height(45)|
        editBottomItem.width(screenWidth / 2)
        editBottomItem.backgroundColor = UIColor(r: 247, g: 247, b: 247)
        editBottomItem.textLabel.textColor = UIColor(r: 126, g: 139, b: 153)
        filterBottomItem.Trailing == editBottomItem.Leading
        |-sideMargin-filterBottomItem-editBottomItem-sideMargin-|
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(reloadEditorViews(data:)), name: NSNotification.Name(rawValue: klibReloadEditor), object: nil)
        
        stackView.Bottom == sepraterView.Top
        stackView.isHidden = true
        
        //        collectionView.isHidden = true
        //        filterBottomItem.isHidden = true
        //        editBottomItem.isHidden = true
        //        imageView.isHidden = true
    }
    
    func layout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 2
        layout.sectionInset = UIEdgeInsets(top: -15, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 100, height: 120)
        return layout
    }
    
    @objc func reloadEditorViews(data : NSNotification) {
        
        let source = ImageSource(source: imageView.image!)
        
        if editingStack == nil {
            
            editingStack = SquareEditingStack.init(source: source, previewSize: CGSize(width: self.bounds.width, height: self.bounds.width), colorCubeStorage: colorCubeStorage)
        }
        
        let options = Options()
        let context: PixelEditContext = .init(options: options)
        //        let context: PixelEditContext = .init(options: options)//.init(options: options)
        
        stackView.push(options.classes.control.rootControl.init(context: context, colorCubeControl: options.classes.control.colorCubeControl.init(context: context, originalImage: editingStack.cubeFilterPreviewSourceImage, filters: editingStack.availableColorCubeFilters)), animated: false)
    }
    
    // MARK: - Bottom buttons
    @objc public func selectFilter() {
        
        stackView.isHidden = true
        collectionView.isHidden = false
        filterBottomItem.backgroundColor = UIColor(r: 126, g: 139, b: 153)
        filterBottomItem.textLabel.textColor = UIColor(r: 274, g: 247, b: 247)
        
        editBottomItem.backgroundColor = UIColor(r: 247, g: 247, b: 247)
        editBottomItem.textLabel.textColor = UIColor(r: 126, g: 139, b: 153)
    }
    
    @objc public func selectEdit() {
        
        //        stackView.isHidden = true
        //        collectionView.isHidden = true
        //        filterBottomItem.backgroundColor = UIColor(r: 247, g: 247, b: 247)
        //        filterBottomItem.textLabel.textColor = UIColor(r: 126, g: 139, b: 153)
        //
        //        editBottomItem.backgroundColor = UIColor(r: 126, g: 139, b: 153)
        //        editBottomItem.textLabel.textColor = UIColor(r: 274, g: 247, b: 247)
        
        //        EditFilterShow
        DispatchQueue.main.async {
            let controller = PixelEditViewController.init(image: self.imageView.image!)
            controller.delegate = self
            controller.isComeFromGallery = self.isComefromGallery
            let  Navtemp = appDelegate.TabLogin.selectedViewController as! UINavigationController
            Navtemp.pushViewController(controller, animated: false)
        }
        
        
        //        NotificationCenter.default.post(name: NSNotification.Name(kImageEditFilterShow), object: nil)
    }
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print("didFinishSavingWithError")
            // we got back an error!
            //            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            //            ac.addAction(UIAlertAction(title: "OK", style: .default))
            //            present(ac, animated: true)
        } else {
            print("didFinishSavingWithError - Success")
            
            //            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            //            ac.addAction(UIAlertAction(title: "OK", style: .default))
            //            present(ac, animated: true)
        }
    }
}

