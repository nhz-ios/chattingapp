//
//  SelectionsGalleryVC.swift
//  YPImagePicker
//
//  Created by Nik Kov || nik-kov.com on 09.04.18.
//  Copyright © 2018 Yummypets. All rights reserved.
//

import UIKit
import Photos
import AVKit
public class YPSelectionsGalleryVC: UIViewController {
    
    internal weak var delegate: YPLibraryViewDelegate?
    internal var vl = YPLibraryView()
    internal let mediaManager = LibraryMediaManager()
    
    public var items: [YPMediaItem] = []
    public var olderItems: [YPMediaItem] = []
    
    public var didFinishHandler: ((_ gallery: YPSelectionsGalleryVC, _ items: [YPMediaItem]) -> Void)?
    private var lastContentOffsetX: CGFloat = 0
    
    var v = YPSelectionsGalleryView()
    var alreadyCellPlay : YPSelectionsGalleryCell!
    var playRow = -1
    var videoAv: AVURLAsset?
    var timerExportUrls : Timer!

    public override func loadView() {
        view = v
    }

    public required init(items: [YPMediaItem],
                         didFinishHandler:
        @escaping ((_ gallery: YPSelectionsGalleryVC, _ items: [YPMediaItem]) -> Void)) {
        super.init(nibName: nil, bundle: nil)
        self.items = items
        self.olderItems = items
        self.didFinishHandler = didFinishHandler
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Register collection view cell
        v.collectionView.register(YPSelectionsGalleryCell.self, forCellWithReuseIdentifier: "item")
        v.collectionView.dataSource = self
        v.collectionView.delegate = self
        
        // Setup navigation bar
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(back))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: YPConfig.wordings.next,
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(done))
        //navigationItem.rightBarButtonItem?.tintColor = YPConfig.colors.tintColor
        
        YPHelper.changeBackButtonIcon(self)
        YPHelper.changeBackButtonTitle(self)
        
        let itemsData:[String: YPMediaItem] = ["items": self.items[0]]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: klibReloadFilter), object: nil, userInfo: itemsData)
    
        NotificationCenter.default.addObserver(self, selector: #selector(applyFilterSelectionGalleryViews(data:)), name: NSNotification.Name(rawValue: klibApplySelectionGallaryFilterReload), object: nil)
    }
    public override func viewWillDisappear(_ animated: Bool) {
        self.stopVideo()
        if timerExportUrls != nil
        {
            timerExportUrls.invalidate()
            timerExportUrls = nil
        }
    }

    @objc
    func back() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    private func done() {
        // Save new images to the photo album.
//        if YPConfig.shouldSaveNewPicturesToAlbum {
//            for m in items {
//                if case let .photo(p) = m, let modifiedImage = p.modifiedImage {
//                    YPPhotoSaver.trySaveImage(modifiedImage, inAlbumNamed: YPConfig.albumName)
//                }
//            }
//        }
        //        didFinishHandler?(self, items)
        //
        //        let nav = appDelegate.TabLogin.selectedViewController  as! UINavigationController
        //
        //        let arrVC : NSArray = (nav.viewControllers) as NSArray
        //        for VC in arrVC
        //        {
        //            if VC is PostCreateViewController
        //            {
        //                nav.popToViewController(VC as! UIViewController, animated: true)
        //                return
        //            }
        //        }
        
            StartSaveUrlsToDirectory()
        
        
        }

    
    
    fileprivate func thumbFromImage(_ img: UIImage) -> CIImage {
    
        let k = img.size.width / img.size.height
        let scale = UIScreen.main.scale
        let thumbnailHeight: CGFloat = 300 * scale
        let thumbnailWidth = thumbnailHeight * k
        let thumbnailSize = CGSize(width: thumbnailWidth, height: thumbnailHeight)
        UIGraphicsBeginImageContext(thumbnailSize)
        img.draw(in: CGRect(x: 0, y: 0, width: thumbnailSize.width, height: thumbnailSize.height))
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return smallImage!.toCIImage()!
    }
    
    private func targetSize(for asset: PHAsset, cropRect: CGRect) -> CGSize {
        var width = (CGFloat(asset.pixelWidth) * cropRect.width).rounded(.toNearestOrEven)
        var height = (CGFloat(asset.pixelHeight) * cropRect.height).rounded(.toNearestOrEven)
        // round to lowest even number
        width = (width.truncatingRemainder(dividingBy: 2) == 0) ? width : width - 1
        height = (height.truncatingRemainder(dividingBy: 2) == 0) ? height : height - 1
        return CGSize(width: width, height: height)
    }
    
    private func fitsVideoLengthLimits(asset: PHAsset) -> Bool {
        guard asset.mediaType == .video else {
            return true
        }
        
        let tooLong = asset.duration > YPConfig.video.libraryTimeLimit
        let tooShort = asset.duration < YPConfig.video.minimumTimeLimit
        
        if tooLong || tooShort {
            DispatchQueue.main.async {
                let alert = tooLong ? YPAlert.videoTooLongAlert(self.view) : YPAlert.videoTooShortAlert(self.view)
                self.present(alert, animated: true, completion: nil)
            }
            return false
        }
        
        return true
    }
    
    private func checkVideoLengthAndCrop(for asset: PHAsset,
                                         withCropRect: CGRect? = nil,
                                         callback: @escaping (_ videoURL: URL) -> Void) {
        if fitsVideoLengthLimits(asset: asset) == true {
            delegate?.libraryViewStartedLoading()
            let normalizedCropRect = withCropRect ?? DispatchQueue.main.sync { vl.currentCropRect() }
            let ts = targetSize(for: asset, cropRect: normalizedCropRect)
            let xCrop: CGFloat = normalizedCropRect.origin.x * CGFloat(asset.pixelWidth)
            let yCrop: CGFloat = normalizedCropRect.origin.y * CGFloat(asset.pixelHeight)
            let resultCropRect = CGRect(x: xCrop,
                                        y: yCrop,
                                        width: ts.width,
                                        height: ts.height)
            mediaManager.fetchVideoUrlAndCrop(for: asset, cropRect: resultCropRect, callback: callback)
        }
    }
    
    func randomStringWithLength(len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 1...len{
    
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    func removeFile(_ fileURL: URL?) {
    
//        let filePath = fileURL?.path
//        let fileManager = FileManager.default
//        if fileManager.fileExists(atPath: filePath ?? "") {
//        
//            var error: Error?
//            if ((try? fileManager.removeItem(at: fileURL!)) != nil) {
//            
//                print("Remove Items")
//            }
//        }
    }

    
}

// MARK: - Collection View
extension YPSelectionsGalleryVC: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item",
                                                            for: indexPath) as? YPSelectionsGalleryCell else {
            return UICollectionViewCell()
        }
        let item = items[indexPath.row]
        switch item
        {
        case .photo(let photo):
            cell.imageView.image = photo.image
        case .video(let video):
            cell.imageView.image = video.thumbnail
        }
        return cell
    }
}

extension YPSelectionsGalleryVC: UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if v.selectedFilter != nil
        {
            appDelegate.dicFilterNameSave.setObject(v.selectedFilter!.name, forKey: "\(indexPath.row)" as NSCopying)
        }
        
        let item = items[indexPath.row]
        var mediaFilterVC: IsMediaFilterVC?
        switch item {
        case .photo(let photo):
            if !YPConfig.filters.isEmpty {
        
                mediaFilterVC = YPPhotoFiltersVC(inputPhoto: photo, isFromSelectionVC: true , iscomefromgallery : true)
            }
        case .video(let video):
            
            mediaFilterVC = YPVideoFiltersVC.initWith(video: video, isFromSelectionVC: true , videoNumber : indexPath.row)
        }

        mediaFilterVC?.didSave = { outputMedia in
            self.items[indexPath.row] = outputMedia
            collectionView.reloadData()
            //self.navigationController?.popViewController(animated: true)
        }
        mediaFilterVC?.didCancel = {
            
            //self.navigationController?.popViewController(animated: true)
        }
        
        if let mediaFilterVC = mediaFilterVC as? UIViewController {

            self.navigationController!.pushViewController(mediaFilterVC, animated: false)
        }
    }
    
    
    // Set "paging" behaviour when scrolling backwards.
    // This works by having `targetContentOffset(forProposedContentOffset: withScrollingVelocity` overriden
    // in the collection view Flow subclass & using UIScrollViewDecelerationRateFast
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isScrollingBackwards = scrollView.contentOffset.x < lastContentOffsetX
        scrollView.decelerationRate = isScrollingBackwards
            ? UIScrollViewDecelerationRateFast
            : UIScrollViewDecelerationRateNormal
        lastContentOffsetX = scrollView.contentOffset.x
    }
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        videoPlayMethod()
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        videoPlayMethod()
    }
    
     //MARK: -  Video add & Play
    
    func videoPlayMethod()
    {
        let visibleRect = CGRect(origin: v.collectionView.contentOffset, size: v.collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = v.collectionView.indexPathForItem(at: visiblePoint)
        if visibleIndexPath == nil
        {
            return
        }
//        print("scrollViewDidEndDecelerating******************",visibleIndexPath!.item)

        if visibleIndexPath!.item < items.count
        {
            
            if playRow == visibleIndexPath!.item && alreadyCellPlay != nil
            {
                alreadyCellPlay.imageView.isHidden = true
                alreadyCellPlay.viewAVPlayer.isHidden = false
                if alreadyCellPlay.videoPlayer != nil
                {
                  alreadyCellPlay.videoPlayer.play()
                  return
                }
            }
            else if alreadyCellPlay != nil
            {
                if alreadyCellPlay.videoPlayer != nil
                {
                    alreadyCellPlay.videoPlayer.pause()
                }
                alreadyCellPlay.videoPlayer = nil
                alreadyCellPlay.avVideoComposition = nil
                alreadyCellPlay.avpController = nil
                alreadyCellPlay.imageView.isHidden = false
                alreadyCellPlay.viewAVPlayer.isHidden = true
                videoAv = nil
                playRow = -1
                alreadyCellPlay = nil
            }

           // self.stopVideo()
            
            let item = items[visibleIndexPath!.item]
            switch item
            {
            case .photo(let photo):
                return
            case .video(let video):
                playRow = visibleIndexPath!.item
                let videoCell = v.collectionView.cellForItem(at: visibleIndexPath!) as? YPSelectionsGalleryCell
                if videoCell != nil
                {
                    videoAv = AVURLAsset(url: video.url as URL, options: nil)
                    alreadyCellPlay = videoCell
                    playVideo(videoAv!)
                }
            }
        }
    }
    func playVideo(_ video:AVURLAsset)
    {
//        print("filterName******",v.selectedFilter?.name)
//        print("correct_filterName******",getColorFilterNameCode(v.selectedFilter?.name ?? "Normal"))
        var filterName = "Normal"
        let diffrentFilter = appDelegate.dicFilterNameSave.valueForNullableKey(key: "\(playRow)")
        if playRow != -1 && diffrentFilter != ""
        {
            filterName = getColorFilterNameCode(diffrentFilter)
        }
        else{
            filterName = getColorFilterNameCode(v.selectedFilter?.name ?? "Normal")
        }
        
        let avPlayerItem = AVPlayerItem(asset: video)
        
        if(filterName != "Normal")
        {
//            alreadyCellPlay.avVideoComposition = AVVideoComposition(asset: self.videoAv!, applyingCIFiltersWithHandler: { request in
//                let source = request.sourceImage.clampedToExtent()
//                let filter = CIFilter(name:filterName)!
//                filter.setDefaults()
//                filter.setValue(source, forKey: kCIInputImageKey)
//                let output = filter.outputImage!
//                request.finish(with:output, context: nil)
//            })
           alreadyCellPlay.avVideoComposition =  createVideoComposition(filterName , self.videoAv!)
            avPlayerItem.videoComposition = alreadyCellPlay.avVideoComposition
        }
        if alreadyCellPlay.videoPlayer == nil {
            alreadyCellPlay.videoPlayer = AVPlayer(playerItem: avPlayerItem)
            alreadyCellPlay.avpController = AVPlayerViewController()
            alreadyCellPlay.avpController.showsPlaybackControls = false
            alreadyCellPlay.avpController.player = alreadyCellPlay.videoPlayer
            alreadyCellPlay.avpController.view.frame = alreadyCellPlay.viewAVPlayer.bounds
            alreadyCellPlay.avpController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
            alreadyCellPlay.avpController.removeFromParentViewController()
            self.addChildViewController(alreadyCellPlay.avpController)
           alreadyCellPlay.viewAVPlayer.addSubview(alreadyCellPlay.avpController.view)
            alreadyCellPlay.videoPlayer.play()
            alreadyCellPlay.imageView.isHidden = true
            alreadyCellPlay.viewAVPlayer.isHidden = false
        }
        else {
            alreadyCellPlay.videoPlayer.replaceCurrentItem(with: avPlayerItem)
            alreadyCellPlay.videoPlayer.play()
            alreadyCellPlay.imageView.isHidden = true
            alreadyCellPlay.viewAVPlayer.isHidden = false
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.playerItemDidReachEnd),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: alreadyCellPlay.videoPlayer.currentItem)

    }
    func stopVideo()
    {
        if alreadyCellPlay != nil
        {
            if alreadyCellPlay.videoPlayer != nil
            {
                alreadyCellPlay.videoPlayer.pause()
            }
            alreadyCellPlay.videoPlayer = nil
            alreadyCellPlay.avVideoComposition = nil
            alreadyCellPlay.avpController = nil
            alreadyCellPlay.imageView.isHidden = false
            alreadyCellPlay.viewAVPlayer.isHidden = true
            alreadyCellPlay = nil
        }
        videoAv = nil
        playRow = -1
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

    }
    @objc func playerItemDidReachEnd(_ notification: Notification)
    {
        if alreadyCellPlay != nil
        {
            alreadyCellPlay.videoPlayer.seek(to: kCMTimeZero)
            alreadyCellPlay.videoPlayer.play()
            alreadyCellPlay.viewAVPlayer.isHidden = false
        }
    }
    
    @objc func applyFilterSelectionGalleryViews(data : NSNotification) {
        self.stopVideo()
        self.v.filtersCollectionLoader.startAnimating()
        self.view.isUserInteractionEnabled = false
        DispatchQueue.global().async {
            if let filter = data.userInfo?["filter"] as? YPFilter {
                self.v.selectedFilter = filter
                for i in (0..<self.olderItems.count) {
                    let m = self.olderItems[i] as YPMediaItem
                    switch m {
                    case .photo(let photo):
                      //  print("Photo: \(photo)")
                        if let applier = filter.applier, let thumbnailImage = photo.image.toCIImage(), let outputImage = applier(thumbnailImage) {
                            
                            self.items[i] = YPMediaItem.photo(p: YPMediaPhoto(image: outputImage.toUIImage(), fromCamera: false))
                        } else {
                            self.items[i] = self.olderItems[i]
                        }
                    case .video(let video):
                     //   print("Video: \(video.url)")
                        if let applier = filter.applier, let thumbnailImage = video.thumbnail.toCIImage(), let outputImage = applier(thumbnailImage) {
                            self.items[i] = YPMediaItem.video(v: YPMediaVideo(thumbnail: outputImage.toUIImage(), videoURL: video.url, fromCamera: false, asset: video.asset))
                            
                        } else {
                            self.items[i] = self.olderItems[i]
                        }
                    }
                }
            }
            DispatchQueue.main.async {
                self.v.collectionView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.view.isUserInteractionEnabled = true
                    self.v.filtersCollectionLoader.stopAnimating()
                    self.videoPlayMethod()
                })
            }
            
            
        }
    }
    func CreateCoverImageWithFilter(tempCoverImage : UIImage , filterEffect : String) -> UIImage
    {
        guard let filter = CIFilter(name: filterEffect) else{
            return tempCoverImage
        }
        let aCGImage = tempCoverImage.cgImage
        let tempCG = CIImage.init(cgImage: aCGImage!)
        filter.setDefaults()
        filter.setValue(tempCG, forKey: kCIInputImageKey)
        return UIImage(ciImage: filter.outputImage!)
    }
    func SaveThumbImage(count : Int , tempimage : UIImage , strFilter : String)
    {
        var sendTempImage = tempimage
        let strThumbPath = "\(appDelegate.strVideoFolderPath)/ThumbImageVideo\(count).jpg"
        appDelegate.arrPostthumbUrls.add(strThumbPath)
        self.RemoveDirectoryUrls(strThumbPath)
        
        if (strFilter != "Normal")
        {
            sendTempImage = CreateCoverImageWithFilter(tempCoverImage: tempimage ?? UIImage(), filterEffect: strFilter)
        }
        self.addImageTODirectory(strThumbPath, sendTempImage ?? UIImage())
    }
    func StartSaveUrlsToDirectory()
    {
        self.stopVideo()
        self.navigationItem.rightBarButtonItem = YPLoaders.defaultLoader
        self.navigationItem.leftBarButtonItem = YPLoaders.defaultLoader
        self.v.filtersCollectionLoader.startAnimating()
        self.view.isUserInteractionEnabled = false
        if timerExportUrls != nil
        {
            timerExportUrls.invalidate()
            timerExportUrls = nil
        }
        timerExportUrls = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(handleCheckToAllUrlsSave), userInfo: nil, repeats: true)

        DispatchQueue.global().async {
            var count = 0
            for m in self.items {
                switch m
                {
                case .photo(let photo):
                    let strPath = "\(self.appDelegate.strVideoFolderPath)/Image\(count).jpg"
                    self.RemoveDirectoryUrls(strPath)
                    //print("photo",strPath)
                    self.addImageTODirectory(strPath, photo.image)
                    count = count + 1
                    self.appDelegate.arrPostUrls.add(strPath)
                self.appDelegate.arrPostthumbUrls.add("")
                

                case .video(let video):
                   // print("video",video.url)
                    let strPath = "\(self.appDelegate.strVideoFolderPath)/Video\(count).mp4"
                    self.RemoveDirectoryUrls(strPath)
                    
                    var filterName = ""
                    let strDiffrentFilter = self.appDelegate.dicFilterNameSave.valueForNullableKey(key: "\(count)")
                    if  strDiffrentFilter != ""
                    {
                        filterName = self.getColorFilterNameCode(strDiffrentFilter)
                        self.SaveThumbImage(count: count , tempimage: video.thumbnail , strFilter: "Normal")

                    }
                    else{
                        filterName = self.getColorFilterNameCode(self.v.selectedFilter?.name ?? "Normal")
                        self.SaveThumbImage(count: count , tempimage: video.thumbnail , strFilter: filterName)
                    }

                    if (filterName == "") || (filterName == "Normal")
                    {
                        let videoData = NSData(contentsOf: video.url)
                        videoData?.write(toFile: strPath, atomically: false)
//                        self.appDelegate.arrPostUrls.add("\(video.url)")
                         self.appDelegate.arrPostUrls.add(strPath)
                    }
                    else
                    {
                        let videoAv = AVURLAsset(url: video.url as URL, options: nil)
                        self.videoExportMethod(videoAv , strPath , filterName)
                    }
                    count = count + 1
                }
        }
       
        }

    }
    @objc func handleCheckToAllUrlsSave()
    {
        //print("items.count",items.count)
        //print("appDelegate.arrPostUrls.count",appDelegate.arrPostUrls.count)

        if items.count == appDelegate.arrPostUrls.count
        {
            self.v.filtersCollectionLoader.stopAnimating()
            self.view.isUserInteractionEnabled = false
            if timerExportUrls != nil
            {
                timerExportUrls.invalidate()
                timerExportUrls = nil
            }
            didFinishHandler?(self, items)
            
            NotificationCenter.default.post(name: NSNotification.Name(klibCloseButtonClick), object: nil)
            
            let nav = appDelegate.TabLogin.selectedViewController  as! UINavigationController
            let arrVC : NSArray = (nav.viewControllers) as NSArray
            for VC in arrVC
            {
                if VC is WriteFeedVC
                {
                    nav.popToViewController(VC as! UIViewController, animated: true)
                    return
                }
            }
        }
    
    }
    func createVideoComposition(_ filterEffect : String , _ videoAsset : AVURLAsset) -> AVVideoComposition {
        let playerItem = AVPlayerItem(asset: videoAsset)
        let composition = AVVideoComposition(asset: playerItem.asset) { (request) in
            let sourceImage = request.sourceImage.clampedToExtent()
            guard let filter = CIFilter(name: filterEffect) else{
                return request.finish(with: NSError())
            }
            filter.setDefaults()
            filter.setValue(sourceImage, forKey: kCIInputImageKey)
            return request.finish(with: filter.outputImage!, context: nil)
        }
        return composition }
    @objc func videoExportMethod(_ videoAv : AVURLAsset , _ strPath : String , _ strFilterName : String)
    {
        let avVideoComposition = createVideoComposition(strFilterName , videoAv)
         
        videoAv.exportFilterVideo(videoComposition: avVideoComposition , pathUrl : strPath, completion: { (url) in
            let convertedVideo = AVURLAsset(url: url as URL!)
            print("convertedVideo",convertedVideo)
//            self.appDelegate.arrPostUrls.add("\(convertedVideo)")
            self.appDelegate.arrPostUrls.add(strPath)

        })
    }
    
}

