//
//  YPSelectionsGalleryView.swift
//  YPImagePicker
//
//  Created by Sacha DSO on 13/06/2018.
//  Copyright © 2018 Yummypets. All rights reserved.
//

import UIKit
import Stevia

class YPSelectionsGalleryView: UIView {
    
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: YPGalleryCollectionViewFlowLayout())
    var filterCollectionView: UICollectionView!
    var filtersLoader: UIActivityIndicatorView!
    var filtersCollectionLoader: UIActivityIndicatorView!
    fileprivate let collectionViewContainer: UIView = UIView()
    
    fileprivate let filters: [YPFilter] = YPConfig.filters
    var selectedFilter: YPFilter?
    fileprivate var filteredThumbnailImagesArray: [UIImage] = []
    fileprivate var thumbnailImageForFiltering: CIImage?
    fileprivate var currentlySelectedImageThumbnail: UIImage?
    fileprivate var v = YPFiltersView()
    public var items: [YPMediaItem] = []
    
    convenience init() {
        self.init(frame: .zero)
    
        v.isComefromGallery = true
        
        filterCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout())
        
        filtersLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        filtersLoader.hidesWhenStopped = true
        filtersLoader.startAnimating()
        filtersLoader.color = YPConfig.colors.tintColor
        
        filtersCollectionLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        filtersCollectionLoader.hidesWhenStopped = true
        filtersCollectionLoader.stopAnimating()
        filtersCollectionLoader.color = YPConfig.colors.tintColor
        
        sv(
            collectionView,
            filtersCollectionLoader,
            collectionViewContainer.sv(
                filterCollectionView.sv(
                    filtersLoader
                )
            )
        )
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFilterViews(data:)), name: NSNotification.Name(rawValue: klibReloadFilter), object: nil)
        
        // Setup of Collection View
        filterCollectionView.register(YPFilterCollectionViewCell.self, forCellWithReuseIdentifier: "FilterCell")
        filterCollectionView.dataSource = self
        filterCollectionView.delegate = self
        
        // Layout collectionView
        //collectionView.heightEqualsWidth()
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenHeight = screenSize.height
       
        let isIphone4 = UIScreen.main.bounds.height == 480
        let sideMargin: CGFloat = isIphone4 ? 20 : 0
        
        |-sideMargin-collectionView.top(64)-sideMargin-|
        
        collectionView.Bottom == collectionViewContainer.Top
        |-sideMargin-collectionViewContainer-sideMargin-|
        
        if #available(iOS 11.0, *) {
            
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            
            if (topPadding == 44) {
                
                |-sideMargin-collectionView.top(44+44)-sideMargin-|
            } else if (topPadding == 20) {
                
                |-sideMargin-collectionView.top(44+20)-sideMargin-|
            } else {
                
                filterCollectionView.centerInContainer()
            }
            
            collectionView.Right == safeAreaLayoutGuide.Right
            collectionView.Left == safeAreaLayoutGuide.Left
            
            collectionView.Height == screenHeight - 310
            
            collectionViewContainer.Bottom == safeAreaLayoutGuide.Bottom + 50
            |filterCollectionView.height(150)|
        } else {
            
            collectionView.Height == screenHeight - 251
            collectionViewContainer.bottom(0)
            
            filterCollectionView.centerInContainer()
            
            collectionViewContainer.Bottom == filterCollectionView.Bottom
            
            |-filterCollectionView.height(150)-|
        }
        
        collectionView.Top == collectionViewContainer.Top
        filtersLoader.centerInContainer()
        filtersCollectionLoader.centerInContainer()
        
        // Apply style
        backgroundColor = UIColor(r: 247, g: 247, b: 247)
        
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        
        filterCollectionView.backgroundColor = .clear
        filterCollectionView.showsHorizontalScrollIndicator = false
    }
    
    func layout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 2
        layout.sectionInset = UIEdgeInsets(top: -15, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 100, height: 120)
        return layout
    }
    
    fileprivate func thumbFromImage(_ img: UIImage) -> CIImage {
        
        let k = img.size.width / img.size.height
        let scale = UIScreen.main.scale
        let thumbnailHeight: CGFloat = 300 * scale
        let thumbnailWidth = thumbnailHeight * k
        let thumbnailSize = CGSize(width: thumbnailWidth, height: thumbnailHeight)
        UIGraphicsBeginImageContext(thumbnailSize)
        img.draw(in: CGRect(x: 0, y: 0, width: thumbnailSize.width, height: thumbnailSize.height))
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return smallImage!.toCIImage()!
    }
    
    @objc func reloadFilterViews(data : NSNotification) {
    
        if let item = data.userInfo?["items"] as? YPMediaItem {
    
            self.filtersLoader.startAnimating()
            
            switch item {
            case .photo(let photo):
                thumbnailImageForFiltering = thumbFromImage(photo.image)
            case .video(let video):
                thumbnailImageForFiltering = thumbFromImage(video.thumbnail)
            }
            
            DispatchQueue.global().async {
                self.filteredThumbnailImagesArray = self.filters.map { filter -> UIImage in
                    if let applier = filter.applier,
                        let thumbnailImage = self.thumbnailImageForFiltering,
                        let outputImage = applier(thumbnailImage) {
                        return outputImage.toUIImage()
                    } else {
                    
                        switch item {
                        case .photo(let photo):
                            return photo.image
                        case .video(let video):
                            return video.thumbnail
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.filterCollectionView.reloadData()
                    self.filterCollectionView.selectItem(at: IndexPath(row: 0, section: 0),
                                                     animated: false,
                                                     scrollPosition: UICollectionView.ScrollPosition.bottom)
                    self.filtersLoader.stopAnimating()
                }
            }
        }
    }
}

class YPGalleryCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        scrollDirection = .horizontal
        let sideMargin: CGFloat = 24
        let spacing: CGFloat = 12
        let overlapppingNextPhoto: CGFloat = 37
        minimumLineSpacing = spacing
        minimumInteritemSpacing = spacing
        let size = UIScreen.main.bounds.width - (sideMargin + overlapppingNextPhoto)
        itemSize = CGSize(width: size, height: size)
        sectionInset = UIEdgeInsets(top: 0, left: sideMargin, bottom: 0, right: sideMargin)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // This makes so that Scrolling the collection view always stops with a centered image.
    // This is heavily inpired form :
    // https://stackoverflow.com/questions/13492037/targetcontentoffsetforproposedcontentoffsetwithscrollingvelocity-without-subcla
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        let spacing: CGFloat = 12
        let overlapppingNextPhoto: CGFloat = 37
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude// MAXFLOAT
        let horizontalOffset = proposedContentOffset.x + spacing + overlapppingNextPhoto/2 // + 5
        
        guard let collectionView = collectionView else {
            return proposedContentOffset
        }
        let targetRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
        guard let array = super.layoutAttributesForElements(in: targetRect) else {
            return proposedContentOffset
        }
        
        for layoutAttributes in array {
            let itemOffset = layoutAttributes.frame.origin.x
            if abs(itemOffset - horizontalOffset) < abs(offsetAdjustment) {
                offsetAdjustment = itemOffset - horizontalOffset
            }
        }
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
    }
}

extension YPSelectionsGalleryView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredThumbnailImagesArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filter = filters[indexPath.row]
        let image = filteredThumbnailImagesArray[indexPath.row]
        if let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "FilterCell",
                                 for: indexPath) as? YPFilterCollectionViewCell {
            cell.name.text = filter.name
            cell.imageView.image = image
            return cell
        }
        return UICollectionViewCell()
    }
}

extension YPSelectionsGalleryView: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedFilter = filters[indexPath.row]
        currentlySelectedImageThumbnail = filteredThumbnailImagesArray[indexPath.row]
        
        // for multi image set data color
        let filterData:[String: YPFilter] = ["filter": selectedFilter!]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: klibApplySelectionGallaryFilterReload), object: nil, userInfo: filterData)
    }
}
