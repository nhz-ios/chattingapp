//
//  ApiHelper.swift
//  ChattingApp
//
//  Created by Deepak iOS on 22/02/18.
//  Copyright © 2018 Nine Hertz India. All rights reserved.
//

import Foundation
import Alamofire



func getUsersObjectArray(arrResult : NSArray) -> NSMutableArray {
    
    let arrObjects = NSMutableArray()
    for dictResult in arrResult
    {
        arrObjects.add(UserModel.init(dict: dictResult as! NSDictionary))
    }
    return arrObjects
}

func getFeedsObjectArray(arrResult : NSArray) -> NSMutableArray {
    
    let arrObjects = NSMutableArray()
    for dictResult in arrResult
    {
        arrObjects.add(FeedModel.init(dict: dictResult as! NSDictionary))
    }
    return arrObjects
}

func getMediaObjectArray(arrResult : NSArray) -> NSMutableArray {
    
    let arrObjects = NSMutableArray()
    for dictResult in arrResult
    {
        arrObjects.add(FeedMediaModel.init(dict: dictResult as? NSDictionary ?? NSDictionary()))
    }
    return arrObjects
}

func getCommentsObjectArray(arrResult : NSArray) -> NSMutableArray {
    
    let arrObjects = NSMutableArray()
    for dictResult in arrResult
    {
        arrObjects.add(CommentModel.init(dict: dictResult as! NSDictionary))
    }
    return arrObjects
}

func getCollectionsObjectArray(arrResult : NSArray) -> NSMutableArray {
    
    let arrObjects = NSMutableArray()
    for dictResult in arrResult
    {
        arrObjects.add(CollectionModel.init(dict: dictResult as! NSDictionary))
    }
    return arrObjects
}

func getActivitiesObjectArray(arrResult : NSArray) -> NSMutableArray {
    
    let arrObjects = NSMutableArray()
    for dictResult in arrResult
    {
        arrObjects.add(ActivityModel.init(dict: dictResult as! NSDictionary))
    }
    return arrObjects
}


func getAllSocialApiResultwithPostMethod(strMethodname : String,Details: NSMutableDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    Details.setObject(UserDefaults.standard.object(forKey: kClientPublicKey) ?? "", forKey: kClientPublicKey as NSCopying)
    
    let url = Base_URL + strMethodname
    print("*****CheckUrl: *****",url)
    print("*****CheckParameters: *****",Details)
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey) as? String ?? ""
    
    print("*****CheckToken: ***** :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer " + strToken
    ]

    Alamofire.request(url, method: .post, parameters: Details as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
        
        DispatchQueue.main.async {
            print("*****CheckResponse: *****",url, response.result.value)
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}


func getAllSocialApiResultwithGetMethod(strMethodname : String,Details: NSMutableDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    Details.setObject(UserDefaults.standard.object(forKey: kClientPublicKey) ?? "", forKey: kClientPublicKey as NSCopying)

    let url = Base_URL + strMethodname
    print("*****CheckUrl: *****",url)
    print("*****CheckParameters: *****",Details)

    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey) as? String ?? ""
    
    print("*****CheckToken: ***** :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer " + strToken
    ]
    
    Alamofire.request(url, method: .get, parameters: Details as? Parameters, encoding: URLEncoding.default, headers: headers).responseJSON{ response in
        
        DispatchQueue.main.async {
            print("*****CheckResponse: *****",url, response.result.value)
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}


func getAllSocialApiResultWithImagePostMethod(strMethodname : String,imgData : Data,strImgKey : String,Details: NSMutableDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    
    Details.setObject(UserDefaults.standard.object(forKey: kClientPublicKey) ?? "", forKey: kClientPublicKey as NSCopying)

    var manager = Alamofire.SessionManager.default
    // manager.session.configuration.timeoutIntervalForRequest = 500
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = 3000
    configuration.timeoutIntervalForResource = 3000
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    manager = Alamofire.SessionManager(configuration: configuration)
    //Alamofire.SessionManager(configuration: configuration)
    let url = Base_URL + strMethodname
    print("*****CheckUrl: *****",url)
    print("*****CheckParameters: *****",Details)
 
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey) as? String ?? ""
    
    print("*****CheckToken: ***** :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer " + strToken
    ]
    
    Alamofire.upload(multipartFormData:
        { multipartFormData in
            // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
            
            
            if imgData.count > 0
            {
                //  print("image\(count)")
                multipartFormData.append((imgData), withName: strImgKey, fileName: "file.jpg", mimeType: "image/jpeg")
            }
            
            for (key, val) in Details {
                
                multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                
            }
            
            
    },to: url,headers:headers, encodingCompletion:
        { encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                 
                    DispatchQueue.main.async {
                        print("*****CheckResponse: *****",url, response.result.value)
                        completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }
            
    }
        
    )
}

func getAllSocialApiResultWithMultipleImagesVideoesPostMethod(strMethodname : String,imgDataArray : NSMutableArray,strImgKeyArray : NSMutableArray,videoDataArray : NSMutableArray,strVideoKeyArray : NSMutableArray,Details: NSMutableDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    Details.setObject(UserDefaults.standard.object(forKey: kClientPublicKey) ?? "", forKey: kClientPublicKey as NSCopying)
    
    var manager = Alamofire.SessionManager.default
    // manager.session.configuration.timeoutIntervalForRequest = 500
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = 3000
    configuration.timeoutIntervalForResource = 3000
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    manager = Alamofire.SessionManager(configuration: configuration)
    //Alamofire.SessionManager(configuration: configuration)
    let url = Base_URL + strMethodname
    print("*****CheckUrl: *****",url)
    print("*****CheckParameters: *****",Details)
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey) as? String ?? ""
    
    print("*****CheckToken: ***** :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer " + strToken
    ]
    
    Alamofire.upload(multipartFormData:
        { multipartFormData in
            // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
            
            if imgDataArray.count > 0
            {
                for i in 0..<imgDataArray.count
                {
                    let imgData = imgDataArray.object(at: i) as! NSData
                    if (imgData as! Data).count > 0
                    {
                        multipartFormData.append(((imgData as! Data)), withName: strImgKeyArray.object(at: i) as! String, fileName: "file\(i).jpg", mimeType: "image/jpeg")
                    }
                }
            }
            if videoDataArray.count > 0
            {
                for i in 0..<videoDataArray.count
                {
                    let videoData = videoDataArray.object(at: i) as! NSData
                    if (videoData as! Data).count > 0
                    {
                        multipartFormData.append(((videoData as! Data)), withName: strVideoKeyArray.object(at: i) as! String, fileName: "video\(i).mp4", mimeType: "video/mp4")
                    }
                }
            }

            for (key, val) in Details {
                multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
    },to: url,headers:headers, encodingCompletion:
        { encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    
                    DispatchQueue.main.async {
                        print("*****CheckResponse: *****",url, response.result.value)
                        completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }
            
    }
        
    )
}


func getSocialPostApiResultMethod(strMethodname : String,mediaThumbArray : NSMutableArray,mediaArray : NSMutableArray, Details: NSMutableDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    Details.setObject(UserDefaults.standard.object(forKey: kClientPublicKey) ?? "", forKey: kClientPublicKey as NSCopying)
    
    var manager = Alamofire.SessionManager.default
    // manager.session.configuration.timeoutIntervalForRequest = 500
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = 3000
    configuration.timeoutIntervalForResource = 3000
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    manager = Alamofire.SessionManager(configuration: configuration)
    //Alamofire.SessionManager(configuration: configuration)
    let url = Base_URL + strMethodname
    print("*****CheckUrl: *****",url)
    print("*****CheckParameters: *****",Details)
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey) as? String ?? ""
    
    print("*****CheckToken: ***** :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer " + strToken
    ]
    
    Alamofire.upload(multipartFormData:
        { multipartFormData in
            // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
            
            if mediaArray.count > 0
            {
                for i in 0..<mediaArray.count
                {
                    let url = URL.init(fileURLWithPath: mediaArray.object(at: i) as? String ?? "")
                    if url.pathExtension == "jpg" || url.pathExtension == "png"
                    {
                        multipartFormData.append(url, withName: "media_\(i+1)", fileName: "media_\(i+1).jpg", mimeType: "image/jpeg")

                    }
                    else
                    {
                        multipartFormData.append(url, withName: "media_\(i+1)", fileName: "media_\(i+1).mp4", mimeType: "video/mp4")

                    }
                    
                    if mediaThumbArray.object(at: i) as? String ?? "" == ""
                    {
                        do {
                            let imgThumb = try thumbFromImage(UIImage.init(data: Data.init(contentsOf: URL.init(fileURLWithPath: mediaArray.object(at: i) as? String ?? ""))) ?? UIImage())
                            
                            multipartFormData.append(UIImageJPEGRepresentation(imgThumb, 1.0) ?? Data(), withName: "media_thumb_\(i+1)", fileName: "media_thumb_\(i+1).jpg", mimeType: "image/jpeg")

                        } catch {
                            print(error)
                        }
                    }
                    else
                    {
                        multipartFormData.append(URL.init(fileURLWithPath: mediaThumbArray.object(at: i) as? String ?? ""), withName: "media_thumb_\(i+1)", fileName: "media_thumb_\(i+1).jpg", mimeType: "image/jpeg")
                    }
                    
                }
            }
            
            for (key, val) in Details {
                multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
    },to: url,headers:headers, encodingCompletion:
        { encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    
                    DispatchQueue.main.async {
                        print("*****CheckResponse: *****",url, response.result.value)
                        completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }
            
    }
        
    )
}

func getAllSocialApiResultWithMultipleImagesPostMethod(strMethodname : String,imgDataArray : NSMutableArray,strImgKeyArray : NSMutableArray,Details: NSMutableDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    Details.setObject(UserDefaults.standard.object(forKey: kClientPublicKey) ?? "", forKey: kClientPublicKey as NSCopying)

    var manager = Alamofire.SessionManager.default
   // manager.session.configuration.timeoutIntervalForRequest = 500
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = 3000
    configuration.timeoutIntervalForResource = 3000
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    manager = Alamofire.SessionManager(configuration: configuration)
    //Alamofire.SessionManager(configuration: configuration)
    let url = Base_URL + strMethodname
    print("*****CheckUrl: *****",url)
    print("*****CheckParameters: *****",Details)
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey) as? String ?? ""
    
    print("*****CheckToken: ***** :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer " + strToken
    ]
    
    Alamofire.upload(multipartFormData:
        { multipartFormData in
            // On the PHP side you can retrive the image using $_FILES["image"]["tmp_name"]
            
            if imgDataArray.count > 0
            {
                for i in 0..<imgDataArray.count
                {
                    let imgData = imgDataArray.object(at: i) as! NSData
                    if (imgData as! Data).count > 0
                    {
                       multipartFormData.append(((imgData as! Data)), withName: strImgKeyArray.object(at: i) as! String, fileName: "file\(i).jpg", mimeType: "image/jpeg")
                    }
                }
            }
            
            for (key, val) in Details {
                multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
    },to: url,headers:headers, encodingCompletion:
        { encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    
                    DispatchQueue.main.async {
                        print("*****CheckResponse: *****",url, response.result.value)
                        completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }
            
    }
        
    )
}
func thumbFromImage(_ img: UIImage) -> UIImage {
    
    let k = img.size.width / img.size.height
    let scale = UIScreen.main.scale
    let thumbnailHeight: CGFloat = 100 * scale
    let thumbnailWidth = thumbnailHeight * k
    let thumbnailSize = CGSize(width: thumbnailWidth, height: thumbnailHeight)
    UIGraphicsBeginImageContext(thumbnailSize)
    img.draw(in: CGRect(x: 0, y: 0, width: thumbnailSize.width, height: thumbnailSize.height))
    let smallImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return smallImage!
}
