//
//  CountryListDataSource.m
//  Country List
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

#import "CountryListDataSource.h"

#define kCountriesFileName @"countries.json"

@interface CountryListDataSource () {
    NSArray *countriesList;
}

@end

@implementation CountryListDataSource

- (id)init {
    self = [super init];
    if (self) {
        [self parseJSON];
    }
    
    return self;
}

- (void)parseJSON {
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    countriesList = (NSArray *)parsedObject;
}

- (NSArray *)countries
{
    return countriesList;
}

-(NSArray*)sortedCountries:(NSString *)str
{
 
    NSString *substring = [NSString stringWithString:str];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c]%@ || dial_code contains[c]%@ || code contains[c]%@",substring, substring, substring];
    NSArray *filteredArry=[[countriesList filteredArrayUsingPredicate:predicate] copy];
    
    return filteredArry;
}
@end
