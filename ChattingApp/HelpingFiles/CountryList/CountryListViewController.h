//
//  CountryListViewController.h
//  Country List
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountryListViewDelegate <NSObject>
- (void)didSelectCountry:(NSDictionary *)country;
@end

@interface CountryListViewController : UIViewController<UISearchBarDelegate>
{
    IBOutlet UISearchBar *searchBarCountry;
    BOOL isSearching;
    BOOL isSelectedRow;
}


@property (nonatomic, assign) id<CountryListViewDelegate>delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate;
@end
