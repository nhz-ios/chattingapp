//
//  CountryListDataSource.h
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define kCountryName        @"name"
//#define kCountryCallingCode @"dial_code"
//#define kCountryCode        @"code"

#define kCountryName        @"country_name"
#define kCountryId          @"country_id"
#define kCountryCode        @"country_code"

@interface CountryListDataSource : NSObject

- (NSArray *)countries;
-(NSArray*)sortedCountries:(NSString *)str;

@end
