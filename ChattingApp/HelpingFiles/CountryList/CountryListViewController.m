//
//  CountryListViewController.m
//  Country List
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

#import "CountryListViewController.h"
#import "CountryListDataSource.h"
#import "CountryCell.h"
#import "ChattingApp-Swift.h"

@interface CountryListViewController ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *filteredRows;

@end

@implementation CountryListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        _delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = NO;
    
    UIBarButtonItem *rightbarbutton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    
    self.title=@"Country List";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    self.navigationItem.rightBarButtonItem = rightbarbutton;
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:43.0f/255.0f green:152.0f/255.0f blue:214.0f/255.0f alpha:1.0f]];
    [_tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [(searchBarCountry.text.length ==0 ? AppDelegateObjC.arrCountriesList : _filteredRows) count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    CountryCell *cell = (CountryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[CountryCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [[(searchBarCountry.text.length ==0 ? AppDelegateObjC.arrCountriesList : _filteredRows)  objectAtIndex:indexPath.row] valueForKey:kCountryName];
      cell.detailTextLabel.text = [[(searchBarCountry.text.length ==0 ? AppDelegateObjC.arrCountriesList : _filteredRows) objectAtIndex:indexPath.row] valueForKey:kCountryCode];
    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    isSelectedRow=YES;
}

#pragma mark -
#pragma mark Actions

- (IBAction)done:(id)sender
{
    
    if ([_delegate respondsToSelector:@selector(didSelectCountry:)])
    {
        if ((searchBarCountry.text.length ==0 ? AppDelegateObjC.arrCountriesList : _filteredRows).count>0)
        {
            if (isSelectedRow)
                [self.delegate didSelectCountry:[(searchBarCountry.text.length ==0 ? AppDelegateObjC.arrCountriesList : _filteredRows) objectAtIndex:[_tableView indexPathForSelectedRow].row]];
        }
        
        
        [self dismissViewControllerAnimated:YES completion:NULL];
    } else
    {
        NSLog(@"CountryListView Delegate : didSelectCountry not implemented");
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if (searchBar.text.length ==0) {
        
        [_tableView reloadData];
        
    }
    else
    {
        _filteredRows = [self sortedCountries: searchText];
    }
    [_tableView reloadData];

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}


-(NSArray*)sortedCountries:(NSString *)str
{
  
    NSString *substring = [NSString stringWithString:str];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"country_name contains[c]%@ || country_code contains[c]%@",substring, substring];
    NSArray *filteredArry=[[AppDelegateObjC.arrCountriesList filteredArrayUsingPredicate:predicate] copy];
    
    return filteredArry;
}
@end
