//
//  Constant.swift
//  ChattingApp
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import Foundation
import UIKit

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    
}

//ToastPosition
let kToastTopPosition = 1
let kToastCenterPosition = 2
let kToastBottomPosition = 3

let appDelegate = UIApplication.shared.delegate as! AppDelegate

////MARK:-
////MARK:- BASE URL
//let Base_URL = "http://demo2server.com/chatapp/api/"
//
////MARK:-
////MARK:- IMAGE URL
//let Image_URL = "http://demo2server.com/chatapp/"


//MARK:-
//MARK:- API Method

let kMethodGetAuthorizationHeader = "auth/get-api-key"
let kMethodGetCountry             = "get-countries"
let kMethodSignUpOtp              = "verify-for-signup-and-get-otp"
let kMethodSignUp                 = "sign-up"
let kMethodLogin                  = "login"
let kMethordUpdatePassword        = "update-password-by-phone-number"
let kMethodChangePassword         = "change-password"
let kMethodLogout                 = "logout"
let kMethodDeactivateAccount      = "deactive-user-account"
let kMethodUpdateUserProfile      = "update-profile"
let kMethodGetUserProfile         = "get-user-profile"
let kMethodPrivateAccount         = "private-account"
let kMethodSocialUSerCheck        = "social-user-check"
let kMethodCheckUser              = "checkuser"
//MARK:-
//MARK:- Server Keys

let kServerTokenKey               = "token"
let kDataClientPublicKey          = "data-client-public-key"
let kDataClientPrivateKey         = "data-client-private-key"
let kClientPublicKey              = "client-public-key"
let kClientPrivateKey             = "client-private-key"
let kServerPublicKey              = "encryptedServerKey"
let kDataServerPublicKey          = "dataEncryptedServerKey"




//MARK:-
//MARK:- Max Limit for textfield

let kPasswordLength = 50
let kEmailLength    = 60
let kPhoneLength    = 15
let kUserNameLength = 30
let kOTPVerificationLength = 1

let kFirstNameLength = 16
let kLastNameLength = 16
let kNameLength = 30
let kCardNumberLength = 16
let kMonthLength = 2
let kYearLength = 4
let kCVVLength = 3
let kExpiryDateLength = 7
let kMaterialTypeLength = 50
let kWeightLength = 5
let kOTPLength = 6

let kAddressLength    = 50
let kPinCodeLength    = 9

//MARK:- Card Detail Error Message
let kEnterCardNumberError = "Please enter your card number."
let kEnterValidCardNumberError = "Please enter a card number."
let kEnterCardHolderNameError = "Please enter card holder name."
let kEnterValidCardHolderNameError = "Please enter a valid card holder name."
let kEnterExpiryDateError = "Please enter expiry date."
let kEnterCVVError = "Please enter your CVV."
let kEnterValidCVVError = "Please enter a valid CVV."


//MARK:- Signup Page error message
//let kInternetError = "Please check internet connetion"
let kInternetErrorMessage = "Make sure your device is connected to the internet."


let kEnterMobileNumber = "Please enter your mobile number."
let kEnterValidMobileNumber = "Please enter a valid mobile number."
let kEnterUsernameError = "Please enter your username."
let kEntervalidUserName = "Username should conatin atleast 3 characters."
let kEnterValidFnameError = "Please enter a valid first name."
let kEnterValidLnameError = "Please enter a valid last name."
let kEnterFnameError = "Please enter your first name."
let kEnterLnameError = "Please enter your last name."
let kEnterOTPError = "Please enter your OTP."
let kEnterValidOTPError = "Please enter a valid OTP."
let kEnterCountryError = "Please enter your country."
let kEnterCountryCodeError = "Please enter country code."
let kEnterValidCountryError = "Please enter a valid country name."
//let kEnterPassError = "Please enter your password."
//let kPasswordAcceptableLengthError = "Password should be minimum 8 characters."

//let kEnterEmailError = "Please enter your email."
//let kEnterVaildEmailError = "Please enter a valid email."
//let kEnterPasswordError = "Please enter your password."
//let kEnterVaildPasswordError = "The password must be at least 8 characters with 1 letter and 1 number."
//let kMobileNumLengthError = "Phone number should be minimum 7 characters."
//let kEnterConfirmPasswordError = "Please enter confirm password."
//let kPasswordsMatchingError = "Password and confirm password does not match."




//MARK:- Produst Detail Screen
let kEnterMaterialType = "Please enter your material type."
let kEnterWeight = "Please enter your materials weight."
let kSelectDate = "Please select date."
let kSelectTime = "Please select time."

let kUserNotFoundOrDeactived = "USERNOTFOUND"
//
//let kEnterVerificationCode = "Please enter your verification code."
let kSendVerificationCode = "Verification code sent successfully."

let kOTPSmsVerify = "The SMS verification code is invalid."
//

let KBrandandModelNotAvailable = "Currently brand and model list is not available.Please try again."
let kBrandSelectFirstError = "Please select brand first.";


//MARK:- NSNotification
let kUpdateUserProfile = "UpdateUserProfile"
let kCallUpdateUserProfile = "CallUpdateUserProfile"




//MARK:- Camera Notification
let kImagePickerScreenUpdate = "ImagePickerScreenUpdate"
let kImagePickerButtonUpdate = "ImagePickerButtonUpdate"
let klibNextButtonClick = "ImagePickerlibNextButton"
let klibCloseButtonClick = "CloseButton"
let klibNextButtonButtonAnimation = "ImagePickerlibNextButtonAnimation"
let klibReloadFilter = "ReloadFilter"
let klibReloadEditor = "ReloadEditor"
let klibApplySelectionGallaryFilterReload = "ApplySelectionGallaryFilterReload"
let kImageEditFilterShow = "ImageEditFilterShow"
let kVideoTimerStop = "VideoTimer"
let kPixelImageEditComplete = "PixelImageEditComplete"
let kPixelImageChangeEditComplete = "ImageChangeEditComplete"
let kMeadiaPickerError = "MeadiaPickerError"



//MARK:- Userdefaults Keys
//let kUserSavedDetails = "data"

let kDeviceName  = "IPHONE"
let kDeviceType  = "device_type"
let kDeviceToken = "device_token"
//let kCurrentUser = "currentUser"
//let kLoginCheck  = "isLogin"

// MARK :- Server Keys
let kUserName        = "username"
//let kEmail           = "email"
let kPassword        = "password"
//let kUserID          = "user_id"
let kCurrentPassword = "old_password"
let kNewPassword     = "new_password"
let kIsPrivate       = "is_private"
let kSocialType      = "social_type"
let kSocialId        = "social_id"
let kFirstname     = "first_name"
let kLastname     = "last_name"
let kMobileNo     = "mobile_no"
let kCountryID     = "country_id"
//let kCountryCode    = "phonecode"
let kCountryName    = "country_name"
let kCountryCode    =    "country_code"

//let kProfilePic = "profile_pic"
let kSocialProfilePic = "profile_Url"
let kThumbProfilePic = "profile_pic_thumb"


let KArealNotAvailable = "Currently area list is not available.Please try again."

// MARK:- Google+ Keys

let kGMapServiceApiKey = "AIzaSyBi0ZjGE7jHzGiek4o_j3A8i7HKpW4X4FY"




///Use Account for Google Map
//rpc7979@gmail.com and password = shreeshyam

