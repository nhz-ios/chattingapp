//
//  ViewController+Extension.swift
//  ChattingApp
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import Foundation
import UIKit
//import Alamofire
import Toast_Swift
import AVKit
import AVFoundation


extension UIViewController
{
    enum CallStatus {
        case incoming
        case outgoing
        case started
        case ended
    }

    
    func navigationBarWithBackButton(strTitle: String, leftbuttonImageName : String)
    {
        self.navigationItem.title = strTitle
         self.navigationController?.navigationBar.isOpaque = false
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        UINavigationBar.appearance().barTintColor = UIColor(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
        //  UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white, NSAttributedStringKey.font: UIFont.init(name: "Lato-Regular", size: 20.0) as Any]
        if (leftbuttonImageName == "back.png")
        {
            let menuButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: leftbuttonImageName)!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodBackButtonClicked(_:)))
            
            self.navigationItem.leftBarButtonItem = menuButton
       }
        else
        {
            let menuButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: leftbuttonImageName)!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodSideMenuClicked(_:)))
            
            self.navigationItem.leftBarButtonItem = menuButton
      }
    }
    func Method_HeightCalculation(text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let rect =  CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude)
        let label:UILabel = UILabel(frame: rect)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
//    func MethodLoadNavigationBarWithArrayOfLeftAndRightButtons(strTitle : String, arrLeftButtonImages : NSArray , arrRightButtonImages : NSArray)
//    {
//        self.navigationItem.title = strTitle
//        self.navigationController?.navigationBar.isHidden = false
//        
//        UINavigationBar.appearance().barTintColor = UIColor(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
//        //  UINavigationBar.appearance().tintColor = UIColor.white
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white, NSAttributedStringKey.font: UIFont.init(name: "Lato-Regular", size: 20.0) as Any]
//        
//        
//        for leftbuttonImageName in arrLeftButtonImages        
//        {
//            let menuButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: leftbuttonImageName as! String)!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodBackButtonClicked(_:)))
//            self.navigationItem.leftBarButtonItem = menuButton
//        }
//        
//        
//        for rightbuttonImageName in arrRightButtonImages
//        {
//            let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(title: rightbuttonImageName as! String, style: .plain, target: self, action: #selector(methodSideMenuClicked(_:)))
//            self.navigationItem.rightBarButtonItem = menuButton1
//        }
//        
//    }
    @objc func methodBackButtonClicked(_ sender : UIButton)
    {
        self.view.endEditing(true)
        
        if self is RegistrationScreen
        {
            for var viewcontroler in self.navigationController!.viewControllers
            {
                if viewcontroler is RegisterScreen
                {
                    self.navigationController?.popToViewController(viewcontroler, animated: true)
                    break
                }
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
     //   self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
    //    }
    }
    
    @objc func methodSideMenuClicked(_ sender : UIButton)
    {
        self.view.endEditing(true)
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    var appDelegate : AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    // show alert controller
    func onShowAlertController(title : String?,message : String?) {
        
        let alertVC = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(alertAction)
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showActivity(text : String)
    {
        let viewTemp = activityView()
        viewTemp.frame = (appDelegate.window?.bounds)!
        viewTemp.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.4)
        viewTemp.showHudFor(view: viewTemp, withText: "")
        if text == "WINDOW"
        {
            self.appDelegate.window?.addSubview(viewTemp)
        }
        else
        {
            self.view.addSubview(viewTemp)
        }
    }
    func hideActivity()
    {
        for view in (self.self.view.subviews) {
            if view is activityView {
                view.removeFromSuperview()
            }
        }
    }
    func hideWindowActivity()
    {
        for view in (self.appDelegate.window?.subviews)! {
            if view is activityView {
                view.removeFromSuperview()
            }
        }
    }
    // MARK: - ************  Image Orientation ************
    
    func fixedOrientation(img : UIImage) -> UIImage {
        
        
        if img.imageOrientation == UIImageOrientation.up {
            return img
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch img.imageOrientation
        {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: img.size.width, y: img.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: img.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: img.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch img.imageOrientation
        {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: img.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: img.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(img.size.width), height: Int(img.size.height), bitsPerComponent: img.cgImage!.bitsPerComponent, bytesPerRow: 0, space: img.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch img.imageOrientation
        {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(img.cgImage!, in: CGRect(origin: CGPoint.zero, size: img.size))
        default:
            ctx.draw(img.cgImage!, in: CGRect(origin: CGPoint.zero, size: img.size))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
    
    

    
    //MARK:- Show toast on controller
    func showToastOnViewController1(title : String?, message : String?, position: Int, controller: UIViewController, image: UIImage?) {
        
        // Make toast with a title
        switch position {
        case 1:
            //Top
            if image != nil {
                
                self.appDelegate.window!.makeToast(message, duration: 1.0, position: .top, title: title, image: nil)
            } else {
                
                
                self.appDelegate.window!.makeToast(message, duration: 1.5, position: .top, title: title, image: image)
            }
            break
            
        case 2:
            //Center
            if image != nil {
                
                self.appDelegate.window!.makeToast(message, duration: 1.0, position: .center, title: title, image: nil)
            } else {
                
                self.appDelegate.window!.makeToast(message, duration: 1.0, position: .center, title: title, image: image)
            }
            break
            
        case 3:
            //Bottom
            if image != nil {
                
                self.appDelegate.window!.makeToast(message, duration: 1.0, position: .bottom, title: title, image: nil)
            } else {
                
                self.appDelegate.window!.makeToast(message, duration: 1.0, position: .bottom, title: title, image: image)
            }
            break
            
        default:
            break
        }
    }
    //MARK:- Show toast on controller
    func showToastOnViewController(title : String?, message : String?, position: Int, controller: UIViewController, image: UIImage?) {
        
        // Make toast with a title
        switch position {
        case 1:
            //Top
            if image != nil {
                
                controller.navigationController?.view.makeToast(message, duration: 1.0, position: .top, title: title, image: nil)
            } else {
                
                
                controller.navigationController?.view.makeToast(message, duration: 1.0, position: .top, title: title, image: image)
            }
            break
            
        case 2:
            //Center
            if image != nil {
                
                controller.navigationController?.view.makeToast(message, duration: 1.0, position: .center, title: title, image: nil)
            } else {
                
                controller.navigationController?.view.makeToast(message, duration: 1.0, position: .center, title: title, image: image)
            }
            break
            
        case 3:
            //Bottom
            if image != nil {
                
                controller.navigationController?.view.makeToast(message, duration: 1.0, position: .bottom, title: title, image: nil)
            } else {
                
                controller.navigationController?.view.makeToast(message, duration: 1.0, position: .bottom, title: title, image: image)
            }
            break
            
        default:
            break
        }
    }
    func getColorFilterNameCode(_ strName : String) -> String
    {
        switch strName.capitalized
        {
        case "Normal":
            return  "Normal"
        case "Chrome":
            return  "CIPhotoEffectChrome"
        case "Fade":
            return "CIPhotoEffectFade"
        case "Instant":
            return  "CIPhotoEffectInstant"
        case "Mono":
            return  "CIPhotoEffectNoir"
        case "Process":
            return  "CIPhotoEffectProcess"
        case "Tonal":
            return "CIPhotoEffectTonal"
        case "Transfer":
            return  "CIPhotoEffectTransfer"
        case "Tone":
            return  "CILinearToSRGBToneCurve"
        case "Linear":
            return  "CISRGBToneCurveToLinear"
        case "Sepia":
            return  "CISepiaTone"
        case "Poly":
            return  "CIColorPolynomial"
        case "FalseColor":
            return  "CIFalseColor"
        default:
            return  "Normal"
        }
        
    }
    func RemoveDirectoryUrls(_ strPath : String)
    {
        if FileManager.default.fileExists(atPath: strPath)
        {
            do
            {
                try FileManager.default.removeItem(atPath: strPath)
            }
            catch let error
            {
                print(error)
            }
        }
    }
    
    func addImageTODirectory(_ strPath : String , _ imageSave : UIImage)
    {
        //        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        //        // choose a name for your image
        //        let fileName = "image.jpg"
        //        // create the destination file url to save your image
        //        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        // get your UIImage jpeg data representation and check if the destination file url already exists
        
        let fileURL = URL(fileURLWithPath: strPath)
        if let data = UIImageJPEGRepresentation(imageSave, 1.0),
            !FileManager.default.fileExists(atPath: strPath)/*fileURL.path*/ {
            do {
                // writes the image data to disk
                try data.write(to: fileURL)
                print("file saved  👍 - " , fileURL)
            } catch {
                print("error saving file:", error)
            }
        }
    }
    func MethodGetShowDateAndTime(strDateTimeStamp : String) -> String
    {
        let doubleTimeStamp: Double = Double(strDateTimeStamp) ?? 0
        let date = Date.init(timeIntervalSince1970: doubleTimeStamp)
        //        let dateFormatFromSever = DateFormatter()
        //        dateFormatFromSever.dateFormat = "d/MM/yy, hh:mm a"
        
        let dateFormatShow = DateFormatter()
        dateFormatShow.dateFormat = "MMM dd"
        
        //        let date = dateFormatFromSever.date(from: strDate)
        //        print("strDate: ",strDate)
//        print("date: ",date)
        var  showDate = dateFormatShow.string(from: date)
        
        dateFormatShow.dateFormat = "hh:mm a"
        showDate = showDate + " at " + dateFormatShow.string(from: date)
//        print("showDate :",showDate)
        
        return showDate
    }

}


extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}



extension UIColor
{
   class var colorAppTheam: UIColor?
    {
       return UIColor.init(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
    }
    
    class var colorGreen: UIColor?
    {
        return UIColor.init(red: 115.0/255.0, green: 183.0/255.0, blue: 108.0/255.0, alpha: 1.0)
    }
    
    class var colorLightGreen: UIColor?
    {
        return UIColor.init(red: 98.0/255.0, green: 214.0/255.0, blue: 161.0/255.0, alpha: 1.0)
    }
    
    class var colorMediaumLight: UIColor?
    {
        return UIColor.init(red: 88.0/255.0, green: 136.0/255.0, blue: 191.0/255.0, alpha: 1.0)
    }
    
    class var colorOrange: UIColor?
    {
        return UIColor.init(red: 241.0/255.0, green: 160.0/255.0, blue: 57.0/255.0, alpha: 1.0)
    }
    
    class var colorLightBrown: UIColor?
    {
        return UIColor.init(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0)
    }
    
    class var colorDarkBrown: UIColor?
    {
        return UIColor.init(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }
    
    class var colorlightYallo: UIColor?
    {
        return UIColor.init(red: 249.0/255.0, green: 216.0/255.0, blue: 120.0/255.0, alpha: 1.0)
    }
    
    class var colorBlueLight: UIColor?
    {
        return  UIColor.init(red: 249.0/255.0, green: 216.0/255.0, blue: 120.0/255.0, alpha: 1.0)
    }
    
    class var colorPinkLight: UIColor?
    {
        return UIColor.init(red: 245.0/255.0, green: 195.0/255.0, blue: 192.0/255.0, alpha: 1.0)
    }
    
    class var colorLightGray: UIColor?
    {
       return UIColor.init(red: 227.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
    }
    
    class var colorDarkGray: UIColor?
    {
       return UIColor.init(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }
    
    class var colorTextBlue: UIColor?
    {
       return UIColor.init(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }
    
    class var colorTextDarkGray: UIColor?
    {
        return UIColor.init(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }
    
    class var colorTextGray: UIColor?
    {
      return UIColor.init(red: 104.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }
}

////Use For Corner Radius
extension UIView {
    
    @IBInspectable
     var cornerRadius: CGFloat {
         get {
             return layer.cornerRadius
         }
         set {
             layer.cornerRadius = newValue
         }
     }
    @IBInspectable
    var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
        return layer.borderWidth
        }
        set {
        layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }

    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

@IBDesignable
open class NHZTextField: UITextField {

    @IBInspectable
    public var placeHolder = NSMutableAttributedString(){
        didSet {
            self.attributedPlaceholder = self.placeHolder
        }
    }
}

@IBDesignable
class NHZPaymentTextField: NHZTextField {
    
    @IBInspectable
    var padding: UIEdgeInsets {
        get {
            return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        }
        set {
            self.padding = newValue
        }
    }
    
    var placeholderRect: CGRect {
        get {
            return self.placeholderRect
        }
        set {
            self.placeholderRect = self.placeholderRect(forBounds: newValue)
        }
    }
   var textRect: CGRect {
        get {
            return self.textRect
        }
        set {
            self.textRect = self.textRect(forBounds: newValue)
        }
    }
    var editingRect: CGRect {
        get {
            return self.editingRect
        }
        set {
            self.editingRect = self.editingRect(forBounds: newValue)
        }
    }
    //let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }


}


class activityView: UIView
{
    
    func showHudFor(view:UIView, withText strText:String)
    {
        let viewContainer : UIView = UIView.init(frame: CGRect.init(x: 30, y: (ScreenSize.SCREEN_HEIGHT/2) - 100, width: (ScreenSize.SCREEN_WIDTH) - 60, height: 200))
        let rectFrame : CGRect = CGRect.init(x: (viewContainer.frame.size.width/2)-30, y: (viewContainer.frame.size.height/2)-58, width: 60, height: 60)
        let objAJProgressView = AJProgressView()
        // Pass your image here which will come in centre of ProgressView
        // Pass the colour for the layer of progressView
        objAJProgressView.firstColor = UIColor.white
        // If you  want to have layer of animated colours you can also add second and third colour
        objAJProgressView.secondColor = UIColor.white
        objAJProgressView.thirdColor = UIColor.white
        // Set duration to control the speed of progressView
        objAJProgressView.duration = 3.0
        // Set width of layer of progressView
        objAJProgressView.lineWidth = 4.0
        //Set backgroundColor of progressView
        objAJProgressView.bgColor =  UIColor.black.withAlphaComponent(0.2)
        objAJProgressView.show()
        viewContainer.backgroundColor = UIColor.white
        self.addSubview(objAJProgressView)
     }
    
    
    
    
}
extension AVPlayer {
    
    var isPlaying: Bool {
        if (self.rate != 0 && self.error == nil) {
            return true
        } else {
            return false
        }
    }
}
