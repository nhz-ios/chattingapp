//
//  CLImageEditorTheme.m
//
//  Created by sho yakushiji on 2013/12/05.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "CLImageEditorTheme.h"

@implementation CLImageEditorTheme

#pragma mark - singleton pattern

static CLImageEditorTheme *_sharedInstance = nil;

+ (CLImageEditorTheme*)theme
{
    static dispatch_once_t  onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[CLImageEditorTheme alloc] init];
    });
    return _sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (_sharedInstance == nil) {
            _sharedInstance = [super allocWithZone:zone];
            return _sharedInstance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.bundleName = @"CLImageEditor";
        self.backgroundColor = [UIColor blackColor];
        self.toolbarColor = [UIColor colorWithRed:68.0/255 green:93.0/255 blue:67.0/255 alpha:0.80f];
        
         self.toolbarColor = [UIColor colorWithRed:164.0/255 green:143.0/255 blue:247.0/255 alpha:1.0f];
        self.toolbarTextColor = [UIColor whiteColor];
        self.toolbarSelectedButtonColor = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
        self.toolbarTextFont = [UIFont systemFontOfSize:10];
    }
    return self;
}

@end
