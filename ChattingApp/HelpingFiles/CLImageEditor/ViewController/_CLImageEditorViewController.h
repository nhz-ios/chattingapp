//
//  _CLImageEditorViewController.h
//
//  Created by sho yakushiji on 2013/11/05.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "CLImageEditor.h"
#import "ACEDrawingView.h"
#import "ACEDrawingTools.h"
#import "DKVerticalColorPicker.h"
@class ACEDrawingView;

@interface _CLImageEditorViewController : CLImageEditor
<UIScrollViewDelegate, UIBarPositioningDelegate,ACEDrawingViewDelegate,DKVerticalColorPickerDelegate>
{
    IBOutlet __weak UINavigationBar *_navigationBar;
    IBOutlet __weak UIScrollView *_scrollView;
    
    IBOutlet UIView *view_blacknwhite;
    IBOutlet    UIButton                 *BtnUndo;
    IBOutlet UIButton *btn_black;
    IBOutlet UIButton *btn_white;
    IBOutlet     UIImageView            *imgcolor;
    IBOutlet    UIButton                 *BtnDraw;
    IBOutlet UIView *view_draw;
    DKVerticalColorPicker *vertPicker;
}

@property (nonatomic, strong) IBOutlet ACEDrawingView *drawingView;

@property (nonatomic, strong) UIImageView  *imageView;
@property (nonatomic, weak) IBOutlet UIScrollView *menuView;

- (IBAction)pushedCloseBtn:(id)sender;
- (IBAction)pushedFinishBtn:(id)sender;

- (id)initWithImage:(UIImage*)image;
- (void)fixZoomScaleWithAnimated:(BOOL)animated;
- (void)resetZoomScaleWithAnimated:(BOOL)animated;


-(void)colorPicked:(UIColor *)color;

// actions
- (IBAction)undo:(id)sender;
-(IBAction)Draw:(id)sender;
- (IBAction)color_Picker:(id)sender;
- (IBAction)method_bwchoose:(id)sender;

@end
