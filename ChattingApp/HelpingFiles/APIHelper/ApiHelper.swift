//
//  ApiHelper.swift
//  ChattingApp
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import Foundation
import Alamofire


func getallApiResultwithPostMethod(Details: NSDictionary,strMethodname : String, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    if UserDefaults.standard.object(forKey: kServerTokenKey) == nil
    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.getAuthorizationToken()
        
        return
    }
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    
    
    print("Token :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer" + strToken
    ]
    let url = Base_URL + strMethodname
    print("getAllApiResults: ",url)
    print("getAllApiResults: ",Details)
    
    Alamofire.request(url, method: .post, parameters: Details as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}


func getallApiResultwithImageAndCoverPostMethod(strMethodname : String, imgData : Data, strImgKey : String, imgCoverData : Data, strCoverImgKey : String,Details: NSDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    if UserDefaults.standard.object(forKey: kServerTokenKey) == nil
    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.getAuthorizationToken()
        
        return
    }
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    
    print("Token :",strToken)
    
    let url = Base_URL + strMethodname
    print("CheckUrl: ",url)
    print("CheckParameters: ",Details)
    
    let headers: HTTPHeaders = [
        "Content-Type":"application/json",
        "Accept": "application/json",
        "Authorization":  "Bearer" + strToken
    ]
    
    Alamofire.upload(multipartFormData:
        { multipartFormData in
            
            if imgData.count > 0
            {
                multipartFormData.append((imgData), withName: strImgKey, fileName: "file.jpg", mimeType: "image/jpeg")
            }
            
            if imgCoverData.count > 0
            {
                multipartFormData.append((imgCoverData), withName: strCoverImgKey, fileName: "file.jpg", mimeType: "image/jpeg")
            }
            
            for (key, val) in Details {
                
                multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                
            }
            
            
    },to: url,method:.post,headers:headers, encodingCompletion:
        { encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    
                    DispatchQueue.main.async {
                        completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }
    }
    )
}


func getallApiResultwithimagePostMethod(strMethodname : String,imgData : Data,strImgKey : String,Details: NSDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    if UserDefaults.standard.object(forKey: kServerTokenKey) == nil
    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.getAuthorizationToken()
        
        return
    }
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    
    print("Token :",strToken)
    
    let url = Base_URL + strMethodname
    print("CheckUrl: ",url)
    print("CheckParameters: ",Details)

    let headers: HTTPHeaders = [
        "Content-Type":"application/json",
        "Accept": "application/json",
        "Authorization":  "Bearer" + strToken
    ]
    
    Alamofire.upload(multipartFormData:
        { multipartFormData in
            
            if imgData.count > 0
            {
                multipartFormData.append((imgData), withName: strImgKey, fileName: "file.jpg", mimeType: "image/jpeg")
            }
            
            for (key, val) in Details {
                
                multipartFormData.append((val as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                
            }
            
            
    },to: url,method:.post,headers:headers, encodingCompletion:
        { encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    
                    DispatchQueue.main.async {
                        completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }
    }
    )
}

func getTokenApiResults(Details: NSDictionary,srtMethod : String, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    
    
    let url = Base_URL + srtMethod
    print("getAllApiResults: ",url)
    print("getAllApiResults: ",Details)
    
    Alamofire.request(url, method: .post, parameters: Details as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON{ response in
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}


func getallApiResultwithGetMethod(strMethodname : String,Details: NSDictionary, completionHandler : @escaping (NSDictionary?, NSError?) -> ())
{
    
    if UserDefaults.standard.object(forKey: kServerTokenKey) == nil
    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.getAuthorizationToken()
        
        return
    }
    
    let strToken = UserDefaults.standard.object(forKey: kServerTokenKey)! as! String
    
    
    print("Token :",strToken)
    
    let headers: HTTPHeaders = [
        "Authorization":  "Bearer" + strToken
    ]
    let url = Base_URL + strMethodname
    print("getAllApiResults: ",url)
    print("getAllApiResults: ",Details)

//    let url = Base_URL + strMethodname
//    print("CheckUrl: ",url)
//    print("CheckParameters: ",Details)
    
    Alamofire.request(url, method: .get, parameters: Details as? Parameters, encoding: URLEncoding.default, headers: headers).responseJSON{ response in
        
        DispatchQueue.main.async {
            completionHandler(response.result.value as! NSDictionary? , response.result.error as NSError?)
        }
        
        #if DEBUG
            //print("signUpResponse Debug Data: \(String(describing: response.result.value))")
        #endif
    }
}


