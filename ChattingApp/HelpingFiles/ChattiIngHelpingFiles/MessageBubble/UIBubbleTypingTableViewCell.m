//
//  UIBubbleTypingTableCell.m
//  UIBubbleTableViewExample
//
//  Created by Александр Баринов on 10/7/12.
//  Copyright (c) 2012 Stex Group. All rights reserved.
//

#import "UIBubbleTypingTableViewCell.h"
#import "UIImage+animatedGIF.h"
@interface UIBubbleTypingTableViewCell ()

@property (nonatomic, retain) UIImageView *typingImageView;
@property (nonatomic, retain) UIImageView *bubbleImage;

@end

@implementation UIBubbleTypingTableViewCell

@synthesize type = _type;
@synthesize typingImageView = _typingImageView;
@synthesize showAvatar = _showAvatar;
@synthesize bubbleImage = _bubbleImage;

+ (CGFloat)height
{
    return 40.0;
}


- (void)setType:(NSBubbleTypingType)value
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor=[UIColor clearColor];

    if (!self.bubbleImage)
    {
        self.bubbleImage = [[UIImageView alloc] init];
        
        [self addSubview:self.bubbleImage];
    }
    if (!self.typingImageView)
    {
        self.typingImageView = [[UIImageView alloc] init];
        self.typingImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.typingImageView];
    }
    if(self.oldType == value)
        return;
        
    self.bubbleImage.image = nil;
    self.typingImageView.image = nil;
    
    CGFloat x = (value == NSBubbleTypingTypeSomebody) ? 0 : [UIScreen mainScreen].bounds.size.width-100;
    if (value == NSBubbleTypingTypeSomebody)
    {
            
        UIImage *image=[[UIImage imageNamed:@"white_other"] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 16, 11, 11) resizingMode:UIImageResizingModeStretch];
        
        //        UIImage *image=[UIImage imageNamed:@"msg_pop_other"];
        self.bubbleImage.image = image;
        dispatch_async(dispatch_get_main_queue(), ^{
           
            UIImage *image1=[UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TypingImageOther" ofType:@"gif"]]];
            
            self.typingImageView.image = image1;

                      //    NSLog(  @"Block Says Reachable");
        });
                
           }
    else if (value == NSBubbleTypingTypeMe)
    {
        UIImage *image=[[UIImage imageNamed:@"blue_my"] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 11, 11, 16)resizingMode:UIImageResizingModeStretch];
        //        UIImage *image=[UIImage imageNamed:@"msg_pop_my"];
        self.bubbleImage.image = image;
        
        UIImage *image1=[UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TypingImageMe" ofType:@"gif"]]];

        self.typingImageView.image = image1;
    }
    self.bubbleImage.frame = CGRectMake(x, 5, 100, 40);
    self.typingImageView.frame = CGRectMake(x+25, 4, 73, 31);
    self.typingImageView.center = self.bubbleImage.center;
    self.oldType = value;
}

@end
