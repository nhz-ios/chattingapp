//
//  NSBubbleData.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import "NSBubbleData.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
//#import "NSAttributedString+CCLFormat.h"
//#import "AnnotationView.h"
#import "CopyLabel.h"

@implementation NSBubbleData 

#pragma mark - Properties

//@synthesize lblMainBody=_lblMainBody;
@synthesize date = _date;
@synthesize type = _type;
@synthesize view = _view;
@synthesize insets = _insets;
@synthesize avatar = _avatar;
@synthesize indicatorView = _indicatorView;
@synthesize progressView=_progressView;
//@synthesize lblMeetupBody=lblMeetupBody;


#pragma mark - Lifecycle



- (CGFloat)heightStringWithEmojis:(NSString*)str fontType:(UIFont *)uiFont ForWidth:(CGFloat)width {
    
    // Get text
    CFMutableAttributedStringRef attrString = CFAttributedStringCreateMutable(kCFAllocatorDefault, 0);
    CFAttributedStringReplaceString (attrString, CFRangeMake(0, 0), (CFStringRef) str );
    CFIndex stringLength = CFStringGetLength((CFStringRef) attrString);
    
    // Change font
    CTFontRef ctFont = CTFontCreateWithName((__bridge CFStringRef) uiFont.fontName, uiFont.pointSize, NULL);
    CFAttributedStringSetAttribute(attrString, CFRangeMake(0, stringLength), kCTFontAttributeName, ctFont);
    
    // Calc the size
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attrString);
    CFRange fitRange;
    CGSize frameSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0), NULL, CGSizeMake(width, CGFLOAT_MAX), &fitRange);
    
    CFRelease(ctFont);
    CFRelease(framesetter);
    CFRelease(attrString);
    
    return frameSize.height ;
    
}
#pragma mark - Text bubble

//const UIEdgeInsets textInsetsMine = {5, 10, 15, 17};
//const UIEdgeInsets textInsetsSomeone = {5, 15, 11, 10};

//                               top, left, bottom, right;
//const UIEdgeInsets textInsetsMine = {8, 15, 16, 22};
//const UIEdgeInsets textInsetsSomeone = {8, 22, 16, 15};

//                               top, left, bottom, right;
//const UIEdgeInsets textInsetsMine = {15, 15, 16, 22};
//const UIEdgeInsets textInsetsSomeone = {15, 17, 16, 10};

//                               top, left, bottom, right;
//const UIEdgeInsets textInsetsMine = {5, 5, 5, 7};
//const UIEdgeInsets textInsetsSomeone = {5, 7, 5, 5};
const UIEdgeInsets textInsetsMine = {8, 6, 6, 11};
const UIEdgeInsets textInsetsSomeone = {8, 11, 6, 6};

+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName
{
    return [[NSBubbleData alloc] initWithText:text date:date type:type senderName:senderName];
}

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName
{
    UIFont *font =[UIFont fontWithName:@"Lato-Regular" size:16.0];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        font =[UIFont fontWithName:@"Lato-Regular" size:20.0];

    
//    CGSize size = [(text ? text : @"") sizeWithFont:font constrainedToSize:CGSizeMake(200, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    CGSize size =[(text ? text : @"")  boundingRectWithSize:CGSizeMake(200, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        size =[(text ? text : @"")  boundingRectWithSize:CGSizeMake(420, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    size.height=[self heightStringWithEmojis:(text ? text : @"") fontType:font ForWidth:size.width];
   
//    if (size.height<22)
//    {
//        size.height=22;
//    }
//    else
//        size.height=size.height+3;
//    
//
//    NSString *strDestinationDate=[self timeAgo:date];
//    
//    CGSize sizeDestinationDate =[(strDestinationDate ? strDestinationDate : @"")  boundingRectWithSize:CGSizeMake(200, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:12.0]} context:nil].size;
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        sizeDestinationDate =[(strDestinationDate ? strDestinationDate : @"")  boundingRectWithSize:CGSizeMake(420, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:12.0]} context:nil].size;
//    
//    if (size.width<sizeDestinationDate.width)
//    {
//        size.width=sizeDestinationDate.width;
//    }

    CopyLabel *_lblMainBody = [[CopyLabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        _lblMainBody = [[CopyLabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        size.height+=5;
//    else
//        size.height+=5;

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    view.backgroundColor=[UIColor clearColor];
    
    _lblMainBody.numberOfLines = 0;
    _lblMainBody.lineBreakMode = NSLineBreakByWordWrapping;
    _lblMainBody.text = (text ? text : @"");
    _lblMainBody.font = font;
    _lblMainBody.textColor=(type == BubbleTypeMine ? [UIColor whiteColor] : [UIColor blackColor]);
    _lblMainBody.backgroundColor=[UIColor clearColor];
    _lblMainBody.layer.masksToBounds=NO;

    [view addSubview:_lblMainBody];
    
    if (!senderName || [senderName isEqualToString:@"(null)"] || ![senderName isEqualToString:@""])
    {
//        if (type == BubbleTypeSomeoneElse)
        {
            UILabel *lblSender = [[UILabel alloc] init];
            lblSender.numberOfLines = 1;
            lblSender.lineBreakMode = NSLineBreakByWordWrapping;
            lblSender.text = senderName;
            lblSender.textAlignment= (type == BubbleTypeSomeoneElse) ? NSTextAlignmentLeft : NSTextAlignmentRight;
            lblSender.font = [UIFont fontWithName:@"Lato-Regular-SemiBold" size:10.0];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                lblSender.font = [UIFont fontWithName:@"Lato-Regular-SemiBold" size:16.0];
            lblSender.textColor= (type == BubbleTypeSomeoneElse) ? [UIColor darkGrayColor] : [UIColor whiteColor];
            lblSender.backgroundColor=[UIColor clearColor];
            
            [view addSubview:lblSender];
            
            CGSize size1 =[(senderName ? senderName : @"")  boundingRectWithSize:CGSizeMake(200, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular-SemiBold" size:10.0]} context:nil].size;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                size1 =[(senderName ? senderName : @"")  boundingRectWithSize:CGSizeMake(420, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular-SemiBold" size:16.0]} context:nil].size;
            if (size1.width>size.width)
            {
                size.width=size1.width;
            }
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                [view setFrame:CGRectMake(0, 0, size.width, size.height+23)];
                [_lblMainBody setFrame:CGRectMake(0, 23, size.width, size.height)];
                [lblSender setFrame:CGRectMake(0, 0, size.width, 17)];
            }
            else
            {
                [view setFrame:CGRectMake(0, 0, size.width, size.height+15)];
                [_lblMainBody setFrame:CGRectMake(0, 15, size.width, size.height)];
                [lblSender setFrame:CGRectMake(0, 0, size.width, 10)];
            }
//            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//                self.lblNewDatePerMessage.frame = CGRectMake(0, 20, size.width,25);
//            else
//                self.lblNewDatePerMessage.frame = CGRectMake(0, 12, size.width,15);
       }
    }

    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:view date:date type:type insets:insets];
}

+ (id)dataWithMeetup:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName
{
    return [[NSBubbleData alloc] initWithMeetup:text date:date type:type senderName:senderName];
}

- (id)initWithMeetup:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName
{
    UIFont *font =[UIFont fontWithName:@"Lato-Regular-Bold" size:16.0];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        font =[UIFont fontWithName:@"Lato-Regular-Bold" size:20.0];
    
    
    //    CGSize size = [(text ? text : @"") sizeWithFont:font constrainedToSize:CGSizeMake(200, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    CGSize size =[(text ? text : @"")  boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-100, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        size =[(text ? text : @"")  boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-100, 9999)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    size.height=[self heightStringWithEmojis:(text ? text : @"") fontType:font ForWidth:size.width];
    UILabel *lblMeetupBody = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        lblMeetupBody = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    //        size.height+=5;
    //    else
    //        size.height+=5;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    view.backgroundColor=[UIColor clearColor];
    
    lblMeetupBody.numberOfLines = 0;
    lblMeetupBody.lineBreakMode = NSLineBreakByWordWrapping;
    lblMeetupBody.text = (text ? text : @"");
    lblMeetupBody.font = font;
    lblMeetupBody.textColor=(type == BubbleTypeMine ? [UIColor darkGrayColor] : [UIColor darkGrayColor]);
    lblMeetupBody.backgroundColor=[UIColor clearColor];
    lblMeetupBody.layer.masksToBounds=NO;
    lblMeetupBody.textAlignment= NSTextAlignmentCenter;
//    lblMeetupBody.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    [view addSubview:lblMeetupBody];
//    view.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;

    
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:view date:date type:type insets:insets];
}

- (NSString *)timeAgo:(NSDate *)compareDate{
    NSTimeInterval timeInterval = -[compareDate timeIntervalSinceNow];
    int temp = 0;
    NSString *result;
    if (timeInterval < 5) {
        result = [NSString stringWithFormat:@"Just now"];   //less than a minute
    }else if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"%d sec ago",temp];   //less than a minute
    }else if((temp = timeInterval/60) <60){
        result = [NSString stringWithFormat:@"%d min ago",temp];   //minutes ago
    }
    else if((temp = temp/60) <24){
        result = [NSString stringWithFormat:@"%d hours ago",temp];   //hours ago
    }else if((temp = temp / 24) <7){
        
        result = [NSString stringWithFormat:@"%d days ago",temp];   //days ago
    }
    else
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMMM dd"];
        result = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:compareDate]];   //10 days ago
    }
    return  result;
}

#pragma mark -
#pragma mark - Custom bubble
#pragma mark -

//const UIEdgeInsets imageInsetsMine = {11, 13, 16, 22};
//const UIEdgeInsets imageInsetsSomeone = {11, 18, 16, 14};
//const UIEdgeInsets imageInsetsMine = {2,2,2,7};
//const UIEdgeInsets imageInsetsSomeone = {2,7,2,2};
////                               top, left, bottom, right;
//const UIEdgeInsets imageInsetsMine = {6, 2, 8, 12};
//const UIEdgeInsets imageInsetsSomeone = {6, 12, 8, 2};
//                               top, left, bottom, right;
const UIEdgeInsets imageInsetsMine = {0, 0, 0, 4};
const UIEdgeInsets imageInsetsSomeone = {0, 4, 0, 0};


+ (id)dataWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:(NSString *)senderName ImageSize:(CGSize)imgSize
{
    return [[NSBubbleData alloc] initWithImageView:image date:date type:type shouldShowProgress:shouldShowProgress senderName:senderName ImageSize:imgSize];
}


- (id)initWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:(NSString *)senderName ImageSize:(CGSize)imgSize
{
    imgSize = image == nil ? imgSize : image.size;
    
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0,200, (image == nil ? 200 : 200 * (image.size.height / image.size.width)))];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, (image == nil ? 200 : 200 * (image.size.height / image.size.width)))];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = image;
    self.imageView.layer.cornerRadius = 4.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    [self.imageView setCenter:newView.center];
    
    
    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, (image == nil ? 200 : 200 * (image.size.height / image.size.width)))];
    
    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor clearColor]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.0f]];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    [newView addSubview:self.btnView];
    [newView bringSubviewToFront:self.btnView];
    [self.btnView setCenter:newView.center];
   
    
    self.lblStatus= [[UILabel alloc] initWithFrame:CGRectMake(5, self.btnView.frame.size.height - 20, 120, 20)];
    self.lblStatus.tag=55;
    self.lblStatus.numberOfLines = 1;
    self.lblStatus.lineBreakMode = NSLineBreakByClipping;
    self.lblStatus.text = @"";
    
    self.lblStatus.textColor=[UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
    self.lblStatus.backgroundColor = [UIColor clearColor];
    self.lblStatus.font=[UIFont fontWithName:@"Lato-Regular" size:12.0];
    self.lblStatus.textAlignment=NSTextAlignmentLeft;
    
    [newView addSubview:self.lblStatus];
    [newView bringSubviewToFront:self.lblStatus];

    self.HUD = [[MBProgressHUD alloc] init];
    [newView addSubview:self.HUD];
    self.HUD.mode = MBProgressHUDModeAnnularDeterminate;
    self.HUD.tag=110;
 
    if (shouldShowProgress)
    {
        [self.HUD show:YES];
        [self.btnView setImage:[UIImage imageNamed:@"cancel_media_icon"] forState:UIControlStateNormal];
    }
    [newView bringSubviewToFront:self.btnView];
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
 
    if (!senderName || [senderName isEqualToString:@"(null)"] || ![senderName isEqualToString:@""])
    {
        if (type == BubbleTypeSomeoneElse)
        {
            UIView *view = [[UIView alloc] initWithFrame:newView.frame];
            view.backgroundColor=[UIColor clearColor];

            UILabel *lblSender = [[UILabel alloc] init];
            lblSender.numberOfLines = 1;
            lblSender.lineBreakMode = NSLineBreakByWordWrapping;
            lblSender.text = senderName;
            lblSender.textAlignment= (type == BubbleTypeSomeoneElse) ? NSTextAlignmentLeft : NSTextAlignmentRight;
            lblSender.font = [UIFont fontWithName:@"Lato-Regular-SemiBold" size:12.0];
            lblSender.textColor= (type == BubbleTypeSomeoneElse) ? [UIColor darkGrayColor] : [UIColor whiteColor];
            lblSender.backgroundColor=[UIColor clearColor];
            
            [view addSubview:lblSender];
            [view addSubview:newView];
            
            [view setFrame:CGRectMake(0, 0, newView.frame.size.width, newView.frame.size.height+15)];
            [newView setFrame:CGRectMake(0, 15, newView.frame.size.width, newView.frame.size.height)];
            [lblSender setFrame:CGRectMake(5, 3, newView.frame.size.width-5, 10)];
         
            return [self initWithView:view date:date type:type insets:insets];
       }
    }
    return [self initWithView:newView date:date type:type insets:insets];
}

+ (id)dataWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:(NSString *)senderName ImageSize:(CGSize)imgSize
{
    return [[NSBubbleData alloc] initWithVideoView:image date:date type:type shouldShowProgress:shouldShowProgress senderName:senderName ImageSize:imgSize];
}


- (id)initWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:senderName ImageSize:(CGSize)imgSize
{
    imgSize = image == nil ? imgSize : image.size;
    
    UIView *newView=[[UIView alloc] initWithFrame:CGRectMake(0, 0,200, (image == nil ? 200 : 200 * (image.size.height / image.size.width)))];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200,  (image == nil ? 200 : 200 * (image.size.height / image.size.width)))];
    newView.backgroundColor=[UIColor clearColor];
    self.imageView.image = image;
    self.imageView.layer.cornerRadius = 4.0;
    self.imageView.layer.masksToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.tag=5;
    [newView addSubview:self.imageView];
    [self.imageView setCenter:newView.center];
    
    

    self.btnView =[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200,  (image == nil ? 200 : 200 * (image.size.height / image.size.width)))];
    
    self.btnView.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnView.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnView setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.0f]];
    [self.btnView.layer setCornerRadius:4.0];
    [self.btnView setClipsToBounds:YES];
    [self.btnView setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnView addTarget:self action:@selector(viewButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.btnView.tag=11;
    self.btnView.layer.cornerRadius = 5.0;
    self.btnView.layer.masksToBounds = YES;
    [newView addSubview:self.btnView];
    [newView bringSubviewToFront:self.btnView];
    [self.btnView setCenter:newView.center];
    
    self.lblStatus= [[UILabel alloc] initWithFrame:CGRectMake(5, self.btnView.frame.size.height - 20, 120, 20)];
    self.lblStatus.tag=55;
    self.lblStatus.numberOfLines = 1;
    self.lblStatus.lineBreakMode = NSLineBreakByClipping;
    self.lblStatus.text = @"";
    
    self.lblStatus.textColor=[UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
    self.lblStatus.backgroundColor = [UIColor clearColor];
    self.lblStatus.font=[UIFont fontWithName:@"Lato-Regular" size:12.0];

    self.lblStatus.textAlignment=NSTextAlignmentLeft;
    
    [newView addSubview:self.lblStatus];
    [newView bringSubviewToFront:self.lblStatus];
   
    
    self.HUD = [[MBProgressHUD alloc] init];
    [newView addSubview:self.HUD];
    self.HUD.mode = MBProgressHUDModeAnnularDeterminate;
    self.HUD.tag=110;
    if (shouldShowProgress)
    {
        [self.HUD show:YES];
        [self.btnView setImage:[UIImage imageNamed:@"cancel_media_icon"] forState:UIControlStateNormal];
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    
    if (!senderName || [senderName isEqualToString:@"(null)"] || ![senderName isEqualToString:@""])
    {
        if (type == BubbleTypeSomeoneElse)
        {
            UIView *view = [[UIView alloc] initWithFrame:newView.frame];
            view.backgroundColor=[UIColor clearColor];
            
            UILabel *lblSender = [[UILabel alloc] init];
            lblSender.numberOfLines = 1;
            lblSender.lineBreakMode = NSLineBreakByWordWrapping;
            lblSender.text = senderName;
            lblSender.textAlignment= (type == BubbleTypeSomeoneElse) ? NSTextAlignmentLeft : NSTextAlignmentRight;
            lblSender.font = [UIFont fontWithName:@"Lato-Regular-SemiBold" size:12.0];
            lblSender.textColor= (type == BubbleTypeSomeoneElse) ? [UIColor darkGrayColor] : [UIColor whiteColor];
            lblSender.backgroundColor=[UIColor clearColor];
            
            [view addSubview:lblSender];
            [view addSubview:newView];
            
            [view setFrame:CGRectMake(0, 0, newView.frame.size.width, newView.frame.size.height+15)];
            [newView setFrame:CGRectMake(0, 15, newView.frame.size.width, newView.frame.size.height)];
            [lblSender setFrame:CGRectMake(5, 3, newView.frame.size.width-5, 10)];
            
            return [self initWithView:view date:date type:type insets:insets];        }
    }
    return [self initWithView:newView date:date type:type insets:insets];
}


#pragma mark - Custom view bubble

+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets
{
    return [[NSBubbleData alloc] initWithView:view date:date type:type insets:insets];
}

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets
{
    self = [super init];
    
    CGRect rectOfvew=view.frame;
//    rectOfvew.size.height=15;
    view.frame=rectOfvew;
    if (self)
    {
        _view = view;
        _date = date;
        _type = type;
        _insets = insets;
    }
    return self;
}

-(IBAction)viewButtonEvent:(UIButton *)sender
{
    NSString *strTag=[NSString stringWithFormat:@"%ld",(long)sender.superview.tag];
    
    
    if (self.type==BubbleTypeSomeoneElse && (!self.senderName || [self.senderName isEqualToString:@"(null)"] || ![self.senderName isEqualToString:@""]))
        strTag=[NSString stringWithFormat:@"%ld",(long)sender.superview.superview.tag];
//    NSLog(@"sender.superview%@",sender.superview);
//    NSLog(@"sender.superview.superview%@",sender.superview.superview);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"viewPhotoClicked" object:strTag];
}

@end
