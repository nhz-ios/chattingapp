//
//  UIBubbleTableViewCell.m
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <QuartzCore/QuartzCore.h>
#import "UIBubbleTableViewCell.h"
#import "NSBubbleData.h"
//#import "NSAttributedString+CCLFormat.h"
#import "UIImageView+WebCache.h"

@interface UIBubbleTableViewCell ()

@property (nonatomic, retain) UIView *customView;
@property (nonatomic, retain) UIImageView *bubbleImage;
@property (nonatomic, retain) UIImageView *avatarImage;
@property (nonatomic, retain) UIButton *avatarButton;

- (void) setupInternalData;

@end

@implementation UIBubbleTableViewCell

@synthesize data = _data;
@synthesize customView = _customView;
@synthesize bubbleImage = _bubbleImage;
@synthesize showAvatar = _showAvatar;
@synthesize avatarImage = _avatarImage;
@synthesize avatarButton = _avatarButton;
@synthesize lblStatus = _lblStatus;
@synthesize lblTime = _lblTime;

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
	[self setupInternalData];
}


- (void)setDataInternal:(NSBubbleData *)value
{
	self.data = value;
	[self setupInternalData];
}

- (void) setupInternalData
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor=[UIColor clearColor];
    
    if (self.data.isMeetupMessage)
    {
        self.customView = self.data.view;
        [self.customView removeFromSuperview];

        self.bubbleImage.hidden = true;
        self.lblTime.hidden = true;
        self.avatarImage.hidden = true;
        self.avatarButton.hidden = true;
        self.lblStatus.hidden = true;
        
        self.customView.frame = CGRectMake(0 + self.data.insets.left, 0 + self.data.insets.top+5, [UIScreen mainScreen].bounds.size.width-100, self.data.view.frame.size.height);
        
        CGPoint pnt=self.customView.center;
        pnt.x = [UIScreen mainScreen].bounds.size.width/2;
        self.customView.center=pnt;
        
        [self.contentView addSubview:self.customView];
        return;
    }
    if (!self.bubbleImage)
    {
        self.bubbleImage = [[UIImageView alloc] init];

        [self addSubview:self.bubbleImage];
    }
    
    if (!self.lblTime)
    {
        self.lblTime = [[UILabel alloc] init];
        
        [self addSubview:self.lblTime];
    }
    if (self.lblStatus)
    {
        [self.lblStatus removeFromSuperview];
    }
    
    
    self.bubbleImage.hidden = false;
    self.lblTime.hidden = false;
    self.avatarImage.hidden = false;
    self.avatarButton.hidden = false;
    self.lblStatus.hidden = false;
    
    NSBubbleType type = self.data.type;
    
    CGFloat width = self.data.view.frame.size.width;
    CGFloat height = self.data.view.frame.size.height;
    
//    if (height<50)
//        height=50;
    self.bubbleImage.image = nil;
    CGFloat x = (type == BubbleTypeSomeoneElse) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
    CGFloat y = 0;
 
    
    // Adjusting the x coordinate for avatar
    if (self.showAvatar)
    {
        [self.avatarImage removeFromSuperview];
        [self.avatarButton removeFromSuperview];
        
        self.avatarImage = [[UIImageView alloc] init];
        self.avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        CGFloat avatarX = (type == BubbleTypeSomeoneElse) ? 2 : self.frame.size.width - 52;
        CGFloat avatarY = self.frame.size.height - 50 ;
     //   if (type == BubbleTypeSomeoneElse)
            avatarY = 0;
        self.avatarImage.frame = CGRectMake(avatarX, avatarY, 50, 50);
        [self addSubview:self.avatarImage];
       
        if (!self.data.senderImage || [self.data.senderImage isEqualToString:@"(null)"] || ![self.data.senderImage isEqualToString:@""])
        {
            [self.avatarImage sd_setImageWithURL:[NSURL URLWithString:self.data.senderImage] placeholderImage:[UIImage imageNamed:@"img_placeholder"] options:SDWebImageRefreshCached];
        }
        else
        {
            [self.avatarImage setImage:[UIImage imageNamed:@"img_placeholder"]];
        }

        self.avatarButton.frame = self.avatarImage.frame;
        [self addSubview:self.avatarButton];
        [self bringSubviewToFront:self.avatarButton];
        self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.width/2;
        self.avatarImage.layer.masksToBounds = YES;
        self.avatarImage.layer.borderColor = [UIColor whiteColor].CGColor;
        self.avatarImage.layer.borderWidth = 2.0;
        self.avatarButton.layer.cornerRadius = self.avatarImage.frame.size.width/2;
        self.avatarButton.layer.masksToBounds = YES;

//        CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
//        if (delta > 0) y = delta;
        
        if (type == BubbleTypeSomeoneElse) x += 54;
        if (type == BubbleTypeMine) x -= 54;
    }
    
    self.bubbleImage.frame = CGRectMake(x, y+5, width + self.data.insets.left + self.data.insets.right, height + self.data.insets.top + self.data.insets.bottom);

    if (type == BubbleTypeMine)
    {
         [self.lblStatus removeFromSuperview];
        self.lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-175, self.bubbleImage.frame.origin.y+self.bubbleImage.frame.size.height+2, 160,15)];
        
        if (self.showAvatar)
            self.lblStatus.frame=CGRectMake(self.frame.size.width-175-50, self.bubbleImage.frame.origin.y+self.bubbleImage.frame.size.height+2, 160,15);

        [self.lblStatus setContentMode:UIViewContentModeCenter];
        self.lblStatus.numberOfLines = 1;
        self.lblStatus.lineBreakMode = NSLineBreakByWordWrapping;
        self.lblStatus.text = @"";
        self.lblStatus.font = [UIFont fontWithName:@"Lato-Regular" size:12.0];
        self.lblStatus.textColor=[UIColor darkGrayColor];
        self.lblStatus.backgroundColor=[UIColor clearColor];
        self.lblStatus.textAlignment=NSTextAlignmentRight;
        self.lblStatus.text = @"";
        
        if (self.data.isSent)
        {
//            self.lblStatus.text = [NSString stringWithFormat: @"Sent-%@", self.data.strSentDatePerMessage];
            self.lblStatus.text = @"Sent";
        }
        if (self.data.isReceived)
        {
//            self.lblStatus.text = [NSString stringWithFormat: @"Delivered-%@", self.data.strReceivedDatePerMessage];
            self.lblStatus.text = @"Delivered";
        }
        if (self.data.isRead)
        {
            self.lblStatus.text = [NSString stringWithFormat: @"Read - %@", self.data.strReadDatePerMessage];
        }
        [self.contentView addSubview:self.lblStatus];
    }
    
    [self.customView removeFromSuperview];
    

    self.customView = self.data.view;
    self.customView.frame = CGRectMake(x + self.data.insets.left, y + self.data.insets.top+5, width, height);
    [self.contentView addSubview:self.customView];
//    self.customView.backgroundColor=[UIColor yellowColor];
    UILongPressGestureRecognizer *onLongTap=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPressGesture:)] ;
    
    onLongTap.minimumPressDuration=1.0;
    [self.contentView addGestureRecognizer:onLongTap];

 
    if (type == BubbleTypeSomeoneElse)
    {
        UIImage *image=[[UIImage imageNamed:@"white_other"] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 16, 11, 11) resizingMode:UIImageResizingModeStretch];
//        UIImage *image=[UIImage imageNamed:@"msg_pop_other"];
        self.bubbleImage.image = image;

        [self.avatarButton addTarget:self action:@selector(avtarButtonEvent:) forControlEvents:UIControlEventTouchUpInside];

     }
    else
    {
        UIImage *image=[[UIImage imageNamed:@"blue_my"] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 11, 11, 16)resizingMode:UIImageResizingModeStretch];
//        UIImage *image=[UIImage imageNamed:@"msg_pop_my"];
        self.bubbleImage.image = image;
    }


//    self.lblTime = [[UILabel alloc] initWithFrame:CGRectMake(7, self.frame.size.height/2-15, (self.bubbleImage.frame.origin.x-15) ,15)];
    
    self.lblTime.frame = CGRectMake(5, self.frame.size.height/2-15, (self.bubbleImage.frame.origin.x-10) ,15);
    if (type == BubbleTypeSomeoneElse)
        self.lblTime.frame = CGRectMake(self.bubbleImage.frame.origin.x + self.bubbleImage.frame.size.width+5, self.frame.size.height/2-7, self.frame.size.width-(self.bubbleImage.frame.origin.x +self.customView.frame.size.width+10) ,15);

    [self.lblTime setContentMode:UIViewContentModeCenter];
    self.lblTime.numberOfLines = 1;
    self.lblTime.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblTime.text = self.data.strDatePerMessage;
    self.lblTime.font = [UIFont fontWithName:@"Lato-Regular" size:11.0];
    self.lblTime.textColor=[UIColor darkGrayColor];
    self.lblTime.backgroundColor=[UIColor clearColor];
    self.lblTime.textAlignment= (type == BubbleTypeSomeoneElse) ? NSTextAlignmentLeft : NSTextAlignmentRight;
    
    CGPoint tcenter = self.lblTime.center;
    tcenter.y = self.bubbleImage.center.y;
    self.lblTime.center = tcenter;
    /*
    if (self.data.isMeetupMessage)
    {
        self.bubbleImage.hidden = true;
        self.lblTime.hidden = true;
        self.avatarImage.hidden = true;
        self.avatarButton.hidden = true;
        self.lblStatus.hidden = true;
        
        self.customView.frame = CGRectMake(0 + self.data.insets.left, 0 + self.data.insets.top+5, screen_width-100, height);
       
        CGPoint pnt=self.customView.center;
        pnt.x = screen_width/2;
        self.customView.center=pnt;
    }
     */
 }

-(IBAction)avtarButtonEvent:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileClicked" object:nil];
}

-(void)handleLongPressGesture:(UIGestureRecognizer*)gestureRecognizer
{
//    NSLog(@"********Selected********");
/*    if (gestureRecognizer.state==UIGestureRecognizerStateEnded || gestureRecognizer.state==UIGestureRecognizerStateChanged)
    {
        return;
    }
    else */ if (gestureRecognizer.state==UIGestureRecognizerStateBegan)
    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kSelectMessageNotification object:gestureRecognizer];

    }
}

@end
