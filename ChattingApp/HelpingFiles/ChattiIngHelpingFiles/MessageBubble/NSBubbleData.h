//
//  NSBubbleData.h
//
//  Created by Alex Barinov
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"



typedef enum _NSBubbleType
{
    BubbleTypeMine = 0,
    BubbleTypeSomeoneElse = 1
} NSBubbleType;

@interface NSBubbleData : NSObject 

//@property (nonatomic, strong) UILabel *lblMainBody;
@property (readonly, nonatomic, strong) NSDate *date;
@property (readonly, nonatomic) NSBubbleType type;
@property (readonly, nonatomic, strong) UIView *view;
@property (readonly, nonatomic) UIEdgeInsets insets;
@property (nonatomic, strong) UIImage *avatar;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) UISlider *sliderView;
@property (nonatomic, strong) UIButton *btnView;
@property (nonatomic, strong) UIImageView *imageView;
@property BOOL shouldShowProgress;
@property BOOL isSent;
@property BOOL isReceived;
@property BOOL isRead;
@property (nonatomic, strong) UILabel *lblStatus;
@property (nonatomic, strong) NSString *strMessageType;
@property (nonatomic, strong) MBProgressHUD *HUD;
@property (nonatomic, strong) NSString *strDatePerMessage;
@property (nonatomic, strong) NSString *senderName;
@property (nonatomic, strong) NSString *senderImage;

@property (nonatomic, strong) NSString *strSentDatePerMessage;
@property (nonatomic, strong) NSString *strReceivedDatePerMessage;
@property (nonatomic, strong) NSString *strReadDatePerMessage;

@property BOOL isMeetupMessage;
//@property (nonatomic, strong) UILabel *lblMeetupBody;
//@property (nonatomic, strong) UILabel *lblNewDatePerMessage;

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName;
+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName;

+ (id)dataWithMeetup:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName;
- (id)initWithMeetup:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type senderName:(NSString *)senderName;

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets;
+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets;


#pragma mark - New CHAT bubble

+ (id)dataWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:(NSString *)senderName ImageSize:(CGSize)imgSize;
- (id)initWithImageView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:(NSString *)senderName ImageSize:(CGSize)imgSize;

+ (id)dataWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:(NSString *)senderName ImageSize:(CGSize)imgSize;
- (id)initWithVideoView:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type shouldShowProgress:(BOOL)shouldShowProgress senderName:(NSString *)senderName ImageSize:(CGSize)imgSize;


@end
