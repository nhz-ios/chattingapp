//
//  UIBubbleHeaderTableViewCell.m
//  UIBubbleTableViewExample
//
//  Created by Александр Баринов on 10/7/12.
//  Copyright (c) 2012 Stex Group. All rights reserved.
//

#import "UIBubbleHeaderTableViewCell.h"
//#import "AppConstants.h"
#import "ChattingApp-Swift.h"

@interface UIBubbleHeaderTableViewCell ()

@property (nonatomic, retain) UILabel *label;

@end

@implementation UIBubbleHeaderTableViewCell

@synthesize label = _label;
@synthesize date = _date;

+ (CGFloat)height
{
    return 0.0;

    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return 38.0;
    else
        return 28.0;

    return 28.0;
}

- (void)setDate:(NSDate *)value
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *text = [dateFormatter stringFromDate:value];
    
    if (self.label)
    {
        self.label.text = text;
        return;
    }
    
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor=[UIColor clearColor];
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ((AppDelegate *)[[UIApplication sharedApplication] delegate]).window.frame.size.width, [UIBubbleHeaderTableViewCell height])];
    self.label.text = text;
    self.label.text = @"";

    UIFont *font=[UIFont fontWithName:@"Lato-SemiBold" size:17.0];

    self.label.font = font;
    
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.shadowOffset = CGSizeMake(0, 1);
    self.label.shadowColor = [UIColor whiteColor];
    self.label.textColor = [UIColor darkGrayColor];
    self.label.backgroundColor = [UIColor clearColor];
    [self addSubview:self.label];
}



@end
