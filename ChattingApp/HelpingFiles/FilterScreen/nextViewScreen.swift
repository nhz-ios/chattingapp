//
//  nextViewScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 16/02/18.
//  Copyright © 2018 Nine Hertz India. All rights reserved.
//

import UIKit
import AVFoundation
import CoreImage
import CoreGraphics

class nextViewScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var _filterSharedContext: CIContext!
      let player = AVPlayer()
    var arrThumbImages = NSMutableArray()
    var arrImages = NSMutableArray()
    var arrTitle : NSArray!
    var arrFilterName : NSArray!

    @IBOutlet var clView: UICollectionView!
    
    @IBOutlet var imgPost: UIImageView!
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
   // var imgMain : UIImage!
    
    var srtSelection = "ColorFilter"
    var strMediaType = ""
    
   // var filterPreviews = [filterPreview]()
   // var filters = [imageFilter]()
    var originalImage = UIImage()
    var thumbImage = UIImage()
    var didSelectImage: ((UIImage, Bool) -> Void)?
    var isImageFiltered = false
    let filterCountry = CountryListViewController()
    
    //Video Filter
    var avPlayer = AVPlayer();
    var avPlayerLayer = AVPlayerLayer();
    var exportSession: AVAssetExportSession?
    var asset: AVAsset?
    var fileAsset_: AVAsset?
    private(set) var naturalSize = CGSize.zero
    var videoAsset: AVAsset?
    var startTime: CGFloat = 0.0
    var stopTime: CGFloat = 0.0
    var tempVideoPath = ""
    
    
    
    //Other Filter
    var aCIImage = CIImage();
    var contrastFilter: CIFilter!;
    var brightnessFilter: CIFilter!;
    var context = CIContext();
    var outputImage = CIImage();
    var newUIImage = UIImage();
    
    
    @IBOutlet var brightnessSlider : UISlider!
    @IBOutlet var viewSilder : UIView!
    @IBOutlet var viewClBack : UIView!
    
    
    var videoUrl : URL!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        imgPost.image = originalImage
     
        // Collection View
        clView.delegate = self
        clView.dataSource = self
                let nib = UINib(nibName: "cellClFilter", bundle: nil)
        clView.register(nib,forCellWithReuseIdentifier: "cellClFilter")
        if let layout =  clView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        clView.reloadData()
        
        //Filter Name
        
       arrFilterName = ["CISepiaTone","CIPhotoEffectMono","CIVignetteEffect","CIPhotoEffectNoir","CIPhotoEffectFade","CIPhotoEffectChrome","CIPhotoEffectTonal","CIColorPolynomial","CIColorMonochrome","CIColorMonochrome","CIPhotoEffectProcess","CIPhotoEffectInstant","CIMaximumComponent","CIMinimumComponent","CIFalseColor","CIMinimumComponent","CIColorPolynomial"]
        
       arrTitle =  ["Normal","1977","Amaro","Brannan","EarlyBird","Hefe","Hudson","Inkwell","Kelvin","Lo-Fi","Nashville","Rise","Sierra","Toster","Valencia","Walden","X-Pro"]
        
        //Get Image
        if strMediaType == "image"
        {
           thumbImage = thumbFromImage(originalImage)
            self.showActivity(text: "WINDOW")
            DispatchQueue.main.async
                {
                     self.setFilter(imageOld: self.thumbImage)
            }
                      // self.performSelector(inBackground: #selector(self.setFilter(imageOld:)), with: self.originalImage)
       }
        else
        {
            //player = aav
            
//            let item = AVPlayerItem(url: self.videoUrl as URL)
//
//            self.avPlayer = AVPlayer(playerItem: item);
//            self.avPlayer.actionAtItemEnd = .none
//            let session = AVAudioSession.sharedInstance()
//            do
//            {
//                try session.setActive(true)
//                try session.setCategory(AVAudioSessionCategoryPlayback)
//            }
//            catch
//            {
//                print("audio issuse")
//            }
//
//            self.avPlayerLayer = AVPlayerLayer(player:self.avPlayer);
//            self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
//
//            self.avPlayerLayer.frame = CGRect(x: 0, y: 64, width: appDelegateRef.screenWidth, height: appDelegateRef.screenWidth)
//
//
//            self.view.layer.addSublayer(self.avPlayerLayer)
//            self.avPlayer.play()
//
//            NotificationCenter.default.addObserver(self,  selector: #selector(self.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,      object: self.avPlayer.currentItem!)
          
            if let thumbnailImage = getThumbnailImage(forUrl: videoUrl) {
                
               thumbImage =  self.fixedOrientation(img: thumbnailImage)
                
                thumbImage = thumbFromImage(thumbImage)
                
               
            }
            arrImages = ["","1red.png","2orange.png","3yellow.png","4blew.png","5green.png","6.png","7.png","8.png","9.png","10.png","11.png","12.png","13.png","14.png","15.png","16.png"]
        }
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func method_Back(_ sender: UIButton)
    {
//         self.appDelegate.isBackFromNext = true
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func method_Next(_ sender: UIButton)
    {
//        let addPost : addPostScrren =  addPostScrren(nibName:"addPostScrren",bundle:nil)
//        
//        if strMediaType == "video"
//        {
//            addPost.img = thumbImage
//           addPost.strMediaType = "video"
//           addPost.videoUrl = self.videoUrl
//        }
//        else
//        {
//            addPost.img = self.imgPost.image!
//          addPost.strMediaType = "image"
//        }
//        self.navigationController?.pushViewController(addPost, animated: true)
     }

    
    @IBAction func method_Filter(_ sender: UIButton)
    {
        if sender.tag == 101
        {
            srtSelection = "ColorFilter"
            
            btnFilter.setTitleColor(UIColor.init(red: 57/255, green: 243/255, blue: 158/255, alpha: 1), for: .normal)
            
            btnEdit.setTitleColor(UIColor.lightGray, for: .normal)
            
             arrTitle =  ["Normal","1977","Amaro","Brannan","EarlyBird","Hefe","Hudson","Inkwell","Kelvin","Lo-Fi","Nashville","Rise","Sierra","Toster","Valencia","Walden","X-Pro"]
            
        }
        else
        {
            srtSelection = "Other"
            let aUIImage = imgPost.image
            let aCGImage = aUIImage?.cgImage
            aCIImage = CIImage.init(cgImage: aCGImage!)
            context = CIContext(options: nil);
            contrastFilter = CIFilter(name: "CIColorControls");
            contrastFilter.setValue(aCIImage, forKey: "inputImage")
            brightnessFilter = CIFilter(name: "CIColorControls");
            brightnessFilter.setValue(aCIImage, forKey: "inputImage")
            
            btnEdit.setTitleColor(UIColor.init(red: 57/255, green: 243/255, blue: 158/255, alpha: 1), for: .normal)
            btnFilter.setTitleColor(UIColor.lightGray, for: .normal)
            
            arrImages = ["tools_brightness.png","tools_contrast.png","tools_warmth.png","tools_saturation.png","tools_shadow.png","tools_vignette.png","tools_sharpness.png",]
            
             arrTitle =  ["BRIGHTNESS","CONTRAST","WARMTH","SATURATION","SHADOWS","VIGNETTE","SHARPNESS"]
        }
        clView.reloadData()
    }
    
    
    
    @IBAction func method_SilderDoneCancel(_ sender: UIButton)
    {
        if sender.tag == 201
        {
            viewSilder.isHidden = true
            viewClBack.isHidden = false
        }
        else
        {
            viewSilder.isHidden = true
            viewClBack.isHidden = false
        }
    }
    
    
    

    //MARK: - UICollection View Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (100), height: (120))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellClFilter", for: indexPath as IndexPath) as! cellClFilter
        
        cell.imgFilter.clipsToBounds = true
        
        cell.lblTitle.text = arrTitle.object(at: indexPath.item) as? String
        
        if srtSelection == "ColorFilter"
        {
            cell.imgFilter.contentMode = .scaleAspectFill
            cell.imgFilter.layer.cornerRadius = 5
            cell.imgFilter.clipsToBounds = true

            if strMediaType == "video"
            {
                cell.backgroundColor = UIColor.clear
                cell.imgVideo.image = self.thumbImage
                cell.imgFilter.image = UIImage.init(named: arrImages.object(at: indexPath.item)  as! String)
            }
            else
            {
                if arrThumbImages.count > 0
                {
                    cell.imgFilter.image = arrThumbImages.object(at: indexPath.item) as? UIImage
                }
            }
        }
        else
        {
            cell.backgroundColor = UIColor.init(red: 164/255, green: 143/255, blue: 247/255, alpha: 1.0)
             cell.imgFilter.image = UIImage.init(named: arrImages.object(at: indexPath.item)  as! String)
            cell.imgFilter.contentMode = .scaleAspectFit
        }
      
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        
        if srtSelection == "ColorFilter"
        {
            if strMediaType == "video"
            {
                self.imgPost.bringSubview(toFront: self.view)
                
                self.imgPost.image = (arrImages.object(at: indexPath.item) as? UIImage)
                
                self.videoFilter(VideoBlack: videoUrl! as NSURL, filterName: arrFilterName.object(at: indexPath.item) as! String)
                
                
            }
            else
            {
                let image = self.getfilIMage(originalImage, tag: indexPath.item)
                
                self.imgPost.image = image
                
//                self.imgPost.image =  (arrThumbImages.object(at: indexPath.item) as? UIImage)
            }
        }
        else
        {
            viewSilder.isHidden = false
            viewClBack.isHidden = true
            brightnessSlider.tag = indexPath.item
            brightnessSlider.minimumValue = -0.2
            brightnessSlider.maximumValue = 0.2
            brightnessSlider.value = 0.0
            brightnessSlider.maximumTrackTintColor = UIColor(red: 0.1, green: 0.7, blue: 0, alpha: 1)
            brightnessSlider.minimumTrackTintColor = UIColor.black
            brightnessSlider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        }
    }
    
    
    
    //MARK  : UIImage Filter
    
    func setFilter(imageOld: UIImage)
    {
        for i in 0...16
        {
            let image = self.getfilIMage(imageOld, tag: i)
            
            if (imageOld == self.thumbImage)
            {
                self.arrThumbImages.add(image)
            }
            else
            {
                self.arrImages.add(image)
            }
            
        }
        
        self.hideWindowActivity()
        self.clView.reloadData()
//        if (imageOld == self.thumbImage)
//        {
//
//        }
    }
    

    func thumbFromImage(_ img: UIImage) -> UIImage {
        let width: CGFloat = img.size.width / 5
        let height: CGFloat = img.size.height / 5
        UIGraphicsBeginImageContext(CGSize(width: width, height: height))
        img.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let smallImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return smallImage!
    }
    
   
    func getfilIMage(_ img: UIImage, tag: Int) -> UIImage
    {
        var resultImg: UIImage?

        switch tag {
        case 0:
            resultImg = img
            break

        case 1:
            
            resultImg = self.filter(img, name: "CISepiaTone")
         
            break

        case 2:
            
            resultImg = self.filter(img, name: "CIPhotoEffectMono")
            
            break

        case 3:
            
            resultImg = self.filter(img, name: "CIVignetteEffect")
        
            break

        case 4:
            
            resultImg = self.filter(img, name: "CIPhotoEffectNoir")
       
            break

        case 5:
           
            resultImg = self.filter(img, name: "CIPhotoEffectFade")

            break
            
        case 6:
            
            resultImg = self.filter(img, name: "CIPhotoEffectChrome")
           
            break

        case 7:
            
            resultImg = self.filter(img, name: "CIPhotoEffectTonal")

            break

        case 8:
            
            resultImg = self.filter(img, name: "CIColorPolynomial")
          
            break

        case 9:
            
            resultImg = self.filter(img, name: "CIColorMonochrome")

            break

        case 10:
            
            resultImg = self.filter(img, name: "CIPhotoEffectProcess")
            
            break

        case 11:
           
             resultImg = self.filter(img, name:"CIPhotoEffectInstant")
             
            break

        case 12:
            
            resultImg = self.filter(img, name:"CIMaximumComponent")

            break

        case 13:
            
            resultImg = self.filter(img, name:"CIMinimumComponent")

            break

        case 14:
            
            resultImg = self.filter(img, name:"CIFalseColor")
         
            break

        case 15:
            
            resultImg = self.filter(img, name:"CIMinimumComponent")
         
            break

        case 16:
            
            resultImg = self.filter(img, name:"CIColorPolynomial")
            
            break

        default:
            break
        }

        return resultImg!
   }
    
    
    func filter(_ image: UIImage, name: String) -> UIImage {
        if name == ""
        {
            return image
        }
        let context = filterSharedContext()
        let ciImage = CIImage(image: image)
        if let filter = CIFilter(name: name) {
            filter.setValue(ciImage, forKey: kCIInputImageKey)
            
            if let outputImage = filter.outputImage,
                let cgImg = context.createCGImage(outputImage, from: outputImage.extent) {
                return UIImage(cgImage: cgImg, scale: image.scale, orientation: image.imageOrientation)
            } else {
                return UIImage()
            }
        }
        return UIImage()
    }
    
    

    func filterSharedContext() -> CIContext {
        if _filterSharedContext == nil {
            if let context = EAGLContext(api: .openGLES2) {
                _filterSharedContext = CIContext(eaglContext: context)
            }
            return _filterSharedContext
        } else {
            return _filterSharedContext
        }
    }


    //MARK : Video Filter
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    
    func videoFilter(VideoBlack:NSURL, filterName: String)
    {
        print("VideoBlackWhiteStart")
        let videoAsset = AVURLAsset(url: VideoBlack as URL, options: nil)
        let strMainPath: String = "\(appDelegate.strVideoFolderPath)/finalDailyBlackWhiteColorVideo.mov"
        let outUrl : NSURL = NSURL(fileURLWithPath: strMainPath)
        
       // let videoAss = AVUtilities.asset(byReversing: videoAsset, filterName: filterName, outputURL: outUrl as URL!) as AVAsset
        
       
           // AVUtilities.asset(byReversing: videoAsset, outputURL: outUrl as URL!) as AVAsset
        do
        {
            try FileManager.default.removeItem(at: VideoBlack as URL)
        }
        catch let error
        {
            print(error)
        }
        print("VideoBlack",VideoBlack)
        do
        {
            try FileManager.default.moveItem(at: outUrl as URL, to: VideoBlack as URL)
          
        }
        catch let error
        {
            print(error)
        }
        print("NEWVideoBlack",VideoBlack)
        print("strMainPath",strMainPath)
        print("VideoBlackWhiteEnd")
        
        let item = AVPlayerItem(url: VideoBlack as URL)
        
        self.avPlayer = AVPlayer(playerItem: item);
        self.avPlayer.actionAtItemEnd = .none
        let session = AVAudioSession.sharedInstance()
        do
        {
            try session.setActive(true)
            try session.setCategory(AVAudioSessionCategoryPlayback)
        }
        catch
        {
            print("audio issuse")
        }
        
        self.avPlayerLayer = AVPlayerLayer(player:self.avPlayer);
        self.avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.avPlayerLayer.frame = CGRect(x: 0, y: 64, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
        
        
        self.view.layer.addSublayer(self.avPlayerLayer)
        self.avPlayer.play()
        
        NotificationCenter.default.addObserver(self,  selector: #selector(self.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,      object: self.avPlayer.currentItem!)
        
        
    }
    
    // MARK: - Video
    
    @objc func playerItemDidReachEnd(notification: NSNotification)
    {
        
        self.avPlayer.seek(to: kCMTimeZero);
       // navigationController?.popViewController(animated: true)
        
        
    }
    
//    func videoFilter()
//    {
//        var compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith: asset!)
//        if compatiblePresets.contains(AVAssetExportPresetMediumQuality) {
//            exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetPassthrough)
//            // Implementation continues.
//            var furl = URL(fileURLWithPath: tempVideoPath)
//            exportSession?.outputURL = furl
//            exportSession?.outputFileType = .mov
//            var start: CMTime = CMTimeMakeWithSeconds(Float64(startTime), asset!.duration.timescale)
//            var duration: CMTime = CMTimeMakeWithSeconds(Float64(stopTime - startTime), asset!.duration.timescale)
//            var range: CMTimeRange = CMTimeRangeMake(start, duration)
//            exportSession?.timeRange = range
//            exportSession?.exportAsynchronously(completionHandler: {() -> Void in
//                switch self.exportSession?.status {
//                case .failed?:
//                    print("Export failed: \(exportSession?.error?.localizedDescription)")
//                case .cancelled?:
//                    print("Export canceled")
//                default:
//                    print("NONE")
//                    DispatchQueue.main.async(execute: {() -> Void in
//                        var movieUrl1 = URL(fileURLWithPath:tempVideoPath)
//
//                        videoAsset = AVAsset(url: movieUrl1)
//                        self.videoOutPut()
//                        //  UISaveVideoAtPathToSavedPhotosAlbum([movieUrl relativePath], self,@selector(video:didFinishSavingWithError:contextInfo:), nil);
//                    })
//                }
//            })
//    }
//  }
  
//    func videoOutPut()
//    {
//
//        // 2 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
//
//        var mixComposition = AVMutableComposition()
//
//
//        // 3 - Video track
//        var videoTrack: AVMutableCompositionTrack? = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
//        //Add audio track
//        var audioTrack: AVMutableCompositionTrack? = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
//        if videoAsset?.tracks(withMediaType: .audio).count > 0 {
//            try? audioTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset!.duration), of: videoAsset?.tracks(withMediaType: .audio)[0], at: kCMTimeZero)
//        }
//
//        videoTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset!.duration), of: videoAsset?.tracks(withMediaType: .video)[0], at: kCMTimeZero)
//        // 3.1 - Create AVMutableVideoCompositionInstruction
//        var mainInstruction = AVMutableVideoCompositionInstruction.init as? AVMutableVideoCompositionInstruction
//        mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, (videoAsset?.duration)!)
//        // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
//        var videolayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack!)
//        var videoAssetTrack: AVAssetTrack? = videoAsset?.tracks(withMediaType: .video)[0]
//        var videoAssetOrientation_: UIImageOrientation = .up
//
//
//        var isVideoAssetPortrait_ = false
//        var videoTransform: CGAffineTransform = videoAssetTrack!.preferredTransform
//        if videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0 {
//            videoAssetOrientation_ = .right
//            isVideoAssetPortrait_ = true
//        }
//        if videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0 {
//            videoAssetOrientation_ = .left
//            isVideoAssetPortrait_ = true
//        }
//        if videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0 {
//            videoAssetOrientation_ = .up
//        }
//        if videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0 {
//            videoAssetOrientation_ = .down
//        }
//
//        videolayerInstruction.setTransform((videoAssetTrack?.preferredTransform)!, at: kCMTimeZero)
//        videolayerInstruction.setOpacity(0.0, at: (videoAsset?.duration)!)
//        // 3.3 - Add instructions
//        mainInstruction.layerInstructions = [videolayerInstruction] as? [AVVideoCompositionLayerInstruction] ?? [AVVideoCompositionLayerInstruction]()
//        var mainCompositionInst = AVMutableVideoComposition.init as? AVMutableVideoComposition
//        var naturalSize = CGSize.zero
//        if isVideoAssetPortrait_ {
//            naturalSize = CGSize(width: (videoAssetTrack?.naturalSize.height)!, height: videoAssetTrack?.naturalSize.width)
//        }
//        else {
//            naturalSize = (videoAssetTrack?.naturalSize)!
//        }
//        var renderWidth: Float = 0.0
//        var renderHeight: Float = 0.0
//        renderWidth = Float(naturalSize.width)
//        renderHeight = Float(naturalSize.height)
//        mainCompositionInst?.renderSize = CGSize(width: CGFloat(renderWidth), height: CGFloat(renderHeight))
//        mainCompositionInst.instructions = [mainInstruction]
//        mainCompositionInst.frameDuration = CMTimeMake(1, 30)
//
//        applyVideoEffects(toComposition: mainCompositionInst, size: naturalSize)
//
//
//
//        // 4 - Get path
//
//        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//        var documentsDirectory: String = paths[0]
//        var myPathDocs: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("FinalVideo-\(arc4random() % 1000).mp4").absoluteString
//        var url = URL(fileURLWithPath: myPathDocs)
//        if FileManager.default.fileExists(atPath: myPathDocs) {
//            try? FileManager.default.removeItem(atPath: myPathDocs)
//        }
//
//
//        // 5 - Create exporter
//
//        var exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetMediumQuality)
//        exporter?.outputURL = url
//        exporter?.outputFileType = .mov
//        exporter?.shouldOptimizeForNetworkUse = true
//        exporter?.videoComposition = mainCompositionInst
//        exporter?.exportAsynchronously(completionHandler: {() -> Void in
//            DispatchQueue.main.async(execute: {() -> Void in
//                switch exporter?.status {
//                case .failed?:
//                    print("AVAssetExportSessionStatusFailed")
//                case .completed?:
//                    print("AVAssetExportSessionStatusCompleted")
//                   // self.exportDidFinish(exporter)
//                   // indicator.stopAnimating()
//                case .waiting?:
//                    print("AVAssetExportSessionStatusWaiting")
//                default:
//                    break
//                }
//            })
//        })
//
//    }
    
  
    
    @objc func sliderValueChanged(sender: UISlider)
    {
       
        switch sender.tag
        {
        case 0:
            contrastFilter.setValue(sender.value, forKey: "inputBrightness")
            //  contrastFilter.setValue(NSNumber(float: sender.value), forKey: "inputBrightness")
            outputImage = contrastFilter.outputImage!
            let cgimg = context.createCGImage(outputImage, from: outputImage.extent)
            newUIImage = UIImage.init(cgImage: cgimg!)
            imgPost.image = newUIImage
            break
            
        case 1:
            contrastFilter.setValue(sender.value, forKey: "inputContrast")
            outputImage = contrastFilter.outputImage!
            var cgimg = context.createCGImage(outputImage, from: outputImage.extent)
            newUIImage = UIImage.init(cgImage: cgimg!)
            imgPost.image = newUIImage
            break
            
        default:
            break
        }
        
        
       
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
