
#import "AVUtilities.h"
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/CGImageProperties.h>
#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>
@implementation AVUtilities

+ (AVAsset *)assetByReversingAsset:(AVAsset *)asset filterName:(NSString *)filterName outputURL:(NSURL *)outputURL {
    NSError *error;
    
    // Initialize the reader
    AVAssetReader *reader = [[AVAssetReader alloc] initWithAsset:asset error:&error];
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] lastObject];
    
    NSDictionary *readerOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange], kCVPixelBufferPixelFormatTypeKey, nil];
    AVAssetReaderTrackOutput* readerOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:videoTrack
                                                                                        outputSettings:readerOutputSettings];
    [reader addOutput:readerOutput];
    [reader startReading];
    
    // read in the samples
    NSMutableArray *samples = [[NSMutableArray alloc] init];
    
    CMSampleBufferRef sample;
    while(sample = [readerOutput copyNextSampleBuffer]) {
        [samples addObject:(__bridge id)sample];
        CFRelease(sample);
    }
    
    // Initialize the writer
    AVAssetWriter *writer = [[AVAssetWriter alloc] initWithURL:outputURL
                                                      fileType:AVFileTypeMPEG4
                                                         error:&error];
    NSDictionary *videoCompressionProps = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @(videoTrack.estimatedDataRate), AVVideoAverageBitRateKey,
                                           nil];
    NSDictionary *writerOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                          AVVideoCodecH264, AVVideoCodecKey,
                                          [NSNumber numberWithInt:videoTrack.naturalSize.width], AVVideoWidthKey,
                                          [NSNumber numberWithInt:videoTrack.naturalSize.height], AVVideoHeightKey,
                                          videoCompressionProps, AVVideoCompressionPropertiesKey,
                                          nil];
    AVAssetWriterInput *writerInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo
                                                                     outputSettings:writerOutputSettings
                                                                   sourceFormatHint:(__bridge CMFormatDescriptionRef)[videoTrack.formatDescriptions lastObject]];
    [writerInput setExpectsMediaDataInRealTime:NO];
    
    // Initialize an input adaptor so that we can append PixelBuffer
    AVAssetWriterInputPixelBufferAdaptor *pixelBufferAdaptor = [[AVAssetWriterInputPixelBufferAdaptor alloc] initWithAssetWriterInput:writerInput sourcePixelBufferAttributes:nil];
    
    [writer addInput:writerInput];
    
    [writer startWriting];
    [writer startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp((__bridge CMSampleBufferRef)samples[0])];
    
    // Append the frames to the output.
    // Notice we append the frames from the tail end, using the timing of the frames from the front.
    for(NSInteger i = 0; i < samples.count; i++) {
        // Get the presentation time for the frame
        CMTime presentationTime = CMSampleBufferGetPresentationTimeStamp((__bridge CMSampleBufferRef)samples[i]);
        
        // take the image/pixel buffer from tail end of the array
        // CVPixelBufferRef imageBufferRef = CMSampleBufferGetImageBuffer((__bridge CMSampleBufferRef)samples[samples.count - i - 1]);
        
        
        CVPixelBufferRef imageBufferRef = CMSampleBufferGetImageBuffer((__bridge CMSampleBufferRef)samples[i]);
        
        while (!writerInput.readyForMoreMediaData) {
            [NSThread sleepForTimeInterval:0.1];
        }
        
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *ciImage = [CIImage imageWithCVPixelBuffer:imageBufferRef];
        
        
        CIFilter *filter1 = [CIFilter filterWithName:filterName keysAndValues: kCIInputImageKey, ciImage, nil];
        CIImage *outputImage = [filter1 outputImage];
        CGImageRef cgimg = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        //UIImage *resultImg = [UIImage imageWithCGImage:cgimg];
        CGImageRelease(cgimg);
        
        CIContext *temporaryContext = [CIContext contextWithOptions:nil];
        //CGImageRef videoImage = [temporaryContext
        //    createCGImage:ciImage
        //    fromRect:CGRectMake(0, 0,
        //              CVPixelBufferGetWidth(imageBufferRef),
        //        CVPixelBufferGetHeight(imageBufferRef))];
        
        //UIImage *tempImage = [UIImage imageWithCGImage:videoImage];
        
        // CGImageRelease(videoImage);
        
        //        var newPixelBuffer: CVPixelBuffer? = nil
        // CVPixelBufferRef newPixelBuffer = nil;
        
        [temporaryContext render:filter1.outputImage toCVPixelBuffer:imageBufferRef bounds:filter1.outputImage.extent colorSpace:nil];
        
        //        CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorDefault, pixelBufferPool, &newPixelBuffer)
        //
        //        temporaryContext.render(
        //                              ciFilter.outputImage!,
        //                              toCVPixelBuffer: newPixelBuffer!,
        //                              bounds: ciFilter.outputImage!.extent,
        //                              colorSpace: nil)
        
        
        
        [pixelBufferAdaptor appendPixelBuffer:imageBufferRef withPresentationTime:presentationTime];
        
    }
    
    [writer finishWriting];
    
    return [AVAsset assetWithURL:outputURL];
    
    
    
    
    //
    //    NSString *strMainPath = [NSString stringWithFormat:@"%@/finalDailyBackSongcrop.m4a",app.strVideoFolderPath];
    //    NSString *strMain = [NSString stringWithFormat:@"%@/finalDailyAudio.m4a",app.strVideoFolderPath];
    //    [[NSFileManager defaultManager] removeItemAtPath:strMain error:nil];
    //
    //
    //    NSURL *audioNewURL = [NSURL fileURLWithPath:strMain];
    //
    //
    //    [[NSFileManager defaultManager] copyItemAtURL:exportSession.outputURL toURL:audioNewURL error:nil];
    //
    //    [[NSFileManager defaultManager] removeItemAtPath:strMainPath error:nil];
    
}

#pragma mark - Video Effect Implement

- (void)applyVideoEffectsToComposition:(AVMutableVideoComposition *)composition size:(CGSize)size overlayImage:(UIImage*)overlayImage
{
    // 1 - set up the overlay
    @autoreleasepool {
        CALayer *overlayLayer = [CALayer layer];
        //UIImage *overlayImage = overlayImage;
        
        [overlayLayer setContents:(id)[overlayImage CGImage]];
        overlayLayer.frame = CGRectMake(0, 0, size.width, size.height);
        [overlayLayer setMasksToBounds:YES];
        
        // 2 - set up the parent layer
        CALayer *parentLayer = [CALayer layer];
        CALayer *videoLayer = [CALayer layer];
        parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
        videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
        [parentLayer addSublayer:videoLayer];
        [parentLayer addSublayer:overlayLayer];
        
        // 3 - apply magic
        composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                     videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    }
    
}


//- (void)videoOutput:(AVAsset *)videoAsset overlayImage:(UIImage*)overlayImage
//{
//    @autoreleasepool {
//
//        // 1 - Early exit if there's no video file selected
//        if (!videoAsset)
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Load a Video Asset First" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//            return;
//        }
//
//        // 2 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
//        AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
//
//        // 3 - Video track
//        AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
//                                                                            preferredTrackID:kCMPersistentTrackID_Invalid];
//
//
//        //Add audio track
//        AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
//                                                                            preferredTrackID:kCMPersistentTrackID_Invalid];
//
//        if ([[videoAsset tracksWithMediaType:AVMediaTypeAudio] count]>0) {
//            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
//                                ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
//                                 atTime:kCMTimeZero error:nil];
//        }
//
//
//
//        [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
//                            ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
//                             atTime:kCMTimeZero error:nil];
//
//        // 3.1 - Create AVMutableVideoCompositionInstruction
//        AVMutableVideoCompositionInstruction *mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
//        mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
//
//        // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
//        AVMutableVideoCompositionLayerInstruction *videolayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
//        AVAssetTrack *videoAssetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
//        UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
//        BOOL isVideoAssetPortrait_  = NO;
//        CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
//        if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
//            videoAssetOrientation_ = UIImageOrientationRight;
//            isVideoAssetPortrait_ = YES;
//        }
//        if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
//            videoAssetOrientation_ =  UIImageOrientationLeft;
//            isVideoAssetPortrait_ = YES;
//        }
//        if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
//            videoAssetOrientation_ =  UIImageOrientationUp;
//        }
//        if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
//            videoAssetOrientation_ = UIImageOrientationDown;
//        }
//        [videolayerInstruction setTransform:videoAssetTrack.preferredTransform atTime:kCMTimeZero];
//        [videolayerInstruction setOpacity:0.0 atTime: videoAsset.duration];
//
//        // 3.3 - Add instructions
//        mainInstruction.layerInstructions = [NSArray arrayWithObjects:videolayerInstruction,nil];
//
//        AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
//
//        CGSize naturalSize;
//        if(isVideoAssetPortrait_){
//            naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
//        } else {
//            naturalSize = videoAssetTrack.naturalSize;
//        }
//
//        float renderWidth, renderHeight;
//        renderWidth = naturalSize.width;
//        renderHeight = naturalSize.height;
//        mainCompositionInst.renderSize = CGSizeMake(renderWidth, renderHeight);
//        mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
//        mainCompositionInst.frameDuration = CMTimeMake(1, 30);
//
//        [self applyVideoEffectsToComposition:mainCompositionInst size:naturalSize overlayImage:overlayImage];
//
//        // 4 - Get path
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
//                                 [NSString stringWithFormat:@"FinalVideo-%d.mp4",arc4random() % 1000]];
//        NSURL *url = [NSURL fileURLWithPath:myPathDocs];
//
//        if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs]) {
//            [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
//        }
//
//        //    //Add Indicator
//        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [indicator setFrame:CGRectMake(14, 5, 20, 20)];
//        //        indicator.center =self.videoController.view.center;// it will display in center of image view
//        //        [self.view addSubview:indicator];
//        [indicator startAnimating];
//        //    if ([[self.navigationController.navigationBar subviews] count]>=2) {
//        //        //Be careful with the next line, Here you get access to an static index,
//        //        //Apple could change the structure of the navbar and your index may change in the future.
//        //        [[[self.navigationController.navigationBar subviews] objectAtIndex:2] addSubview:indicator];
//        //
//        //        [[[self.navigationController.navigationBar subviews] objectAtIndex:2] bringSubviewToFront:indicator];
//        //
//        //    }
//        //
//        // 5 - Create exporter
//        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition
//                                                                          presetName:AVAssetExportPresetMediumQuality];
//        exporter.outputURL=url;
//        exporter.outputFileType = AVFileTypeQuickTimeMovie;
//        exporter.shouldOptimizeForNetworkUse = YES;
//        exporter.videoComposition = mainCompositionInst;
//
//
//
//        [exporter exportAsynchronouslyWithCompletionHandler:^{
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                switch (exporter.status)
//                {
//                    case AVAssetExportSessionStatusFailed:
//                        NSLog(@"AVAssetExportSessionStatusFailed");
//                        break;
//                    case AVAssetExportSessionStatusCompleted:
//                        NSLog(@"AVAssetExportSessionStatusCompleted");
//                        [self exportDidFinish:exporter];
//                        break;
//                    case AVAssetExportSessionStatusWaiting:
//                        NSLog(@"AVAssetExportSessionStatusWaiting");
//                        break;
//                    default:
//                        break;
//                }
//
//
//            });
//        }];
//    }
//
//}
//
//
//- (void)exportDidFinish:(AVAssetExportSession*)session {
//    if (session.status == AVAssetExportSessionStatusCompleted)
//    {
//       // [img_view.image fixOrientation];
//
//        NSURL *outputURL = session.outputURL;
//
//        NSLog("Print OutPut Url ------------%@",outputURL);
//
//
//       // videoUrl=outputURL;
//       // appdelegate.videoURL=outputURL;
//       // MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:outputURL];
//        //[player stop];
//
////        appdelegate.MediaData1 = UIImageJPEGRepresentation(img_view.image,1.0f);
////        ShareViewController *share = [[ShareViewController alloc] initWithNibName:@"ShareViewController" bundle:nil];
////        share.shareType = @"VIDEO";
////        [self.navigationController pushViewController:share animated:YES];
////        [self.videoController stop];
//    }
//
//}




@end
