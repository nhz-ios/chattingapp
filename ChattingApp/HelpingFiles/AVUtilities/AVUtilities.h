#import <Foundation/Foundation.h>

@class AVAsset;

@interface AVUtilities : NSObject

+ (AVAsset *)assetByReversingAsset:(AVAsset *)asset filterName:(NSString *)filterName outputURL:(NSURL *)outputURL;

//+ (AVAsset *)videoFilter:(AVAsset *)videoAsset overlayImage:                                                          (UIImage *)overlayImage outputURL:(NSURL *)outputURL;

//+ (AVAsset *)assetByReversingAsset:(AVAsset *)asset outputURL:(NSURL *)outputURL;

@end
