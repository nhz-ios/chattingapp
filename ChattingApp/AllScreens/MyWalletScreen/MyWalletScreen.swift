//
//  MyWalletScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 10/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MyWalletScreen: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    @IBOutlet weak var tblWallet:  UITableView!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var btnAddMoney:  UIButton!
    @IBOutlet var btnCashOut:  UIButton!
    @IBOutlet var lblWalletBalance:  UILabel!

    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblWallet.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "WALLET", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "WALLET", leftbuttonImageName: "back.png")
        }
    }
    
    @IBAction func methodAddMoneyButton(_ sender: UIButton)
    {
    }
    @IBAction func methodCashOutButton(_ sender: UIButton)
    {
    }

    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 85
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 135
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "WalletActivityTableCell"
        var cell : WalletActivityTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? WalletActivityTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("WalletActivityTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? WalletActivityTableCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        
        cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        
        //            cell?.lblSubTotal.textColor = UIColor.lightGray
        cell?.lblTitle.text = indexPath.row % 2 ==  0 ? "Added to Wallet" : "Paid for Order"
        cell?.lblPrice.text = indexPath.row % 2 ==  0 ? "+$1000" : "-$1500"
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




