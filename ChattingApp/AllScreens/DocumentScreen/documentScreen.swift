//
//  documentScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class documentScreen: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet var tblDoc: UITableView!
    var arrDoc = NSArray()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tblDoc.tableFooterView = UIView.init(frame: CGRect.zero)

         arrDoc = ["Document1.docx","Resume.docx","Backup.docx","project.pdf","Mydoc.docx","event.docx","details.xlsx","project2.pdf"]

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "DOCUMENTS", leftbuttonImageName: "back.png")
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        self.navigationItem.rightBarButtonItem  = menuButton1
   }
    
    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }

    // MARK : UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDoc.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "cellDocument"
        var cell : cellDocument? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? cellDocument
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("cellDocument", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? cellDocument
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.zero
        }
        
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 5
        {
            cell?.imgDoc.image = UIImage.init(named: "Document1.png")
        }
        else if indexPath.row == 3 || indexPath.row == 7
        {
            cell?.imgDoc.image = UIImage.init(named: "Document3.png")
        }
        else if indexPath.row == 4 || indexPath.row == 6
        {
            cell?.imgDoc.image = UIImage.init(named: "Document4.png")
        }
        else{
            cell?.imgDoc.image = UIImage.init(named: "Document2.png")
        }
        
        cell?.lblDoc.text = "\(arrDoc.object(at: indexPath.item))"
        
            
       return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
