//
//  SearchContactScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SearchContactScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblContacts:  UITableView!
    @IBOutlet weak var viewNav:  UIView!
    @IBOutlet weak var btnBack:  UIButton!
    @IBOutlet weak var txtFieldSearch:  UITextField!
    @IBOutlet var toolBar: UIToolbar!
   var arrMessages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFieldSearch.inputAccessoryView = toolBar
        btnBack.tintColor = UIColor.init(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
        tblContacts.tableFooterView = UIView.init(frame: CGRect.zero)
        arrMessages = [["name": "Prisa Den",
                        "message": "Lorem Ipsum is simply dummy text...",
                        "image": "user.png",
                        "time": "15 Apr 2018",
                        "count": "3"], 
                       ["name": "Lisa Group",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "George Camp",
                        "message": "Where does it come from?...",
                        "image": "user.png",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "William Rose",
                        "message": "Heyhowru?...",
                        "image": "user 4.png",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Alfred Waney",
                        "message": "Content here, content here...",
                        "image": "video_round.png",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Luke Thomas",
                        "message": "Where can I get some...",
                        "image": "user.png",
                        "time": "2 Feb 2018",
                        "count": ""],
                       ["name": "Rose Merry",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "5 Jan 2018",
                        "count": ""]]
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationItem.titleView = viewNav
        
    }
    // MARK: - UIButtonsMethod
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        txtFieldSearch.resignFirstResponder()
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        txtFieldSearch.resignFirstResponder()
    }
    @IBAction func methodBackClicked(_ sender: UIButton)
    {
        txtFieldSearch.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
        //   self.menuContainerViewController.toggleLeftSideMenuCompletion {
        
        //    }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMessages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "CallHistoryTableCell"
        var cell : CallHistoryTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CallHistoryTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("CallHistoryTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? CallHistoryTableCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        }
        cell?.lblMessage.sizeToFit()
        cell?.lblMessage2.sizeToFit()
        cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
        cell?.imgViewUser.layer.masksToBounds = true
        
        cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
        cell?.lblName.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! String
        cell?.lblMessage.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "message") as! String 
        //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
        //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
        
        cell?.imgStatus.isHidden = false
        cell?.lblMessage2.isHidden = false
        cell?.lblMessage.isHidden = true
        cell?.imgStatus.isHidden = true
        cell?.btnAudio.isHidden = true
        cell?.btnVideo.isHidden = true
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

