//
//  locationScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 11/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import MapKit

class locationScreen: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {
    
    @IBOutlet var mapView: MKMapView!
    
    @IBOutlet var tblLoc: UITableView!
    @IBOutlet var viewHeader: UIView!
    
    @IBOutlet var viewPopUp: UIView!
    @IBOutlet var btnMap: UIButton!
    @IBOutlet var btnSatellite: UIButton!
    @IBOutlet var btnHybrid: UIButton!

    var arrAddress = NSArray()
    var viewPopUpShow: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrAddress = ["Send your current Location","Share My Live Location for","Starbucks","Gia Pronto","Classical Coffee","Ven Liabrary","Nuton Academy","Street Cafe"]

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationBarWithBackButton(strTitle: "LOCATIONS", leftbuttonImageName: "back.png")
        
        let button1 = UIBarButtonItem(image: UIImage(named: "white_dots"), style: .plain, target: self, action:  #selector(methodRightButton(_sender:))) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = button1
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.viewPopUp.removeFromSuperview()
        viewPopUpShow = false
    }
    // MARK : Button Action
    
    @IBAction func methodRightButton(_sender: UIBarButtonItem)
    {
        if (viewPopUpShow == true)
        {
            self.viewPopUp.removeFromSuperview()
        }
        else
        {
            self.viewPopUp.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 105, y:0 , width: 100, height: 105)
            self.viewPopUp.layer.cornerRadius = 5
            appDelegate.window!.addSubview(self.viewPopUp)
            
            let std = appDelegate.window!.convert((self.navigationController?.navigationBar.frame)!, from: self.navigationItem.titleView)
            var frame =  self.viewPopUp.frame
            frame.origin.y = std.origin.y + std.size.height - 10
            self.viewPopUp.frame = frame
        }
        viewPopUpShow = !viewPopUpShow
    }
    
    
    @IBAction func methodMapSelect(_sender: UIButton)
    {
        if _sender.tag == 101
        {
            mapView.mapType = .standard
        }
        else if _sender.tag == 102
        {
            mapView.mapType = .satellite
        }
        else
        {
            mapView.mapType = .hybrid
        }
        self.viewPopUp.removeFromSuperview()
        mapView.reloadInputViews()
        tblLoc.reloadData()
    }
    
    
    
    
    // MARK : UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrAddress.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier:String = "cellLocationAddress"
        var cell : cellLocationAddress? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? cellLocationAddress
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("cellLocationAddress", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? cellLocationAddress
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.zero
        }
        
          cell?.lblAddressTitle.text = "\(arrAddress.object(at: indexPath.item))"
        
        if indexPath.row == 0
        {
            cell?.lblAddressTitle.textColor = UIColor.init(red: 84.0/255.0, green: 144.0/255.0, blue: 42.0/255.0, alpha: 1.0)
            cell?.imgIcon.image = UIImage.init(named: "curent_location.png")
            cell?.lblAddress.text = "Accurate to 21 meters"
        }
        else if indexPath.row == 1
        {
            cell?.lblAddressTitle.textColor = UIColor.init(red: 178.0/255.0, green: 36.0/255.0, blue: 24.0/255.0, alpha: 1.0)
            cell?.imgIcon.image = UIImage.init(named: "live_location.png")
            cell?.lblAddress.text = "Updated in real time as you move"
        }
        else
        {
            cell?.lblAddressTitle.textColor = UIColor.black
             cell?.imgIcon.image = UIImage.init(named: "location_pin.png")
            cell?.lblAddress.text = "Walnut Street, New York"

        }
        return cell!
    }
    
    //MARK:- UITableView DatSource And Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 170
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.viewPopUp.removeFromSuperview()
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
