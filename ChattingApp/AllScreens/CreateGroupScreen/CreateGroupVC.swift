//
//  CreateGroupVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 11/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class CreateGroupVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var nameTextFeild: UITextField!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var toolBar: UIToolbar!
 
    var userImageData = Data()

    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextFeild.inputAccessoryView = toolBar
       self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationBarWithBackButton(strTitle: "CREATE GROUP", leftbuttonImageName: "back.png")
        
        let rightBarButton = UIBarButtonItem.init(image: UIImage.init(named: "search"), style: .done, target: self, action: #selector(searchButtonAction(_:)))
        
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    //MARK:- Helper Method
    func initialSetUp() {
        self.userImageView.layer.cornerRadius = self.userImageView.frame.width/2
        self.cameraButton.layer.cornerRadius = self.cameraButton.frame.width/2
        self.cameraButton.layer.borderWidth = 2
        self.cameraButton.layer.borderColor = UIColor.white.cgColor
    }

    //MARK:- UITextFeild Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        var image: UIImage!
        
        // fetch the selected image
        
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        self.userImageData = UIImageJPEGRepresentation(image, 0.6)!
        self.userImageView.image = UIImage.init(data: self.userImageData)
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
   @IBAction func cameraButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Open Camera")
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Open Gallery")
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func specialGroupButton(_ sender: UIButton!) {
        sender.isSelected = !sender.isSelected
    }
    
    @objc func searchButtonAction(_ sender: Any) {
        
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        let register = CreateGroupListVC.init(nibName: "CreateGroupListVC", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
