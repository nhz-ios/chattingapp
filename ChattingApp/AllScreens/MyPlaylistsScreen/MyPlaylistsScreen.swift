//
//  MyPlaylistsScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit


class MyPlaylistsScreen: UIViewController, UITableViewDataSource, UITableViewDelegate,  UIGestureRecognizerDelegate
{
    @IBOutlet weak var tblPlaylist:  UITableView!
    @IBOutlet var viewHeaderSongs: UIView!
    @IBOutlet var viewHeaderPlaylist: UIView!
    @IBOutlet var viewMenu: UIView!
    @IBOutlet weak var viewInnerMenu: UIView!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet var viewChoosePlaylist: UIView!
    @IBOutlet weak var viewInnerPlaylist: ShadowView!
    @IBOutlet weak var tblChoosePlaylist: UITableView!
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblPlaylist.tableFooterView = UIView.init(frame: CGRect.zero)
        tblMenu.tableFooterView = UIView.init(frame: CGRect.zero)
        tblChoosePlaylist.tableFooterView = UIView.init(frame: CGRect.zero)
//        viewChoosePlaylist.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "MY PLAYLISTS", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "MY PLAYLISTS", leftbuttonImageName: "back.png")
        }

        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        self.navigationItem.rightBarButtonItem  = menuButton1
    }
    
    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }
    // MARK : UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        switch tableView.tag
        {
        case 0:
            switch section
            {
            case 0:
                return 0
            default:
                return 40
            }
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        switch tableView.tag
        {
        case 0:
            switch section
            {
            case 1:
                return viewHeaderPlaylist
            case 2:
                return viewHeaderSongs
            default:
                return nil
            }
        default:
            return nil
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        switch tableView.tag
        {
        case 0:
            return 3
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch tableView.tag
        {
        case 0:
            switch section
            {
            case 0:
                return 1
            case 1:
                return 2
            case 2:
                return 8
            default:
                return 0
            }
        case 1:
            return 4
        case 2:
            return 2
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch tableView.tag
        {
        case 0:
            switch indexPath.section
            {
            case 0:
                return 40
            case 2:
                return 54
            default:
                return 30
            }
             
        case 2:
            return 54
        default:
            return 30
        }

     }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch tableView.tag
        {
        case 0:
            switch indexPath.section
            {
            case 0:
                let cellIdentifier:String = "PlaylistsTableViewCell"
                var cell : PlaylistsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PlaylistsTableViewCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("PlaylistsTableViewCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? PlaylistsTableViewCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                cell?.imgLogo.image = UIImage(named: "create_playlist")
                cell?.lblPlaylistName.text = "Create Playlist" 
                cell?.lblNoOfSongs.text = "" 
                cell?.lblNoOfSongs.isHidden = true 
                //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
                //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
                
                return cell!
            case 1:
                let cellIdentifier:String = "PlaylistsTableViewCell"
                var cell : PlaylistsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PlaylistsTableViewCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("PlaylistsTableViewCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? PlaylistsTableViewCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: indexPath.row == 0 ? 15 : 0, bottom: 0, right: indexPath.row == 0 ? 15 : 0)

                cell?.imgLogo.image = UIImage(named: "folder")
                cell?.lblPlaylistName.text = indexPath.row == 0 ? "Maddy Songs" : "Chat Songs"
                cell?.lblNoOfSongs.text =  indexPath.row == 0 ? "7" : "19" 
                cell?.lblNoOfSongs.isHidden = false 
                //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
                //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
                
                return cell!
            default:
                let cellIdentifier:String = "SongsTableViewCell"
                var cell : SongsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SongsTableViewCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("SongsTableViewCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? SongsTableViewCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
                let strImgName : String = String(format:"Audio_%02d", indexPath.row + 1)
                let strSongName : String = String(format:"Audio%d.mp3", indexPath.row + 1)
                let strSongSec : String = String(format:"%d:%02d", arc4random_uniform(9),arc4random_uniform(20))
                
                cell?.imgLogo.image = UIImage(named: strImgName)
                cell?.lblSongName.text = strSongName
                cell?.lblNoOfSecs.text = strSongSec 
                cell?.btnMore.addTarget(self, action: #selector(methodMoreClicked(_:)), for: .touchUpInside)
                cell?.btnMore.tag = indexPath.row
                //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
                //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
                
                return cell!
                
            }
        case 1:
            let cellIdentifier:String = "SongsMenuTableCell"
            var cell : SongsMenuTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SongsMenuTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("SongsMenuTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? SongsMenuTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)

           switch indexPath.row
            {
            case 0:
                cell?.lblName.text = "Play"
            case 1:
                cell?.lblName.text = "Add to playlist"
            case 2:
                cell?.lblName.text = "Delete"
            case 3:
                cell?.lblName.text = "Rename"
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            default:
                cell?.lblName.text = ""
            }

            return cell!

        default:
            let cellIdentifier:String = "PlaylistsTableViewCell"
            var cell : PlaylistsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PlaylistsTableViewCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("PlaylistsTableViewCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? PlaylistsTableViewCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            cell?.imgLogo.image = UIImage(named: "folder")
            cell?.lblPlaylistName.text = indexPath.row == 0 ? "Maddy Songs" : "Chat Songs"
            cell?.lblNoOfSongs.text = "" 
            cell?.lblNoOfSongs.isHidden = true 
            return cell!
        }

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch tableView.tag
        {
        case 0:
            switch indexPath.section
            {
            case 0:
                let register = CreatePlaylistScreen.init(nibName: "CreatePlaylistScreen", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)
                break
            case 1:
                let register = PlaylistSongsScreen.init(nibName: "PlaylistSongsScreen", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)
                break
            default:
                self.navigationController?.popViewController(animated: true)
                break
            }
            break
        case 1:
            if(indexPath.row == 1)
            {
                viewMenu.removeFromSuperview()
                viewChoosePlaylist.removeFromSuperview()
                
                self.view.addSubview(viewChoosePlaylist)
                viewChoosePlaylist.frame = self.view.frame
                let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
                tap.delegate = self
                viewChoosePlaylist.addGestureRecognizer(tap)
            }
            else
            {
                viewMenu.removeFromSuperview()
            }
            break
        case 2:
            viewChoosePlaylist.removeFromSuperview()
            break
        default:
            break
        }

    }

    @IBAction func methodMoreClicked(_ sender: UIButton)
    {
        viewMenu.removeFromSuperview()

        self.view.addSubview(viewMenu)
        viewMenu.frame = self.view.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewMenu.addGestureRecognizer(tap)

        let cell = tblPlaylist.cellForRow(at: IndexPath.init(row: sender.tag, section: 2)) as! SongsTableViewCell
        let std = self.view.convert(cell.btnMore.frame, from: cell)
        var frame = viewInnerMenu.frame
        frame.origin.y = std.origin.y + cell.btnMore.frame.size.height
        viewInnerMenu.frame = frame
        
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tblMenu))! == true || (touch.view?.isDescendant(of: tblChoosePlaylist))! == true
        {
            return false
        }
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
