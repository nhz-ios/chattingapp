//
//  GroupsInCommonScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 13/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class GroupsInCommonScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblContacts:  UITableView!
    var arrMessages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblContacts.tableFooterView = UIView.init(frame: CGRect.zero)
        arrMessages = [["name": "Lisa Group",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "George Camp",
                        "message": "Where does it come from?...",
                        "image": "user.png",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "William Rose",
                        "message": "Heyhowru?...",
                        "image": "user 4.png",
                        "time": "12 Mar 2018",
                        "count": ""]]
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "GROUPS IN COMMON", leftbuttonImageName: "back.png")
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMessages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "RecentsCell"
        var cell : RecentsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RecentsCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("RecentsCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? RecentsCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        }
        cell?.lblTime.isHidden = true
        cell?.imgChatType.isHidden = true
        cell?.imgStatus.isHidden = true
        cell?.lblCount.isHidden = true
        cell?.lblMessage.sizeToFit()
        cell?.lblMessage.isHidden = true

        cell?.lblCount.layer.cornerRadius = (cell?.lblCount.frame.size.height)!/2
        cell?.lblCount.layer.masksToBounds = true
        cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
        cell?.imgViewUser.layer.masksToBounds = true
        
        cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
        cell?.lblName.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as? String
//        cell?.lblMessage.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "message") as? String
        cell?.imgChatType.image = UIImage(named: "groupIcon")
        cell?.imgChatType.isHidden = false
        cell?.lblMessage.isHidden = true

        var frame = cell!.lblName.frame
        frame.size.height = cell!.imgViewUser.frame.size.height
        cell?.lblName.frame = frame

        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


