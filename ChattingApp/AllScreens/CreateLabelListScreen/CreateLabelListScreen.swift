//
//  CreateLabelListScreen.swift
//  ChattingApp
//
//  Created by Shreya-ios on 11/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class CreateLabelListScreen: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UICollectionViewDelegateFlowLayout  {

    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet var sectionHeaderView: UIView!
    @IBOutlet weak var userCollectionView: UICollectionView!
    @IBOutlet weak var nameTextFeild: UITextField!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var toolBar: UIToolbar!
   
    var userArray = NSMutableArray()
    var contactListArray = NSMutableArray()
    var userImageData = Data()
    
    //MARK:- UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextFeild.inputAccessoryView = toolBar
       self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationBarWithBackButton(strTitle: "CREATE LABEL", leftbuttonImageName: "back.png")
        let rightBar = UIBarButtonItem.init(image: UIImage.init(named: "search"), style: .done, target: self, action: #selector(searchButtonAction))
        
        self.navigationItem.rightBarButtonItem = rightBar
    }
    
    //MARK:- Helper Method
    func initialSetUp() {
        self.contactListArray = ["Prisa Den", "Lisa Bhatt", "George Hamilton", "William rose", "Alfred Waney", "Luke Thomas", "Rose Marry"]
        self.userImageView.layer.cornerRadius = self.userImageView.frame.width/2
        self.cameraButton.layer.cornerRadius = self.cameraButton.frame.width/2
        self.cameraButton.layer.borderWidth = 2
        self.cameraButton.layer.borderColor = UIColor.white.cgColor
    }
    
    //MARK:- UITableView DataSource And Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: "userCollectionCell", bundle: nil)
        self.userCollectionView.register(nib, forCellWithReuseIdentifier: "userCollectionCell") 
        
        if self.userArray.count > 0 {
            self.userCollectionView.isHidden = false
            
        } else {
            self.userCollectionView.isHidden = true
        }
        
        return self.sectionHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.userArray.count > 0 {
            return 283
            
        } else {
            return 158
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let indentifier:String = "ContactUserCell"
        var cell : ContactUserCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? ContactUserCell
        
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("ContactUserCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? ContactUserCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        
        cell?.nameLabel.text = self.contactListArray.object(at: indexPath.row) as? String
        
        cell?.checkUncheckButton.tag = indexPath.row
        cell?.checkUncheckButton.isUserInteractionEnabled = false
        
        cell?.checkUncheckButton.isSelected = self.userArray.contains(indexPath.row)
            
        
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.userArray.contains(indexPath.row) {
            self.userArray.remove(indexPath.row)
            
        } else {
            self.userArray.add(indexPath.row)

        }
        self.contactTableView.reloadData()
        self.userCollectionView.reloadData()
    }
    
    //MARK:- UICollectionView DataSource And Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for: indexPath) as! userCollectionCell
        cell.userImageView.layer.borderWidth = 0
        cell.userImageView.frame.size = CGSize(width: 70, height: 70)
        cell.addUserButton.isHidden = true
        cell.userBgView.layer.borderWidth = 0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 70, height: 100)
    }
    
    //MARK:- UITextFeild Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        var image: UIImage!
        
        // fetch the selected image
        
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        self.userImageData = UIImageJPEGRepresentation(image, 0.6)!
        self.userImageView.image = UIImage.init(data: self.userImageData)
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
   @objc func searchButtonAction(_ sender: Any) {
        
    }

    @IBAction func tickButtonAction(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    @IBAction func cameraButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Open Camera")
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Open Gallery")
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK:- Memory Management method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
