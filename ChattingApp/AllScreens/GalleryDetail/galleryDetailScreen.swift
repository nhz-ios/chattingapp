//
//  galleryDetailScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class galleryDetailScreen: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
     @IBOutlet var clGalleryDetail : UICollectionView!
    var arrImages = NSArray()

    
    var strTitle = ""
    var isCameFrom = "Chat"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "cellClShare", bundle: nil)
        clGalleryDetail.register(nib, forCellWithReuseIdentifier: "cellClShare")
   
        arrImages = ["5.png","4.png","3.png","2.png","6.png","1.png"]

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: (isCameFrom == "Chat") ? "VIDEOS" : strTitle, leftbuttonImageName: "back.png")
    }
    
    
    //Mark : Uicollection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellClShare", for: indexPath) as! cellClShare
        cell.imgMedia.image = UIImage.init(named: "\(arrImages.object(at: indexPath.item))")
        cell.btnPlay.isHidden = (indexPath.item % 2 == 0) ? (isCameFrom == "Chat") ? false : true : false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/2) - 3, height: (collectionView.frame.width/2) - 10)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let loginScreen  =  shareMediaDetail(nibName:"shareMediaDetail",bundle:nil)
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
