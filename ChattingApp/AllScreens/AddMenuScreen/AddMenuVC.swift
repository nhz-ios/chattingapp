//
//  AddMenuVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import ASExtendedCircularMenu

class AddMenuVC: UIViewController, ASCircularButtonDelegate {


    @IBOutlet weak var colourPickerButton: ASCircularMenuButton!

    let colourArray: NSMutableArray = ["new_chat", "favorite" , "like" , "user 3" , "live" ]

    //MARK:- UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    
    //MARK:- Helper Method
    
    
    func initialSetUp() {
        configureCircularMenuButton(button: colourPickerButton , numberOfMenuItems:5, menuRedius: 70, postion: .center)
        colourPickerButton.menuButtonSize = .medium
        colourPickerButton.sholudMenuButtonAnimate = false

    }
    override func viewDidAppear(_ animated: Bool) {
        colourPickerButton.onClickMenuButton()

    }
    //MARK:- UIButton action Methods
    @IBAction func backButtonAction(_ sender: Any) {
//        self.dismiss(animated: false, completion: nil)
        self.view.removeFromSuperview()
    }
    
    func buttonForIndexAt(_ menuButton: ASCircularMenuButton, indexForButton: Int) -> UIButton {
        let button: UIButton = UIButton()

        if menuButton == colourPickerButton{
            button.setBackgroundImage(UIImage.init(named: self.colourArray.object(at: indexForButton) as! String), for: .normal)
        }
        return button
    }
    
    //MARK:- ASExtendedCircularMenu Delegate Method
    func didClickOnMainMenuButton(_ menuButton: ASCircularMenuButton) {
        self.view.removeFromSuperview()
    }
    func didClickOnCircularMenuButton(_ menuButton: ASCircularMenuButton, indexForButton: Int, button: UIButton) {
        if menuButton == colourPickerButton{
//            self.dismiss(animated: false, completion: nil)
            self.view.removeFromSuperview()
            
            switch(indexForButton)
            {
            case 0:
                
                let register = WriteFeedVC.init(nibName: "WriteFeedVC", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)

                break
            case 1:
                let register = SavedVC.init(nibName: "SavedVC", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)
                break
            case 2:
                let register = LikesVC.init(nibName: "LikesVC", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)
                break
            case 3:
                let register = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)
                break
            case 4:
//                let register = WriteFeedVC.init(nibName: "WriteFeedVC", bundle: nil)
//                self.navigationController?.pushViewController(register, animated: true)
                break
            default:
                break
            }
        }
    }

    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}
