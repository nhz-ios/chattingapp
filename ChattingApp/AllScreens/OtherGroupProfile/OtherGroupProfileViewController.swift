//
//  OtherGroupProfileViewController.swift
//  ChattingApp
//
//  Created by Vishal-IOS on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OtherGroupProfileViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var OtherGroupTableView: UITableView!
    @IBOutlet weak var ViewHeaderImage: UIView!
    @IBOutlet weak var ViewHeaderConact: UIView!
    @IBOutlet weak var imgBack: UIImageView!
     @IBOutlet weak var tblOptionPopup: UITableView!
    @IBOutlet weak var ViewOptionPopup: UIView!
    var iscomefor = "own" // own /other
    var arrOptionTitles = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
//        if #available(iOS 11.0, *) {
//            OtherGroupTableView.contentInsetAdjustmentBehavior = .never
//            //print("contentInsetAdjustmentBehavior")
//        } else {
//            automaticallyAdjustsScrollViewInsets = false
//        }
        if iscomefor == "other"
        {
            arrOptionTitles = ["Search Members","Leave Group"]

        }
        else
        {
            arrOptionTitles = ["Covert to special group","Convert to super group","Search Members","Set Admins","Delete and Leave Group"]
        }
        imgBack.contentMode = .scaleAspectFill
        imgBack.layer.masksToBounds = true
        tblOptionPopup.tableFooterView = UIView()
        OtherGroupTableView.tableFooterView = UIView()
        tblOptionPopup.layer.cornerRadius = 5.0

        ViewOptionPopup.layer.cornerRadius = 5.0
        
        appDelegate.window?.addSubview(ViewOptionPopup)
       // tblOptionPopup.frame.size.height = CGFloat(arrOptionTitles.count * 15)
        //self.view.bringSubview(toFront: tblOptionPopup)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "", leftbuttonImageName: "back.png")
        
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"white_call"), style: .plain, target: self, action: #selector(methodCallHistory(_:)))
        let menuButton3 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"video_call.png"), style: .plain, target: self, action: #selector(methodVideoCall(_:)))
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"white_dots.png"), style: .plain, target: self, action: #selector(methodWhiteMenu(_:)))
        //tblOptionPopup.center = (menuButton1.customView?.center)!
        self.navigationItem.rightBarButtonItems = [menuButton1, menuButton2, menuButton3]
        //self.navigationItem.titleView =
        //let frame = self.navigationItem.rightBarButtonItems?.first?.frame
       //
       // print("frameXXX",frame)
      //  print("menuButton1.frame",menuButton1.frame)
        print("frameXXX",self.view.frame)
        print("navigationItem",self.navigationController?.navigationBar.frame ?? 0)
        let NavValue = CGFloat((self.navigationController?.navigationBar.frame.size.height)! + (self.navigationController?.navigationBar.frame.origin.y)!)
        let hegitValue = (arrOptionTitles.count * 30) + 11
        if UIScreen.main.bounds.size.width > 375
        {
            ViewOptionPopup.frame = CGRect(x: UIScreen.main.bounds.size.width - 156, y: NavValue - 12, width: 145, height: CGFloat(hegitValue))
        }
        else
        {
            ViewOptionPopup.frame = CGRect(x: UIScreen.main.bounds.size.width - 153, y: NavValue - 12, width: 145, height: CGFloat(hegitValue))
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        ViewOptionPopup.isHidden = true
    }

        
    @objc func methodWhiteMenu(_ sender : UIBarButtonItem)
    {
        self.view.endEditing(true)
        if ViewOptionPopup.isHidden == true
        {
           ViewOptionPopup.isHidden = false
        }
        else
        {
            ViewOptionPopup.isHidden = true
        }
    }
    @objc func methodVideoCall(_ sender : UIButton)
    {
        self.view.endEditing(true)
        //   self.menuContainerViewController.toggleLeftSideMenuCompletion {
        
        //    }
        let loginScreen  =  GroupCallViewController(nibName:"GroupCallViewController",bundle:nil)
        loginScreen.callStatus = .started
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    @objc func methodCallHistory(_ sender : UIButton)
    {
        self.view.endEditing(true)
        //   self.menuContainerViewController.toggleLeftSideMenuCompletion {
        
        //    }
        let loginScreen  =  GroupAudioDetailsViewController(nibName:"GroupAudioDetailsViewController",bundle:nil)
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    @IBAction func btnEditClick(_ sender : UIButton)
    {
        
    }
    @IBAction func btnCameraClick(_ sender : UIButton)
    {
        
    }
    @IBAction func toggleButtonAction(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected
    }
    //MARK:- UITableView Delegate And DataSource Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView.tag == 1001
        {
            //grouptableview
            return 2
        }
        else
        {
            //popup tableview 1002
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1001
        {
            if section == 0
            {
                return 3
            }
            return 3
        }
        else
        {
            return arrOptionTitles.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView.tag == 1001
        {
            if indexPath.section == 0
            {
                return 44
            }
            else
            {
                return 80
            }
        }
        else
        {
            return 30
        }

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1001
        {
            if indexPath.section == 0
            {
                let indentifier:String = "cellOtherGroupTableViewCell"
                var cell : cellOtherGroupTableViewCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? cellOtherGroupTableViewCell
                
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("cellOtherGroupTableViewCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? cellOtherGroupTableViewCell
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell!.btntoggleImage.setTitle("", for: .normal)
                cell!.btntoggleImage.setImage(nil, for: .normal)
                cell!.btntoggleImage.setImage(nil, for: .normal)
               if indexPath.row == 0
                {
                    cell!.btntoggleImage.setImage(UIImage(named:"toggle"), for: .normal)
                    cell!.btntoggleImage.setImage(UIImage(named:"toggle_active"), for: .selected)
                    cell?.btntoggleImage.addTarget(self, action: #selector(toggleButtonAction(_:)), for: .touchUpInside)
                   cell?.lblTitle.text = "Notification"
                }
                else if indexPath.row == 1
                {
                    cell?.lblTitle.text = "Shared Media"
                    cell!.btntoggleImage.setTitle("5", for: .normal)
                }
                else
                {
                    cell?.lblTitle.text = "Add Member"
                }
                return cell!
            }
            else
            {
                let indentifier:String = "LikeCell"
                var cell : LikeCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? LikeCell
                
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("LikeCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? LikeCell
                }
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell!.dateLabel.text = "online"
                cell!.descriptionLabel.text = "Prisa Den"
                if indexPath.row == 0
                {
                    cell!.postImageView.image = #imageLiteral(resourceName: "crown")
                }
                else
                {
                    cell!.dateLabel.text = "last seen at 7:23 PM"
                    cell!.postImageView.image = #imageLiteral(resourceName: "dust_bin")
                }
                cell!.postImageView.backgroundColor = UIColor.clear
                cell!.postImageView.contentMode = .center
                return cell!
            }
        }
        else
        {
            
            let indentifier:String = "cellPopupOptionGroupTableViewCell"
            var cell : cellPopupOptionGroupTableViewCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? cellPopupOptionGroupTableViewCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("cellPopupOptionGroupTableViewCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? cellPopupOptionGroupTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell!.lblTitle.text = arrOptionTitles.object(at: indexPath.row) as? String
            if indexPath.row == arrOptionTitles.count - 1
            {
                cell!.lblline.isHidden = true
            }
            else
            {
                cell!.lblline.isHidden = false
            }
            return cell!
        }
        

    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView.tag == 1001
        {
            if section == 0
            {
                return ViewHeaderImage
            }
            return ViewHeaderConact
        }
        else
        {
            return nil

        }

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if tableView.tag == 1001
        {
            if section == 0
            {
                return UIScreen.main.bounds.size.width/2 + 40
            }
            return 50
        }
        else
        {
            return 0
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        ViewOptionPopup.isHidden = true
        
        if tableView.tag == 1001
        {
            if indexPath.section == 0 && indexPath.row == 1
            {
                let loginScreen  =  shareMediaScreen(nibName:"shareMediaScreen",bundle:nil)
                self.navigationController?.pushViewController(loginScreen, animated: true)              
            }
            else if indexPath.section == 1
            {
                if iscomefor != "other"
                {
                    let loginScreen  =  OtherGroupProfileViewController(nibName:"OtherGroupProfileViewController",bundle:nil)
                    loginScreen.iscomefor = "other"
                    self.navigationController?.pushViewController(loginScreen, animated: true)     
                }
            }
            
        }
        else if tableView.tag == 1002 && iscomefor != "other"
        {
            switch indexPath.row
            {
            case 0:
                let loginScreen  =  GroupSpecialConvertViewController(nibName:"GroupSpecialConvertViewController",bundle:nil)
                loginScreen.iscomefor = "special_group"
                self.navigationController?.pushViewController(loginScreen, animated: true)
                break
            case 1:
                let loginScreen  =  GroupSpecialConvertViewController(nibName:"GroupSpecialConvertViewController",bundle:nil)
                self.navigationController?.pushViewController(loginScreen, animated: true)
                break
            case 2:
                break
            default:
                break
            }
        }
        else
        {
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
