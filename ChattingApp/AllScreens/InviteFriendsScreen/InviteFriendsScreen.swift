//
//  InviteFriendsScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 10/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class InviteFriendsScreen: UIViewController {
    
    @IBOutlet var scrollViewBG: UIScrollView!
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 11.0, *) {
            self.scrollViewBG.contentInsetAdjustmentBehavior = .never
            ////print("contentInsetAdjustmentBehavior")
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "INVITE FRIENDS", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "INVITE FRIENDS", leftbuttonImageName: "back.png")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
