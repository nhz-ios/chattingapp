//
//  GroupAudioDetailsViewController.swift
//  ChattingApp
//
//  Created by Vishal-IOS on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class GroupAudioDetailsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet  var GroupAudioDetailsTableView: UITableView!
    @IBOutlet  var ViewHeadercall: UIView!
    @IBOutlet  var ViewHeaderConact: UIView!
     @IBOutlet  var Viewfooter: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if #available(iOS 11.0, *) {
            GroupAudioDetailsTableView.contentInsetAdjustmentBehavior = .never
            //print("contentInsetAdjustmentBehavior")
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "GROUP CALL", leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"Group_chat")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodMessage(_:)))
        menuButton1.tintColor = .white

        self.navigationItem.rightBarButtonItems = [menuButton1]
    }
    @objc func methodMessage(_ sender : UIButton)
    {
        self.view.endEditing(true)
        //   self.menuContainerViewController.toggleLeftSideMenuCompletion {
        
        //    }
    }

    //MARK:- UITableView Delegate And DataSource Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let indentifier:String = "LikeCell"
        var cell : LikeCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? LikeCell
        
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("LikeCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? LikeCell
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.backgroundColor = (UIColor.clear)
        cell!.dateLabel.text = "Ringing..."
        cell!.descriptionLabel.text = "Prisa Den"
        cell!.postImageView.isHidden = true
        return cell!
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0
        {
            return ViewHeadercall
        }
        return ViewHeaderConact
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        return nil

    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {

        return 0
    }
    @IBAction func btncallEndClick(_ sender : UIButton)
    {

    }
    @IBAction func btnSpeakerClick(_ sender : UIButton)
    {
        
    }
    @IBAction func btnMicClick(_ sender : UIButton)
    {
        
    }
    @IBAction func btnVideoStopClick(_ sender : UIButton)
    {
        
    }

}
