//
//  SongsTableViewCell.swift
//  ChattingApp
//
//  Created by ramprakash on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SongsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var lblNoOfSecs: UILabel!
    @IBOutlet weak var btnMore: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
