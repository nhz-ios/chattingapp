//
//  ContactsScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ContactsScreen: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblContacts: UITableView!
    @IBOutlet var viewSection: UIView!
    var strFrom = ""

    
    var arrMessages = NSArray()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblContacts.tableFooterView = UIView.init(frame: CGRect.zero)
        // Do any additional setup after loading the view.
        
        arrMessages = [["name": "Prisa Den",
                        "message": "My Life my rules",
                        "image": "user.png",
                        "time": "15 Apr 2018",
                        "count": "3"],
                       ["name": "Lisa Group",
                        "message": "Available for chats only",
                        "image": "user 2.png",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "George Camp",
                        "message": "Make your morning happy",
                        "image": "user.png",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "William Rose",
                        "message": "995 batter only",
                        "image": "user 4.png",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Alfred Waney",
                        "message": "Listen my words broke down",
                        "image": "video_round.png",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Luke Thomas",
                        "message": "I need more space...",
                        "image": "user.png",
                        "time": "2 Feb 2018",
                        "count": ""],
                       ["name": "Rose Merry",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "5 Jan 2018",
                        "count": ""]]

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "CONTACTS", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "CONTACTS", leftbuttonImageName: "back.png")
        }

        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"plus_white.png"), style: .plain, target: self, action: #selector(methodCallHistory(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1, menuButton2]
        
        
    }
    
    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }
    
    @objc func methodCallHistory(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = AddContactScreen.init(nibName: "AddContactScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    
    // MARK : UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
           return 1
        }
        return arrMessages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
           return 140
        }
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            let cellIdentifier:String = "cellContactsTop"
            var cell : cellContactsTop? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? cellContactsTop
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("cellContactsTop", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? cellContactsTop
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            cell?.btnNewGroup.addTarget(self, action: #selector(methodNewGroupClicked(_:)), for: .touchUpInside)
            cell?.btnNewSecretChat.addTarget(self, action: #selector(methodNewSecretChatChatClicked(_:)), for: .touchUpInside)
            cell?.btnNewChannel.addTarget(self, action: #selector(methodNewChannelClicked(_:)), for: .touchUpInside)
            cell?.btnNewLabel.addTarget(self, action: #selector(methodNewLabelClicked(_:)), for: .touchUpInside)

            return cell!
        }
        else
        {
            let cellIdentifier:String = "RecentsCell"
            var cell : RecentsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RecentsCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("RecentsCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? RecentsCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            cell?.lblMessage.sizeToFit()
            cell?.lblTime.isHidden = true
            cell?.imgChatType.isHidden = true
            cell?.imgStatus.isHidden = true
            cell?.lblCount.isHidden = true
            cell?.lblMessage.sizeToFit()
           
            cell?.lblCount.layer.cornerRadius = (cell?.lblCount.frame.size.height)!/2
            cell?.lblCount.layer.masksToBounds = true
            cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
            cell?.imgViewUser.layer.masksToBounds = true
            
            cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
            cell?.lblName.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as? String
            cell?.lblMessage.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "message") as? String
            return cell!
        }
    }
    
    
    
    //MARK:- UITableView DataSource And Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
      if section == 0
      {
        return nil
        }
        return viewSection
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 0
        }
       
        return 50
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }

    @IBAction func methodNewGroupClicked(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = CreateGroupVC.init(nibName: "CreateGroupVC", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @IBAction func methodNewSecretChatChatClicked(_ sender : UIButton)
    {
        self.view.endEditing(true)
//        let register = AddContactScreen.init(nibName: "AddContactScreen", bundle: nil)
//        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @IBAction func methodNewChannelClicked(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = CreateChannelVC.init(nibName: "CreateChannelVC", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @IBAction func methodNewLabelClicked(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = CreateLabelListScreen.init(nibName: "CreateLabelListScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
