//
//  AddContactScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class AddContactScreen: UIViewController {

    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtPhoneNo: UITextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var btnSave: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        ///Text Field IBoutlets
 
        txtFirstName .inputAccessoryView = toolBar
        txtLastName .inputAccessoryView = toolBar
        txtPhoneNo .inputAccessoryView = toolBar
        txtCountry .inputAccessoryView = toolBar
        txtEmail .inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "ADD CONTACT", leftbuttonImageName: "back.png")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == 1
        {
            self.view.endEditing(true)
            let MainView = CountryListViewController(nibName: "CountryListViewController",delegate: self)
            self.present(MainView!, animated: true, completion: nil)
            return false
        }
        
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //        if range.location == 0 && string == " "
        //        {
        //            return false
        //        }
        
        if string == " "
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        
        if (textField == txtFirstName)
        {
             if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        
        if (textField == txtLastName)
        {
            return newLength > kEmailLength ? false : true
        }
        
        if (textField == txtPhoneNo)
        {
            return newLength > kPhoneLength ? false : true
        }
        
        if (textField == txtCountry)
        {
             if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        
         return true
    }
    
    // MARK: - UIButtonsMethod
    @IBAction func methodCancelKeyPad(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func methodKeyPadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodSave(_ sender: Any)
    {
        
        self.navigationController?.popViewController(animated: true)
        //        let register = MyPlaylistsScreen.init(nibName: "MyPlaylistsScreen", bundle: nil)
        //        self.navigationController?.pushViewController(register, animated: true)
        
        //        appDelegate.showtabbar()
        //        appDelegate.TabLogin.selectedIndex = 2
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
