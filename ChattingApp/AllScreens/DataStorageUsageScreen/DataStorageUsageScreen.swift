//
//  DataStorageUsageScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 10/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class DataStorageUsageScreen: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblDataUsage:  UITableView!
    @IBOutlet var viewHeader:  UIView!
    
    var strFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblDataUsage.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "DATA AND STORAGE USAGE", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "DATA AND STORAGE USAGE", leftbuttonImageName: "back.png")
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 6
        }
       return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 && (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5)
        {
            return 64
        }
        else if indexPath.section == 1 && indexPath.row == 1
        {
            return 74
        }
        return 44
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0
        {
            return nil
        }
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 0
        }
        return 70
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0 && (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5)
        {
            let indentifier:String = "OtherUsrStatusTableCell"
            var cell : OtherUsrStatusTableCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? OtherUsrStatusTableCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OtherUsrStatusTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OtherUsrStatusTableCell
            }
            if (indexPath.section == 0 && (indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 5))
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                
            }
            else
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.btntoggleImage.isHidden = true
            cell?.lblBottomLine.isHidden = true
            cell?.lblStatus.font = UIFont.init(name: "Lato-Regular", size: 14.0)
            cell?.lblStatusValue.font = UIFont.init(name: "Lato-Regular", size: 12.0)
            
            cell?.lblStatus.textColor = UIColor.init(red: 75.0/255.0, green: 76.0/255.0, blue: 77.0/255.0, alpha: 1)
            cell?.lblStatusValue.textColor = UIColor.init(red: 95.0/255.0, green: 96.0/255.0, blue: 97.0/255.0, alpha: 1)
            if (indexPath.row == 3)
            {
                cell?.lblStatus.text = "When using mobile data"
                cell?.lblStatusValue.text = "Photos"
            }
            else if (indexPath.row == 4)
            {
                cell?.lblStatus.text = "When connecting on WI-Fl"
                cell?.lblStatusValue.text = "All Media"
            }
            else if (indexPath.row == 5)
            {
                cell?.lblStatus.text = "When roaming"
                cell?.lblStatusValue.text = "No Media"
            }
//
            return cell!
        }
        else if indexPath.section == 1
        {
            let cellIdentifier:String = "FilterTableCell"
            var cell : FilterTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FilterTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FilterTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FilterTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            
            //            cell?.lblSubTotal.textColor = UIColor.lightGray
            cell?.btnSelect.isHidden = true
            var font = UIFont.init(name: "Lato-Regular", size: 14.0)
            
            if (indexPath.row == 0)
            {
                cell?.btnSelect.isHidden = true
                cell?.lblFilterDetails.isHidden = true
                cell?.lblFilter.text = "Call Settings"
                cell?.lblFilter.textColor = .black
               font = UIFont.init(name: "Lato-Bold", size: 16.0)
            }
            else if (indexPath.row == 1)
            {
                cell?.btnSelect.isHidden = false
                cell?.lblFilterDetails.isHidden = false
                cell?.lblFilter.text = "Low data usage"
                cell?.lblFilter.textColor = UIColor.init(red: 75.0/255.0, green: 76.0/255.0, blue: 77.0/255.0, alpha: 1)
                cell?.lblFilterDetails.text = "Lower the amount of data used during a call when using mobile data"
            }
            
            cell?.btnSelect.isUserInteractionEnabled = true
            cell?.btnSelect.addTarget(self, action: #selector(toggleButtonAction(_:)), for: .touchUpInside)
            cell?.lblFilter.font = font
            cell?.lblFilterDetails.font = font
            return cell!

        }
        let indentifier:String = "cellOtherGroupTableViewCell"
        var cell : cellOtherGroupTableViewCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? cellOtherGroupTableViewCell
        
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("cellOtherGroupTableViewCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? cellOtherGroupTableViewCell
        }
        if (indexPath.section == 0 && (indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 5)) || (indexPath.section == 1)
        {
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            
        }
        else
        {
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.backgroundColor = (UIColor.clear)
        cell!.btntoggleImage.setTitle("", for: .normal)
        cell!.btntoggleImage.setImage(nil, for: .normal)
        cell!.btntoggleImage.setImage(nil, for: .normal)
        cell!.btntoggleImage.isHidden = true
        cell?.lblBottomLine.isHidden = true
       cell?.lblTitle.font = UIFont.init(name: "Lato-Regular", size: 14.0)
        cell?.lblTitle.textColor = UIColor.init(red: 75.0/255.0, green: 76.0/255.0, blue: 77.0/255.0, alpha: 1)
       if (indexPath.row == 0)
        {
            cell?.lblTitle.text = "Network usage"
        }
        else if (indexPath.row == 1)
        {
            cell?.lblTitle.text = "Storage usage"
        }
        else if (indexPath.row == 2)
        {
            cell?.lblTitle.text = "Media Auto-Download"
            cell?.lblTitle.font = UIFont.init(name: "Lato-Bold", size: 16.0)
            cell?.lblTitle.textColor = .black
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    @IBAction func toggleButtonAction(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}





