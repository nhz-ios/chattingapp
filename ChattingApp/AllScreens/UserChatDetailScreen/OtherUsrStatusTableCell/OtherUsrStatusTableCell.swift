//
//  OtherUsrStatusTableCell.swift
//  ChattingApp
//
//  Created by Vishal-IOS on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OtherUsrStatusTableCell: UITableViewCell {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStatusValue: UILabel!
    @IBOutlet weak var btntoggleImage: UIButton!
    @IBOutlet weak var lblBottomLine: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
