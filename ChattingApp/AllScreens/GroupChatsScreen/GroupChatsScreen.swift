//
//  GroupChatsScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class GroupChatsScreen: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblMessages:  UITableView!
    var arrMessages = NSMutableArray()
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMessages.tableFooterView = UIView.init(frame: CGRect.zero)
        // Do any additional setup after loading the view.
        
        arrMessages = [["name": "Prisa Den Group",
                        "message": "Lorem Ipsum is simply dummy text...",
                        "image": "user.png",
                        "time": "15 Apr 2018",
                        "count": "3"], 
                       ["name": "Lisa Group",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "George Camp Group",
                        "message": "Where does it come from?...",
                        "image": "user.png",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "William Rose Group",
                        "message": "Heyhowru?...",
                        "image": "user 4.png",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Alfred Waney Group",
                        "message": "Content here, content here...",
                        "image": "video_round.png",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Luke Thomas Group",
                        "message": "Where can I get some...",
                        "image": "user.png",
                        "time": "2 Feb 2018",
                        "count": ""],
                       ["name": "Rose Merry Group",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "5 Jan 2018",
                        "count": ""]]
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "GROUP CHATS", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "GROUP CHATS", leftbuttonImageName: "back.png")
        }
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]

    }

    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }
    
    // MARK : UITableViewDelegate
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMessages.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "RecentsCell"
        var cell : RecentsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RecentsCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("RecentsCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? RecentsCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        }
        cell?.lblCount.layer.cornerRadius = (cell?.lblCount.frame.size.height)!/2
        cell?.lblCount.layer.masksToBounds = true
        cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
        cell?.imgViewUser.layer.masksToBounds = true
        cell?.lblMessage.sizeToFit()
        
        cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
        cell?.lblName.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! String
        cell?.lblMessage.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "message") as! String
        cell?.lblTime.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "time") as! String
        cell?.lblCount.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "count") as! String
        //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
        //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
        
        if ((arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "count") as! NSString).integerValue > 0
        {
            cell?.lblCount.isHidden = false
        }
        else
        {
            cell?.lblCount.isHidden = true
        }
        cell?.imgChatType.isHidden = true
        cell?.imgStatus.isHidden = true
        cell?.imgChatType.image = UIImage(named: "groupIcon")
        cell?.imgChatType.isHidden = false
        if (indexPath.row) == 5
        {
            cell?.imgStatus.image = UIImage(named: "read")
            cell?.imgStatus.isHidden = false
        }
        else if (indexPath.row) == 6
        {
            cell?.imgStatus.image = UIImage(named: "unread")
            cell?.imgStatus.isHidden = false
        }
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let chat = ChatScreen.init(nibName: "ChatScreen", bundle: nil)
        chat.isGroupChat = true
        self.navigationController?.pushViewController(chat, animated: true)
    }
     /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
