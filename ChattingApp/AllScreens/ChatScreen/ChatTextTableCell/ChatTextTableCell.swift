//
//  ChatTextTableCell.swift
//  ChattingApp
//
//  Created by ramprakash on 11/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ChatTextTableCell: UITableViewCell {

    @IBOutlet weak var viewMe: UIView!
    @IBOutlet weak var imgMe: UIImageView!
    @IBOutlet weak var imgBGMe: UIImageView!
    @IBOutlet weak var lblMe: UILabel!
    @IBOutlet weak var btnMoreMe: UIButton!
    @IBOutlet weak var lblTimeMe: UILabel!

    @IBOutlet weak var viewOther: UIView!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var imgBGOther: UIImageView!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var btnMoreOther: UIButton!
    @IBOutlet weak var imgMsgStatus: UIImageView!
    @IBOutlet weak var lblTimeOther: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let imgOther = UIImage.init(named: "sender")
        let imageOther =  imgOther?.resizableImage(withCapInsets: UIEdgeInsets(top: 25, left: 16, bottom: 11, right: 11), resizingMode: .stretch)
        self.imgBGOther.image = imageOther;

        let imgMe = UIImage.init(named: "reciever")
        let imageMe =  imgMe?.resizableImage(withCapInsets: UIEdgeInsets(top: 25, left: 11, bottom: 11, right: 16), resizingMode: .stretch)
        self.imgBGMe.image = imageMe;

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
