//
//  ChatScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ChatScreen: UIViewController , UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate, UITextFieldDelegate,HPGrowingTextViewDelegate {
    
    @IBOutlet weak var tblMessages:  UITableView!
    @IBOutlet weak var viewNav:  UIView!
    var arrMessages = NSMutableArray()

    @IBOutlet var btnMore: UIButton!

    @IBOutlet var viewMenu: UIView!
    @IBOutlet weak var viewInnerMenu: UIView!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet var viewChoosePlaylist: UIView!
    @IBOutlet weak var viewInnerPlaylist: ShadowView!
    @IBOutlet weak var tblChoosePlaylist: UITableView!
    @IBOutlet weak var viewBottomTxtField:  UIView!
    
    @IBOutlet var textView: HPGrowingTextView!
    @IBOutlet var btnSend: UIButton!
    @IBOutlet var btnAudioAttach: UIButton!
    @IBOutlet var btnAttach: UIButton!
    @IBOutlet var btnSmiley: UIButton!
    @IBOutlet var viewAttach: UIView!
    var isAttachmentShown : Bool = false


    @IBOutlet var viewChatHearder1: UIView!
    @IBOutlet var viewChatHearder2: UIView!
    @IBOutlet var lblChatHearder1: UIView!
    @IBOutlet var lblChatHearder2: UIView!

     var isGroupChat: Bool = false
    @IBOutlet var toolBar: UIToolbar!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMessages.tableFooterView = UIView.init(frame: CGRect.zero)
        tblMenu.tableFooterView = UIView.init(frame: CGRect.zero)
        tblChoosePlaylist.tableFooterView = UIView.init(frame: CGRect.zero)
   
        lblChatHearder1.layer.cornerRadius = (lblChatHearder1.frame.size.height)/2
        lblChatHearder1.layer.masksToBounds = true
        lblChatHearder2.layer.cornerRadius = (lblChatHearder2.frame.size.height)/2
        lblChatHearder2.layer.masksToBounds = true
        
        self.performSelector(onMainThread: #selector(loadView1), with: nil, waitUntilDone: false)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(resignTextView))
        tap.delegate = self
        tblMessages.addGestureRecognizer(tap)
        
        self.view.addSubview(self.viewAttach)
        var frame = self.viewAttach.frame
        frame.origin.y = ScreenSize.SCREEN_HEIGHT
        self.viewAttach.frame = frame
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationItem.titleView = viewNav
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
   }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tblMenu))! == true || (touch.view?.isDescendant(of: tblChoosePlaylist))! == true
        {
            return false
        }
        return true
    }

    @objc func methodChatMoreClicked(_ sender: UIButton)
    {
        viewMenu.removeFromSuperview()
        viewChoosePlaylist.removeFromSuperview()
        
        self.view.addSubview(viewChoosePlaylist)
        viewChoosePlaylist.frame = self.view.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewChoosePlaylist.addGestureRecognizer(tap)
    }

    @IBAction func methodMoreClicked(_ sender: UIButton)
    {
        viewMenu.removeFromSuperview()
        viewChoosePlaylist.removeFromSuperview()

        appDelegate.window?.addSubview(viewMenu)
        viewMenu.frame = self.view.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewMenu.addGestureRecognizer(tap)

        let std = appDelegate.window!.convert(self.btnMore.frame, from: self.navigationItem.titleView)
        var frame = viewInnerMenu.frame
        frame.origin.y = std.origin.y + std.size.height
        viewInnerMenu.frame = frame
    }

    @IBAction func methodBackClicked(_ sender: UIButton)
    {
        resignTextView()
        self.navigationController?.popViewController(animated: true)
        //   self.menuContainerViewController.toggleLeftSideMenuCompletion {
        
        //    }
    }
    
    @IBAction func methodDetailClicked(_ sender: UIButton)
    {
        if self.isGroupChat == true
        {
            let register = OtherGroupProfileViewController.init(nibName: "OtherGroupProfileViewController", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
        }
        else
        {
            let register = UserChatDetailScreen.init(nibName: "UserChatDetailScreen", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
        }
    }
    @IBAction func methodAttachClicked(_ sender: UIButton)    
    {
        self.showHideAttachmentView(shouldShow:!self.isAttachmentShown)
   }
    @IBAction func methodSmileClicked(_ sender: UIButton)    
    {
        
    }
    @IBAction func methodMiceClicked(_ sender: UIButton)    
    {
        
    }
    @IBAction func methodSendClicked(_ sender: UIButton)    
    {
        
    }
    @IBAction func methodAudioCall(_ sender : UIButton)
    {
        resignTextView()
        let register = VideoCallScreen.init(nibName: "VideoCallScreen", bundle: nil)
//        register.callStatus = .outgoing  TEMPORARY FOR NAVIGATION
        register.callStatus = .incoming
        register.isVideo = false
       self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodVideoCall(_ sender : UIButton)
    {
        resignTextView()
        let register = VideoCallScreen.init(nibName: "VideoCallScreen", bundle: nil)
//        register.callStatus = .outgoing  TEMPORARY FOR NAVIGATION
        register.callStatus = .incoming
        register.isVideo = true
        self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodCreatePlaylistCall(_ sender : UIButton) 
    {
        let register = CreatePlaylistScreen.init(nibName: "CreatePlaylistScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }   
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        resignTextView()
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        resignTextView()
    }

    @IBAction func methodAllAttachmentsClicked(_ sender : UIButton)    
    {
        switch(sender.tag)
        {
        case 0:
//            let register = PlaylistSongsScreen.init(nibName: "PlaylistSongsScreen", bundle: nil)
//            self.navigationController?.pushViewController(register, animated: true)
            break
        case 1:
            let register = galleryScreen.init(nibName: "galleryScreen", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
            break
        case 2:
            let register = PlaylistSongsScreen.init(nibName: "PlaylistSongsScreen", bundle: nil)
            register.isCameFrom = "Chat"
            self.navigationController?.pushViewController(register, animated: true)
            break
        case 3:
            let register = documentScreen.init(nibName: "documentScreen", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
            break
        case 4:
            let register = galleryDetailScreen.init(nibName: "galleryDetailScreen", bundle: nil)
            register.isCameFrom = "Chat"
            self.navigationController?.pushViewController(register, animated: true)
            break
        case 5:
            let register = ChooseContactScreen.init(nibName: "ChooseContactScreen", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
            break
        case 6:
            let register = locationScreen.init(nibName: "locationScreen", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
            break
        default:
            break
        }
        if self.isAttachmentShown == true
        {
            showHideAttachmentView(shouldShow: false)
        }
    }
    
    func showHideAttachmentView(shouldShow: Bool)
    {
        // get keyboard size and loctaion
        //    if (uploadView.hidden)
        //    {
        textView.resignFirstResponder()
        
        var containerFrame : CGRect = self.viewBottomTxtField.frame
        var attachFrame : CGRect = self.viewAttach.frame
        
        let viewkeyboardSizeheight: CGFloat = (shouldShow == true) ? (attachFrame.size.height + containerFrame.size.height) : 0.0

        containerFrame.origin.y = self.appDelegate.window!.frame.size.height - containerFrame.size.height
        attachFrame.origin.y = self.appDelegate.window!.frame.size.height - viewkeyboardSizeheight

        UIView.animate(withDuration: 0.3, animations: { 
            
            self.viewBottomTxtField.frame = containerFrame
            self.viewAttach.frame = attachFrame

                
            self.tblMessages.frame = CGRect(x: 0.0, y: self.tblMessages.frame.origin.y , width: self.appDelegate.window!.frame.size.width, height: self.appDelegate.window!.frame.size.height -  viewkeyboardSizeheight - self.tblMessages.frame.origin.y)
                
                self.performSelector(onMainThread: #selector(self.moveContentViewToEnd), with: nil, waitUntilDone: false)
        }) { (animated) in
            self.viewBottomTxtField.frame = containerFrame
            self.viewAttach.frame = attachFrame
            self.isAttachmentShown = shouldShow
            DispatchQueue.main.async{
                self.tblMessages.reloadData()
            }
        }
        
//        let info : NSDictionary = note.userInfo! as NSDictionary
//        
//        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
//        //        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
//        let duration = (note.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber)
//        let curve = (note.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber)
//        
//        containerFrame = viewBottomTxtField.frame
//        containerFrame.origin.y = appDelegate.window!.frame.size.height - (keyboardSize.height + containerFrame.size.height)
//        viewBottomTxtField.frame = containerFrame
//        
//        tblMessages.frame = CGRect(x: 0.0, y: 44 , width: appDelegate.window!.frame.size.width, height: appDelegate.window!.frame.size.height - containerFrame.size.height - keyboardSize.height - 44  )
//        
//        self.performSelector(onMainThread: #selector(moveContentViewToEnd), with: nil, waitUntilDone: false)
//        
//        // animations settings
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(CDouble(truncating: duration))
//        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: Int(CInt(truncating: curve)))!)
//        // set views with new info
//        viewBottomTxtField.frame = containerFrame
//        //        tblMessages.frame = CGRect(x: 0.0, y: 64.0, width: appDelegate.window!.frame.size.width, height: containerFrame.origin.y-64)
//        // commit animations
//        UIView.commitAnimations()
//        DispatchQueue.main.async{
//            self.tblMessages.reloadData()
//        }
    }
    // MARK : UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        switch tableView.tag
        {
        case 0:
            switch section
            {
            case 0:
                return 60
            case 1:
                return 60
            default:
                return 0
            }
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        switch tableView.tag
        {
        case 0:
            switch section
            {
            case 0:
                return viewChatHearder1
            case 1:
                return viewChatHearder2
            default:
                return nil
            }
        default:
            return nil
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        switch tableView.tag
        {
        case 0:
            return 4
        default:
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch tableView.tag
        {
        case 0:
            switch section
            {
            case 0:
                return 8 
            case 1:
                return 4
            case 2:
                return 2
            case 3:
                return 2
                
            default:
                return 0
            }
        case 1:
            return 6
        case 2:
            return 2
            
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch tableView.tag
        {
        case 0:
            switch indexPath.section
            {
            case 0:
                return 120 
            case 1:
                return 220
            case 2:
                return 60
            case 3:
                return 60
                
            default:
                return 0
            }
        case 2:
            return 54
        default:
            return 30
        }

    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch tableView.tag
        {
        case 0:
            
            switch indexPath.section
            {
            case 0:
                let cellIdentifier:String = "ChatTextTableCell"
                var cell : ChatTextTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ChatTextTableCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("ChatTextTableCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? ChatTextTableCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
               
                //            cell?.lblCount.layer.cornerRadius = (cell?.lblCount.frame.size.height)!/2
                //            cell?.lblCount.layer.masksToBounds = true
                //            cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
                //            cell?.imgViewUser.layer.masksToBounds = true
                //            
                //            cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
                //            cell?.lblName.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! String
                //            cell?.lblMessage.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "message") as! String 
                //            cell?.lblTime.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "time") as! String
                //            cell?.lblCount.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "count") as! String
                //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
                //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
                
                
                cell?.btnMoreMe.addTarget(self, action: #selector(methodChatMoreClicked(_:)), for: .touchUpInside)
                cell?.btnMoreOther.addTarget(self, action: #selector(methodChatMoreClicked(_:)), for: .touchUpInside)
                cell?.viewMe.isHidden = true
                cell?.viewOther.isHidden = true
                if (indexPath.row) % 2 == 0
                {
                    cell?.viewMe.isHidden = false
                    
                    if (indexPath.row) == 0
                    {
                        cell?.imgMsgStatus.image = UIImage(named: "send.png")
                    }
                    else if (indexPath.row) == 2
                    {
                        cell?.imgMsgStatus.image = UIImage(named: "read.png")
                    }
                    else
                    {
                        cell?.imgMsgStatus.image = UIImage(named: "unread.png")
                    }
                }
                else
                {
                    cell?.viewOther.isHidden = false
                }
                return cell!
          
            case 2:
                let cellIdentifier:String = "CallChatTableCell"
                var cell : CallChatTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CallChatTableCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("CallChatTableCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? CallChatTableCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                } 
                cell?.viewMe.isHidden = true
                cell?.viewOther.isHidden = true
                if (indexPath.row) == 0
                {
                    cell?.viewMe.isHidden = false
                }
                else
                {
                    cell?.viewOther.isHidden = false
                }
                return cell!
            case 3:
                let cellIdentifier:String = "TypingTableCell"
                var cell : TypingTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TypingTableCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("TypingTableCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? TypingTableCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                } 
                cell?.viewMe.isHidden = true
                cell?.viewOther.isHidden = true
                if (indexPath.row) == 0
                {
                    cell?.viewMe.isHidden = false
                }
                else
                {
                    cell?.viewOther.isHidden = false
                }
                return cell!
        
            default:
                let cellIdentifier:String = "ImageVideoTextTableCell"
                var cell : ImageVideoTextTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ImageVideoTextTableCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("ImageVideoTextTableCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? ImageVideoTextTableCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                //            cell?.lblCount.layer.cornerRadius = (cell?.lblCount.frame.size.height)!/2
                //            cell?.lblCount.layer.masksToBounds = true
                //            cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
                //            cell?.imgViewUser.layer.masksToBounds = true
                //            
                //            cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
                //            cell?.lblName.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! String
                //            cell?.lblMessage.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "message") as! String 
                //            cell?.lblTime.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "time") as! String
                //            cell?.lblCount.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "count") as! String
                //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
                //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
                
                
                cell?.btnMoreMe.addTarget(self, action: #selector(methodChatMoreClicked(_:)), for: .touchUpInside)
                cell?.btnMoreOther.addTarget(self, action: #selector(methodChatMoreClicked(_:)), for: .touchUpInside)
                cell?.viewMe.isHidden = true
                cell?.viewOther.isHidden = true
                if (indexPath.row) % 2 == 0
                {
                    cell?.viewMe.isHidden = false
                    if (indexPath.row) == 2
                    {
                        cell?.imgPlayMe.isHidden = true
                        cell?.imgMsgStatus.image = UIImage(named: "read.png")
                    }
                    else
                    {
                        if (indexPath.row) == 0
                        {
                            cell?.imgMsgStatus.image = UIImage(named: "send.png")
                        }
                        else
                        {
                            cell?.imgMsgStatus.image = UIImage(named: "unread.png")
                        }

                        cell?.imgPlayMe.isHidden = false
                    }
                }
                else
                {
                    cell?.viewOther.isHidden = false
                    if (indexPath.row) == 1
                    {
                        cell?.imgPlayOther.isHidden = true
                    }
                    else
                    {
                        cell?.imgPlayMe.isHidden = false
                    }
                }
                return cell!
            }//TypingTableCell
            
        case 1:
            let cellIdentifier:String = "SongsMenuTableCell"
            var cell : SongsMenuTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SongsMenuTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("SongsMenuTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? SongsMenuTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
            switch indexPath.row
            {
            case 0:
                cell?.lblName.text = "Translate Chat"
            case 1:
                cell?.lblName.text = "Block"
            case 2:
                cell?.lblName.text = "Search"
            case 3:
                cell?.lblName.text = "Clear History"
            case 4:
                cell?.lblName.text = "Delete Chat"
            case 5:
                cell?.lblName.text = "Mute Notifications"
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            default:
                cell?.lblName.text = ""
            }
            
            return cell!
            
        default:
            let cellIdentifier:String = "PlaylistsTableViewCell"
            var cell : PlaylistsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PlaylistsTableViewCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("PlaylistsTableViewCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? PlaylistsTableViewCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            cell?.imgLogo.image = UIImage(named: "folder")
            cell?.lblPlaylistName.text = indexPath.row == 0 ? "Maddy Songs" : "Chat Songs"
            cell?.lblNoOfSongs.text = "" 
            cell?.lblNoOfSongs.isHidden = true 
            
            if (indexPath.row == 1)
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            }
            else
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }

            return cell!
        }       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch tableView.tag
        {
        case 0:
            break
        case 1:
            viewMenu.removeFromSuperview()
            break
        case 2:
            viewChoosePlaylist.removeFromSuperview()
            let register = PlaylistSongsScreen.init(nibName: "PlaylistSongsScreen", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)

            break
        default:
            break
        }
        
    }
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }

    //MARK:- Show/HideKeyboard
    // MARK: - KeyBoard Notification Handlers -
    
    @objc func keyboardWillShow(_ note: NSNotification)
    {
        // get keyboard size and loctaion
        //    if (uploadView.hidden)
        //    {
       
        var containerFrame : CGRect
        let info : NSDictionary = note.userInfo! as NSDictionary
        
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        //        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let duration = (note.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber)
        let curve = (note.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber)
        
        containerFrame = viewBottomTxtField.frame
        containerFrame.origin.y = appDelegate.window!.frame.size.height - (keyboardSize.height + containerFrame.size.height)
        viewBottomTxtField.frame = containerFrame
        
        tblMessages.frame = CGRect(x: 0.0, y: self.tblMessages.frame.origin.y , width: appDelegate.window!.frame.size.width, height: appDelegate.window!.frame.size.height - containerFrame.size.height - keyboardSize.height - self.tblMessages.frame.origin.y  )
        
        self.performSelector(onMainThread: #selector(moveContentViewToEnd), with: nil, waitUntilDone: false)
       

        // animations settings
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(CDouble(truncating: duration))
        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: Int(CInt(truncating: curve)))!)
        // set views with new info
        viewBottomTxtField.frame = containerFrame
        
        var frame = self.viewAttach.frame
        frame.origin.y = ScreenSize.SCREEN_HEIGHT
        self.viewAttach.frame = frame
        //        tblMessages.frame = CGRect(x: 0.0, y: 64.0, width: appDelegate.window!.frame.size.width, height: containerFrame.origin.y-64)
        // commit animations
        UIView.commitAnimations()
        DispatchQueue.main.async{
            self.tblMessages.reloadData()
        }
    }
    
    @objc func keyboardWillHide(_ note: NSNotification)
    {
        // get keyboard size and loctaion
        //    if (uploadView.hidden)
        //    {
        
        var containerFrame : CGRect
        let info : NSDictionary = note.userInfo! as NSDictionary
        
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        //        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let duration = (note.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber)
        let curve = (note.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber)
        
        containerFrame = viewBottomTxtField.frame
        
        containerFrame.origin.y = appDelegate.window!.frame.size.height - containerFrame.size.height
        
        //        tblMessages.frame = CGRect(x: 0.0, y: 64.0, width: appDelegate.window!.frame.size.width, height: appDelegate.window!.frame.size.height - containerFrame.size.height-64)
        tblMessages.frame = CGRect(x: 0.0, y: self.tblMessages.frame.origin.y , width: appDelegate.window!.frame.size.width, height: containerFrame.origin.y - self.tblMessages.frame.origin.y  )
        
        
        // animations settings
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(CDouble(truncating: duration))
        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: Int(CInt(truncating: curve)))!)
        // set views with new info
        viewBottomTxtField.frame = containerFrame
        // commit animations
        UIView.commitAnimations()
//        tblMessages.typingBubble = NSBubbleTypingTypeNobody
//        sendMessageTypingMesssage(kTYPE_Typing: kTYPE_TSTOP)
        DispatchQueue.main.async{
            self.tblMessages.reloadData()
        }
    }
    
    
    // MARK: - GrowingTextView Delegates -
    
    func growingTextView(_ growingTextView: HPGrowingTextView!, willChangeHeight height: Float)
    {
        let diff: CGFloat = CGFloat(growingTextView.frame.size.height) - CGFloat(height)
        var r: CGRect = viewBottomTxtField.frame
        r.size.height = r.size.height - diff
        r.origin.y = r.origin.y + diff
        viewBottomTxtField.frame = r
        tblMessages.frame = CGRect(x: 0.0, y: self.tblMessages.frame.origin.y , width: appDelegate.window!.frame.size.width, height: viewBottomTxtField.frame.origin.y - self.tblMessages.frame.origin.y  )
        
    }
    
    func growingTextViewDidChange(_ growingTextView: HPGrowingTextView)
    {
        let trimmedString: String = textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if trimmedString.count > 0 {
            //            tblMessages.typingBubble = NSBubbleTypingTypeMe
//            sendMessageTypingMesssage(kTYPE_Typing: kTYPE_TSTART)
            btnSend.isHidden = false
            btnAttach.isHidden = true
            btnAudioAttach.isHidden = true
       }
        else {
            //            tblMessages.typingBubble = NSBubbleTypingTypeNobody
//            sendMessageTypingMesssage(kTYPE_Typing: kTYPE_TSTOP)
            self.automaticallyAdjustsScrollViewInsets = false
            btnSend.isHidden = true
            btnAttach.isHidden = false
            btnAudioAttach.isHidden = false
            textView.internalTextView.contentSize = CGSize(width: textView.frame.size.width, height: textView.frame.size.height)
            textView.internalTextView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        }
        //        DispatchQueue.main.async{
        //            self.tblMessages.reloadData()
        //        }  
    }
    //: MARK - loadView Methods
    
    @objc func loadView1()
    {
        let font: UIFont = UIFont(name: "Lato-Regular", size: CGFloat(16))!
        textView.textColor = .darkGray
        textView.placeholderColor = .gray
        textView.backgroundColor = .clear
        textView.internalTextView.font = font
        textView.placeholder = "Start Typing"
        textView.minNumberOfLines = 1
        textView.maxNumberOfLines = 4
        textView.returnKeyType = UIReturnKeyType.default
        textView.font = font
        textView.delegate = self
        textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 7, 5, 7)
        textView.layer.masksToBounds = true
        textView.animateHeightChange = true
        textView.internalTextView.inputAccessoryView = toolBar

        btnSend.isHidden = true
        btnAttach.isHidden = false
        btnAudioAttach.isHidden = false
        //        if (self.currentContact["is_block"] as! NSNumber == 1)
        //        {
        //            if (self.currentContact["is_block_byme"] as! NSNumber == 1)
        //            {
        //                self.textView.placeholder = "You have blocked this user!"
        //                self.textView.isUserInteractionEnabled = false
        //                return
        //            }
        //            else
        //            {
        //                self.textView.placeholder = "You have been blocked by this user!"
        //                self.textView.isUserInteractionEnabled = false
        //                return
        //            }
        //        }
    }
    
    // MARK: - moveContentToEnd -
    
    @objc func moveContentViewToEnd()
    {
        DispatchQueue.main.async {
            if (self.tblMessages.numberOfSections == 0 || self.tblMessages.numberOfRows(inSection: self.tblMessages.numberOfSections - 1) == 0)
            {
                return
            }
            if self.tblMessages.contentSize.height > self.tblMessages.frame.size.height
            {
                //            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                let numberOfSections = self.tblMessages.numberOfSections
                let numberOfRows = self.tblMessages.numberOfRows(inSection: numberOfSections-1)
                
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.tblMessages.scrollToRow(at: indexPath, at: .none, animated: false)
                    
                    // let contentYoffset: CGFloat = self.tblMessages.contentOffset.y
                    
                    // self.tblMessages.setContentOffset(CGPoint(x: CGFloat(self.tblMessages.contentOffset.x), y: CGFloat(contentYoffset + 20)), animated: false)
                    
                }
                
                //            }
            }
            
        }
    }
    
    func showTextView()
    {
        textView.becomeFirstResponder()
    }
    
    @objc func resignTextView()
    {
//        sendMessageTypingMesssage(kTYPE_Typing: kTYPE_TSTOP)
        textView.resignFirstResponder()
        if self.isAttachmentShown == true
        {
            showHideAttachmentView(shouldShow: false)
        }
        
//        var containerFrame : CGRect
//        containerFrame = viewBottomTxtField.frame
//        containerFrame.origin.y = appDelegate.window!.frame.size.height - containerFrame.size.height
//        viewBottomTxtField.frame = containerFrame
//        tblMessages.frame = CGRect(x: 0.0, y: self.tblMessages.frame.origin.y , width: appDelegate.window!.frame.size.width, height: viewBottomTxtField.frame.origin.y - self.tblMessages.frame.origin.y)
        //        self.performSelector(onMainThread: #selector(moveContentViewToEnd), with: nil, waitUntilDone: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
