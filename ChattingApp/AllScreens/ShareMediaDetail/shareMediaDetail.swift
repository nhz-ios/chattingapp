//
//  shareMediaDetail.swift
//  ChattingApp
//
//  Created by Parikshit on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class shareMediaDetail: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UIGestureRecognizerDelegate {
    
    @IBOutlet var clShareDetail: UICollectionView!
    
    var arrImages = NSArray()
    
    
    @IBOutlet var viewPopUp: UIView!
    @IBOutlet var btnShowChat: UIButton!
    @IBOutlet var btnSaaveGallery: UIButton!
    @IBOutlet var btnDelete: UIButton!
    var viewPopUpShow: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        arrImages = ["5.png","4.png","3.png"]
        
        let nib = UINib(nibName: "cellClShare", bundle: nil)
       clShareDetail.register(nib, forCellWithReuseIdentifier: "cellClShare")
       clShareDetail.delegate = self
       clShareDetail.dataSource = self
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.navigationController?.isNavigationBarHidden = false
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = .clear

        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.tintColor = UIColor.white

        let arrcount = arrImages.count
        self.title = NSString.init(format: "1 of %d",arrcount) as String
        
        self.navigationBarWithBackButton(strTitle: "1 of 3", leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"forword.png"), style: .plain, target: self, action: #selector(methodShare(_sender:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"three_dots.png"), style: .plain, target: self, action: #selector(methodMore(_sender:)))
        self.navigationItem.rightBarButtonItems = [menuButton2, menuButton1]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
//        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white

        self.viewPopUp.removeFromSuperview()
        viewPopUpShow = false
    }
    //Mark : MEthod Button Action
    @IBAction func methodShare(_sender: UIButton)
    {
        viewPopUp.removeFromSuperview()
    }
    
    @IBAction func methodMore(_sender: UIButton)
    {
        if (viewPopUpShow == true)
        {
            self.viewPopUp.removeFromSuperview()
        }
        else
        {
            self.viewPopUp.frame = CGRect(x: ScreenSize.SCREEN_WIDTH - 155, y:0 , width: 150, height: 105)
            self.viewPopUp.layer.cornerRadius = 5
            appDelegate.window!.addSubview(self.viewPopUp)
            
            let std = appDelegate.window!.convert((self.navigationController?.navigationBar.frame)!, from: self.navigationItem.titleView)
            var frame =  self.viewPopUp.frame
            frame.origin.y = std.origin.y + std.size.height - 10
            self.viewPopUp.frame = frame
        }
        viewPopUpShow = !viewPopUpShow
    }
    
    @IBAction func methodPopUp(_sender: UIButton)
    {
        viewPopUp.removeFromSuperview()
    }
    
    
    //Mark : Uicollection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellClShare", for: indexPath) as! cellClShare
        
        cell.backgroundColor = UIColor.clear
        cell.imgMedia.image = UIImage.init(named: "\(arrImages.object(at: indexPath.item))")
        
        cell.btnPlay.isHidden = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.width)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = clShareDetail.contentOffset
        visibleRect.size = clShareDetail.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = clShareDetail.indexPathForItem(at: visiblePoint) else { return }
        print(indexPath.row)
        
        let arrcount = arrImages.count
        self.title = NSString.init(format: "%d of %d",indexPath.row + 1,arrcount) as String
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
