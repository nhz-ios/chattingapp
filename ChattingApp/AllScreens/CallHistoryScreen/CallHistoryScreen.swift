//
//  CallHistoryScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class CallHistoryScreen: UIViewController  , UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblHistory:  UITableView!
    var arrMessages = NSMutableArray()
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblHistory.tableFooterView = UIView.init(frame: CGRect.zero)
        arrMessages = [["name": "Prisa Den",
                        "message": "Lorem Ipsum is simply dummy text...",
                        "image": "user.png",
                        "time": "6:00 PM",
                        "count": "3"], 
                       ["name": "Lisa Group",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "10 Apr 2018, 4:30 PM",
                        "count": ""],
                       ["name": "George Camp",
                        "message": "Where does it come from?...",
                        "image": "user.png",
                        "time": "15 Mar 2018, ",
                        "count": ""],
                       ["name": "William Rose",
                        "message": "Heyhowru?...",
                        "image": "user 4.png",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Alfred Waney",
                        "message": "Content here, content here...",
                        "image": "video_round.png",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Luke Thomas",
                        "message": "Where can I get some...",
                        "image": "user.png",
                        "time": "2 Feb 2018",
                        "count": ""],
                       ["name": "Rose Merry",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "5 Jan 2018",
                        "count": ""]]
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "CALL HISTORY", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "CALL HISTORY", leftbuttonImageName: "back.png")
        }

        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"add_call.png"), style: .plain, target: self, action: #selector(methodAddCallClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]
        
    }
    @objc func methodAddCallClicked(_ sender: UIButton)
    {
        let register = ChooseContactScreen.init(nibName: "ChooseContactScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMessages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "CallHistoryTableCell"
        var cell : CallHistoryTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CallHistoryTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("CallHistoryTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? CallHistoryTableCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        }
        cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
        cell?.imgViewUser.layer.masksToBounds = true
        cell?.lblMessage.sizeToFit()
        cell?.lblMessage2.sizeToFit()
    
        cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
        cell?.lblName.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! String
        cell?.lblMessage.text = (arrMessages.object(at: indexPath.row) as! NSDictionary).object(forKey: "time") as! String 
        //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
        //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
        
        cell?.imgStatus.isHidden = false
        cell?.lblMessage.isHidden = false
        cell?.lblMessage2.isHidden = true
        cell?.btnAudio.isHidden = true
        cell?.btnVideo.isHidden = false

        if (indexPath.row) % 2 == 0
        {
            cell?.btnVideo.setImage(UIImage(named: "blue_video"), for: .normal)
        }
        else
        {
            cell?.btnVideo.setImage(UIImage(named: "blue_call"), for: .normal)
        }
        cell?.btnVideo.isHidden = false

        if (indexPath.row) == 1
        {
            cell?.imgStatus.image = UIImage(named: "down_green")
        }
        else if (indexPath.row) == 3        {
            cell?.imgStatus.image = UIImage(named: "red_down")
        }
        else if (indexPath.row) == 5
        {
            cell?.imgStatus.image = UIImage(named: "red_down")
        }
        else
        {
            cell?.imgStatus.image = UIImage(named: "green_up")
        }
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
