//
//  MainSettingsScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 09/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MainSettingsScreen: UIViewController, UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    @IBOutlet weak var tblSettings:  UITableView!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var txtName : UITextField!
    @IBOutlet var txtNumber : UITextField!
    @IBOutlet var imgPhoto : UIImageView!

    var userImageData : Data!
    var arrSettings = NSMutableArray()
    var selectedTheme = 0
    var strFrom = ""
    var strCurrentLanguage = "English"
    var strCurrentTheme = "Dark"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSettings.tableFooterView = UIView.init(frame: CGRect.zero)
        
        NotificationCenter.default.addObserver(self, selector: #selector(methodNotificationHandler(_:)), name: NSNotification.Name.init(rawValue: "LanguageChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(methodNotificationHandler(_:)), name: NSNotification.Name.init(rawValue: "ThemeChanged"), object: nil)

        arrSettings = [["cat_name": "Accounts",
                        "sub_cat_name": ["Change Password","Private Account","Search History","Data and Storage","Theme","Language","Chat Background"]],
                       ["cat_name": "Messages",
                        "sub_cat_name": ["In-App Browser", "Direct Share", "Stickers", "Message Text Size", "Raise to Speak", "Send by Enter","Autoplay GIFs","Save to gallery"]],
                       ["cat_name": "Privacy and Security",
                        "sub_cat_name": ["Account Privacy","Blocked Accounts", "Activity Status", "Privacy and Security Help"]],
                       ["cat_name": "Notifications",
                        "sub_cat_name": ["Push Notifications and Sound", "Email and SMS Notifications"]],
                       ["cat_name": "Support",
                        "sub_cat_name": ["Help Center", "Report a Problem"]]]
        
        imgPhoto.layer.cornerRadius =  imgPhoto.frame.size.width / 2
        imgPhoto.contentMode = .scaleAspectFill
        imgPhoto.layer.masksToBounds = true
        
        SetdefaultData()
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileUpdateNotification), name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateProfile), object: nil)
    

    }
    func SetdefaultData()
    {
        userImageData = nil
        
        txtName.text = appDelegate.currentLoginUser.strName
        txtName.isUserInteractionEnabled = false
        txtNumber.isUserInteractionEnabled = false
        txtNumber.text = appDelegate.currentLoginUser.strCountryCode + "-" + appDelegate.currentLoginUser.strphone
        if appDelegate.currentLoginUser.strProfilepicImage != ""
        {
            // \(Image_URL)
            imgPhoto.sd_addActivityIndicator()
            imgPhoto.sd_showActivityIndicatorView()
            let url = URL.init(string: appDelegate.currentLoginUser.strProfilepicImage)
            imgPhoto.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                self.imgPhoto.sd_removeActivityIndicator()
            })
        }
        else
        {
            imgPhoto.image = UIImage(named:"reg_USER")
        }
        
        imgPhoto.layer.cornerRadius =  imgPhoto.frame.size.width / 2
        imgPhoto.contentMode = .scaleAspectFill
        imgPhoto.layer.masksToBounds = true
    }
    @objc func ProfileUpdateNotification(data : NSNotification)
    {
        SetdefaultData()
        tblSettings.reloadData()
    }
    @objc func methodNotificationHandler(_ sender: Notification)
    {
        if (sender.name.rawValue == "LanguageChanged")
        {
            strCurrentLanguage = sender.object as! String
        }
        else if (sender.name.rawValue == "ThemeChanged")
        {
            strCurrentTheme = sender.object as! String
        }
        tblSettings.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "SETTINGS", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "SETTINGS", leftbuttonImageName: "back.png")
        }
    }
    
    @IBAction func methodSubmitButton(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ((arrSettings.object(at: section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).count + 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 2 && (indexPath.row - 1) == 2
        {
            return 70
        }
        return 44
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0
        {
            return viewHeader
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 140
        }
        return 0
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 2 && (indexPath.row - 1) == 2
        {
            let indentifier:String = "OtherUsrStatusTableCell"
            var cell : OtherUsrStatusTableCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? OtherUsrStatusTableCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OtherUsrStatusTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OtherUsrStatusTableCell
            }
             cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.lblStatus.text = "Activity Status"
            cell?.lblStatusValue.text = "Last activated at 25 Jun, 05:30 PM"
            cell?.btntoggleImage.isHidden = false
             cell?.lblBottomLine.isHidden = true
            cell?.lblStatus.font = UIFont.init(name: "Lato-Regular", size: 14.0)
            cell?.lblStatusValue.font = UIFont.init(name: "Lato-Regular", size: 12.0)

            cell?.lblStatus.textColor = UIColor.init(red: 75.0/255.0, green: 76.0/255.0, blue: 77.0/255.0, alpha: 1)
            cell?.lblStatusValue.textColor = UIColor.init(red: 75.0/255.0, green: 76.0/255.0, blue: 77.0/255.0, alpha: 1)
            cell?.btntoggleImage.addTarget(self, action: #selector(toggleButtonAction(_:)), for: .touchUpInside)
            return cell!
        }

        let indentifier:String = "cellOtherGroupTableViewCell"
        var cell : cellOtherGroupTableViewCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? cellOtherGroupTableViewCell
        
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("cellOtherGroupTableViewCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? cellOtherGroupTableViewCell
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.backgroundColor = (UIColor.clear)
        cell!.btntoggleImage.setTitle("", for: .normal)
        cell!.btntoggleImage.setImage(nil, for: .normal)
        cell!.btntoggleImage.setImage(nil, for: .normal)
        cell!.btntoggleImage.isHidden = false
        cell?.lblBottomLine.isHidden = true
        cell?.lblTitle.font = UIFont.init(name: "Lato-Regular", size: 14.0)
        
        if (indexPath.row == 0)
        {
            cell!.btntoggleImage.isHidden = true
            cell?.lblTitle.text = "\((arrSettings.object(at: indexPath.section) as! NSDictionary).object(forKey: "cat_name")!)"
            cell?.lblTitle.font = UIFont.init(name: "Lato-Bold", size: 16.0)
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
        }
        else
        {
            cell?.lblTitle.text = "\(((arrSettings.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).object(at: indexPath.row - 1))"
            
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            
            switch (indexPath.section)
            {
            case 0:
                switch (indexPath.row - 1)
                {
                case 0:
                    break
                case 1:
                    cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_off"), for: .normal)
                    cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_on"), for: .selected)
                    cell?.btntoggleImage.addTarget(self, action: #selector(toggleButtonAction(_:)), for: .touchUpInside)
                    break
                case 4:
                    cell!.btntoggleImage.setTitle(strCurrentTheme, for: .normal)
                    break
                case 5:
                    cell!.btntoggleImage.setTitle(strCurrentLanguage, for: .normal)
                    break
                case 6:
                   cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                    break
                default:
                    break
                }
                break
            case 1:
                
                if ((indexPath.row - 1) == 0 || (indexPath.row - 1) == 1 || (indexPath.row - 1) == 4 || (indexPath.row - 1) == 5 || (indexPath.row - 1) == 6 || (indexPath.row - 1) == 7)
                {
                    cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_off"), for: .normal)
                    cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_on"), for: .selected)
                    cell?.btntoggleImage.addTarget(self, action: #selector(toggleButtonAction(_:)), for: .touchUpInside)
                }
                else
                {
                    
                }
                switch (indexPath.row - 1)
                {
                case 2:
                    cell!.btntoggleImage.setTitle("20", for: .normal)
                    break
                case 3:
                    cell!.btntoggleImage.setTitle("18", for: .normal)
                    break
                case 7:
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                    break
                default:
                    break
                }
                break
            case 2:
                if ((indexPath.row - 1) == 2)
                {
                    cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_off"), for: .normal)
                    cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_on"), for: .selected)
                    cell?.btntoggleImage.addTarget(self, action: #selector(toggleButtonAction(_:)), for: .touchUpInside)
                }
                else if ((indexPath.row - 1) == 3)
                {
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                }
                break
            case 3:
                cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_off"), for: .normal)
                cell!.btntoggleImage.setImage(UIImage(named:"setting_toggle_on"), for: .selected)
                cell?.btntoggleImage.addTarget(self, action: #selector(toggleButtonAction(_:)), for: .touchUpInside)
                if ((indexPath.row - 1) == 1)
                {
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                }
                break
            case 4:
                if ((indexPath.row - 1) == 1)
                {
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                }
                break
            default:
                break
            }
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.row == 0)
        {
            return
        }
        
        switch (indexPath.section)
        {
        case 0:
            switch (indexPath.row - 1)
            {
            case 0:
                let loginScreen = ChangePasswordScreen(nibName:"ChangePasswordScreen",bundle:nil)
                self.navigationController?.pushViewController(loginScreen, animated: true)
                break
            case 3:
                let loginScreen = DataStorageUsageScreen(nibName:"DataStorageUsageScreen",bundle:nil)
                self.navigationController?.pushViewController(loginScreen, animated: true)
                break
           case 4:
                let loginScreen = ColorThemeScreen(nibName:"ColorThemeScreen",bundle:nil)
                loginScreen.strSelectedTheme = strCurrentTheme
                self.navigationController?.pushViewController(loginScreen, animated: true)
                break
            case 5:
                let loginScreen = LanguageScreen(nibName:"LanguageScreen",bundle:nil)
                loginScreen.strSelectedLanguage = strCurrentLanguage
                self.navigationController?.pushViewController(loginScreen, animated: true)
                break
            default:
                break
            }
            break
        case 1:
            break
        case 2:
            switch (indexPath.row - 1)
            {
            case 1:

//            let loginScreen = BlockedContactsScreen(nibName:"BlockedContactsScreen",bundle:nil)
//            self.navigationController?.pushViewController(loginScreen, animated: true)
                let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
                likesUser.strFeedId = ""
                likesUser.strType = "BLOCKED"
                self.navigationController?.pushViewController(likesUser, animated: true)
                break
            default:
                break
            }
            break
        default:
            break
        }
    }
    @IBAction func toggleButtonAction(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func editButtonAction(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = EditProfileScreen.init(nibName: "EditProfileScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
//        sender.isSelected = !sender.isSelected
    }
    @IBAction func cameraButtonAction(_ sender: UIButton!) {
        self.view.endEditing(true)
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            return
        }
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Open Camera")
            
            self.appDelegate.checkCameraPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
                    {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
                        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            })

            
        })
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Open Gallery")
            
            self.appDelegate.checkGalleryPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            })


        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        var image: UIImage!
        
        // fetch the selected image
        
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        self.userImageData = UIImageJPEGRepresentation(image, 0.75)!
        self.imgPhoto.image = image
        
        self.showActivity(text: "WINDOW")
        self.performSelector(inBackground: #selector(self.EditProfileApi), with: getEditProfileApiKeys())
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getEditProfileApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(appDelegate.currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kUserID as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strName.getEncryptedString() ?? "", forKey: kName as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strEmail.getEncryptedString() ?? "", forKey: kEmail as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strGender.getEncryptedString() ?? "" , forKey: kGender as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strBio.getEncryptedString() ?? "", forKey: kBio as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strDob.getEncryptedString() ?? "", forKey: kDOB as NSCopying)
        return dicrequest
    }
    //MARK : - EditProfileApi
    @objc func EditProfileApi(dicreq : NSMutableDictionary)
    {
        var imgdata = Data()
        if userImageData != nil
        {
            imgdata = userImageData
        }
        UsergetallApiResultwithimagePostMethod(strMethodname: kEditProfileUrl, imgData: imgdata, strImgKey: kProfilePic, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            self.appDelegate.saveCurrentUser(dictResult: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                            self.appDelegate.currentLoginUser = self.appDelegate.getloginUser()
                            
                            self.userImageData = nil
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateProfile), object: nil)
                            
                            
                            //                            self.profileTableView.reloadData()
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}






