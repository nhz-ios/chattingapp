//
//  GroupSpecialConvertViewController.swift
//  ChattingApp
//
//  Created by Vishal-IOS on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class GroupSpecialConvertViewController: UIViewController {
    @IBOutlet weak var btnViewConvert: UIButton!
    @IBOutlet weak var ViewMain: UIView!
    @IBOutlet weak var lblSpecialGroup: UILabel!
    var iscomefor = "super_group" //special_group\super_group
    override func viewDidLoad() {
        super.viewDidLoad()
        btnViewConvert.layer.borderColor = UIColor(red: (3/255.0), green: (152/255.0), blue: (215/255.0), alpha: 1.0).cgColor
        btnViewConvert.layer.borderWidth = 1.0

        
         //btnViewConvert.layer.cornerRadius = 
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if iscomefor == "special_group"
        {
            btnViewConvert.setTitle("Convert to special group", for: .normal)
            self.navigationBarWithBackButton(strTitle: "CONVERT TO SPECIAL GROUP", leftbuttonImageName: "back.png")
            lblSpecialGroup.text = "In Special Group"
        }
        else
        {
            btnViewConvert.setTitle("Convert to super group", for: .normal)
            self.navigationBarWithBackButton(strTitle: "CONVERT TO SUPER GROUP", leftbuttonImageName: "back.png")
            lblSpecialGroup.text = "In Super Group"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
