//
//  VideoCallScreen.swift
//  ChattingApp
//
//  Created by vishal singh on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class VideoCallScreen: UIViewController {

    
    
    @IBOutlet  var btnAttendCall: UIButton!
    @IBOutlet  var btnRejectCall: UIButton!
    @IBOutlet  var btnEndCall: UIButton!
    @IBOutlet  var viewName: UIView!
    @IBOutlet  var viewIncomingCall: UIView!
    @IBOutlet  var viewOutgoingCall: UIView!
    @IBOutlet  var viewAudioCallStarted: UIView!
    @IBOutlet  var viewCallStarted: UIView!
    @IBOutlet  var btnSpeakerOutgoing: UIButton!
    @IBOutlet  var btnSpeakerCallStarted: UIButton!
    @IBOutlet  var btnSpeakerAudioCallStarted: UIButton!
    @IBOutlet  var btnMuteOutgoing: UIButton!
    @IBOutlet  var btnMuteCallStarted: UIButton!
    @IBOutlet  var btnMuteAudioCallStarted: UIButton!
    @IBOutlet  var btnVideoOutgoing: UIButton!
    @IBOutlet  var btnVideoCallStarted: UIButton!
    @IBOutlet  var btnAddMemberToAudioCall: UIButton!
    @IBOutlet  var btnAddMemberToCall: UIButton!
    @IBOutlet  var btnLockCall: UIButton!
    @IBOutlet  var lblName: UILabel!
    @IBOutlet  var lblTime: UILabel!
    @IBOutlet  var imgME: UIImageView!
    @IBOutlet  var imgMeVideo: UIImageView!
    @IBOutlet  var imgBGVideo: UIImageView!
   @IBOutlet  var scrllBG: UIScrollView!
    
    var isVideo: Bool = false
    var  timerToStartCall: Timer!
    var seconds = 0
    var  callStatus: CallStatus = .incoming
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgME.layer.masksToBounds = true
        imgMeVideo.layer.masksToBounds = true
        imgBGVideo.layer.masksToBounds = true
        // Do any additional setup after loading the view.

       
         
        imgME.isHidden = true
        imgMeVideo.isHidden = true
        imgBGVideo.isHidden = true

        if self.isVideo == true
        {
            imgMeVideo.isHidden = false
            imgBGVideo.isHidden = false
            lblName.textColor = UIColor.white
            lblTime.textColor = UIColor.black
        }
        else
        {
            imgME.isHidden = false
            lblName.textColor = UIColor.black
//            lblTime.textColor = UIColor.black
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if #available(iOS 11.0, *) {
            scrllBG.contentInsetAdjustmentBehavior = .never
            //print("contentInsetAdjustmentBehavior")
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
       
        self.navigationController?.isNavigationBarHidden = false
       if self.isVideo == true
        {
            self.navigationBarWithBackButton(strTitle: "VIDEO CALL", leftbuttonImageName: "back.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "AUDIO CALL", leftbuttonImageName: "back.png")
        }
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"Group_chat")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodMessage(_:)))
        menuButton1.tintColor = .white

        self.navigationItem.rightBarButtonItems = [menuButton1]
        
//        DispatchQueue.main.async {
//            self.scrllBG.frame = self.view.frame
//        }
        self.setCallViews()
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
    }
    @objc func methodMessage(_ sender : UIButton)
    {
      
    }
    func setCallViews()
    {
        viewIncomingCall.isHidden = true
        viewOutgoingCall.isHidden = true
        viewCallStarted.isHidden = true
        viewAudioCallStarted.isHidden = true
        btnEndCall.isHidden = true
        switch callStatus 
        {
        case .incoming:
            viewIncomingCall.isHidden = false
             lblTime.text = "Calling..."
            break
        case .outgoing:
            viewOutgoingCall.isHidden = false
            lblTime.text = "Ringing..."
            self.perform(#selector(methodChangeStausCall), with: nil, afterDelay: 10)
            
            btnEndCall.isHidden = false
            break
        case .started:
            viewAudioCallStarted.isHidden = self.isVideo == true ? true :false
            viewCallStarted.isHidden = self.isVideo == false ? true :false
            btnVideoCallStarted.isSelected = true
            imgMeVideo.isHidden = self.isVideo == false ? true: false
            btnEndCall.isHidden = false
            timerToStartCall = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(methodChangeLabelStaus), userInfo: nil, repeats: true)
           break
        case .ended:
            timerToStartCall.invalidate()
            timerToStartCall = nil
            
            break
        }
    }
    @objc func methodChangeStausCall()
    {
        callStatus = .started
        self.setCallViews()
       
    }
    @objc func methodChangeLabelStaus()
    {
        seconds += 1
        lblTime.text = timeString(time: TimeInterval(seconds))
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return hours > 0 ? String(format:"%02i:%02i:%02i", hours, minutes, seconds) :  String(format:"%02i:%02i", minutes, seconds)
    }
    
    @IBAction func methodAttendCallClicked(_ sender : UIButton)
    {
//        callStatus = .started          TEMPORARY FOR NAVIGATION
        callStatus = .outgoing
        self.setCallViews()

    }
    @IBAction func methodRejectCallClicked(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func methodEndCallClicked(_ sender : UIButton)
    {
        self.setCallViews()
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func methodSpeakerOutgoingClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
    }
    @IBAction func methodSpeakerCallStartedClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
    }
    @IBAction func methodMuteOutgoingClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
    }
    @IBAction func methodMuteCallStartedClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
    }
    @IBAction func methodVideoOutgoingClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
    }
    @IBAction func methodVideoCallStartedClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
        imgMeVideo.isHidden = !sender.isSelected 
    }
    @IBAction func methodAddMemberToCallClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
    }
    @IBAction func methodLockCallClicked(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

