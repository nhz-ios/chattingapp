//
//  cellClGallery.swift
//  ChattingApp
//
//  Created by Parikshit on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class cellClGallery: UICollectionViewCell {
    
    @IBOutlet var imgFolder : UIImageView!
    @IBOutlet var viewBack : UIView!
    @IBOutlet var imgFolderIcon : UIImageView!
    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet var btnPlay: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
