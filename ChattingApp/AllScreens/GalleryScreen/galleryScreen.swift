//
//  galleryScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class galleryScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet var clGallery : UICollectionView!
    var arrGalley = NSArray()
    var arrImages = NSArray()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrGalley = ["CAMERA","ALL MEDIA","DOWNLOAD","SCREENSHOTS","BLUETOOTH","ANIMATED GIFS"]
        arrImages = ["1.png","2.png","3.png","4.png","5.png","6.png"]

        let nib = UINib(nibName: "cellClGallery", bundle: nil)
       clGallery.register(nib, forCellWithReuseIdentifier: "cellClGallery")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "GALLERY", leftbuttonImageName: "back.png")
    }

   
    //Mark : Uicollection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrGalley.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellClGallery", for: indexPath) as! cellClGallery
        
        cell.lblTitle.text = "\(arrGalley.object(at: indexPath.item))"
        cell.imgFolder.image = UIImage.init(named: "\(arrImages.object(at: indexPath.item))")
        cell.btnPlay.isHidden = true

        
        if indexPath.item == 0
        {
            cell.imgFolderIcon.image = UIImage.init(named: "camera_icon.png")
        }
        else
        {
             cell.imgFolderIcon.image = UIImage.init(named: "folder_icon.png")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((collectionView.frame.width/2)), height: (collectionView.frame.width/2))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
       
       let detail : galleryDetailScreen = galleryDetailScreen(nibName : "galleryDetailScreen", bundle : nil)
        detail.strTitle = "\(arrGalley.object(at: indexPath.item))"
       self.navigationController?.pushViewController(detail, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
