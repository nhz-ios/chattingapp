//
//  LanguageScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 09/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class LanguageScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblLanguages:  UITableView!
    
    var arrLanguages = NSMutableArray()
//    var arrSelectedLanguages = NSMutableArray()
    var strFrom = ""
    var strSelectedLanguage = ""
    var selectedLanguage = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblLanguages.tableFooterView = UIView.init(frame: CGRect.zero)
        
        arrLanguages = ["English", "Arabic", "Chechen", "Spanish", "French", "Hebrew", "Italian", "Japanese", "Polish", "Chinese"]
        selectedLanguage = arrLanguages.index(of: strSelectedLanguage)
//        arrSelectedLanguages.add(0)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "LANGUAGE", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "LANGUAGE", leftbuttonImageName: "back.png")
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrLanguages.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
            let cellIdentifier:String = "FilterTableCell"
            var cell : FilterTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FilterTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FilterTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FilterTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            
            //            cell?.lblSubTotal.textColor = UIColor.lightGray
            cell?.btnSelect.isHidden = true
            var font = UIFont.init(name: "Lato-Semibold", size: 15.0)
 
            cell?.btnSelect.isHidden = false
        cell?.lblFilter.text = "\(arrLanguages.object(at: indexPath.row))"
//                cell?.btnSelect.isSelected = arrSelectedLanguages.contains(indexPath.row)
        cell?.btnSelect.isSelected = selectedLanguage == indexPath.row ? true : false
              font = UIFont.init(name: "Lato-Regular", size: 13.0)
            cell?.lblFilter.font = font
            return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        if (arrSelectedLanguages.contains(indexPath.row))
//        {
//            arrSelectedLanguages.remove(indexPath.row)
//        }
//        else
//        {
//            arrSelectedLanguages.add(indexPath.row)
//        }
        selectedLanguage = indexPath.row
        tblLanguages.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "LanguageChanged"), object: "\(arrLanguages.object(at: indexPath.row))")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




