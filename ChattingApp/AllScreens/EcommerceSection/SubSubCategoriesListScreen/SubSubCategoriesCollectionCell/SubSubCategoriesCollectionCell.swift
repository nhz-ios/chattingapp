//
//  SubSubCategoriesCollectionCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SubSubCategoriesCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgUser.layer.masksToBounds = true
        self.imgUser.layer.cornerRadius = 4
   }

}
