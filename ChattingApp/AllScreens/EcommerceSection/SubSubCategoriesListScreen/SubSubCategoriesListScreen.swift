//
//  SubSubCategoriesListScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class customCollectionReusableView: UICollectionReusableView
{
    let lblCat = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup(){
        
        self.addSubview(lblCat)
        lblCat.font = UIFont.init(name: "Lato-Bold", size: 20)
        lblCat.textColor = UIColor.darkGray
        lblCat.textAlignment = .center
        lblCat.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
}

class SubSubCategoriesListScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    @IBOutlet weak var clcnViewSubCategroies: UICollectionView!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    
    var arrSubCategories = NSMutableArray()
    var strTitle : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrSubCategories = [["cat_name": "WESTERN WEAR",
                             "sub_cat": [["cat_name": "Tops & Tunics",
                                          "image": "tops"],
                                         ["cat_name": "T-shirts & Shirts",
                                          "image": "t_shirts"],
                                         ["cat_name": "Dresses",
                                          "image": "dress"],
                                         ["cat_name": "Skirts",
                                          "image": "skirts"],
                                         ["cat_name": "Shorts",
                                          "image": "shorts"],
                                         ["cat_name": "Jeans",
                                          "image": "jeans"],
                                         ["cat_name": "Sports Wear",
                                          "image": "sport_wear"],
                                         ["cat_name": "Trousers & Capris",
                                          "image": "capri"],
                                         ["cat_name": "Outer Wear",
                                          "image": "outer_wear"]]],
                            ["cat_name": "ETHNIC WEAR",
                             "sub_cat": [["cat_name": "Kurtas/Kurtis",
                                          "image": "kurta"],
                                         ["cat_name": "Sarees",
                                          "image": "saree"],
                                         ["cat_name": "Lehenga Cholis",
                                          "image": "lehenga"]]]]

        let nib = UINib(nibName: "SubSubCategoriesCollectionCell", bundle: nil)
        clcnViewSubCategroies.register(nib, forCellWithReuseIdentifier: "SubSubCategoriesCollectionCell")

        clcnViewSubCategroies.register(customCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "customCollectionReusableView")
//        clcnViewSubCategroies.register(customCollectionReusableView.self, withReuseIdentifier: "Header")

    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: strTitle.uppercased(), leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearchClicked(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
      self.navigationItem.rightBarButtonItems = [menuButton1,menuButton2]
        
    }
    @IBAction func methodMyCart(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
//    cell?.lblSubCategory.text = "\((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "cat_name")!)"
//    cell?.lblSubSubCategory.text = "\(((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).object(at: indexPath.row - 1))"
//    return arrSelected.contains(section) ? ((arrSubCategories.object(at: section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).count + 1 : 1

    
    
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrSubCategories.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ((arrSubCategories.object(at: section) as! NSDictionary).object(forKey: "sub_cat") as! NSArray).count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: (collectionView.frame.width/3)-8, height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "customCollectionReusableView", for: indexPath) as! customCollectionReusableView
            
//            headerView.backgroundColor = UIColor.blue
//            headerView.setup(strName: "\((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "cat_name")!)")
//            print("\((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "cat_name")!)")
            headerView.lblCat.text = "\((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "cat_name")!)"
            
            return headerView
            
//        case UICollectionElementKindSectionFooter:
//            let footerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Footer", forIndexPath: indexPath) as! UICollectionReusableView
//
//            footerView.backgroundColor = UIColor.greenColor();
//            return footerView
            
        default:
            
//            assert(false, "Unexpected element kind")
            fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubSubCategoriesCollectionCell", for: indexPath) as! SubSubCategoriesCollectionCell
        
        let dic = (((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_cat") as! NSArray).object(at: indexPath.item) as! NSDictionary)
        cell.lblName.text = "\(dic.object(forKey: "cat_name")!)"
        cell.imgUser.image = UIImage.init(named: "\(dic.object(forKey: "image")!)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/3)-8, height: (collectionView.frame.width/3)-8+25)

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dic = (((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_cat") as! NSArray).object(at: indexPath.item) as! NSDictionary)
       let loginScreen  =  ProductsListScreen(nibName:"ProductsListScreen",bundle:nil)
        loginScreen.strTitle = "\(dic.object(forKey: "cat_name")!)"
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



