//
//  MyOrderTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 26/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MyOrderTableCell: UITableViewCell {

    @IBOutlet var lblOrderId: UILabel!
    @IBOutlet var lblOderDate: UILabel!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblMerchantName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblOrderStatus: UILabel!
    @IBOutlet var btnReorder: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
