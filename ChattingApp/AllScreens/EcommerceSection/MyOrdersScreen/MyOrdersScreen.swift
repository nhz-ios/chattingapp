//
//  MyOrdersScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 26/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MyOrdersScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var tblOrders: UITableView!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        tblOrders.tableFooterView = UIView.init(frame: CGRect.zero)

    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "MY ORDERS", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "MY ORDERS", leftbuttonImageName: "back.png")
        }

        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearchClicked(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
        //        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"ecom_cart.png"), style: .plain, target: self, action: #selector(methodCartClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1,menuButton2]
        tblOrders.reloadData()
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    @IBAction func methodCartClicked(_ sender: UIButton)
    {
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    
    //MARK:- UITableView DatSource And Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 7
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 110
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier:String = "MyOrderTableCell"
        var cell : MyOrderTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MyOrderTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("MyOrderTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? MyOrderTableCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.zero
        }
        switch indexPath.row {
        case 0:
            cell?.lblOrderStatus.text = "Received"
            cell?.lblOrderStatus.textColor =  .purple
            break
        case 1:
            cell?.lblOrderStatus.text = "Confirmed"
            cell?.lblOrderStatus.textColor =  UIColor.colorBlueLight
            break
       case 2:
            cell?.lblOrderStatus.text = "Cancelled"
            cell?.lblOrderStatus.textColor =  .orange
            break
        case 3:
            cell?.lblOrderStatus.text = "Payment Failed"
            cell?.lblOrderStatus.textColor =  .red
            break
        case 4:
            cell?.lblOrderStatus.text = "Dispatched"
            cell?.lblOrderStatus.textColor =  .darkGray
            break
        case 5:
            cell?.lblOrderStatus.text = "Out for Delivery"
            cell?.lblOrderStatus.textColor =  .blue
            break
        case 6:
            cell?.lblOrderStatus.text = "Delivered"
            cell?.lblOrderStatus.textColor =  .green
            break
       default:
            cell?.lblOrderStatus.text = "Out for Delivery"
            cell?.lblOrderStatus.textColor =  .purple
            break
       }
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let register = OrderProcessingScreen.init(nibName: "OrderProcessingScreen", bundle: nil)
        switch indexPath.row {
        case 0:
            register.strStatus = "1"
            register.strStatusLabel = "Order Received"
            break
        case 1:
            register.strStatus = "2"
            register.strStatusLabel = "Order Confirmed"
            break
        case 2:
            register.strStatus = "2"
            register.strStatusLabel = "Order Cancelled"
            break
        case 3:
            register.strStatus = "2"
            register.strStatusLabel = "Order Payment Failed"
            break
        case 4:
            register.strStatus = "3"
            register.strStatusLabel = "Order Dispatched"
            break
        case 5:
            register.strStatus = "4"
            register.strStatusLabel = "Order Out for Delivery"
            break
        case 6:
            register.strStatus = "4"
            register.isDelivered = true
            register.strStatusLabel = "Order Delivered"
            break
        default:
            register.strStatus = "4"
            register.strStatusLabel = "Order Out for Delivery"
            break
        }
        self.navigationController?.pushViewController(register, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}





