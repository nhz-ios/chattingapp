//
//  MainCategoriesListScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 05/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MainCategoriesListScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    @IBOutlet weak var clcnViewSubCategroies: UICollectionView!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    
    var arrMainCategories = NSMutableArray()
    var strTitle : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrMainCategories = [["cat_name": "Men", "image": "t_shirts"],
                             ["cat_name": "Women", "image": "lehenga"],
                             ["cat_name": "Kids & Baby", "image": "shorts"]]
        
        
        
        let nib = UINib(nibName: "SubSubCategoriesCollectionCell", bundle: nil)
        clcnViewSubCategroies.register(nib, forCellWithReuseIdentifier: "SubSubCategoriesCollectionCell")
        //        clcnViewSubCategroies.register(customCollectionReusableView.self, withReuseIdentifier: "Header")
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: strTitle.uppercased(), leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearchClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]
//        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
//        self.navigationItem.rightBarButtonItems = [menuButton1,menuButton2]
        
    }
    @IBAction func methodMyCart(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    cell?.lblSubCategory.text = "\((arrMainCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "cat_name")!)"
    //    cell?.lblSubSubCategory.text = "\(((arrMainCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).object(at: indexPath.row - 1))"
    //    return arrSelected.contains(section) ? ((arrMainCategories.object(at: section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).count + 1 : 1
    
    
    
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainCategories.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubSubCategoriesCollectionCell", for: indexPath) as! SubSubCategoriesCollectionCell
        
        let dic = (arrMainCategories.object(at: indexPath.item) as! NSDictionary)
        cell.lblName.text = "\(dic.object(forKey: "cat_name")!)"
        cell.imgUser.image = UIImage.init(named: "\(dic.object(forKey: "image")!)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/3)-8, height: (collectionView.frame.width/3)-8+25)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dic = (arrMainCategories.object(at: indexPath.item) as! NSDictionary)
        let loginScreen  =  SubCategoriesListScreen(nibName:"SubCategoriesListScreen",bundle:nil)
        loginScreen.strTitle = "\(dic.object(forKey: "cat_name")!)"
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




