//
//  EcomFilterScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class EcomFilterScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblFilters:  UITableView!
    @IBOutlet var viewFooter:  UIView!
    
    @IBOutlet var sliderPriceRange: NHRangeSlider!
    @IBOutlet var lblMinPriceRange: UILabel!
    @IBOutlet var lblMaxPriceRange: UILabel!
    
    var minPriceRange: Double = 0
    var maxPriceRange: Double = 2000

    var arrSubCategories = NSMutableArray()
    var arrSelectedCat = NSMutableArray()
    var arrSelectedFilter = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblFilters.tableFooterView = UIView.init(frame: CGRect.zero)
        
        arrSubCategories = [["cat_name": "Price",
                             "sub_cat_name": []],
                            ["cat_name": "Color",
                             "sub_cat_name": ["Beige", "Black", "Blue", "Brown", "Dark Blue", "Light Blue"]],
                            ["cat_name": "Size",
                             "sub_cat_name": ["XS","S", "M", "L", "XL", "XXL", "3XL"]],
                            ["cat_name": "Brand",
                             "sub_cat_name": ["Nike", "Reebok", "Adidas", "CrossFix", "Duke", "Levi's"]],
                            ["cat_name": "Availability",
                             "sub_cat_name": ["In stock", "Out of stock"]]]

        
        //        arrSubCategories = [["cat_name":"Men","sub_cat_name":["Clothing", "Footwear", "Beauty & Wellness", "Fragrances", "Handbags & Cluthes", "Watches", "Precious Jewellery", "Fashion Jewellery", "Eyewear"]],
        //                            ["cat_name":"Women","sub_cat_name":["Clothing", "Footwear", "Beauty & Wellness", "Fragrances", "Handbags & Cluthes", "Watches", "Precious Jewellery", "Fashion Jewellery", "Eyewear"]],
        //                            ["cat_name":"Kids & Baby","sub_cat_name":["Clothing", "Footwear", "Beauty & Wellness", "Fragrances", "Handbags & Cluthes", "Watches", "Precious Jewellery", "Fashion Jewellery", "Eyewear"]]]
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "FILTER", leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"ecom_reset.png"), style: .plain, target: self, action: #selector(methodResetClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]
        
    }
    @objc func methodResetClicked(_ sender: UIButton)
    {
        minPriceRange =  0
        maxPriceRange =  2000
        arrSelectedCat.removeAllObjects()
        arrSelectedFilter.removeAllObjects()
        tblFilters.reloadData()
    }
    
    @IBAction func methodSliderValueChanged(_ sender: NHRangeSlider)
    {
        setSliderValues()
    }
    
    func setSliderValues()
    {
        minPriceRange = sliderPriceRange.lowerValue
        maxPriceRange = sliderPriceRange.upperValue
        lblMinPriceRange.text = Int(minPriceRange) == 0 ? "Min" : "$\(Int(minPriceRange))"
        lblMaxPriceRange.text = Int(maxPriceRange) == 0 ? "Min" : Int(maxPriceRange) == 2000 ? "$2000+" : "$\(Int(maxPriceRange))"
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSubCategories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrSelectedCat.contains(section) ? ((arrSubCategories.object(at: section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).count + 1 : 1
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if section == arrSubCategories.count-1 {
            return viewFooter
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == arrSubCategories.count-1 {
            return 60
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 110
        }
        return 44
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0
        {
            let cellIdentifier:String = "FilterPriceRangeTableCell"
            var cell : FilterPriceRangeTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FilterPriceRangeTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FilterPriceRangeTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FilterPriceRangeTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            
            cell?.sliderPriceRange.addTarget(self, action: #selector(methodSliderValueChanged(_:)), for: .valueChanged)
            sliderPriceRange = cell?.sliderPriceRange
            lblMinPriceRange = cell?.lblMinPriceRange
            lblMaxPriceRange = cell?.lblMaxPriceRange
            
            sliderPriceRange.lowerValue =  minPriceRange
            sliderPriceRange.upperValue =  maxPriceRange
            setSliderValues()
            return cell!
        }
        else
        {
            let cellIdentifier:String = "FilterTableCell"
            var cell : FilterTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FilterTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FilterTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FilterTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }

            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            
            //            cell?.lblSubTotal.textColor = UIColor.lightGray
            cell?.btnSelect.isHidden = true
            var font = UIFont.init(name: "Lato-Semibold", size: 15.0)
            if indexPath.row == 0
            {
                cell?.lblFilter.text = "\((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "cat_name")!)"
                //            cell?.btnSelect.isHidden = ((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).count == 0 ? true : false
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            }
            else
            {
                cell?.btnSelect.isHidden = false
                cell?.lblFilter.text = "\(((arrSubCategories.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_cat_name") as! NSArray).object(at: indexPath.row - 1))"
                cell?.btnSelect.isSelected = arrSelectedFilter.contains("\(indexPath.section)_\(indexPath.row)")
                font = UIFont.init(name: "Lato-Regular", size: 13.0)
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            cell?.lblFilter.font = font
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.row == 0)
        {
            if (arrSelectedCat.contains(indexPath.section))
            {
                arrSelectedCat.remove(indexPath.section)
            }
            else
            {
                arrSelectedCat.add(indexPath.section)
            }
        }
        else
        {
            if (arrSelectedFilter.contains("\(indexPath.section)_\(indexPath.row)"))
            {
                arrSelectedFilter.remove("\(indexPath.section)_\(indexPath.row)")
            }
            else
            {
                arrSelectedFilter.add("\(indexPath.section)_\(indexPath.row)")
            }
        }
        tblFilters.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



