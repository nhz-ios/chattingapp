//
//  FilterPriceRangeTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 22/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class FilterPriceRangeTableCell: UITableViewCell {

    @IBOutlet var sliderPriceRange: NHRangeSlider!
    @IBOutlet var lblMinPriceRange: UILabel!
    @IBOutlet var lblMaxPriceRange: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
