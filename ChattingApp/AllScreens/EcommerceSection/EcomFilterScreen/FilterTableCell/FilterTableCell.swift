//
//  FilterTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 22/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class FilterTableCell: UITableViewCell {

    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var lblFilterDetails: UILabel!

    @IBOutlet weak var btnSelect: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
