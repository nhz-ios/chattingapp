//
//  OrderProcessingScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 02/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OrderProcessingScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var tblOrders: UITableView!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var imgStatus:  UIImageView!
    @IBOutlet var lblStatus:  UILabel!
    @IBOutlet var btnCancelOrder:  UIButton!
    
    var strStatus = "3"
    var strStatusLabel = ""
    var isDelivered = false

    override func viewDidLoad() {
        super.viewDidLoad()
         tblOrders.tableFooterView = UIView.init(frame: CGRect.zero)
        viewFooter.layer.masksToBounds = true
        imgStatus.image = UIImage(named: "step_" + strStatus)
        lblStatus.text = strStatusLabel.uppercased()
        
        DispatchQueue.main.async {
            var frame = self.tblOrders.frame
            var frameFooter = self.viewFooter.frame
            switch (self.strStatus)
            {
            case "1":
                //            tblOrders.tableFooterView = viewFooter
                frameFooter.size.height = 40
                frameFooter.origin.y =  ScreenSize.SCREEN_HEIGHT - 40
                break
            case "2":
                //            tblOrders.tableFooterView = UIView.init(frame: CGRect.zero)
                frameFooter.size.height =  0
                frameFooter.origin.y =  ScreenSize.SCREEN_HEIGHT
                break
            case "3":
                frameFooter.size.height =  0
                frameFooter.origin.y =  ScreenSize.SCREEN_HEIGHT
                break
            case "4":
                self.btnCancelOrder.setTitle("RATE & REVIEW ORDER", for: .normal)
                frameFooter.size.height =  150
                frameFooter.origin.y =  ScreenSize.SCREEN_HEIGHT - 150
                break
            default: break
                
            }
            frame.size.height =  ScreenSize.SCREEN_HEIGHT - frameFooter.size.height
            self.tblOrders.frame = frame
            self.viewFooter.frame = frameFooter
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "ORDER PROCESS", leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
        //        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"ecom_cart.png"), style: .plain, target: self, action: #selector(methodCartClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]
        tblOrders.reloadData()
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    @IBAction func methodCartClicked(_ sender: UIButton)
    {
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    
    //MARK:- UITableView DatSource And Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(strStatus == "2")
        {
            if(strStatusLabel == "Order Cancelled" || strStatusLabel == "Order Payment Failed")
            {
                return 5
            }
            return 6
        }
        
        return 7
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 120
    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
//    {
//        if(strStatus == "2" || strStatus == "3")
//        {
//            return nil
//        }
//        return viewFooter
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
//    {
//        if(strStatus == "2" || strStatus == "3")
//        {
//            return 0
//        }
////        if(strStatus == "4" && isDelivered == true)
////        {
////            return 150
////        }
//        if(strStatus == "4")
//        {
//            return 150
//        }
//        return 44
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.row {
        case 0:
            return 90
        case 1:
            return 100
        case 5:
            if(strStatus == "4" && isDelivered == true)
            {
                return 100
            }
            return 65
        case 6:
            if(strStatus == "3")
            {
                return 235
            }
            if(strStatus == "4")
            {
                
                return 110
            }
            return 140
        default:
            return 40
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.row {
        case 0:
            let cellIdentifier:String = "OrderUpperTableCell"
            var cell : OrderUpperTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderUpperTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderUpperTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderUpperTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            return cell!
        case 1:
            let cellIdentifier:String = "OrderHistoryTableCell"
            var cell : OrderHistoryTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderHistoryTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderHistoryTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderHistoryTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            return cell!
        case 5:
            let cellIdentifier:String = "PaymentMethodTableCell"
            var cell : PaymentMethodTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentMethodTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("PaymentMethodTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? PaymentMethodTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            cell?.layer.masksToBounds = true
            cell?.lblDeliverdDate.text = "Delivered Till      :      20 Aug 2017"
            if(strStatus == "4" && isDelivered == true )
            {
                cell?.lblDeliverdDate.text = "Delivered Date      :      20 Aug 2017"
            }
            return cell!
        case 6:
            if(strStatus == "3" || strStatus == "4" )
            {
                let cellIdentifier:String = "OrderDeliveredTableCell"
                var cell : OrderDeliveredTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderDeliveredTableCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("OrderDeliveredTableCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? OrderDeliveredTableCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                cell?.lblOrderDeliveredBy.text =  (strStatus == "3" || (strStatus == "4" && isDelivered == false)) ? "Order Dispatched By" : "Order Delivered By"
               cell?.layer.masksToBounds = true
               return cell!
            }
            let cellIdentifier:String = "OrderProcessingNoteTableCell"
            var cell : OrderProcessingNoteTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderProcessingNoteTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderProcessingNoteTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderProcessingNoteTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            return cell!
        case 7:
            let cellIdentifier:String = "OrderDeliveredTableCell"
            var cell : OrderDeliveredTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderDeliveredTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderDeliveredTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderDeliveredTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            cell?.lblOrderDeliveredBy.text =  (strStatus == "3" || (strStatus == "4"  && isDelivered == false)) ? "Order Dispatched By" : "Order Delivered By"
            return cell!
        default:
            let cellIdentifier:String = "CartTotalCell"
            var cell : CartTotalCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CartTotalCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("CartTotalCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? CartTotalCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            var font = UIFont.init(name: "Lato-Regular", size: 14.0)
            cell?.lblSubTotal.textColor = UIColor.lightGray
            
            switch indexPath.row {
            case 2:
                cell?.lblSubTotal.textColor = UIColor.darkGray
                cell?.lblSubTotal.text = "Price"
                cell?.lblSubTotalPrice.text = "$200"
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                break
            case 3:
                cell?.lblSubTotal.textColor = UIColor.darkGray
                cell?.lblSubTotal.text = "Shipping Price"
                cell?.lblSubTotalPrice.text = "$30"
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                break
            case 4:
                cell?.lblSubTotal.textColor = UIColor.black
                cell?.lblSubTotal.text = "Total"
                cell?.lblSubTotalPrice.text = "$230"
                font = UIFont.init(name: "Lato-Semibold", size: 18.0)
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
                break
                //            case 7:
                //                cell?.lblOrderStatus.text = "Payment Failed"
                //                cell?.lblOrderStatus.textColor =  .red
                //                break
                //            case 8:
                //                cell?.lblOrderStatus.text = "Cancelled"
                //                cell?.lblOrderStatus.textColor =  .orange
                //                break
                //            case 9:
                //                cell?.lblOrderStatus.text = "Dispatched"
                //                cell?.lblOrderStatus.textColor =  UIColor.colorBlueLight
            //                break
            default:
                cell?.lblSubTotal.textColor = UIColor.lightGray
                cell?.lblSubTotal.text = "Sub-Total"
                cell?.lblSubTotalPrice.text = "$1400"
                break
            }
            
            cell?.lblSubTotal.font = font
            cell?.lblSubTotalPrice.font = font

            if (indexPath.row == 6)
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            }
            else
            {
                 cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            
            return cell!
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}







