//
//  OrderProcessingNoteTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 07/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OrderProcessingNoteTableCell: UITableViewCell {

    @IBOutlet var lblNoteDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
