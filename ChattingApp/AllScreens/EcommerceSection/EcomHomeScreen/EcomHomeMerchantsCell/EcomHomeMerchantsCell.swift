//
//  EcomHomeMerchantsCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 20/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class EcomHomeMerchantsCell: UICollectionViewCell {

    @IBOutlet var imgFolder : UIImageView!
    @IBOutlet var lblTitle : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgFolder.layer.masksToBounds = true
        self.imgFolder.layer.cornerRadius = 4
//        self.imgFolder.layer.borderWidth = 1
//        self.imgFolder.layer.borderColor = UIColor.init(red: 179.0/255, green: 179.0/255, blue: 179.0/255, alpha: 1).cgColor
        
        self.lblTitle.layer.masksToBounds = true
        self.lblTitle.layer.cornerRadius = 4
        //        self.imgFolder.layer.borderWidth = 1
        //        self.imgFolder.layer.borderColor = UIColor.init(red: 179.0/255, green: 179.0/255, blue: 179.0/255, alpha: 1).cgColor

        // Initialization code
    }

}
