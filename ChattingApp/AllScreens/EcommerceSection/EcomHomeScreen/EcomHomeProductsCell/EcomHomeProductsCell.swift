//
//  EcomHomeProductsCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 20/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import MapKit

class EcomHomeProductsCell: UITableViewCell {

    @IBOutlet var clcnViewProduct: UICollectionView!
    @IBOutlet var mapView: MKMapView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
