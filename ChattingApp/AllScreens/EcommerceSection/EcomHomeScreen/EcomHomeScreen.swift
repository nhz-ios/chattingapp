//
//  EcomHomeScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 19/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class EcomHomeScreen: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate {
    
    @IBOutlet var tblProducts: UITableView!
    
    @IBOutlet var viewHeaderBack: UIView!
    @IBOutlet var viewHeader: UIView!
    
    @IBOutlet var btnMerchants: UIButton!
    @IBOutlet var btnProducts: UIButton!
    @IBOutlet weak var btnShowViewMap: UIButton!
    
    @IBOutlet var viewEnterLocation: UIView!
    @IBOutlet weak var txtEnterLocation: NHZTextField!
    @IBOutlet weak var btnEnterLocation: UIButton!
    @IBOutlet weak var btnAdvanceSearch: UIButton!
    @IBOutlet var viewAdvanceLocation: UIView!
    @IBOutlet weak var txtAdvanceLocation: NHZTextField!
    @IBOutlet weak var btnAdvanceLocation: UIButton!
    @IBOutlet weak var txtCity: NHZTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet var toolBar: UIToolbar!

    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!

    var arrImages = NSArray()
    var arrCategories = NSArray()
    var arrCategoriesImages = NSArray()

    var arrProducts = NSArray()
    var arrProductsImages = NSArray()

    var arrMerchants = NSArray()
    var arrMerchantsImages = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewHeader.layer.masksToBounds = true
        viewHeaderBack.layer.cornerRadius = 3
        viewHeaderBack.layer.borderColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0).cgColor
        viewHeaderBack.layer.borderWidth = 1
        
        btnProducts.layer.cornerRadius = 3
        btnMerchants.layer.cornerRadius = 3
        
        
        txtEnterLocation.inputAccessoryView = toolBar
        txtAdvanceLocation.inputAccessoryView = toolBar
        txtCity.inputAccessoryView = toolBar

        arrImages = ["5.png","4.png","3.png","2.png","6.png","1.png"]
        arrCategories = ["ALL", "MOBILES","ELECTRONICS","FASHION","FURNITURE","SPORTS","KITCHEN","BOOKS","FITNESS"]
        arrCategoriesImages = ["ecom_all", "ecom_mobile", "ecom_electronic", "ecom_fashion", "ecom_furniture", "ecom_sport", "ecom_electronic", "ecom_mobile", "ecom_fashion"]

        arrProducts = ["MOBILES","ELECTRONICS","FASHION","FURNITURE","SPORTS","KITCHEN","BOOKS","FITNESS"]
       arrProductsImages = ["ecom_cat_mobile", "ecom_cat_electornics", "ecom_cat_fashion", "ecom_cat_furniture","ecom_cat_sport","ecom_cat_kitchen","ecom_cat_book","ecom_cat_fitness"]

        arrMerchants = ["Alec L Quinn", "Joseph D Castro", "Randy D White", "Christopher K", "Christine R Burrus", "Anthony McCauley", "Walter C Finnie", "Rhonda A Silvernail", "Pauline C Acevedo", "Walter C Finnie", "Joseph D Castro", "Randy D White"]
        arrMerchantsImages = ["nike","rebook","addidas","crossfit","duke","instinct","fraterniti","jack_daniels","levis","people","kiddo_clothing","scottie"]
        
        
        tblProducts.tableFooterView = UIView.init(frame: CGRect.zero)
        
        DispatchQueue.main.async {
            self.view.addSubview(self.viewEnterLocation)
            self.viewEnterLocation.frame = self.view.frame
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "SHOPPING", leftbuttonImageName: "menu.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
         let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
//        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage.init(named:"ecom_cart.png")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodMyCart(_:)))
        // Add the rightBarButtonItems on the navigation bar
        
        self.navigationItem.rightBarButtonItems = [menuButton1, menuButton2]
        
        self.appDelegate.TabLogin.tabBar.isHidden = false
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.appDelegate.TabLogin.tabBar.isHidden = true
    }
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }

    @IBAction func methodMyCart(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }
    
    // MARK: - UIButtonsMethod
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }

    @IBAction func methodTopHeaderButtons(_ sender: UIButton)
    {
        tblProducts.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        if sender.isSelected == true
        {
            return
        }
        btnMerchants.backgroundColor = UIColor.clear
        btnProducts.backgroundColor = UIColor.clear
        btnMerchants.isSelected = false
        btnProducts.isSelected = false
        if sender.tag == 101
        {
            btnMerchants.backgroundColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else
        {
            btnProducts.backgroundColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        sender.isSelected = true
        btnShowViewMap.isHidden = !btnMerchants.isSelected
        tblProducts.reloadData()
    }
    @IBAction func methodEnterLocationClicked(_ sender: Any)
    {
        self.view.endEditing(true)
        self.viewEnterLocation.removeFromSuperview()
    }
    @IBAction func methodAdvanceSearchClicked(_ sender: Any)
    {
        self.view.endEditing(true)
       self.viewEnterLocation.removeFromSuperview()
        self.view.addSubview(self.viewAdvanceLocation)
        self.viewAdvanceLocation.frame = self.view.frame
    }
    @IBAction func methodAdvanceLocationClicked(_ sender: Any)
    {
        self.view.endEditing(true)
        self.viewEnterLocation.removeFromSuperview()
        self.viewAdvanceLocation.removeFromSuperview()
    }
    @IBAction func methodSubmitClicked(_ sender: Any)
    {
        self.view.endEditing(true)
        self.viewEnterLocation.removeFromSuperview()
        self.viewAdvanceLocation.removeFromSuperview()
    }
    
    
    
    @IBAction func methodShowViewMapClicked(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        tblProducts.reloadData()
    }
    //MARK:- UITableView DatSource And Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch section
        {
        case 0:
            return viewHeader
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        switch section
        {
        case 0:
            return 50
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if btnMerchants.isSelected == true
        {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if btnMerchants.isSelected == true && indexPath.row == 0
        {
            return 135
        }
        print( "********" ,tableView.frame.size.height , tableView.frame.size.height - 50 - (btnMerchants.isSelected == true ? 135 : 0))
        if ScreenSize.SCREEN_HEIGHT > 736
        {
            return tableView.frame.size.height - 50 - (btnMerchants.isSelected == true ? 135 : 0) - 49 - 50
        }
        return tableView.frame.size.height - 50 - (btnMerchants.isSelected == true ? 135 : 0) - 49
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if btnMerchants.isSelected == true && indexPath.row == 0
        {
            let indentifier:String = "EcomHomeCategoryCell"
            var cell : EcomHomeCategoryCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? EcomHomeCategoryCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("EcomHomeCategoryCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? EcomHomeCategoryCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            
            let nib = UINib(nibName: "userCollectionCell", bundle: nil)
            cell?.clcnViewCategory.register(nib, forCellWithReuseIdentifier: "userCollectionCell")
            cell?.clcnViewCategory.backgroundColor = .clear
            cell?.clcnViewCategory.delegate = self
            cell?.clcnViewCategory.dataSource = self
            cell?.clcnViewCategory.tag = 100
            
            return cell!
        }
        let cellIdentifier:String = "EcomHomeProductsCell"
        var cell : EcomHomeProductsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EcomHomeProductsCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("EcomHomeProductsCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? EcomHomeProductsCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.zero
        }
        if btnMerchants.isSelected == true
        {
            let nib = UINib(nibName: "EcomMechantsCell", bundle: nil)
            cell?.clcnViewProduct.register(nib, forCellWithReuseIdentifier: "EcomMechantsCell")
            cell?.mapView.isHidden = !btnShowViewMap.isSelected
            cell?.clcnViewProduct.isHidden = btnShowViewMap.isSelected
        }
        else
        {
            let nib = UINib(nibName: "EcomHomeMerchantsCell", bundle: nil)
            cell?.clcnViewProduct.register(nib, forCellWithReuseIdentifier: "EcomHomeMerchantsCell")
            cell?.mapView.isHidden = true
            cell?.clcnViewProduct.isHidden = false
        }
        cell?.clcnViewProduct.delegate = self
        cell?.clcnViewProduct.dataSource = self
        cell?.clcnViewProduct.reloadData()
        
        return cell!
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if (touch.view?.isDescendant(of: tblMenu))! == true
//        {
//            return false
//        }
        return true
    }
    
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100
        {
            return arrCategories.count
        }
        if btnMerchants.isSelected == true
        {
            return arrMerchants.count
        }
        else
        {
            return arrProducts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView.tag == 100
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for: indexPath) as! userCollectionCell
            cell.addUserButton.isHidden = true
            if indexPath.item == 0 {
                cell.nameLabel.textColor = UIColor.init(red: 5.0/255, green: 152.0/255, blue: 214.0/255, alpha: 1)
                
            } else {
                cell.nameLabel.textColor = UIColor.darkGray
            }
            cell.nameLabel.text = (arrCategories.object(at: indexPath.item) as! String).capitalized
            cell.nameLabel.font = UIFont.init(name: "Lato-Regular", size: 12.0)
            
            cell.userImageView.image = UIImage(named: arrCategoriesImages.object(at: indexPath.item) as! String)
            cell.userBgView.layer.borderWidth = 0

            return cell
        }
        else
        {
            if btnMerchants.isSelected == true
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EcomMechantsCell", for: indexPath) as! EcomMechantsCell
                
                cell.lblName.text = "\(arrMerchants.object(at: indexPath.item))"
                cell.imgUser.image = UIImage.init(named: "\(arrMerchantsImages.object(at: indexPath.item))")
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EcomHomeMerchantsCell", for: indexPath) as! EcomHomeMerchantsCell
                
                cell.lblTitle.text = "\(arrProducts.object(at: indexPath.item))"
                cell.imgFolder.image = UIImage.init(named: "\(arrProductsImages.object(at: indexPath.item))")
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 100
        {
            return CGSize(width: 66, height: 100)
        }
        
        if btnMerchants.isSelected == true
        {
            return CGSize(width: (collectionView.frame.width/3)-8, height: (collectionView.frame.width/3)-8+50)
        }
        else
        {
            return CGSize(width: (collectionView.frame.width/2)-4, height: ((collectionView.frame.width/2)-4) * 0.84)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView.tag == 100
        {
        }
        else{
            if btnMerchants.isSelected == true
            {
                let loginScreen  =  EcomMerchantDetailScreen(nibName:"EcomMerchantDetailScreen",bundle:nil)
                //            loginScreen.strTitle = "\(arrMerchants.object(at: indexPath.item))"
                self.navigationController?.pushViewController(loginScreen, animated: true)
            }
            else
            {
                let loginScreen  =  MainCategoriesListScreen(nibName:"MainCategoriesListScreen",bundle:nil)
                loginScreen.strTitle = "\(arrProducts.object(at: indexPath.item))"
                self.navigationController?.pushViewController(loginScreen, animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

