//
//  EcomMechantsCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 20/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class EcomMechantsCell: UICollectionViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgUser.layer.masksToBounds = true
        self.imgUser.layer.cornerRadius = 4
        
    }

}
