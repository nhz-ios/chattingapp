//
//  MyWishListScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MyWishListScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    var arrProducts = NSMutableArray()
    @IBOutlet var clcnViewProduct: UICollectionView!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "ProductCollectionCell", bundle: nil)
        clcnViewProduct.register(nib, forCellWithReuseIdentifier: "ProductCollectionCell")

        arrProducts = [["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "15 Apr 2018",
                        "count": "3"],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "Stylis peach dress",
                        "price": "$150",
                        "image": "dress_3",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "Princess gown",
                        "price": "$550",
                        "image": "dress_4",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "2 Feb 2018",
                        "count": ""]]
    }
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "MY WISHLIST", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "MY WISHLIST", leftbuttonImageName: "back.png")
        }
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearchClicked(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
//        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"ecom_cart.png"), style: .plain, target: self, action: #selector(methodCartClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1,menuButton2]
        clcnViewProduct.reloadData()
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    @IBAction func methodCartClicked(_ sender: UIButton)
    {
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    @IBAction func methodRemoveProductClicked(_ sender: UIButton)
    {
       
    }
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell

        cell.btnRemove.isHidden = false
        cell.lblName.text = "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "name")!)"
        cell.lblPrice.text = "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "price")!)"
        cell.imgUser.image = UIImage.init(named: "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "image")!)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/2)-8, height: ((collectionView.frame.width/2)-8) * 0.70 + 55)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let loginScreen  =  ProductDetailScreen(nibName:"ProductDetailScreen",bundle:nil)
        //            loginScreen.strTitle = "\(arrMerchants.object(at: indexPath.item))"
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




