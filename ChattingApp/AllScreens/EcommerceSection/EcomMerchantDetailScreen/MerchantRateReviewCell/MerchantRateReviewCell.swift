//
//  MerchantRateReviewCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 22/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MerchantRateReviewCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        lblDescription.sizeToFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
