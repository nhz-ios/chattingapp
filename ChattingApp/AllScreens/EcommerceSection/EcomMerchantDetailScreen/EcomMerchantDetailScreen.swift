//
//  EcomMerchantDetailScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 22/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class EcomMerchantDetailScreen: UIViewController, UITableViewDataSource, UITableViewDelegate,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UIGestureRecognizerDelegate
{
    @IBOutlet weak var tblMerchant:  UITableView!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var btnCompanyProfile:  UIButton!
    @IBOutlet var btnPostedItems:  UIButton!
    @IBOutlet var btnRateReviews:  UIButton!
    
    @IBOutlet var viewTradeAssurance: UIView!
    @IBOutlet var viewInnerTradeAssurance: UIView!
    @IBOutlet var btnCloseTradeAssurance: UIButton!

    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    
    var arrProducts = NSMutableArray()

    var arrSubCategories = NSMutableArray()
    var strTitle : String = "MECHANT DETAIL"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMerchant.tableFooterView = UIView.init(frame: CGRect.zero)
        
        arrProducts = [["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "15 Apr 2018",
                        "count": "3"],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "Stylis peach dress",
                        "price": "$150",
                        "image": "dress_3",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "Princess gown",
                        "price": "$550",
                        "image": "dress_4",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "2 Feb 2018",
                        "count": ""]]
    }
    
    @IBAction func methodShowTradeAssuranceClicked(_ sender: UIButton)
    {
        self.appDelegate.window!.addSubview(viewTradeAssurance)
        viewTradeAssurance.frame = self.appDelegate.window!.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewTradeAssurance.addGestureRecognizer(tap)
    }
    
    @IBAction func methodCloseTradeAssuranceClicked(_ sender: UIButton)
    {
        viewTradeAssurance.removeFromSuperview()
    }
    
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }
    //MARK:- UIGestureRecognizer Delegate Method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: viewInnerTradeAssurance))! == true
        {
            return false
        }
        return true
    }
    
    @IBAction func methodHeaderButtonsClicked(_ sender: UIButton)
    {
        if sender.isSelected == true
        {
            return
        }
        btnCompanyProfile.isSelected = false
        btnPostedItems.isSelected = false
        btnRateReviews.isSelected = false
        btnCompanyProfile.backgroundColor = UIColor.clear
        btnPostedItems.backgroundColor = UIColor.clear
        btnRateReviews.backgroundColor = UIColor.clear
        switch sender.tag {
        case 1:
            btnCompanyProfile.backgroundColor = UIColor.colorAppTheam
            break
        case 2:
            btnPostedItems.backgroundColor = UIColor.colorAppTheam
           break
        case 3:
            btnRateReviews.backgroundColor = UIColor.colorAppTheam
            break
        default:
           break
        }
        sender.isSelected = true
        tblMerchant.reloadData()
        //        let register = EcomFilterScreen.init(nibName: "EcomFilterScreen", bundle: nil)
        //        self.navigationController?.pushViewController(register, animated: true)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "MERCHANT DETAIL", leftbuttonImageName: "back.png")
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
        //        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage.init(named:"ecom_cart.png")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodMyCart(_:)))
        // Add the rightBarButtonItems on the navigation bar
        
        self.navigationItem.rightBarButtonItems = [menuButton1, menuButton2]
    }
    @IBAction func methodMyCart(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            if btnCompanyProfile.isSelected == true
            {
                return 8
            }
            else if btnPostedItems.isSelected == true
            {
                return 1
            }
            else
            {
                return 8
            }
        }
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            return viewHeader
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 172
        }
        if btnCompanyProfile.isSelected == true
        {
            return 40
        }
        else if btnPostedItems.isSelected == true
        {
            return tableView.frame.size.height - 150 - 50
        }
        else
        {
            return 80
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0
        {
            let cellIdentifier:String = "MerchantDetailTableCell"
            var cell : MerchantDetailTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantDetailTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("MerchantDetailTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? MerchantDetailTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)

            cell?.btnShowTradeAssurance.addTarget(self, action: #selector(methodShowTradeAssuranceClicked(_:)), for: .touchUpInside)
            return cell!
        }
        else
        {
            if btnCompanyProfile.isSelected == true
            {
                let cellIdentifier:String = "MerchantCompanyProfileCell"
                var cell : MerchantCompanyProfileCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantCompanyProfileCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("MerchantCompanyProfileCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? MerchantCompanyProfileCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                if (indexPath.row == 8 - 1)
                {
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                }
                else
                {
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                }
                cell?.lblName.isHidden = false
                cell?.lblDescription.isHidden = false
                cell?.lblCompanyOverview.isHidden = true
                

                switch(indexPath.row)
                {
                case 0:
                    cell?.lblName.isHidden = true
                    cell?.lblDescription.isHidden = true
                    cell?.lblCompanyOverview.isHidden = false
                    break
                case 1:
                    cell?.lblName.text = "Business Type"
                    cell?.lblDescription.text = "Manufacturer, Trading Company"
                    break
                case 2:
                    cell?.lblName.text = "Year Established:"
                    cell?.lblDescription.text = "2011"
                    break
                case 3:
                    cell?.lblName.text = "Total Annual Revenue:"
                    cell?.lblDescription.text = "US$10 Million - US$50 Million"
                    break
                case 4:
                    cell?.lblName.text = "Total Employees:"
                    cell?.lblDescription.text = "101 - 200 People"
                    break
                case 5:
                    cell?.lblName.text = "Total Annual Revenue:"
                    cell?.lblDescription.text = "US$50 Million - US$100 Million"
                    break
                case 6:
                    cell?.lblName.text = "Certifications (1):"
                    cell?.lblDescription.text = "FTA"
                    break
                case 7:
                    cell?.lblName.text = "Product Certifications:"
                    cell?.lblDescription.text = "FACILITY AND MERCHANDISE AUTHORIZATION"
                    break
//                case 8:
//                    cell?.lblName.text = "Total Employees:"
//                    cell?.lblDescription.text = "101 - 200 People"
//                    break
                default:
                    break
                    
                }
                return cell!
            }
            else if btnPostedItems.isSelected == true
            {
                let cellIdentifier:String = "EcomHomeProductsCell"
                var cell : EcomHomeProductsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EcomHomeProductsCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("EcomHomeProductsCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? EcomHomeProductsCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                }
                let nib = UINib(nibName: "ProductCollectionCell", bundle: nil)
                cell?.clcnViewProduct.register(nib, forCellWithReuseIdentifier: "ProductCollectionCell")
                cell?.mapView.isHidden = true
                cell?.clcnViewProduct.isHidden = false
                
                cell?.clcnViewProduct.delegate = self
                cell?.clcnViewProduct.dataSource = self
                cell?.clcnViewProduct.reloadData()
                
                return cell!
            }
            else
            {
                let cellIdentifier:String = "MerchantRateReviewCell"
                var cell : MerchantRateReviewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantRateReviewCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("MerchantRateReviewCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? MerchantRateReviewCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                    cell?.separatorInset = UIEdgeInsets.zero
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                }
                return cell!
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        
        cell.lblName.text = "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "name")!)"
        cell.lblPrice.text = "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "price")!)"
        cell.imgUser.image = UIImage.init(named: "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "image")!)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/2)-8, height: (collectionView.frame.width/2)-8+55)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let loginScreen  =  ProductDetailScreen(nibName:"ProductDetailScreen",bundle:nil)
        //            loginScreen.strTitle = "\(arrMerchants.object(at: indexPath.item))"
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




