//
//  MerchantCompanyProfileCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 22/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MerchantCompanyProfileCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblCompanyOverview: UILabel!
    @IBOutlet var imgQRCode: UIImageView!
    @IBOutlet var lblQRCode: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
