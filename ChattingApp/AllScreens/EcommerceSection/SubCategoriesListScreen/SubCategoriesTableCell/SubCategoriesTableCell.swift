//
//  SubCategoriesTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SubCategoriesTableCell: UITableViewCell {

    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var lblSubSubCategory: UILabel!

    @IBOutlet weak var btnPlus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
