//
//  SubCategoriesListScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SubCategoriesListScreen: UIViewController ,  UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
   // @IBOutlet weak var tblSubCategroies:  UITableView!
    @IBOutlet weak var clcnViewSubSubCategroies: UICollectionView!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!

    var arrSubCategories = NSMutableArray()
    var arrSelected = NSMutableArray()
    var strTitle : String = ""
    var arrSubSubCategories = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
       // tblSubCategroies.tableFooterView = UIView.init(frame: CGRect.zero)
        
//        arrSubCategories = [["cat_name": "Men",
//                            "sub_cat_name": ["Clothing", "Footwear", "Fragrances", "Watches", "Eyewear"]],
//                            ["cat_name": "Women",
//                             "sub_cat_name": ["Clothing", "Footwear", "Beauty & Wellness", "Fragrances", "Handbags & Cluthes", "Watches", "Precious Jewellery", "Fashion Jewellery", "Eyewear"]],
//                            ["cat_name": "Kids & Baby",
//                             "sub_cat_name": ["Clothing", "Footwear", "Beauty & Wellness", "Eyewear"]]]
        if strTitle == "Men"
        {
            arrSubSubCategories = [["cat_name": "Clothing",
                                    "image": "tops"],
                                   ["cat_name": "Footwear",
                                    "image": "t_shirts"],
                                   ["cat_name": "Fragrances",
                                    "image": "dress"],
                                   ["cat_name": "Watches",
                                    "image": "skirts"],
                                   ["cat_name": "Eyewear",
                                    "image": "shorts"]]
        }
        else if strTitle == "Women"
        {
            arrSubSubCategories = [["cat_name": "Clothing",
                                    "image": "tops"],
                                   ["cat_name": "Footwear",
                                    "image": "t_shirts"],
                                   ["cat_name": "Beauty & Wellness",
                                    "image": "t_shirts"],
                                   ["cat_name": "Fragrances",
                                    "image": "t_shirts"],
                                   ["cat_name": "Handbags & Cluthes",
                                    "image": "dress"],
                                   ["cat_name": "Watches",
                                    "image": "skirts"],
                                   ["cat_name": "Precious Jewellery",
                                    "image": "shorts"]]
        }
        else
        {
            arrSubSubCategories = [["cat_name": "Clothing",
                                    "image": "tops"],
                                   ["cat_name": "Footwear",
                                    "image": "t_shirts"],
                                   ["cat_name": "Watches",
                                    "image": "skirts"],
                                   ["cat_name": "Eyewear",
                                    "image": "shorts"]]
        }
        

        let nib = UINib(nibName: "SubSubCategoriesCollectionCell", bundle: nil)
        clcnViewSubSubCategroies.register(nib, forCellWithReuseIdentifier: "SubSubCategoriesCollectionCell")
        clcnViewSubSubCategroies.register(customCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "customCollectionReusableView")

//        arrSubCategories = [["cat_name":"Men","sub_cat_name":["Clothing", "Footwear", "Beauty & Wellness", "Fragrances", "Handbags & Cluthes", "Watches", "Precious Jewellery", "Fashion Jewellery", "Eyewear"]],
//                            ["cat_name":"Women","sub_cat_name":["Clothing", "Footwear", "Beauty & Wellness", "Fragrances", "Handbags & Cluthes", "Watches", "Precious Jewellery", "Fashion Jewellery", "Eyewear"]],
//                            ["cat_name":"Kids & Baby","sub_cat_name":["Clothing", "Footwear", "Beauty & Wellness", "Fragrances", "Handbags & Cluthes", "Watches", "Precious Jewellery", "Fashion Jewellery", "Eyewear"]]]
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: strTitle.uppercased(), leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearchClicked(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
        self.navigationItem.rightBarButtonItems = [menuButton1,menuButton2]
        
    }
    @IBAction func methodMyCart(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSubSubCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubSubCategoriesCollectionCell", for: indexPath) as! SubSubCategoriesCollectionCell
        
        let dic = (arrSubSubCategories.object(at: indexPath.item) as! NSDictionary)
        cell.lblName.text = "\(dic.object(forKey: "cat_name")!)"
        cell.imgUser.image = UIImage.init(named: "\(dic.object(forKey: "image")!)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/3)-8, height: (collectionView.frame.width/3)-8+25)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {

        let dic = (arrSubSubCategories.object(at: indexPath.item) as! NSDictionary)

        let loginScreen  =  SubSubCategoriesListScreen(nibName:"SubSubCategoriesListScreen",bundle:nil)
        loginScreen.strTitle = "\(self.strTitle.uppercased())" + "'s \(dic.object(forKey: "cat_name")!)"
        self.navigationController?.pushViewController(loginScreen, animated: true)
     
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

