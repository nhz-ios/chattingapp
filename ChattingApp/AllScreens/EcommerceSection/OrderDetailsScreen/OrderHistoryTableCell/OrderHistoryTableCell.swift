//
//  OrderHistoryTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 27/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OrderHistoryTableCell: UITableViewCell {

    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblProductDescription: UILabel!
    @IBOutlet var lblSize: UILabel!
    @IBOutlet var lblQty: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
