//
//  PaymentMethodTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 27/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class PaymentMethodTableCell: UITableViewCell {

    @IBOutlet var lblPaymentPaid: UILabel!
    @IBOutlet var lblDeliverdDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
