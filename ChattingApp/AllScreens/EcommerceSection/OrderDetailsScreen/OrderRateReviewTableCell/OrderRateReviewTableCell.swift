//
//  OrderRateReviewTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 02/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OrderRateReviewTableCell: UITableViewCell {

    
    @IBOutlet var viewRate: RateView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var txtViewReview: SZTextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewRate.starSize = 20
        self.viewRate.padding = 15
        self.viewRate.starCount = 5
        self.viewRate.step = 0.5
        self.viewRate.canRate = true
        self.viewRate.starNormalColor = UIColor.init(red: 196.0/255.0, green: 196.0/255.0, blue: 196.0/255.0, alpha: 1)
        self.viewRate.starFillColor = UIColor.colorAppTheam
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
