//
//  OrderDetailsScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 26/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OrderDetailsScreen: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate,RateViewDelegate
{
    @IBOutlet var tblOrders: UITableView!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var viewRateReview: UIView!
    @IBOutlet var viewInnerRateReview: UIView!
    @IBOutlet var tblRateReview: UITableView!
    @IBOutlet var toolBar: UIToolbar!

    override func viewDidLoad() {
        super.viewDidLoad()
        tblOrders.tableFooterView = UIView.init(frame: CGRect.zero)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "ORDER HISTORY", leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
        //        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"ecom_cart.png"), style: .plain, target: self, action: #selector(methodCartClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]
        tblOrders.reloadData()
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    @IBAction func methodCartClicked(_ sender: UIButton)
    {
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    
    //MARK:- UITableView DatSource And Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.tag == 555
        {
            return 2
        }
        return 7
    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
//    {
//        if tableView.tag == 555
//        {
//            return nil
//        }
//        return viewFooter
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
//    {
//        if tableView.tag == 555
//        {
//            return 0
//        }
//        return 180
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView.tag == 555
        {
            return 200
        }
        switch indexPath.row
        {
        case 0:
            return 80
        case 1:
            return 90
        case 5:
            return 100
        case 6:
            return 110
        default:
            return 35
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == 555
        {
            let cellIdentifier:String = "OrderRateReviewTableCell"
            var cell : OrderRateReviewTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderRateReviewTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderRateReviewTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderRateReviewTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            cell?.txtViewReview.inputAccessoryView = toolBar
            cell?.viewRate.delegate = self
           return cell!
            
        }
//
        switch indexPath.row {
        case 0:
            let cellIdentifier:String = "OrderUpperTableCell"
            var cell : OrderUpperTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderUpperTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderUpperTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderUpperTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            return cell!
        case 1:
            let cellIdentifier:String = "OrderHistoryTableCell"
            var cell : OrderHistoryTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderHistoryTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderHistoryTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderHistoryTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            return cell!
       case 5:
        let cellIdentifier:String = "PaymentMethodTableCell"
        var cell : PaymentMethodTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentMethodTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("PaymentMethodTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? PaymentMethodTableCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.zero
        }
        return cell!
       case 6:
        let cellIdentifier:String = "OrderDeliveredTableCell"
           var cell : OrderDeliveredTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OrderDeliveredTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OrderDeliveredTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OrderDeliveredTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            return cell!
        default:
            let cellIdentifier:String = "CartTotalCell"
            var cell : CartTotalCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CartTotalCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("CartTotalCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? CartTotalCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            var font = UIFont.init(name: "Lato-Regular", size: 14.0)
            cell?.lblSubTotal.textColor = UIColor.lightGray
            
            switch indexPath.row {
            case 2:
                cell?.lblSubTotal.textColor = UIColor.darkGray
                cell?.lblSubTotal.text = "Price"
                cell?.lblSubTotalPrice.text = "$200"
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                break
            case 3:
                cell?.lblSubTotal.textColor = UIColor.darkGray
                cell?.lblSubTotal.text = "Shipping Price"
                cell?.lblSubTotalPrice.text = "$30"
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                break
            case 4:
                cell?.lblSubTotal.textColor = UIColor.black
                cell?.lblSubTotal.text = "Total"
                cell?.lblSubTotalPrice.text = "$230"
                font = UIFont.init(name: "Lato-Semibold", size: 18.0)
                cell?.separatorInset = UIEdgeInsets.zero
                break
//            case 7:
//                cell?.lblOrderStatus.text = "Payment Failed"
//                cell?.lblOrderStatus.textColor =  .red
//                break
//            case 8:
//                cell?.lblOrderStatus.text = "Cancelled"
//                cell?.lblOrderStatus.textColor =  .orange
//                break
//            case 9:
//                cell?.lblOrderStatus.text = "Dispatched"
//                cell?.lblOrderStatus.textColor =  UIColor.colorBlueLight
//                break
            default:
                cell?.lblSubTotal.textColor = UIColor.lightGray
                cell?.lblSubTotal.text = "Sub-Total"
                cell?.lblSubTotalPrice.text = "$1400"
                break
            }
            
            cell?.lblSubTotal.font = font
            cell?.lblSubTotalPrice.font = font
            if (indexPath.row == 6)
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            }
            else
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }

            return cell!
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIButtonsMethod
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        viewRateReview.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        viewRateReview.endEditing(true)
    }
    

    @IBAction func methodRateReviewClicked(_ sender: UIButton)
    {
        //        ViewRateReview.removeFromSuperview()
        
        self.appDelegate.window!.addSubview(viewRateReview)
        viewRateReview.frame = self.appDelegate.window!.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewRateReview.addGestureRecognizer(tap)
    }
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        viewRateReview.endEditing(true)
       rec.view?.removeFromSuperview()
    }
    @IBAction func methodRateReviewSubmitClicked(_ sender: UIButton)
    {
        viewRateReview.endEditing(true)
        viewRateReview.removeFromSuperview()
    }
    @IBAction func methodRateReviewSkipClicked(_ sender: UIButton)
    {
        viewRateReview.endEditing(true)
       viewRateReview.removeFromSuperview()
    }
    //MARK:- UIGestureRecognizer Delegate Method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: viewInnerRateReview))! == true
        {
            return false
        }
        return true
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}






