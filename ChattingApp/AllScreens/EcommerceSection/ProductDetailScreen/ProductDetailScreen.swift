//
//  ProductDetailScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 25/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import MapKit

class ProductDetailScreen: UIViewController, UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate, MKMapViewDelegate, UICollectionViewDataSource,UICollectionViewDelegate
{
    @IBOutlet weak var tblProductDetail:  UITableView!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var btnCompanyProfile:  UIButton!
    @IBOutlet var btnItemDetails:  UIButton!
    @IBOutlet var btnRateReviews:  UIButton!
    
    @IBOutlet var viewTradeAssurance: UIView!
    @IBOutlet var viewInnerTradeAssurance: UIView!
    @IBOutlet var btnCloseTradeAssurance: UIButton!
    
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    
    var arrProducts = NSMutableArray()
    
    var arrSubCategories = NSMutableArray()
    var strTitle : String = "MECHANT DETAIL"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblProductDetail.tableFooterView = UIView.init(frame: CGRect.zero)
        
        arrProducts = [["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "15 Apr 2018",
                        "count": "3"],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "Stylis peach dress",
                        "price": "$150",
                        "image": "dress_3",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "Princess gown",
                        "price": "$550",
                        "image": "dress_4",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "2 Feb 2018",
                        "count": ""]]
    }
    
    @IBAction func methodShowTradeAssuranceClicked(_ sender: UIButton)
    {
        self.appDelegate.window!.addSubview(viewTradeAssurance)
        viewTradeAssurance.frame = self.appDelegate.window!.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewTradeAssurance.addGestureRecognizer(tap)
    }
    
    @IBAction func methodAddToCartClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodAddtoWishlistClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        let register = MyWishListScreen.init(nibName: "MyWishListScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }

    @IBAction func methodCloseTradeAssuranceClicked(_ sender: UIButton)
    {
        viewTradeAssurance.removeFromSuperview()
    }
    
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }
    //MARK:- UIGestureRecognizer Delegate Method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: viewInnerTradeAssurance))! == true
        {
            return false
        }
        return true
    }
    
    @IBAction func methodHeaderButtonsClicked(_ sender: UIButton)
    {
        if sender.isSelected == true
        {
            return
        }
        btnCompanyProfile.isSelected = false
        btnItemDetails.isSelected = false
        btnRateReviews.isSelected = false
        btnCompanyProfile.backgroundColor = UIColor.clear
        btnItemDetails.backgroundColor = UIColor.clear
        btnRateReviews.backgroundColor = UIColor.clear
        switch sender.tag {
        case 1:
            btnCompanyProfile.backgroundColor = UIColor.colorAppTheam
            break
        case 2:
            btnItemDetails.backgroundColor = UIColor.colorAppTheam
            break
        case 3:
            btnRateReviews.backgroundColor = UIColor.colorAppTheam
            break
        default:
            break
        }
        sender.isSelected = true
        tblProductDetail.reloadData()
        //        let register = EcomFilterScreen.init(nibName: "EcomFilterScreen", bundle: nil)
        //        self.navigationController?.pushViewController(register, animated: true)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "PRODUCT DETAIL", leftbuttonImageName: "back.png")
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
        //        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage.init(named:"ecom_cart.png")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodMyCart(_:)))
        // Add the rightBarButtonItems on the navigation bar
        
        self.navigationItem.rightBarButtonItems = [menuButton1, menuButton2]
    }
    @IBAction func methodMyCart(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            if btnCompanyProfile.isSelected == true
            {
                return 10
            }
            else if btnItemDetails.isSelected == true
            {
                return 14
            }
            else
            {
                return 9
            }
        }
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            return viewHeader
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50
        }
        return 0
    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        
//        if section == 1 {
//            return viewFooter
//        }
//        return nil
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if section == 1 {
//            return 40
//        }
//        return 0
//    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 450
        }
        if btnCompanyProfile.isSelected == true
        {
            if indexPath.row == 0
            {
                return 80
            }
            else if indexPath.row == 6
            {
                return 40
            }
            else if indexPath.row == 9
            {
                return 200
            }
            else
            {
                return 40
            }
        }
        else if btnItemDetails.isSelected == true
        {
            if indexPath.row == 1
            {
                return 120
            }
            if indexPath.row == 5
            {
                return 50
//                return ScreenSize.SCREEN_WIDTH <= 375 ? 50 : 40
            }
            return 40
        }
        else
        {
            return 100
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0
        {
            let cellIdentifier:String = "ProductDetailTableCell"
            var cell : ProductDetailTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ProductDetailTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("ProductDetailTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? ProductDetailTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.btnShowTradeAssurance.addTarget(self, action: #selector(methodShowTradeAssuranceClicked(_:)), for: .touchUpInside)
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            return cell!
        }
        else
        {
            if btnCompanyProfile.isSelected == true
            {
                if indexPath.row == 0
                {
                    let cellIdentifier:String = "MerchantDetailTableCell"
                    var cell : MerchantDetailTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantDetailTableCell
                    if (cell == nil)
                    {
                        let nib:Array = Bundle.main.loadNibNamed("MerchantDetailTableCell", owner: nil, options: nil)! as [Any]
                        cell = nib[0] as? MerchantDetailTableCell
                        cell?.selectionStyle = UITableViewCellSelectionStyle.none
                        cell?.backgroundColor = (UIColor.clear)
                        cell?.layer.masksToBounds = true
                   }
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
//                    cell?.btnShowTradeAssurance.addTarget(self, action: #selector(methodShowTradeAssuranceClicked(_:)), for: .touchUpInside)
                    //            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
                    return cell!
                }
                else if indexPath.row == 6
                {
                    let cellIdentifier:String = "TradeAssuranceTableCell"
                    var cell : TradeAssuranceTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TradeAssuranceTableCell
                    if (cell == nil)
                    {
                        let nib:Array = Bundle.main.loadNibNamed("TradeAssuranceTableCell", owner: nil, options: nil)! as [Any]
                        cell = nib[0] as? TradeAssuranceTableCell
                        cell?.selectionStyle = UITableViewCellSelectionStyle.none
                        cell?.backgroundColor = (UIColor.clear)
                        cell?.layer.masksToBounds = true
                   }
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                    //                    cell?.btnShowTradeAssurance.addTarget(self, action: #selector(methodShowTradeAssuranceClicked(_:)), for: .touchUpInside)
                    //            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
                    return cell!
                }
                else if indexPath.row == 9
                {
                    let cellIdentifier:String = "EcomHomeProductsCell"
                    var cell : EcomHomeProductsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EcomHomeProductsCell
                    if (cell == nil)
                    {
                        let nib:Array = Bundle.main.loadNibNamed("EcomHomeProductsCell", owner: nil, options: nil)! as [Any]
                        cell = nib[0] as? EcomHomeProductsCell
                        cell!.selectionStyle = UITableViewCellSelectionStyle.none
                        cell?.backgroundColor = (UIColor.clear)
                    }
                    cell?.mapView.isHidden = false
                    cell?.mapView.mapType = .satellite
                    cell?.clcnViewProduct.isHidden = true
                    let nib = UINib(nibName: "ProductCollectionCell", bundle: nil)
                    cell?.clcnViewProduct.register(nib, forCellWithReuseIdentifier: "ProductCollectionCell")
                    cell?.clcnViewProduct.register(ProductCollectionCell.self, forCellWithReuseIdentifier: "ProductCollectionCell")
                    cell?.clcnViewProduct.delegate = self
                    cell?.clcnViewProduct.dataSource = self
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)

                    var region = MKCoordinateRegion()
                    var ltdf = 0.0
                    var lntdf = 0.0
                    region.center.latitude = ltdf as? CLLocationDegrees ?? CLLocationDegrees()
                    region.center.longitude = lntdf as? CLLocationDegrees ?? CLLocationDegrees()
                    // for zooming the map
                    region.span.longitudeDelta = 100.0 as? CLLocationDegrees ?? CLLocationDegrees()
                    region.span.latitudeDelta = 90.0 as? CLLocationDegrees ?? CLLocationDegrees()
                    cell?.mapView.setRegion(region, animated: true)
                    cell?.mapView.delegate = self
                    return cell!
                }
                else
                {
                    let cellIdentifier:String = "MerchantCompanyProfileCell"
                    var cell : MerchantCompanyProfileCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantCompanyProfileCell
                    if (cell == nil)
                    {
                        let nib:Array = Bundle.main.loadNibNamed("MerchantCompanyProfileCell", owner: nil, options: nil)! as [Any]
                        cell = nib[0] as? MerchantCompanyProfileCell
                        cell!.selectionStyle = UITableViewCellSelectionStyle.none
                        cell?.backgroundColor = (UIColor.clear)
                    }
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)

                    cell?.lblName.isHidden = false
                    cell?.lblDescription.isHidden = false
                    cell?.lblCompanyOverview.isHidden = true
                    
                    
                    switch(indexPath.row - 1)
                    {
                    case 0:
                        cell?.lblName.isHidden = true
                        cell?.lblDescription.isHidden = true
                        cell?.lblCompanyOverview.isHidden = false
                        cell?.lblCompanyOverview.text = "Company Overview"
                        break
                    case 1:
                        cell?.lblName.text = "Business Type"
                        cell?.lblDescription.text = "Manufacturer, Trading Company"
                        break
                    case 2:
                        cell?.lblName.text = "Year Established:"
                        cell?.lblDescription.text = "2011"
                        break
                    case 3:
                        cell?.lblName.text = "Total Annual Revenue:"
                        cell?.lblDescription.text = "US$10 Million - US$50 Million"
                        break
                    case 4:
                        cell?.lblName.text = "Total Employees:"
                        cell?.lblDescription.text = "101 - 200 People"
                        break
                    case 5:
                        cell?.lblName.text = "Trade Assurance:"
                        cell?.lblDescription.text = "101 - 200 People"
                        break
                    case 6:
                        cell?.lblName.isHidden = false
                        cell?.lblDescription.isHidden = true
                        cell?.lblCompanyOverview.isHidden = true
                        
                        cell?.lblName.text = "Let us protect your orders from payment to delivery. Start your safe journey today!"
                        break
                    case 7:
                        cell?.lblName.isHidden = true
                        cell?.lblDescription.isHidden = true
                        cell?.lblCompanyOverview.isHidden = false
                        cell?.lblCompanyOverview.text = "Company Overview"
                        break
                    default:
                        break
                    }
                    return cell!
                }
            }
            else if btnItemDetails.isSelected == true
            {
                let cellIdentifier:String = "MerchantCompanyProfileCell"
                var cell : MerchantCompanyProfileCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantCompanyProfileCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("MerchantCompanyProfileCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? MerchantCompanyProfileCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                if (indexPath.row == 14 - 1)
                {
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                }
                else
                {
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                }
                cell?.lblName.isHidden = false
                cell?.lblDescription.isHidden = false
                cell?.lblCompanyOverview.isHidden = true
                
                
                switch(indexPath.row)
                {
                case 0:
                    cell?.lblName.isHidden = true
                    cell?.lblDescription.isHidden = true
                    cell?.lblCompanyOverview.isHidden = false
                    cell?.lblCompanyOverview.text = "Description"
                    break
                case 1:
                    cell?.lblName.isHidden = false
                    cell?.lblDescription.isHidden = true
                    cell?.lblCompanyOverview.isHidden = true

                    cell?.lblName.text = "It is a long established fact that a reader will be distracted by the read-able content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like read-able English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'Iorem ipsum' will uncover many web sites still in their infancy. Various versions"
                    break
                case 2:
                    cell?.lblName.isHidden = true
                    cell?.lblDescription.isHidden = true
                    cell?.lblCompanyOverview.isHidden = false
                    cell?.lblCompanyOverview.text = "Item Details"
                    break
                case 3:
                    cell?.lblName.text = "Port:"
                    cell?.lblDescription.text = "Guangzhou,China"
                    break
                case 4:
                    cell?.lblName.text = "Supply Ability:"
                    cell?.lblDescription.text = "5000 Piece/Pieces per Month"
                    break
                case 5:
                    cell?.lblName.text = "Payment Terms:"
                    cell?.lblDescription.text = "T/T,Western Union,MoneyGram, PayPal payments for offline orders."
                    break
                case 6:
                    cell?.lblName.text = "Supply Type:"
                    cell?.lblDescription.text = "In-Stock Items"
                    break
                case 7:
                    cell?.lblName.text = "Place of Origin:"
                    cell?.lblDescription.text = "Guangdong, China (Mainland)"
                    break
                case 8:
                    cell?.lblName.text = "Brand Name:"
                    cell?.lblDescription.text = "SABADO"
                    break
                case 9:
                    cell?.lblName.text = "Model Number:"
                    cell?.lblDescription.text = "YF-X7"
                    break
               case 10:
                    cell?.lblName.text = "Feature:"
                    cell?.lblDescription.text = "Anti-Shrink, Breathable"
                    break
                case 11:
                    cell?.lblName.text = "Shell Fabric"
                    cell?.lblDescription.text = "Soft Shell"
                    break
                case 12:
                    cell?.lblName.text = "Color:"
                    cell?.lblDescription.text = "Black/Red/Green/White"
                    break
                case 13:
                    cell?.lblName.text = "Size:"
                    cell?.lblDescription.text = "S-XXXL"
                    break
                default:
                    break
                    
                }
                return cell!
            }
            else
            {
                let cellIdentifier:String = "MerchantRateReviewCell"
                var cell : MerchantRateReviewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantRateReviewCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("MerchantRateReviewCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? MerchantRateReviewCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                    cell?.separatorInset = UIEdgeInsets.zero
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                }
                return cell!
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        
        cell.lblName.text = ""
        cell.lblPrice.text = ""
        cell.imgUser.image = UIImage.init(named: "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 0, height: 0)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}





