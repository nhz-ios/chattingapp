//
//  MyCartScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class MyCartScreen: UIViewController , UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblCart:  UITableView!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var btnProceedToCheckout:  UIButton!
//    var arrCartItems = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblCart.tableFooterView = UIView.init(frame: CGRect.zero)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "SHOPPING CART", leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearchClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]
        
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    
    @IBAction func methodProceedToCheckoutClicked(_ sender: UIButton)
    {
        let register = PaymentScreen.init(nibName: "PaymentScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row >= 4
        {
            return 44
        }
        return 101
    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
//    {
//        return viewFooter
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
//    {
//        return 80
//    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6
        {
            let cellIdentifier:String = "CartTotalCell"
            var cell : CartTotalCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CartTotalCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("CartTotalCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? CartTotalCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            var font = UIFont.init(name: "Lato-Semibold", size: 15.0)
            cell?.lblSubTotal.textColor = UIColor.lightGray

            if indexPath.row == 4
            {
                cell?.lblSubTotal.textColor = UIColor.lightGray
                cell?.lblSubTotal.text = "Sub-Total"
                cell?.lblSubTotalPrice.text = "$1400"
           }
            else if indexPath.row == 5
            {
                cell?.lblSubTotal.textColor = UIColor.lightGray
                cell?.lblSubTotal.text = "Tax"
                cell?.lblSubTotalPrice.text = "$20"
            }
            else if indexPath.row == 6
            {
                cell?.lblSubTotal.textColor = UIColor.black
                cell?.lblSubTotal.text = "Total"
                cell?.lblSubTotalPrice.text = "$1420"
                font = UIFont.init(name: "Lato-Semibold", size: 18.0)
                
           }
            cell?.lblSubTotal.font = font
            cell?.lblSubTotalPrice.font = font
            if (indexPath.row == 6)
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            }
            else
            {
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            return cell!

        }
          let cellIdentifier:String = "CartItemTableCell"
        var cell : CartItemTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CartItemTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("CartItemTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? CartItemTableCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
        cell?.imgItem.layer.cornerRadius = (cell?.imgItem.frame.size.height)!/2
        cell?.imgItem.layer.masksToBounds = true
        
        return cell!
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

