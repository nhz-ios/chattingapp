//
//  PaymentScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 26/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class PaymentScreen: UIViewController, UITableViewDataSource, UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate
{
    @IBOutlet var tblPayment: UITableView!
    @IBOutlet var viewHeaderBack: UIView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var btnShipping: UIButton!
    @IBOutlet var btnPayment: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var btnContinue:  UIButton!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var pickerAllType: UIPickerView!

    var isSelectedTxtF : Int = 0
    var strCountry = ""
    var strCardType = ""
    
    var arrCountry = NSArray()
    var arrCardType = NSArray()

    var isSavedAddress = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        arrCardType = ["Visa"]
        arrCountry = ["India", "United State", "Canada", "United Kingdom", "Afghanistan", "Austria", "Bangladesh", "China", "Italy", "Japan"]

        tblPayment.tableFooterView = UIView.init(frame: CGRect.zero)
      // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "PAYMENT", leftbuttonImageName: "back.png")
    }

    //MARK:- Button Click Method
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
        
        let indexSltd = pickerAllType.selectedRow(inComponent: 0)
        
        if isSelectedTxtF == 5
        {
            strCountry = arrCountry.object(at: indexSltd) as! String
            tblPayment.reloadData()
        }
        else if isSelectedTxtF == 9
        {
            strCardType = arrCardType.object(at: indexSltd) as! String
            tblPayment.reloadData()
        }
    }
    
    @IBAction func methodContinueButton(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if sender.currentTitle == "CONTINUE"
        {
            sender.setTitle("PAYMENT", for: UIControlState.normal)
            self.methodTopHeaderButtons(btnPayment)
        }
        else if sender.currentTitle == "PAYMENT"
        {
            sender.setTitle("DONE", for: UIControlState.normal)
            self.methodTopHeaderButtons(btnConfirm)
        }
        else if sender.currentTitle == "DONE"
        {
//            self.navigationController?.popToRootViewController(animated: true)
            let loginScreen  =  OrderDetailsScreen(nibName:"OrderDetailsScreen",bundle:nil)
            //            loginScreen.strTitle = "\(arrMerchants.object(at: indexPath.item))"
            self.navigationController?.pushViewController(loginScreen, animated: true)
        }
    }
    
    @IBAction func methodTopHeaderButtons(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if btnContinue.currentTitle == "CONTINUE" || (btnContinue.currentTitle == "DONE" && sender.tag != 103)
        {
            return
        }
        else if btnContinue.currentTitle == "PAYMENT" && sender.tag == 103
        {
            return
        }
        if sender.isSelected == true
        {
            return
        }
        self.view.endEditing(true)
        btnShipping.backgroundColor = UIColor.clear
        btnPayment.backgroundColor = UIColor.clear
        btnConfirm.backgroundColor = UIColor.clear
        
        btnShipping.isSelected = false
        btnPayment.isSelected = false
        btnConfirm.isSelected = false
        
        if sender.tag == 101
        {
            btnShipping.backgroundColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            btnContinue.setTitle("CONTINUE", for: UIControlState.normal)
        }
        else if sender.tag == 102
        {
            btnPayment.backgroundColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            btnContinue.setTitle("PAYMENT", for: UIControlState.normal)
        }
        else
        {
            btnConfirm.backgroundColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            btnContinue.setTitle("DONE", for: UIControlState.normal)
        }
        sender.isSelected = true
        tblPayment.reloadData()
    }
    @IBAction func methodPayByWalletClicked(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func methodSavedAddressClicked(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        
        self.isSavedAddress = sender.isSelected
        self.tblPayment.reloadData()
    }
    //MARK:- UITableView Delegate Method
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if btnShipping.isSelected == true
        {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if btnShipping.isSelected == true
        {
            if section == 0
            {
                return 2
            }
            else
            {
                
                return self.isSavedAddress == true ? 0 : 8
            }
        }
        else if btnPayment.isSelected == true
        {
            return 7
        }
        else
        {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if btnShipping.isSelected == true && section == 1
        {
            return nil
        }
        else
        {
            return viewHeader
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if btnShipping.isSelected == true && section == 1
        {
            return 0
        }
        else
        {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if btnShipping.isSelected == true && section == 0
        {
            return nil
        }
        else
        {
            return viewFooter
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if btnShipping.isSelected == true && section == 0
        {
            return 0
        }
        else
        {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if btnShipping.isSelected == true
        {
            if indexPath.section == 0
            {
                if indexPath.row == 0
                {
                    return 20
                }
                return 44
            }
            else
            {
                if indexPath.row == 0
                {
                    return 20
                }
                return 82
            }
        }
        else if btnPayment.isSelected == true
        {
            if indexPath.row == 0
            {
                return 100
            }
            else if indexPath.row == 1
            {
                return 90
            }
            else if indexPath.row == 2
            {
                return 40
            }
            else
            {
                return 82
            }
        }
        else
        {
            return 320
        }
     }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if btnShipping.isSelected == true
        {
            if indexPath.row == 0
            {
                let cellIdentifier:String = "CartTotalCell"
                var cell : CartTotalCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CartTotalCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("CartTotalCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? CartTotalCell
                    cell?.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                cell?.lblSubTotalPrice.isHidden = true
                cell?.lblSubTotal.font = UIFont.init(name: "Lato-SemiBold", size: 14.0)
                if indexPath.section == 0
                {
                    cell?.lblSubTotal.text = "Saved Address"
                }
                else
                {
                    cell?.lblSubTotal.text = "New Address"
                }
                return cell!
                
            }
            else
            {
                if indexPath.section == 0
                {
                    let cellIdentifier:String = "PaymentAddressTableCell"
                    var cell : PaymentAddressTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentAddressTableCell
                    if (cell == nil)
                    {
                        let nib:Array = Bundle.main.loadNibNamed("PaymentAddressTableCell", owner: nil, options: nil)! as [Any]
                        cell = nib[0] as? PaymentAddressTableCell
                        cell?.selectionStyle = UITableViewCellSelectionStyle.none
                        cell?.backgroundColor = (UIColor.clear)
                    }
                    cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
                    cell?.btnSavedAddress.addTarget(self, action: #selector(methodSavedAddressClicked(_:)), for: .touchUpInside)
                    cell?.btnSavedAddress.isSelected = self.isSavedAddress
                    return cell!
               }
            }
            let cellIdentifier:String = "PaymentTableCell"
            var cell : PaymentTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("PaymentTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? PaymentTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            cell?.viewDouble.isHidden = true
            cell?.viewSingle.isHidden = true
            cell?.imgSingleName.isHidden = true
            cell?.txtDoubleMonth.isHidden = true
            cell?.txtDoubleYear.isHidden = true
            cell?.txtDoubleFirstName.isHidden = false
            cell?.txtSingleName.inputAccessoryView = toolBar
            cell?.txtDoubleFirstName.inputAccessoryView = toolBar
            cell?.txtDoubleLastName.inputAccessoryView = toolBar
            cell?.txtDoubleMonth.inputAccessoryView = toolBar
            cell?.txtDoubleYear.inputAccessoryView = toolBar
            cell?.txtSingleName.delegate = self
            cell?.txtDoubleFirstName.delegate = self
            cell?.txtDoubleLastName.delegate = self
            cell?.txtDoubleMonth.delegate = self
            cell?.txtDoubleYear.delegate = self
            cell?.txtSingleName.text = ""
            cell?.txtViewReview.isHidden = true

            cell?.txtSingleName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleFirstName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleLastName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleMonth.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleYear.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)

            switch(indexPath.row-1)
            {
            case 0:
                cell?.viewDouble.isHidden = false
                cell?.lblDoubleFirstName.text = "First Name"
                cell?.lblDoubleLastName.text = "Last Name"
                cell?.txtDoubleFirstName.placeholder = "Type First Name"
                cell?.txtDoubleLastName.placeholder = "Type Last Name"
                cell?.txtDoubleFirstName.keyboardType = .default
                cell?.txtDoubleLastName.keyboardType = .default
                cell?.txtDoubleFirstName.tag = 0
                cell?.txtDoubleLastName.tag = 1
                break
            case 1:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Phone Number"
                cell?.txtSingleName.placeholder = "Type Phone Number"
                cell?.txtSingleName.keyboardType = .numberPad
                cell?.txtSingleName.tag = 2
                break
            case 2:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Email Address"
                cell?.txtSingleName.placeholder = "Type Email Address"
                cell?.txtSingleName.keyboardType = .emailAddress
                cell?.txtSingleName.tag = 3
                break
            case 3:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Address"
                cell?.txtSingleName.placeholder = "Type Address"
                cell?.txtSingleName.keyboardType = .default
                cell?.txtSingleName.tag = 4
                break
            case 4:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Country"
                cell?.txtSingleName.placeholder = "Select Country"
                cell?.txtSingleName.inputView = pickerAllType
                cell?.txtSingleName.tag = 5
                cell?.txtSingleName.text = strCountry
                break
            case 5:
                cell?.viewDouble.isHidden = false
                cell?.lblDoubleFirstName.text = "State"
                cell?.lblDoubleLastName.text = "City"
                cell?.txtDoubleFirstName.placeholder = "Type State Name"
                cell?.txtDoubleLastName.placeholder = "Type City Name"
                cell?.txtDoubleFirstName.keyboardType = .default
                cell?.txtDoubleLastName.keyboardType = .default
                cell?.txtDoubleFirstName.tag = 6
                cell?.txtDoubleLastName.tag = 7
                break
            case 6:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Zip Code"
                cell?.txtSingleName.placeholder = "Type Zip Code"
                cell?.txtSingleName.keyboardType = .numberPad
                cell?.txtSingleName.tag = 8
               break
            default:
                break
            }
            return cell!
        }
        else if btnPayment.isSelected == true
        {
            if indexPath.row == 2
            {
                let cellIdentifier:String = "MerchantCompanyProfileCell"
                var cell : MerchantCompanyProfileCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantCompanyProfileCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("MerchantCompanyProfileCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? MerchantCompanyProfileCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
                cell?.lblName.isHidden = true
                cell?.lblDescription.isHidden = true
                cell?.lblCompanyOverview.isHidden = false
                cell?.lblCompanyOverview.text = "Card Detail"
                return cell!
            }
            else if indexPath.row == 0 || indexPath.row == 1
            {
                let cellIdentifier:String = "PayByWalletTableCell"
                var cell : PayByWalletTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PayByWalletTableCell
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("PayByWalletTableCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? PayByWalletTableCell
                    cell!.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)

                cell?.viewGray.isHidden = true
                cell?.viewWhite.isHidden = true
                cell?.btnPayByWallet.addTarget(self, action: #selector(methodPayByWalletClicked(_:)), for: .touchUpInside)

                if indexPath.row == 0
                {
                    cell?.viewGray.isHidden = false
                }
                else
                {
                    cell?.viewWhite.isHidden = false
                }
                return cell!
            }
            let cellIdentifier:String = "PaymentTableCell"
            var cell : PaymentTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("PaymentTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? PaymentTableCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            cell?.viewDouble.isHidden = true
            cell?.viewSingle.isHidden = true
            cell?.imgSingleName.isHidden = true
            cell?.txtDoubleMonth.isHidden = true
            cell?.txtDoubleYear.isHidden = true
            cell?.txtDoubleFirstName.isHidden = false
            cell?.txtSingleName.inputAccessoryView = toolBar
            cell?.txtDoubleFirstName.inputAccessoryView = toolBar
            cell?.txtDoubleLastName.inputAccessoryView = toolBar
            cell?.txtDoubleMonth.inputAccessoryView = toolBar
            cell?.txtDoubleYear.inputAccessoryView = toolBar
//            cell?.txtSingleName.inputView = .default
            cell?.txtSingleName.delegate = self
            cell?.txtDoubleFirstName.delegate = self
            cell?.txtDoubleLastName.delegate = self
            cell?.txtDoubleMonth.delegate = self
            cell?.txtDoubleYear.delegate = self
            cell?.txtSingleName.text = ""
            cell?.txtViewReview.isHidden = true

            cell?.txtSingleName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleFirstName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleLastName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleMonth.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            cell?.txtDoubleYear.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            
            switch(indexPath.row - 3)
            {
            case 0:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Card Type"
                cell?.txtSingleName.placeholder = "Visa"
                cell?.imgSingleName.isHidden = false
                cell?.txtSingleName.inputView = pickerAllType
                cell?.txtSingleName.tag = 9
                cell?.txtSingleName.text = strCardType
               break
            case 1:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Card Number"
                cell?.txtSingleName.placeholder = "Type Card Number"
                cell?.txtSingleName.keyboardType = .numberPad
                cell?.txtSingleName.tag = 10
                break
            case 2:
                cell?.viewDouble.isHidden = false
                cell?.txtDoubleMonth.isHidden = false
                cell?.txtDoubleYear.isHidden = false
                cell?.txtDoubleFirstName.isHidden = true
                cell?.lblDoubleFirstName.text = "Exp. Date"
                cell?.lblDoubleLastName.text = "CVV Number"
                cell?.txtDoubleMonth.placeholder = "MM"
                cell?.txtDoubleYear.placeholder = "YYYY"
                cell?.txtDoubleLastName.placeholder = "Type CVV Number"
                cell?.txtDoubleMonth.keyboardType = .numberPad
                cell?.txtDoubleYear.keyboardType = .numberPad
                cell?.txtDoubleLastName.keyboardType = .numberPad
                cell?.txtDoubleMonth.tag = 11
                cell?.txtDoubleYear.tag = 12
                cell?.txtDoubleLastName.tag = 13
                break
            case 3:
                cell?.viewSingle.isHidden = false
                cell?.lblSingleName.text = "Card Holder Name"
                cell?.txtSingleName.placeholder = "Type Card Holder Name"
                cell?.txtSingleName.keyboardType = .default
                cell?.txtSingleName.tag = 14
                break
            default:
                break
            }
            return cell!
        }
        let cellIdentifier:String = "PaymentConfirmTableCell"
        var cell : PaymentConfirmTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentConfirmTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("PaymentConfirmTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? PaymentConfirmTableCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
        //            cell?.btnShowTradeAssurance.addTarget(self, action: #selector(methodShowTradeAssuranceClicked(_:)), for: .touchUpInside)
        return cell!
    }

    // MARK: - UIPickerView Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        return  isSelectedTxtF == 5 ? arrCountry.count : arrCardType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return isSelectedTxtF == 5 ? arrCountry.object(at: row) as? String : arrCardType.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == 5
        {
            isSelectedTxtF = 5
            // methodPickerReload()
        }
        else if textField.tag == 9
        {
            isSelectedTxtF = 9
            // methodPickerReload()
        }
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //        if range.location == 0 && string == " "
        //        {
        //            return false
        //        }
        
        if string == " " && (textField.tag != 4)
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        
        if (textField.tag == 0) || (textField.tag == 1) || (textField.tag == 6) || (textField.tag == 7)
        {
            return newLength > kFirstNameLength ? false : true
        }
        if (textField.tag == 1)
        {
            return newLength > kLastNameLength ? false : true
        }
        if (textField.tag == 2)
        {
            return newLength > kPhoneLength ? false : true
        }
        if (textField.tag == 3)
        {
            return newLength > kEmailLength ? false : true
        }
        if (textField.tag == 4)
        {
            return newLength > kAddressLength ? false : true
        }
        if (textField.tag == 8)
        {
            return newLength > kPinCodeLength ? false : true
        }
        if (textField.tag == 10)
        {
            return newLength > kCardNumberLength ? false : true
        }
        if (textField.tag == 11)
        {
            return newLength > kMonthLength ? false : true
        }
        if (textField.tag == 12)
        {
            return newLength > kYearLength ? false : true
        }
        if (textField.tag == 13)
        {
            return newLength > kCVVLength ? false : true
        }
        if (textField.tag == 14)
        {
             if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
