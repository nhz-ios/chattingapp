//
//  PaymentTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 26/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class PaymentTableCell: UITableViewCell {

    @IBOutlet var viewSingle: UIView!
    @IBOutlet var imgSingleName: UIImageView!
    @IBOutlet var lblSingleName: UILabel!
    @IBOutlet var txtSingleName: NHZPaymentTextField!
    @IBOutlet var txtViewReview: SZTextView!
    @IBOutlet var viewSinglePhone: UIView!
    @IBOutlet var txtSingleCountryCode: NHZPaymentTextField!
    @IBOutlet var txtSinglePhone: NHZPaymentTextField!

    @IBOutlet var viewDouble: UIView!
    @IBOutlet var lblDoubleFirstName: UILabel!
    @IBOutlet var txtDoubleFirstName: NHZPaymentTextField!
    @IBOutlet var txtDoubleMonth: NHZPaymentTextField!
    @IBOutlet var txtDoubleYear: NHZPaymentTextField!

    @IBOutlet var lblDoubleLastName: UILabel!
    @IBOutlet var txtDoubleLastName: NHZPaymentTextField!

    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
