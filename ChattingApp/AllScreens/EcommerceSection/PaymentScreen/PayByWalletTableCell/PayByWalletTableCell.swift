//
//  PayByWalletTableCell.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 26/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class PayByWalletTableCell: UITableViewCell {

    @IBOutlet var viewGray: UIView!
    @IBOutlet var lblTotalPayment: UILabel!
    @IBOutlet var viewWhite: UIView!
    @IBOutlet var btnPayByWallet: UIButton!
    @IBOutlet var lblCurrentBalance: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
