//
//  ProductsListScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 21/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ProductsListScreen: UIViewController, UITableViewDataSource, UITableViewDelegate , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UIGestureRecognizerDelegate, NHRangeSliderViewDelegate
{
    @IBOutlet var tblProducts: UITableView!
    @IBOutlet var headerViewProducts: UIView!
    @IBOutlet var ViewLocation: UIView!
    @IBOutlet var ViewInnerLocation: UIView!
    @IBOutlet var sliderLocationRange: NHRangeSlider!
    @IBOutlet var lblMinLocationRange: UILabel!
    @IBOutlet var lblMaxLocationRange: UILabel!
    @IBOutlet var viewCartButton: UIView!
    @IBOutlet var lblCartButton: UILabel!
    @IBOutlet var btnCartButton: UIButton!
    

    var arrProducts = NSMutableArray()
    var strTitle : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrProducts = [["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "15 Apr 2018",
                        "count": "3"],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "Stylis peach dress",
                        "price": "$150",
                        "image": "dress_3",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "Princess gown",
                        "price": "$550",
                        "image": "dress_4",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Blue slim dress",
                        "price": "$350",
                        "image": "dress_1",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Lady long blue",
                        "price": "$350",
                        "image": "dress_2",
                        "time": "2 Feb 2018",
                        "count": ""]]
       
        
    }
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: strTitle.uppercased(), leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearchClicked(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(customView: viewCartButton)
//        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"ecom_cart.png"), style: .plain, target: self, action: #selector(methodCartClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1,menuButton2]
        tblProducts.reloadData()
    }
    @objc func methodSearchClicked(_ sender: UIButton)
    {
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
    }
    @IBAction func methodCartClicked(_ sender: UIButton)
    {
        let register = MyCartScreen.init(nibName: "MyCartScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    @IBAction func methodFilerClicked(_ sender: UIButton)
    {
        let register = EcomFilterScreen.init(nibName: "EcomFilterScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodNearbyProductsClicked(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        //        let register = EcomFilterScreen.init(nibName: "EcomFilterScreen", bundle: nil)
        //        self.navigationController?.pushViewController(register, animated: true)
    }

    @IBAction func methodLocationClicked(_ sender: UIButton)
    {
//        ViewLocation.removeFromSuperview()
        
        self.appDelegate.window!.addSubview(ViewLocation)
        ViewLocation.frame = self.appDelegate.window!.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        ViewLocation.addGestureRecognizer(tap)
    }
    
    @IBAction func methodListClicked(_ sender: UIButton)
    {
//        let register = EcomFilterScreen.init(nibName: "EcomFilterScreen", bundle: nil)
//        self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodSliderValueChanged(_ sender: NHRangeSlider)
    {
        lblMinLocationRange.text = Int(sender.lowerValue) == 1 ? "\(Int(sender.lowerValue)) Mile" : "\(Int(sender.lowerValue)) Miles"
        lblMaxLocationRange.text = Int(sender.upperValue) == 1 ? "\(Int(sender.upperValue)) Mile" : "\(Int(sender.upperValue)) Miles"
        //        let register = EcomFilterScreen.init(nibName: "EcomFilterScreen", bundle: nil)
        //        self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodApplyLocationClicked(_ sender: UIButton)
    {
        ViewLocation.removeFromSuperview()
    }
    
    //MARK:- UIGestureRecognizer Delegate Method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: ViewInnerLocation))! == true
        {
            return false
        }
        return true
    }
   
    //MARK:- UITableView DatSource And Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return headerViewProducts
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return tableView.frame.size.height - 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier:String = "EcomHomeProductsCell"
        var cell : EcomHomeProductsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EcomHomeProductsCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("EcomHomeProductsCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? EcomHomeProductsCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
            cell?.separatorInset = UIEdgeInsets.zero
        }
        let nib = UINib(nibName: "ProductCollectionCell", bundle: nil)
        cell?.clcnViewProduct.register(nib, forCellWithReuseIdentifier: "ProductCollectionCell")
        cell?.mapView.isHidden = true
        cell?.clcnViewProduct.isHidden = false

        cell?.clcnViewProduct.delegate = self
        cell?.clcnViewProduct.dataSource = self
        cell?.clcnViewProduct.reloadData()
        
        return cell!
    }
    
    //MARK:- UICollectionView DatSource And Delegate Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        
        cell.lblName.text = "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "name")!)"
        cell.lblPrice.text = "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "price")!)"
        cell.imgUser.image = UIImage.init(named: "\((arrProducts.object(at: indexPath.row) as! NSDictionary).object(forKey: "image")!)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/2)-8, height: (collectionView.frame.width/2)-8+55)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let loginScreen  =  ProductDetailScreen(nibName:"ProductDetailScreen",bundle:nil)
        //            loginScreen.strTitle = "\(arrMerchants.object(at: indexPath.item))"
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




