//
//  shareMediaScreen.swift
//  ChattingApp
//
//  Created by Parikshit on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class shareMediaScreen: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UIGestureRecognizerDelegate {

    @IBOutlet var tblShare: UITableView!
    
    @IBOutlet var viewHeaderBack: UIView!
    @IBOutlet var viewHeader: UIView!
    
    @IBOutlet var btnSharedByMe: UIButton!
    @IBOutlet var btnSharedByOther: UIButton!
    @IBOutlet var viewMenu: UIView!
    @IBOutlet weak var viewInnerMenu: UIView!
    @IBOutlet weak var tblMenu: UITableView!
    var arrImages = NSArray()



    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeaderBack.layer.cornerRadius = 3
        viewHeaderBack.layer.borderColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0).cgColor
        viewHeaderBack.layer.borderWidth = 1
        
        btnSharedByOther.layer.cornerRadius = 3
        btnSharedByMe.layer.cornerRadius = 3
        
        arrImages = ["5.png","4.png","3.png","2.png","6.png","1.png"]
        
        tblShare.tableFooterView = UIView.init(frame: CGRect.zero)
        tblMenu.tableFooterView = UIView.init(frame: CGRect.zero)

        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "SHARED MEDIA", leftbuttonImageName: "back.png")
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"three_dots.png"), style: .plain, target: self, action: #selector(methodMoreClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]

    }
    
    @IBAction func methodMoreClicked(_ sender: UIButton)
    {
        viewMenu.removeFromSuperview()
        
        appDelegate.window?.addSubview(viewMenu)
        viewMenu.frame = self.view.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewMenu.addGestureRecognizer(tap)
        
        let std = appDelegate.window!.convert((self.navigationController?.navigationBar.frame)!, from: self.navigationItem.titleView)
//       let std = appDelegate.window!.convert(self.btnMore.frame, from: self.navigationItem.titleView)
        var frame = viewInnerMenu.frame
        frame.origin.y = std.origin.y + std.size.height - 10
        viewInnerMenu.frame = frame
    }
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        rec.view?.removeFromSuperview()
    }

    //Mark : Button Action
    @IBAction func methodTop(_sender: UIButton)
    {
        if _sender.tag == 101
        {
            btnSharedByMe.backgroundColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            btnSharedByMe.setTitleColor(.white, for: .normal)
            
            btnSharedByOther.backgroundColor = UIColor.clear
            btnSharedByOther.setTitleColor(UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
        }
        else
        {
            
            btnSharedByOther.backgroundColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            btnSharedByOther.setTitleColor(.white, for: .normal)
            
            btnSharedByMe.backgroundColor = UIColor.clear
            btnSharedByMe.setTitleColor(UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
            
        }
    }
    
    
    // MARK : UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch tableView.tag
        {
        case 0:
            return 2
        default:
            return 3
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch tableView.tag
        {
        case 0:
            return 150
        default:
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch tableView.tag
        {
        case 0:
            let cellIdentifier:String = "cellShareMedia"
            var cell : cellShareMedia? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? cellShareMedia
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("cellShareMedia", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? cellShareMedia
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            
            let nib = UINib(nibName: "cellClShare", bundle: nil)
            cell?.clShare.register(nib, forCellWithReuseIdentifier: "cellClShare")
            cell?.clShare.delegate = self
            cell?.clShare.dataSource = self
            
            if let layout = cell?.clShare.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            
            cell?.clShare.tag = indexPath.row
            cell?.clShare.reloadData()
            
            if indexPath.row == 1
            {
                cell?.lblDate.text = "May 2018"
            }
            else
            {
                cell?.lblDate.text = "April 2018"
            }
            
            
            return cell!
        default:
            let cellIdentifier:String = "SongsMenuTableCell"
            var cell : SongsMenuTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SongsMenuTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("SongsMenuTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? SongsMenuTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.zero
            }
            switch indexPath.row
            {
            case 0:
                cell?.lblName.text = "Share Files"
            case 1:
                cell?.lblName.text = "Share Links"
            case 2:
                cell?.lblName.text = "Share Music"
            default:
                cell?.lblName.text = ""
            }
            return cell!
        }
        
    }
    
    //MARK:- UITableView DatSource And Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch tableView.tag
        {
        case 0:
            return viewHeader
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        switch tableView.tag
        {
        case 0:
            return 50
        default:
            return 0
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch tableView.tag
        {
        case 0:
            break
        case 1:
            viewMenu.removeFromSuperview()
            if indexPath.row == 0
            {
                let loginScreen  =  documentScreen(nibName:"documentScreen",bundle:nil)
                self.navigationController?.pushViewController(loginScreen, animated: true)
            }
            else if indexPath.row == 1
            {
                let loginScreen  =  galleryScreen(nibName:"galleryScreen",bundle:nil)
                self.navigationController?.pushViewController(loginScreen, animated: true)
            }
            else if indexPath.row == 2
            {
                let loginScreen  =  PlaylistSongsScreen(nibName:"PlaylistSongsScreen",bundle:nil)
                self.navigationController?.pushViewController(loginScreen, animated: true)
            }
            break
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tblMenu))! == true
        {
            return false
        }
        return true
    }

    //Mark : Uicollection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellClShare", for: indexPath) as! cellClShare
        cell.imgMedia.image = UIImage.init(named: "\(arrImages.object(at: indexPath.item))")
        
        cell.btnPlay.isHidden = true
        
        if collectionView.tag == 1
        {
            if indexPath.item == 2
            {
                cell.btnPlay.isHidden = false
            }
            else
            {
                cell.btnPlay.isHidden = true
            }
        }
        
  
     
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/3), height: (collectionView.frame.width/3))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
            let loginScreen  =  shareMediaDetail(nibName:"shareMediaDetail",bundle:nil)
            self.navigationController?.pushViewController(loginScreen, animated: true)              
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
