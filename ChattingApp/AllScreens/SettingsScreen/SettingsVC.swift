//
//  SettingsVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 11/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    @IBOutlet  var publicChannelButton: UIButton!
    @IBOutlet  var inviteLinkLabel: UILabel!
    @IBOutlet  var privateChannelButton: UIButton!
    
    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationBarWithBackButton(strTitle: "SETTINGS", leftbuttonImageName: "back.png")
    }

    //MARK:- UIButton Action Methods
    @IBAction func tickButtonAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func channelToggleButton(_ sender: UIButton!) {
        
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        case 100:
            self.publicChannelButton.isSelected = true
            self.privateChannelButton.isSelected = false
            break
            
        case 200:
            self.publicChannelButton.isSelected = false
            self.privateChannelButton.isSelected = true

            break
            
        default:
            break
        }
    }
    
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
