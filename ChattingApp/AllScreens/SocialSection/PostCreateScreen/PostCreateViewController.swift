//
//  PostCreateViewController.swift
//  ChattingApp
//
//  Created by vishal-ios on 12/02/19.
//  Copyright © 2019 NineHertzIndia. All rights reserved.
//

import UIKit

class PostCreateViewController: UIViewController,YPImagePickerDelegate  {
    
    var Camerapicker : YPImagePicker!
    var temppicker = YPPickerVC()

    @IBOutlet  var MeadiaView : UIView!
    @IBOutlet  var buttonBGView : UIView!

    @IBOutlet var btnVideo: UIButton!
    @IBOutlet var btnPhoto: UIButton!
    
    @IBOutlet var btnLibrary: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonBGView.layer.borderWidth = 1
        self.buttonBGView.layer.borderColor = UIColor.darkGray.cgColor
        self.view.backgroundColor = fusumaBackgroundColor
        self.buttonBGView.layer.cornerRadius = 5
        
        self.setupCamera()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kImagePickerButtonUpdate), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateScreenButtonSNotification), name: NSNotification.Name(rawValue: kImagePickerButtonUpdate), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MeadiaErrorSNotification), name: NSNotification.Name(rawValue: kMeadiaPickerError), object: nil)

        
        

//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "klibNextButtonClick"), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(animationScreenButtonsNotification), name: NSNotification.Name(rawValue: klibNextButtonButtonAnimation), object: nil)
        
        

        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        ResetPageNavigationBar()

    }
    func ResetPageNavigationBar()
    {
        self.view.isUserInteractionEnabled = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = "LIBRARY"
        let leftButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(leftButtonAction(_:)))
        self.navigationItem.leftBarButtonItem = leftButton
        if btnLibrary.isSelected == true
        {
            let rightButton : UIBarButtonItem = UIBarButtonItem.init(title: "NEXT", style: .plain, target: self, action: #selector(rightButtonAction(_:)))
            self.navigationItem.rightBarButtonItem = rightButton
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        
    }

    //MARK:- setupCamera
    //https://github.com/Yummypets/YPImagePicker

    func setupCamera()
    {
        if Camerapicker != nil
        {
            Camerapicker.dismiss(animated: false, completion: nil)
            Camerapicker = nil
        }
        var config = YPImagePickerConfiguration()
        // [Edit configuration here ...]
        // Build a picker with your configuration
        //        General
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = false
        config.showsFilters = true
        
        config.shouldSaveNewPicturesToAlbum = true
        config.albumName = "DefaultYPImagePickerAlbumName"
        config.startOnScreen = YPPickerScreen.library
        config.screens = [.library, .photo,.video]
        config.showsCrop = .none
        config.targetImageSize = YPImageSize.original
        config.overlayView = UIView()
        config.hidesStatusBar = true
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.bottomMenuItemSelectedColour = UIColor.black
        config.bottomMenuItemUnSelectedColour = UIColor.gray
        config.hidesBottomBar = true
        config.filters = [
            YPFilter(name: "Normal", applier: nil),
//            YPFilter(name: "Nashville", applier: YPFilter.nashvilleFilter),
//            YPFilter(name: "Toaster", applier: YPFilter.toasterFilter),
//            YPFilter(name: "1977", applier: YPFilter.apply1977Filter),
//            YPFilter(name: "Clarendon", applier: YPFilter.clarendonFilter),
//            YPFilter(name: "HazeRemoval", applier: YPFilter.hazeRemovalFilter),
            YPFilter(name: "Chrome", coreImageFilterName: "CIPhotoEffectChrome"),
            YPFilter(name: "Fade", coreImageFilterName: "CIPhotoEffectFade"),
            YPFilter(name: "Instant", coreImageFilterName: "CIPhotoEffectInstant"),
            YPFilter(name: "Mono", coreImageFilterName: "CIPhotoEffectMono"),
            YPFilter(name: "Noir", coreImageFilterName: "CIPhotoEffectNoir"),
            YPFilter(name: "Process", coreImageFilterName: "CIPhotoEffectProcess"),
            YPFilter(name: "Tonal", coreImageFilterName: "CIPhotoEffectTonal"),
            YPFilter(name: "Transfer", coreImageFilterName: "CIPhotoEffectTransfer"),
            YPFilter(name: "Tone", coreImageFilterName: "CILinearToSRGBToneCurve"),
            YPFilter(name: "Linear", coreImageFilterName: "CISRGBToneCurveToLinear"),
            YPFilter(name: "Sepia", coreImageFilterName: "CISepiaTone"),
        ]
        config.wordings.libraryTitle = "Gallery"
        config.wordings.cameraTitle = "Camera"
        config.wordings.next = "NEXT"
    
        //        Library
        config.library.options = nil
        config.library.onlySquare = false
        config.library.minWidthForItem = nil
        config.library.mediaType = YPlibraryMediaType.photoAndVideo
        //config.library.maxNumberOfItems = 1
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 3
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
//         config.library
        
        //Video
//        config.video.compression = AVAssetExportPresetHighestqua //AVAssetExportPresetHighestQuality
        //config.video.compression = AVAssetExportPresetMediumQuality
        config.video.fileType = .mp4 //.mov
        config.video.recordingTimeLimit = 60.0
        config.video.libraryTimeLimit = 600.0
         config.video.minimumTimeLimit = 0 // 3.0
        config.video.trimmerMaxDuration = 60.0
        config.video.trimmerMinDuration = 3.0
        
        //        Default Configuration
        // Set the default configuration for all pickers
        YPImagePickerConfiguration.shared = config
    
        // And then use the default configuration like so:
        
        config.library.maxNumberOfItems = 10
        self.Camerapicker = YPImagePicker(configuration: config)
        self.Camerapicker.tempPushNav = self.navigationController!
        
     //Then you can handle multiple selection in the same callback you know and love :
        self.Camerapicker.didFinishPicking { (items, true) in
            for item in items {
                switch item {
                case .photo(let photo):
                    print(photo)
                case .video(let video):
                    print(video)
                }
            }
            self.Camerapicker.dismiss(animated: true, completion: nil)
        }
        
        
        self.Camerapicker.navigationBar.isHidden = true
//        present(self.Camerapicker, animated: true, completion: nil)
        
        
        self.MeadiaView.addSubview(self.Camerapicker.view)
        self.addChildViewController(self.Camerapicker)
        self.Camerapicker.view.frame = CGRect.init(x: 0, y: 0, width: self.MeadiaView.frame.size.width, height: self.MeadiaView.frame.size.height)
        
        temppicker = self.Camerapicker.picker
        //        self.Camerapicker.view.frame =
        
    }
    
    func noPhotos() {
    }



     //MARK:- btnAction
    @objc func leftButtonAction(_ sender: Any) {
        
        self.temppicker.close()
     //   NotificationCenter.default.post(name: NSNotification.Name(klibCloseButtonClick), object: nil)
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func rightButtonAction(_ sender: Any)
    {
        appDelegate.arrPostUrls.removeAllObjects()
        appDelegate.arrPostthumbUrls.removeAllObjects()
        appDelegate.dicFilterNameSave.removeAllObjects()
        RemoveDirectoryUrls(appDelegate.strVideoFolderPath)
        appDelegate.createFolders()
        self.view.isUserInteractionEnabled = false
        
        self.navigationItem.rightBarButtonItem = YPLoaders.defaultLoader
        self.navigationItem.leftBarButtonItem = YPLoaders.defaultLoader

        temppicker.DonePickup()
       

        
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: klibNextButtonClick), object: self.navigationController)
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "klibNextButtonClick"), object: nil)

    }
    
    func MeadiaButtonsUpdates(_ sender: Int)
    {
        self.btnLibrary.backgroundColor = UIColor.clear
        self.btnVideo.backgroundColor = UIColor.clear
        self.btnPhoto.backgroundColor = UIColor.clear
        self.btnVideo.isSelected = false
        self.btnPhoto.isSelected = false
        self.btnLibrary.isSelected = false
        if sender == 0
        {
            self.btnLibrary.isSelected = true
            self.btnLibrary.backgroundColor = UIColor.init(red: (126.0/255.0), green: (139.0/255.0), blue: (153.0/255.0), alpha: 1.0)
        }
        else if sender == 1
        {
            self.btnPhoto.isSelected = true
            self.btnPhoto.backgroundColor = UIColor.init(red: (126.0/255.0), green: (139.0/255.0), blue: (153.0/255.0), alpha: 1.0)
        }
        else if sender == 2
        {
            self.btnVideo.isSelected = true
            self.btnVideo.backgroundColor = UIColor.init(red: (126.0/255.0), green: (139.0/255.0), blue: (153.0/255.0), alpha: 1.0)
        }
        
        if btnLibrary.isSelected == true
        {
            let rightButton : UIBarButtonItem = UIBarButtonItem.init(title: "NEXT", style: .plain, target: self, action: #selector(rightButtonAction(_:)))
            self.navigationItem.rightBarButtonItem = rightButton
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil
        }
        
    }
    @IBAction func btnMeadiaClick(_ sender: UIButton)
    {
        if sender.isSelected == true
        {
            return
        }
        self.temppicker.showPage(sender.tag)
        self.MeadiaButtonsUpdates(sender.tag)
        
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: kImagePickerScreenUpdate), object: sender.tag)
    }
    @objc func MeadiaErrorSNotification(data : NSNotification)
    {
       ResetPageNavigationBar()
    }
    @objc func updateScreenButtonSNotification(data : NSNotification)
    {
         let tag = data.object as! Int
         self.MeadiaButtonsUpdates(tag)
    }

//    @objc func animationScreenButtonsNotification(data : NSNotification) {
//
//        self.navigationItem.rightBarButtonItem = YPLoaders.defaultLoader
//    }
    

}
