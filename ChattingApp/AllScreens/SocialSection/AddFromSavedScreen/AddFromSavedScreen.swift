//
//  AddFromSavedScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 07/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class AddFromSavedScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnAddCollection: UIButton!
    @IBOutlet weak var savedCollectionView: UICollectionView!
    
    var strFrom = "Saved"
    var imageArray = NSMutableArray()
    var collectionData = CollectionModel()
    var arrFeeds = NSMutableArray()
    var arrSelectedFeeds = NSMutableArray()

    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!

    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        savedCollectionView.addSubview(refreshControl)
        
                if #available(iOS 11.0, *) {
//                    savedScrollView.contentInsetAdjustmentBehavior = .never
                    //print("contentInsetAdjustmentBehavior")
                } else {
//                    automaticallyAdjustsScrollViewInsets = false
                }
        
        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationBarWithBackButton(strTitle: "ADD FROM SAVED", leftbuttonImageName: "back.png")
        
        self.tabBarController?.tabBar.isHidden = true
        self.callListAPI()
    }
    
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.currentPageNo = 1
            self.maxPage = 0
            self.callListAPI()
        })
    }
    

    //MARK:- Helper Method
    func initialSetUp() {
        let nib = UINib(nibName: "ImageCollectionCell", bundle: nil)
        
        self.savedCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCollectionCell")
        self.getSelectedFeeds()
    }
    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            if arrFeeds.count == 0
            {
                self.showActivity(text: "")
            }
            
            self.performSelector(inBackground: #selector(self.getSavedFeedsListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }

    //MARK:- UICollectionView DataSource And Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFeeds.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((collectionView.frame.width/3) - 5), height: (collectionView.frame.width/3) - 5)
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
 
        let feedData = self.arrFeeds.object(at: indexPath.item) as! FeedModel
        if feedData.arrMedia.count > 0
        {
            let feedMediaData = feedData.arrMedia.object(at: 0) as! FeedMediaModel
            
            if feedMediaData.strMediaURL != ""
            {
                if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                {
                    let url = URL.init(string: feedMediaData.strMediaURL)
                    cell.feedImageView.sd_addActivityIndicator()
                    cell.feedImageView.sd_showActivityIndicatorView()
                    cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell.feedImageView.sd_removeActivityIndicator()
                    })
                    cell.feedImageView.isHidden = false
                }
                else
                {
                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell.feedImageView.sd_addActivityIndicator()
                    cell.feedImageView.sd_showActivityIndicatorView()
                    cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell.feedImageView.sd_removeActivityIndicator()
                    })
                    cell.feedImageView.isHidden = false
                }
            }
        }

        cell.btnEditCollection.isHidden = false
        cell.btnEditCollection.isSelected = self.arrSelectedFeeds.contains(feedData.strFeedId)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let feedData = self.arrFeeds.object(at: indexPath.item) as! FeedModel
        if self.arrSelectedFeeds.contains(feedData.strFeedId)
        {
           self.arrSelectedFeeds.remove(feedData.strFeedId)
        }
        else
        {
            self.arrSelectedFeeds.add(feedData.strFeedId)
        }
        self.savedCollectionView.reloadData()
    }
    
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.arrFeeds.count > 0
        {
            if  self.arrFeeds.count < self.maxPage && !boolGetData 
            {
                let arrIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems
                let lastVisibleIndexPath =  IndexPath(row: self.arrFeeds.count-1, section: 0)
                //                if let lastVisibleIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems.last {
                if lastVisibleIndexPath != nil && arrIndexPath.contains(lastVisibleIndexPath)
                {
                    let dataCount = self.arrFeeds.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.item)
                    
                    if dif == 0
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
        }
    }

    
    //MARK:- UIButton Action Methods
    
    @IBAction func addCollectionButtonsAction(_ sender: UIButton!) {
        
        if self.arrSelectedFeeds.count == 0
        {
            self.showToastOnViewController(title: kError, message: kMessageSocialSelectCollectionFeed, position: kToastTopPosition, controller: self, image: nil)
        }
        else
        {
            if self.appDelegate.isInternetAvailable() == true
            {
                self.showActivity(text: "")
                self.performSelector(inBackground: #selector(self.createCollectionApi), with: nil)
            }
            else
            {
                self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
            }

        }
//        self.navigationController?.popViewController(animated: true)
    }
    
    func createCollectionApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.collectionData.strCollectionName.getEncryptedString() ?? "", forKey: kKeySocialCollectionName as NSCopying)
        dicrequest.setObject(self.arrSelectedFeeds.componentsJoined(by: ",").getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        return dicrequest
    }
    //MARK : - createCollectionApi
    @objc func createCollectionApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialCreateCollection, Details: createCollectionApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let collectionData = CollectionModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            NotificationCenter.default.post(name: NSNotification.Name.init(kNotificationSocialAddNewCollection), object: collectionData)

                            for controller in self.navigationController!.viewControllers as Array {
                                if self.strFrom == "Saved" && controller.isKind(of: SavedVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                                else if self.strFrom == "SavedDetail" && controller.isKind(of: SavedDetailScreen.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.savedCollectionView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func getSavedFeedsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.appDelegate.currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getSavedFeedsListApi
    @objc func getSavedFeedsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialSavedPostList, Details: getSavedFeedsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.arrFeeds.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kKeySocialFeedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrFeeds.insert((getFeedsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getFeedsObjectArray(arrResult: arr).count - 1))
                                self.arrFeeds.addObjects(from: (getFeedsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.savedCollectionView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

    func getSelectedFeeds()
    {
        self.collectionData.arrFeeds.enumerateObjects({(object, idx, stop) -> Void in
            print("idx",idx)
            let oldFeedObj = object as! FeedModel
            if self.arrSelectedFeeds.contains(oldFeedObj.strFeedId) == false
            {
                self.arrSelectedFeeds.add(oldFeedObj.strFeedId)
            }
        })
    }

    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

