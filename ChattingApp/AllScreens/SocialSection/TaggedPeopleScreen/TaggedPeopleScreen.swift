//
//  TaggedPeopleScreen.swift
//  ChattingApp
//
//  Created by Shreya-ios on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class TaggedPeopleScreen: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var followTableView: UITableView!
    var followersArray = NSMutableArray()
    var arrTaggedPeople = NSMutableArray()
    var arrSelectedTaggedPeople = NSMutableArray()

    var strFeedId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
 
//        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
//        followTableView.addSubview(refreshControl)
        
        if strFeedId == ""
        {
            navigationBarWithBackButton(strTitle: "TAG PEOPLE", leftbuttonImageName: "back.png")
            let rightButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "social_col_chk")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(rightButtonAction(_:)))
            
            self.navigationItem.rightBarButtonItem = rightButton
        }
        else
        {
            navigationBarWithBackButton(strTitle: "TAGGED PEOPLE", leftbuttonImageName: "back.png")
        }

        self.callListAPI()
        
    }

    //MARK:- Helper Method
    func initialSetUp() {

    }
    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.getFollowListApi), with: nil)
            if strFeedId != ""
            {
                self.performSelector(inBackground: #selector(self.getTaggedPeopleListApi), with: nil)
            }
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }
    @objc func rightButtonAction(_ sender: Any) {
        
        if self.arrSelectedTaggedPeople.count == 0
        {
            self.showToastOnViewController(title: kError, message: kMessageSocialSelectTagPeople, position: kToastTopPosition, controller: self, image: nil)
        }
        else
        {
            NotificationCenter.default.post(name: NSNotification.Name.init(kNotificationSocialUpdateTaggedPeople), object: self.arrSelectedTaggedPeople.mutableCopy() as! NSMutableArray )
            self.navigationController?.popViewController(animated: true)
        }
    }

    //MARK:- UITableView DatSource And Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let arr : NSMutableArray!
        if strFeedId != ""
        {
            arr = arrTaggedPeople
        }
        else
        {
            arr = followersArray
        }
        if  arr.count != 0
        {
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            let font = UIFont(name: "Lato-Bold", size: 17)
            noDataLabel.font = font
            noDataLabel.text = "No user found."
            noDataLabel.textColor = UIColor.colorAppTheam
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let indentifier:String = "ContactUserCell"
        var cell : ContactUserCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? ContactUserCell
        
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("ContactUserCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? ContactUserCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        
        cell?.checkUncheckButton.tag = indexPath.row
        cell?.checkUncheckButton.isUserInteractionEnabled = false
        
        cell?.userImageView.layer.cornerRadius = (cell?.userImageView.frame.size.height)!/2
        cell?.userImageView.layer.masksToBounds = true
        
        let arr : NSMutableArray!
        if strFeedId != ""
        {
            arr = arrTaggedPeople
        }
        else
        {
            arr = followersArray
        }
        let userObj = arr.object(at: indexPath.row) as! UserModel
        
        cell?.checkUncheckButton.isSelected = self.arrSelectedTaggedPeople.contains(userObj.strUserId)
        
        cell?.checkUncheckButton.isHidden = userObj.strUserId == self.appDelegate.currentLoginUser.strUserId
        
        cell?.checkUncheckButton.isHidden = self.strFeedId != ""

        cell?.nameLabel.text = userObj.strName
        cell?.qutaLabel.text = userObj.strEmail
        //        cell?.lblMessage.sizeToFit()
        //        cell?.lblMessage2.sizeToFit()
        if userObj.strProfilepicImage != ""
        {
            // \(Image_URL)
            let url = URL.init(string: userObj.strProfilepicImage)
            cell?.userImageView.sd_addActivityIndicator()
            cell?.userImageView.sd_showActivityIndicatorView()
            cell?.userImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                cell?.userImageView.sd_removeActivityIndicator()
            })
        }
        else
        {
            cell?.userImageView.image = UIImage(named:"reg_USER")
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.strFeedId != ""
        {
            let userObj = self.arrTaggedPeople.object(at: indexPath.row) as! UserModel
            if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
            {
                let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
                other.userObj = userObj
                other.strUserID = userObj.strUserId
                other.userNameString = userObj.strName
                self.navigationController?.pushViewController(other, animated: true)
            }
        }
        else
        {
            let userObj = self.followersArray.object(at: indexPath.row) as! UserModel
            if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
            {
                if self.arrSelectedTaggedPeople.contains(userObj.strUserId) {
                    self.arrSelectedTaggedPeople.remove(userObj.strUserId)
                    
                } else {
                    self.arrSelectedTaggedPeople.add(userObj.strUserId)
                    
                }
                self.followTableView.reloadData()
            }
        }
    }

    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func getFollowListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("following".getEncryptedString() ?? "", forKey: kKeySocialType as NSCopying)
//        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject("-1".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.appDelegate.currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getFollowListApi
    @objc func getFollowListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialFollowList, Details: getFollowListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            self.followersArray = getUsersObjectArray(arrResult: dicRes.object(forKey: kData) as? NSArray ?? NSArray())
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.followTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    func getTaggedPeopleListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        //        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject("-1".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        return dicrequest
    }
    //MARK : - getTaggedPeopleListApi
    @objc func getTaggedPeopleListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialTaggedPeopleList, Details: getTaggedPeopleListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            self.arrTaggedPeople = getUsersObjectArray(arrResult: dicRes.object(forKey: kData) as? NSArray ?? NSArray())
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.followTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    

}


