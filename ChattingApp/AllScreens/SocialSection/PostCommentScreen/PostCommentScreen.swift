//
//  PostCommentScreen.swift
//  Bitez
//
//  Created by Parikshit on 05/07/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit
//class PostCommentScreen: UIViewController{
class PostCommentScreen: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,HPGrowingTextViewDelegate {
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var readmoreButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet var viewTxtTypeBG: UIView!
    @IBOutlet var tblComment: UITableView!
    var refreshControl = UIRefreshControl()

   // @IBOutlet var btnSend: UIButton!
    @IBOutlet var textView: HPGrowingTextView!
    @IBOutlet var toolBar: UIToolbar!

    var feedData = FeedModel()
    var commentArray = NSMutableArray()
    var currentPageNo = 1
    var maxPage = 0
    var safeAreaBottom:CGFloat = 0
    var safeAreaTop:CGFloat = 0

    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTxtTypeBG.layer.masksToBounds = true
        tblComment.tableFooterView = UIView()
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        tblComment.addSubview(refreshControl)

        if #available(iOS 11.0, *)
        {
                let window = UIApplication.shared.keyWindow
                let topPadding = window?.safeAreaInsets.top
                let bottomPadding = window?.safeAreaInsets.bottom

                safeAreaBottom = bottomPadding!
                safeAreaTop = topPadding!
        }
        DispatchQueue.main.async {
            var tbleFrame = self.tblComment.frame

            tbleFrame.size.height =  ScreenSize.SCREEN_HEIGHT - self.safeAreaTop   - 60  - self.safeAreaBottom

            self.tblComment.frame = tbleFrame

            var btnFrame = self.readmoreButton.frame
            
            btnFrame.origin.y = tbleFrame.origin.y
            
            self.readmoreButton.frame = btnFrame

        }
        self.initialSetUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationBarWithBackButton(strTitle: "COMMENTS", leftbuttonImageName: "back.png")
        self.callListAPI()

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.appDelegate.TabLogin.tabBar.isHidden = true
        textView.resignFirstResponder()

    }
    
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.currentPageNo = 1
            self.maxPage = 0
            self.callListAPI()
        })
    }
    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.getCommentsListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }

    //MARK:- Helper Method
    func initialSetUp() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.errorLabel.isHidden = true
        self.loadView1()
    }

    @objc func loadView1()
    {
        let font: UIFont = UIFont(name:  "Lato-Regular", size: CGFloat(14))!
        textView.internalTextView.font = font
        textView.placeholder = "Type here your comment"
        textView.minNumberOfLines = 1
        textView.maxNumberOfLines = 4
        textView.returnKeyType = UIReturnKeyType.default
        textView.font = font
        textView.delegate = self
        textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 7, 5, 7)
        textView.layer.masksToBounds = true
        textView.animateHeightChange = true
        textView.layer.cornerRadius =  textView.frame.size.height/2
        textView.backgroundColor?.withAlphaComponent(0.50)
        textView.internalTextView.inputAccessoryView = toolBar
    }
    @IBAction func methodKeyPadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }

    // MARK: - GrowingTextView Delegates -
    
    func growingTextView(_ growingTextView: HPGrowingTextView!, willChangeHeight height: Float)
    {
        let diff: CGFloat = CGFloat(growingTextView.frame.size.height) - CGFloat(height)
        var r: CGRect = viewTxtTypeBG.frame
        r.size.height = r.size.height - diff
        r.origin.y = r.origin.y + diff
        viewTxtTypeBG.frame = r
        tblComment.frame = CGRect(x: 0, y: 0, width: appDelegate.window!.frame.size.width, height: viewTxtTypeBG.frame.origin.y)
        
        
    }
    func growingTextViewDidChange(_ growingTextView: HPGrowingTextView)
    {
        let trimmedString: String = textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if trimmedString.characters.count > 0 {
            //doneBtn.isEnabled = true
        }
        else {
//            self.automaticallyAdjustsScrollViewInsets = false
            //doneBtn.isEnabled = false
            textView.internalTextView.contentSize = CGSize(width: textView.frame.size.width, height: textView.frame.size.height)
            textView.internalTextView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
        }
        
    }
    // MARK: - UIBUTTON Action Method
    
    @objc func methodUserProfileClicked(_ sender: UIButton)
    {
        let commentObj = self.commentArray.object(at: sender.tag) as! CommentModel
        let userObj = commentObj.UserInfo
        if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
        {
            let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
            other.userObj = userObj
            other.strUserID = userObj.strUserId
            other.userNameString = userObj.strName
            self.navigationController?.pushViewController(other, animated: true)
        }
        else
        {
            let register = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
        }
        
    }

    @IBAction func readMoreButtonAction(_ sender: Any) {

        if self.currentPageNo < self.maxPage {
            self.currentPageNo += 1
            self.callListAPI()
        }
    }
    @IBAction func sendButtonAction(_ sender: UIButton!) {
        self.textView.resignFirstResponder()

        if textView.text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).isEmpty == true
        {
            self.onShowAlertController(title: "Error", message: "Please enter your comment.")
        }
        else {
            if self.appDelegate.isInternetAvailable() == true {
                let  strComment = self.textView.text
                self.textView.text = ""
                self.showActivity(text: "")
                self.performSelector(inBackground: #selector(self.postCommentApi(strComment:)), with: strComment)

            } else {
                self.onShowAlertController(title: "No Internet Connection", message: "Please check your internet connetion")

            }
        }
    }
    //MARK:- Notification Observer Methods
    @objc func keyboardWillShow(_ note: NSNotification)
    {
        // get keyboard size and loctaion
        //    if (uploadView.hidden)
        //    {
        
        var containerFrame : CGRect
        let info : NSDictionary = note.userInfo! as NSDictionary
        
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let duration = (note.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber)
        let curve = (note.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber)
        
        
        containerFrame = viewTxtTypeBG.frame
        containerFrame.origin.y = appDelegate.window!.frame.size.height - (keyboardSize!.height + containerFrame.size.height)
        viewTxtTypeBG.frame = containerFrame
        
        //70+36
        tblComment.frame = CGRect(x: 0, y: 0, width: appDelegate.window!.frame.size.width, height: appDelegate.window!.frame.size.height - (containerFrame.size.height + keyboardSize!.height))
        
        self.performSelector(onMainThread: #selector(moveContentViewToEnd), with: nil, waitUntilDone: false)
        
        // animations settings
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(CDouble(duration))
        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: Int(CInt(curve)))!)
        // set views with new info
        viewTxtTypeBG.frame = containerFrame
        //        tblComment.frame = CGRect(x: 0, y: 0, width: appDelegate.window!.frame.size.width, height: containerFrame.origin.y-64)
        // commit animations
        UIView.commitAnimations()
        
    }
    
    @objc func keyboardWillHide(_ note: NSNotification)
    {
        // get keyboard size and loctaion
        //    if (uploadView.hidden)
        //    {
        
        var containerFrame : CGRect
        let info : NSDictionary = note.userInfo! as NSDictionary
        
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let duration = (note.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber)
        let curve = (note.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber)
        
        
        
        containerFrame = viewTxtTypeBG.frame
        
        containerFrame.origin.y = appDelegate.window!.frame.size.height - (containerFrame.size.height) - safeAreaBottom
        
        viewTxtTypeBG.frame = containerFrame
        
        //        tblComment.frame = CGRect(x: 0, y: 0, width: appDelegate.window!.frame.size.width, height: appdelegate.window!.frame.size.height - containerFrame.size.height-64)
        
        
        tblComment.frame = CGRect(x: 0, y: 0, width: appDelegate.window!.frame.size.width, height: appDelegate.window!.frame.size.height - (containerFrame.size.height ) - safeAreaBottom)
        
        
        
        
        // animations settings
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(CDouble(duration))
        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: Int(CInt(curve)))!)
        // set views with new info
        viewTxtTypeBG.frame = containerFrame
        // commit animations
        UIView.commitAnimations()
        
    }
    @objc func moveContentViewToEnd()
    {
        if tblComment.contentSize.height > tblComment.frame.size.height
        {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                let numberOfSections = self.tblComment.numberOfSections
                let numberOfRows = self.tblComment.numberOfRows(inSection: numberOfSections-1)
                
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.tblComment.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
            }
            
        }
    }
    // MARK: - TextFieldsDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }


    // MARK: - UITableView DataSource And Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentArray.count
    }

//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 80
//    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let commntObj = self.commentArray.object(at: indexPath.row) as! CommentModel
        
        if (commntObj.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) || self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId
        {
            return true
        }
        else
        {
            return false
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete)
        {
            let commntObj = self.commentArray.object(at: indexPath.row) as! CommentModel
            
            if ((commntObj.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) || self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) == false
            {
                return
            }
            
            let alert  =   UIAlertController(title: kAppName, message: kMessageSocialDeleteComment, preferredStyle: .alert)
            
            let cancelAction  = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in
            }
            
            let okAction  = UIAlertAction(title: "Delete", style: .default) { (alert: UIAlertAction!) in
                
                if self.appDelegate.isInternetAvailable() == true
                {
                    self.showActivity(text: "")
                    self.performSelector(inBackground: #selector(self.deleteCommentApi(idx:)), with: "\(indexPath.row)")
                }
                else
                {
                    self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                }
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
            let cellIdentifier:String = "PostCommentCell"
            var cell:PostCommentCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PostCommentCell

            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("PostCommentCell", owner: nil, options: nil)! as [Any]

                cell = nib[0] as? PostCommentCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear);
            }
        cell?.btnUser.tag = indexPath.row
        cell?.btnUser.addTarget(self, action: #selector(methodUserProfileClicked(_:)), for: .touchUpInside)

        let commntObj = self.commentArray.object(at: indexPath.row) as! CommentModel
        
        cell?.commentLabel.text = commntObj.strComment

        cell?.nameLabel.text = commntObj.UserInfo.strName

        if commntObj.UserInfo.strProfilepicImage != ""
        {
            // \(Image_URL)
            let url = URL.init(string: commntObj.UserInfo.strProfilepicImage)
            cell?.userImageView.sd_addActivityIndicator()
            cell?.userImageView.sd_showActivityIndicatorView()
            cell?.userImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                cell?.userImageView.sd_removeActivityIndicator()
            })
        }
        else
        {
            cell?.userImageView.image = UIImage(named:"reg_USER")
        }
         return cell!
    }

    //MARK:- UIScrollView Delegate Method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }


    //MARK:- API Integration Methods
    func getCommentsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.feedData.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)

        return dicrequest
    }
    //MARK : - getCommentsListApi
    @objc func getCommentsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialCommentsList, Details: getCommentsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.commentArray.removeAllObjects()
                        }

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kData) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
                                self.commentArray.insert((getCommentsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getCommentsObjectArray(arrResult: arr).count - 1))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                            
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.tblComment.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
                self.reloadTableData()
            }
        }
    }

    
    //MARK:- API Integration Methods
    func postCommentApiKeys(strComment: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.feedData.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(strComment.getEncryptedString() ?? "", forKey: kKeySocialComment as NSCopying)

        return dicrequest
    }
    //MARK : - postCommentApi
    @objc func postCommentApi(strComment: String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialPostComment, Details: postCommentApiKeys(strComment: strComment)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let commentData = CommentModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            self.commentArray.add(commentData)
                            self.maxPage += 1
                            self.errorLabel.isHidden = true
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialAddNewComment), object: commentData)

                            self.reloadTableData()

                            self.performSelector(onMainThread: #selector(self.moveContentViewToEnd), with: nil, waitUntilDone: false)


                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.tblComment.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
                self.reloadTableData()
            }
        }
    }

    //MARK:- API Integration Methods
    func deleteCommentApiKeys(idx: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.feedData.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        
        let commntObj = self.commentArray.object(at: Int(idx)!) as! CommentModel

        dicrequest.setObject(commntObj.strCommentId.getEncryptedString() ?? "", forKey: kKeySocialCommentId as NSCopying)

        return dicrequest
    }
    //MARK : - deleteCommentApi
    @objc func deleteCommentApi(idx: String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialDeleteComment, Details: deleteCommentApiKeys(idx: idx)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
//                            self.commentArray = getUsersObjectArray(arrResult: dicRes.object(forKey: kData) as? NSArray ?? NSArray())
                            let commentData = self.commentArray.object(at: Int(idx)!) as! CommentModel
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialDeleteComment), object: commentData)
                            
                            self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                            self.commentArray.removeObject(at: Int(idx)!)
                            self.maxPage -= 1
                            self.errorLabel.isHidden = self.commentArray.count > 0

                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.tblComment.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
                self.reloadTableData()
            }
        }
    }


    func reloadTableData()
    {
        if self.commentArray.count < self.maxPage
        {
            self.readmoreButton.isHidden = false
            DispatchQueue.main.async {
                var btnFrame = self.readmoreButton.frame
                
                btnFrame.origin.y = self.tblComment.frame.origin.y + self.safeAreaTop + (self.navigationController?.navigationBar.frame.size.height)!
                
                self.readmoreButton.frame = btnFrame
                
            }
        }
        else
        {
            self.readmoreButton.isHidden = true
            
        }
        self.tblComment.reloadData()
        if self.commentArray.count > 0 {
            
            if self.currentPageNo == 1 {
                // First figure out how many sections there are
                let lastSectionIndex = self.tblComment.numberOfSections - 1
                
                // Then grab the number of rows in the last section
                let lastRowIndex = self.tblComment!.numberOfRows(inSection: lastSectionIndex) - 1
                
                // Now just construct the index path
                let pathToLastRow = IndexPath.init(row: lastRowIndex, section: lastSectionIndex)//NSIndexPath(forRow: lastRowIndex, inSection: lastSectionIndex)
                
                // Make the last row visible
                self.tblComment?.scrollToRow(at: pathToLastRow as IndexPath, at: UITableViewScrollPosition.none, animated: true)
                //                            self.performSelector(onMainThread: #selector(self.moveContentViewToEnd), with: nil, waitUntilDone: false)
            }
            
            self.errorLabel.isHidden = true
        }else {
            self.errorLabel.isHidden = false
            
        }
        
    }
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
