//
//  PostCommentCell.swift
//  Bitez
//
//  Created by Shreya on 04/09/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit

class PostCommentCell: UITableViewCell {

    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImageView.layer.cornerRadius = self.userImageView.frame.width/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
