//
//  ProfileCell.swift
//  ChattingApp
//
//  Created by Shreya-ios on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var nameTextFeild: UITextField!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var feedButton: UIButton!
//    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImgBgView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet var lblFeedsCount: UILabel!
    @IBOutlet var lblFollowersCount: UILabel!
    @IBOutlet var lblFollowingsCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImgBgView.layer.cornerRadius = self.userImgBgView.layer.frame.width/2
        self.userImgBgView.layer.borderWidth = 1
        self.userImgBgView.layer.borderColor = UIColor.white.cgColor
        self.userImgBgView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
