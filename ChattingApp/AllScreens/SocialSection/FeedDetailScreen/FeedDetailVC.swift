//
//  FeedDetailVC.swift
//  arrMediaTingApp
//
//  Created by Shreya-ios on 07/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class FeedDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var feedTableView: UITableView!
    var feedData = FeedModel()
    var refreshControl = UIRefreshControl()
    var avPlayer: AVPlayer!
    var avPlayerViewController: AVPlayerViewController!

    var arrMediaPhotos = [Media]()
    var arrMediaThumbs = [Media]()
    var selectedRow = 0
    var cllctnViewMedia: UICollectionView!

    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        feedTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        feedTableView.addSubview(refreshControl)
        
        feedTableView.rowHeight = UITableViewAutomaticDimension
        feedTableView.estimatedRowHeight = 300

        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationBarWithBackButton(strTitle: "FEED", leftbuttonImageName: "back.png")
        let rigthButton = UIBarButtonItem.init(image: UIImage.init(named: "white_dots"), style: .done, target: self, action: #selector(rightBarButtonAction(_:)))
        self.navigationItem.rightBarButtonItem = rigthButton
        self.feedTableView.reloadData()
//        self.scrollToMedia()
       self.callDetailsAPI()
        if self.avPlayer != nil && self.avPlayerViewController != nil
        {
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                print(error)
            }
            
            self.avPlayer.play()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.removeAVPlayer()
    }
    @objc func callDetailsAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.getFeedsDetailsApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }
    
    @objc func scrollToMedia()
    {
        if cllctnViewMedia != nil && feedData.arrMedia.count > 0 && selectedRow > 0
        {
//            DispatchQueue.main.async {
                self.cllctnViewMedia?.scrollToItem(at: IndexPath.init(row: self.selectedRow, section: 0), at: .left, animated: true)
//            }
            selectedRow = 0
        }
    }
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.callDetailsAPI()
        })
    }

    //MARK:- Helper Method
    func initialSetUp()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCommentData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialDeleteComment), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addNewCommentData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialAddNewComment), object: nil)
    }
    
    //MARK:- Notification Observer Methods
    @objc func deleteCommentData(notiObj: Notification)
    {
        feedData.strCommentsCount = "\((feedData.strCommentsCount as NSString).integerValue - 1)"
        self.feedTableView.reloadData()
    }
    @objc func addNewCommentData(notiObj: Notification)
    {
        feedData.strCommentsCount = "\((feedData.strCommentsCount as NSString).integerValue + 1)"
        self.feedTableView.reloadData()
    }
    

    //MARK:- UITableView DataSource And Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1 + (self.feedData.arrLast3Comments.count > 0 ? self.feedData.arrLast3Comments.count+1 : 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension

        } else if indexPath.row == 1 {
            return (feedData.strCommentsCount as NSString).integerValue <= 3 ? 0 : 44
            
        } else {
            return UITableViewAutomaticDimension
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if indexPath.row == 0 || indexPath.row == 1
        {
            return false
        }
        let commntObj = self.feedData.arrLast3Comments.object(at: indexPath.row-2) as! CommentModel
        
        if (commntObj.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) || self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId
        {
            return true
        }
        else
        {
            return false
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        if indexPath.row == 0 || indexPath.row == 1
        {
            return
        }
        if (editingStyle == UITableViewCellEditingStyle.delete)
        {
            let commntObj = self.feedData.arrLast3Comments.object(at: indexPath.row-2) as! CommentModel
            
            if ((commntObj.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) || self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) == false
            {
                return
            }
            
            let alert  =   UIAlertController(title: kAppName, message: kMessageSocialDeleteComment, preferredStyle: .alert)
            
            let cancelAction  = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in
            }
            
            let okAction  = UIAlertAction(title: "Delete", style: .default) { (alert: UIAlertAction!) in
                
                if self.appDelegate.isInternetAvailable() == true
                {
                    self.showActivity(text: "")
                    self.performSelector(inBackground: #selector(self.deleteCommentApi(idx:)), with: "\(indexPath.row-2)")
                }
                else
                {
                    self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                }
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let indentifier:String = "FeedVideoCell"
            var cell : FeedVideoCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? FeedVideoCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FeedVideoCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FeedVideoCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            
            cell?.btnUser.tag = indexPath.row
            cell?.btnLikes.tag = indexPath.row
            cell?.btnDislikes.tag = indexPath.row
            cell?.btnLikeFeed.tag = indexPath.row
            cell?.btnDislikeFeed.tag = indexPath.row
            cell?.btnSavedFeed.tag = indexPath.row
            cell?.cllctnViewMedia.tag = indexPath.row
            cell?.pageControll.tag = indexPath.row
            cell?.btnComments.tag = indexPath.row
            cell?.btnTag.tag = indexPath.row
            cell?.btnUser.addTarget(self, action: #selector(methodUserProfileClicked(_:)), for: .touchUpInside)
            cell?.btnLikes.addTarget(self, action: #selector(methodLikesUsersListClicked(_:)), for: .touchUpInside)
            cell?.btnDislikes.addTarget(self, action: #selector(methodDislikesUsersListClicked(_:)), for: .touchUpInside)
            cell?.btnLikeFeed.addTarget(self, action: #selector(methodLikeFeedClicked(_:)), for: .touchUpInside)
            cell?.btnDislikeFeed.addTarget(self, action: #selector(methodDislikeFeedClicked(_:)), for: .touchUpInside)
            cell?.btnSavedFeed.addTarget(self, action: #selector(methodSaveFeedClicked(_:)), for: .touchUpInside)
            cell?.btnComments.addTarget(self, action: #selector(methodCommentsClicked(_:)), for: .touchUpInside)
            cell?.btnTag.addTarget(self, action: #selector(methodTagPeopleClicked(_:)), for: .touchUpInside)

            let nib = UINib(nibName: "FeedMediaCell", bundle: nil)
            cell?.cllctnViewMedia.register(nib, forCellWithReuseIdentifier: "FeedMediaCell")
            
            cell?.cllctnViewMedia.delegate = self
            cell?.cllctnViewMedia.dataSource = self
            cell?.cllctnViewMedia.pageControll = cell?.pageControll
            cllctnViewMedia = cell?.cllctnViewMedia
            
            cell?.lblTitle.text = feedData.UserInfo.strName
            cell?.lblCaption.text = feedData.strCaption
            cell?.lblTime.text = self.MethodGetShowDateAndTime(strDateTimeStamp: feedData.strCreatedAtTimeStamp)
            cell?.btnLikes.setTitle(feedData.strLikeCount + ((feedData.strLikeCount as NSString).integerValue == 1 ? " Like" : " Likes"), for: .normal)
            cell?.btnDislikes.setTitle(feedData.strDislikeCount + ((feedData.strDislikeCount as NSString).integerValue == 1 ? " Dislike" : " Dislikes"), for: .normal)
            cell?.btnComments.setTitle(feedData.strCommentsCount + ((feedData.strCommentsCount as NSString).integerValue == 1 ? " Comment" : " Comments"), for: .normal)
            
            cell?.btnLikeFeed.isSelected = (feedData.strIsLiked as NSString).boolValue
            cell?.btnDislikeFeed.isSelected = (feedData.strIsDisliked as NSString).boolValue
            cell?.btnSavedFeed.isSelected = (feedData.strIsSaved as NSString).boolValue
            cell?.heightConstaintCllctnView.constant = feedData.arrMedia.count == 0 ? 0 : cell!.cllctnViewMedia.frame.size.width
            cell?.pageControll.numberOfPages = feedData.arrMedia.count
            cell?.btnSavedFeed.isHidden = feedData.arrMedia.count == 0
            cell?.btnTag.isHidden = (feedData.strTaggedPeople == "" || feedData.arrMedia.count == 0)

            if feedData.UserInfo.strProfilepicImage != ""
            {
                let url = URL.init(string: Profile_Thumb_Pic_URL + feedData.UserInfo.strUserId)
                cell?.imgUser.sd_addActivityIndicator()
                cell?.imgUser.sd_showActivityIndicatorView()
                cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.imgUser.sd_removeActivityIndicator()
                })
            }
            else
            {
                cell?.imgUser.image = UIImage(named:"reg_USER")
            }
            
            return cell!

        } else if indexPath.row == 1 {
            let indentifier:String = "CommentButtonCell"
            var cell : CommentButtonCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? CommentButtonCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("CommentButtonCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? CommentButtonCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.btnViewAllComment.isHidden = (feedData.strCommentsCount as NSString).integerValue <= 3
            cell?.btnViewAllComment.addTarget(self, action: #selector(methodCommentsClicked(_:)), for: .touchUpInside)
            cell?.btnViewAllComment.setTitle("View all " + ((feedData.strCommentsCount as NSString).integerValue == 1 ? "comments" : (feedData.strCommentsCount + " comments")), for: .normal)
            return cell!
            
        } else {
            let cellIdentifier:String = "PostCommentCell"
            var cell:PostCommentCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PostCommentCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("PostCommentCell", owner: nil, options: nil)! as [Any]
                
                cell = nib[0] as? PostCommentCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear);
            }
            cell?.btnUser.tag = indexPath.row-2
            cell?.btnUser.addTarget(self, action: #selector(methodUserProfileClicked(_:)), for: .touchUpInside)
            
            let commntObj = self.feedData.arrLast3Comments.object(at: indexPath.row-2) as! CommentModel
            
            cell?.commentLabel.text = commntObj.strComment
            
            cell?.nameLabel.text = commntObj.UserInfo.strName
            
            if commntObj.UserInfo.strProfilepicImage != ""
            {
                // \(Image_URL)
                let url = URL.init(string: commntObj.UserInfo.strProfilepicImage)
                cell?.userImageView.sd_addActivityIndicator()
                cell?.userImageView.sd_showActivityIndicatorView()
                cell?.userImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.userImageView.sd_removeActivityIndicator()
                })
            }
            else
            {
                cell?.userImageView.image = UIImage(named:"reg_USER")
            }
            return cell!
        }
    }

    // MARK: - UICollectionView Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100
        {
            return 5
        }
        else
        {
            return feedData.arrMedia.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView.tag == 100
        {
            return CGSize(width: 56, height: 100)
        }
        else
        {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.width)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let identifier: String = "FeedMediaCell"
        
        let cell: FeedMediaCell? = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)as? FeedMediaCell
        
        let feedMediaData = feedData.arrMedia.object(at: indexPath.item) as! FeedMediaModel
        
        if feedMediaData.strMediaURL != ""
        {
            // \(Image_URL)
            
            if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
            {
                let url = URL.init(string: feedMediaData.strMediaURL)
                cell?.imgUser.sd_addActivityIndicator()
                cell?.imgUser.sd_showActivityIndicatorView()
                cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.imgUser.sd_removeActivityIndicator()
                })
                cell?.imgUser.isHidden = false
            }
            else
            {
                if ((feedMediaData.strMediaThumbURL != "" && self.avPlayer == nil && self.avPlayerViewController == nil) || self.urlOfCurrentlyPlayingInPlayer() != URL(string: feedMediaData.strMediaURL)!) == false
                {
                    cell?.imgUser.isHidden = true
                }
                else
                {
                    cell?.imgUser.isHidden = false
                }
                let url = URL.init(string: feedMediaData.strMediaThumbURL)
                cell?.imgUser.sd_addActivityIndicator()
                cell?.imgUser.sd_showActivityIndicatorView()
                cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.imgUser.sd_removeActivityIndicator()
                })
            }
        }
        else
        {
            cell?.imgUser.image = UIImage(named:"slider-img-placeholder")
        }
        cell?.backgroundColor = UIColor.clear
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        let cell1: FeedMediaCell? = cell as? FeedMediaCell
        
        cell1!.imgUser.isHidden = false
        
        let feedMediaData = feedData.arrMedia.object(at: indexPath.item) as! FeedMediaModel
        
        if feedMediaData.strMediaURL != ""
        {
            if (feedMediaData.strMediaType != kKeySocialMediaTypeImage)
            {
                if feedMediaData.strMediaURL.count > 0
                {
                    self.removeAVPlayer()
                    if (feedMediaData.strMediaThumbURL != "" && self.avPlayer == nil && self.avPlayerViewController == nil) || self.urlOfCurrentlyPlayingInPlayer() != URL(string: feedMediaData.strMediaURL)!
                    {
                        self.removeAVPlayer()
                        let urlMainPost = URL.init(string: feedMediaData.strMediaURL)
                        
                        let item = AVPlayerItem(url: urlMainPost as! URL)
                        
                        do {
                            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                        } catch {
                            print(error)
                        }
                        self.avPlayer = AVPlayer(playerItem: item);
                        self.avPlayer.actionAtItemEnd = .none
                        print("SecvideoUrl",urlMainPost)
                        print("item",item)
                        
                        self.avPlayerViewController = AVPlayerViewController()
                        self.avPlayerViewController.player = self.avPlayer
                        self.avPlayerViewController.view.frame = cell1!.imgUser!.frame
                        self.avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                        
                        cell1!.contentView.addSubview(self.avPlayerViewController.view)
                        self.addChildViewController(self.avPlayerViewController)
                        self.avPlayer.play()
                        
                        
                        NotificationCenter.default.addObserver(self,
                                                               selector: #selector(self.playerItemDidReachEnd),
                                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                               object: self.avPlayer.currentItem!)
                        cell1!.avPlayer = self.avPlayer
                        cell1!.avPlayerViewController = self.avPlayerViewController
                        cell1!.imgUser.isHidden = true
                        cell1!.contentView.tag = collectionView.tag
                        cell1!.imgUser.tag = indexPath.item
                        self.avPlayerViewController.view.tag = indexPath.item
                    }
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        if self.avPlayer != nil && self.avPlayerViewController != nil && self.avPlayerViewController.view.tag == indexPath.row
        {
            removeAVPlayer()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.OpenFullImage(idx: indexPath.item)
    }
    func urlOfCurrentlyPlayingInPlayer() -> URL?
    {
        return ((self.avPlayer.currentItem?.asset) as? AVURLAsset)?.url
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification)
    {
        self.removeAVPlayer()
    }
    
    func removeAVPlayer()  {
        
        if (self.avPlayer != nil && self.avPlayerViewController != nil)
        {
            if self.avPlayerViewController.view.superview != nil
            {
                if self.avPlayer.isPlaying == true
                {
                    self.avPlayer.pause()
                }
                let idxPath = IndexPath(row: (self.avPlayerViewController.view.superview?.tag ?? 0), section: 0)
                if let feedTBLVideoCell = feedTableView.cellForRow(at: idxPath) as? FeedVideoCell
                {
                    let idxCllctnPath = IndexPath(row: self.avPlayerViewController.view.tag, section: 0)
                    let cell = feedTBLVideoCell.cllctnViewMedia.cellForItem(at: idxCllctnPath) as? FeedMediaCell
                    
                    cell?.imgUser.isHidden = false
                    
                    let feedMediaData = feedData.arrMedia.object(at: idxCllctnPath.item) as! FeedMediaModel
                    
                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
            }
            
            self.avPlayer = nil
            self.avPlayerViewController.view.removeFromSuperview()
            self.avPlayerViewController = nil
        }
    }
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView is customCollectionView
        {
            if let customCllctnView = scrollView as? customCollectionView
            {
                customCllctnView.pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                
                customCllctnView.pageControll.currentPage = Int(customCllctnView.pageNumber)
            }
        }
    }
    //MARK:- UIButton Action Methods
    @objc func rightBarButtonAction(_ sender: Any)
    {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        let firstAction = UIAlertAction(title: (self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) ? "Delete" : "Report", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.reportPost()
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(firstAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }

    @objc func methodUserProfileClicked(_ sender: UIButton)
    {
        let userObj = self.feedData.UserInfo
        if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
        {
            let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
            other.userObj = userObj
            other.strUserID = userObj.strUserId
            other.userNameString = userObj.strName
            self.navigationController?.pushViewController(other, animated: true)
        }
        else
        {
            let register = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
        }
        
    }
    @objc func methodLikeFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.LikeFeedApi(feedObj:)), with: self.feedData)
        }
    }
    @objc func methodDislikeFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.DislikeFeedApi(feedObj:)), with: self.feedData)
        }
    }
    @objc func methodSaveFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.BookmarkFeedApi(feedObj:)), with: self.feedData)
        }
    }
    @objc func methodLikesUsersListClicked(_ sender: UIButton)
    {
        let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
        likesUser.strFeedId = feedData.strFeedId
        likesUser.strType = "LIKES"
        self.navigationController?.pushViewController(likesUser, animated: true)
    }
    @objc func methodDislikesUsersListClicked(_ sender: UIButton)
    {
        let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
        likesUser.strFeedId = feedData.strFeedId
        likesUser.strType = "DISLIKES"
        self.navigationController?.pushViewController(likesUser, animated: true)
    }
    @objc func methodCommentsClicked(_ sender: UIButton)
    {
        let CommentVCObjc = PostCommentScreen.init(nibName: "PostCommentScreen", bundle: nil)
        CommentVCObjc.feedData = feedData
        self.navigationController?.pushViewController(CommentVCObjc, animated: true)
    }
    @objc func methodTagPeopleClicked(_ sender: UIButton)
    {
        let taggedPeople = TaggedPeopleScreen.init(nibName: "TaggedPeopleScreen", bundle: nil)
        taggedPeople.strFeedId = feedData.strFeedId
        taggedPeople.arrSelectedTaggedPeople = NSMutableArray.init(array: feedData.strTaggedPeople.components(separatedBy: ","))
        self.navigationController?.pushViewController(taggedPeople, animated: true)
    }
    func reportPost()
    {
        let alertContr  =   UIAlertController(title: kAppName, message: (self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) ? kMessageSocialDeletePost : kMessageSocialReportPost, preferredStyle: .alert)
        
        if (self.feedData.UserInfo.strUserId != self.appDelegate.currentLoginUser.strUserId)
        {
            alertContr.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Enter Message"
            })
        }
        let cancelAction  = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in
        }
        
        let okAction  = UIAlertAction(title: (self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) ? "Delete" : "Report", style: .default) { (alert: UIAlertAction!) in
            
            var str = ""
            if (self.feedData.UserInfo.strUserId != self.appDelegate.currentLoginUser.strUserId)
            {
                let firstTextField = alertContr.textFields![0] as UITextField
                str = firstTextField.text!
                if (firstTextField.text == "")
                {
                    self.showToastOnViewController1(title: kError, message: kMessageSocialEnterReport, position: kToastTopPosition, controller: self, image: nil)
                    return
                }
            }
            DispatchQueue.main.async
                {
                    if self.appDelegate.isInternetAvailable() == false
                    {
                        self.showToastOnViewController1(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        self.view.endEditing(true)
                        self.showActivity(text: "")
                        self.performSelector(inBackground: #selector(self.ReportDeletePostApi(strMessage:)), with: str)
                    }
            }
        }
        alertContr.addAction(cancelAction)
        alertContr.addAction(okAction)
        
        self.present(alertContr, animated: true, completion: nil)

    }

    //MARK:- MediaBrowser OpenFullImage Method
    func OpenFullImage(idx: Int)
    {
        var photos = [Media]()
        var thumbs = [Media]()
        var photo: Media!
        var thumb: Media!
        let displaySelectionButtons: Bool = false
        let displayMediaNavigationArrows: Bool = true
        let enableGrid: Bool = false
        let startOnGrid: Bool = false
        let displayActionButton: Bool = false
        let autoPlayOnAppear: Bool = true
        
        
        for io in 0..<self.feedData.arrMedia.count
        {
            let feedMediaData = feedData.arrMedia.object(at: io) as! FeedMediaModel
            
            if feedMediaData.strMediaURL != ""
            {
                // \(Image_URL)
                
                if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                {
                    let thumburl = URL.init(string: feedMediaData.strMediaThumbURL)
                    let url = URL.init(string: feedMediaData.strMediaURL)
                    
                    photo = Media(url: url!)
                    photo.isVideo = false
                    photos.append((photo! as Media))
                    
                    thumb = Media(url: thumburl ?? url!)
                    thumb.isVideo = false
                    thumbs.append((thumb! as Media))
                }
                else
                {
                    
                    let thumburl = URL.init(string: feedMediaData.strMediaThumbURL)!
                    let url = URL.init(string: feedMediaData.strMediaURL)!
                    
                    photo = Media(videoURL: url, previewImageURL: thumburl)
                    photo.isVideo = true
                    photos.append((photo! as Media))
                    
                    thumb = Media(url: thumburl)
                    thumb.isVideo = true
                    thumbs.append((thumb! as Media))
                }
            }
        }
        self.arrMediaPhotos = photos
        self.arrMediaThumbs = thumbs
        
        if  self.arrMediaPhotos.count > 0
        {
            let browser = MediaBrowser(delegate: self)
            browser.displayActionButton = displayActionButton
            browser.displayMediaNavigationArrows = displayMediaNavigationArrows
            browser.displaySelectionButtons = displaySelectionButtons
            browser.alwaysShowControls = displaySelectionButtons
            browser.zoomPhotosToFill = true
            browser.enableGrid = enableGrid
            browser.startOnGrid = startOnGrid
            browser.enableSwipeToDismiss = true
            browser.autoPlayOnAppear = autoPlayOnAppear
            //            browser.cachingImageCount = 2
            browser.setCurrentIndex(at:idx)

            navigationController?.pushViewController(browser, animated: true)
        }
    }
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- All API Calling Methods
    func getFeedsDetailsApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedData.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        return dicrequest
    }
    //MARK : - getFeedsDetailsApi
    @objc func getFeedsDetailsApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialPostDetails, Details: getFeedsDetailsApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            self.feedData = FeedModel.init(dict: dicRes.object(forKey: kData) as? NSDictionary ?? NSDictionary())
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                        self.perform(#selector(self.scrollToMedia), with: nil, afterDelay: 0.01)
//                        self.selectedRow = 0
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    func LikeFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsLiked == "1" || feedObj.strIsLiked == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - LikeFeedApi
    @objc func LikeFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialLikePost, Details: LikeFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.performSelector(inBackground: #selector(self.getFeedsDetailsApi), with: nil)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func DislikeFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsDisliked == "1" || feedObj.strIsDisliked == "true") ? "0" : "-1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - DislikeFeedApi
    @objc func DislikeFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialLikePost, Details: DislikeFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.performSelector(inBackground: #selector(self.getFeedsDetailsApi), with: nil)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func BookmarkFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsSaved == "1" || feedObj.strIsSaved == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - BookmarkFeedApi
    @objc func BookmarkFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialSavePost, Details: BookmarkFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.performSelector(inBackground: #selector(self.getFeedsDetailsApi), with: nil)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

    func deleteCommentApiKeys(idx: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.feedData.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        
        let commntObj = self.feedData.arrLast3Comments.object(at: Int(idx)!) as! CommentModel
        
        dicrequest.setObject(commntObj.strCommentId.getEncryptedString() ?? "", forKey: kKeySocialCommentId as NSCopying)
        
        return dicrequest
    }
    //MARK : - deleteCommentApi
    @objc func deleteCommentApi(idx: String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialDeleteComment, Details: deleteCommentApiKeys(idx: idx)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            //                            self.commentArray = getUsersObjectArray(arrResult: dicRes.object(forKey: kData) as? NSArray ?? NSArray())
                            
                            let commentData = self.feedData.arrLast3Comments.object(at: Int(idx)!) as! CommentModel
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialDeleteComment), object: commentData)
                            
                            self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                            self.feedData.arrLast3Comments.removeObject(at: Int(idx)!)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
                self.feedTableView.reloadData()
            }
        }
    }
    
    func ReportDeletePostApiKeys(strMessage: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.feedData.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        if (self.feedData.UserInfo.strUserId != self.appDelegate.currentLoginUser.strUserId)
        {
            dicrequest.setObject(strMessage.getEncryptedString() ?? "", forKey: kKeySocialReportMessage as NSCopying)
        }
        return dicrequest
    }
    //MARK : - ReportDeletePostApi
    @objc func ReportDeletePostApi(strMessage: String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: (self.feedData.UserInfo.strUserId == self.appDelegate.currentLoginUser.strUserId) ? kAPISocialDeletePost : kAPISocialReportPost, Details: ReportDeletePostApiKeys(strMessage: strMessage)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)
                            self.showToastOnViewController1(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)

                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.showToastOnViewController1(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController1(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController1(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    

}
//https://github.com/younatics/MediaBrowser
//MARK: MediaBrowserDelegate
extension FeedDetailVC: MediaBrowserDelegate
{
    func thumbnail(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < arrMediaThumbs.count {
            return arrMediaThumbs[index]
        }
        return  Media(image: UIImage.init(named: "slider-img-placeholder") ?? UIImage(), caption: "")
        
    }
    
    func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < arrMediaPhotos.count {
            return arrMediaPhotos[index]
        }
        return  Media(image: UIImage.init(named: "slider-img-placeholder") ?? UIImage(), caption: "")
    }
    
    func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
        return arrMediaPhotos.count
    }
    
//    func isMediaSelected(at index: Int, in mediaBrowser: MediaBrowser) -> Bool {
//        return Chatselections[index] as! Bool
//
//    }
    
    func didDisplayMedia(at index: Int, in mediaBrowser: MediaBrowser) {
        print("Did start viewing photo at index \(index)")
        
    }
    
//    func mediaDid(selected: Bool, at index: Int, in mediaBrowser: MediaBrowser) {
//        Chatselections[index] = selected
//    }
}
