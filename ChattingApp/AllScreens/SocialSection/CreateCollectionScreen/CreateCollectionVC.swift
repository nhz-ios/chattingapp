//
//  CreateCollectionVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 07/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class CreateCollectionVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nameTextFeild: UITextField!
    @IBOutlet var toolBar: UIToolbar!
  
    @IBOutlet weak var contectScrollView: UIScrollView!
    var strFrom = "Saved"
    var collectionData = CollectionModel()

    //MARK:- UIviewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextFeild.inputAccessoryView = toolBar
       self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationBarWithBackButton(strTitle: "CREATE COLLECTION", leftbuttonImageName: "back.png")
    }

    //MARK:- Helper Method
    func initialSetUp() {
        self.contectScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.nextButton.frame.origin.y + self.nextButton.frame.size.height + 20)
        
        self.nextButton.layer.borderWidth = 1
        self.nextButton.layer.borderColor = UIColor.init(red: 3.0/255, green: 152.0/255, blue: 215.0/255, alpha: 1).cgColor
        self.nextButton.layer.cornerRadius = 3
        
        nameTextFeild.text =  collectionData.strCollectionName
    }
    
    //MARK:- UITextFeild Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        if self.CheckValidation()
        {
            let addFromSaved = AddFromSavedScreen.init(nibName: "AddFromSavedScreen", bundle: nil)
            addFromSaved.strFrom = strFrom
            addFromSaved.collectionData = collectionData
            addFromSaved.collectionData.strCollectionName = nameTextFeild.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
            self.navigationController?.pushViewController(addFromSaved, animated: true)

        }
    }
    
    //MARK: - CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if nameTextFeild.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" == "" && self.appDelegate.arrPostUrls.count == 0
        {
            isGo = false
            errorMessage = kMessageSocialEnterCollection
        }
        if !isGo {
            
            self.showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        return isGo
    }
    

    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
