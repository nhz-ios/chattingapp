//
//  OtherProfileVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class OtherProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var headerView: UIView!
    @IBOutlet weak var gridButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var profileTableView: UITableView!
    
    var userNameString = "WILLIAM ROSE"
    
    var userObj: UserModel!
    var strUserID = ""

    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!
    var currentPageNoMedia = 1
    var maxPageMedia = 0
    var boolGetDataMedia:Bool!

    var currentPageNoTagged = 1
    var maxPageTagged = 0
    var boolGetDataTagged:Bool!
    
    var arrFeeds = NSMutableArray()
    var arrFeedsMedia = NSMutableArray()
    var arrFeedsTagged = NSMutableArray()

    var avPlayer: AVPlayer!
    var avPlayerViewController: AVPlayerViewController!
    

    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        profileTableView.addSubview(refreshControl)
        
        profileTableView.rowHeight = UITableViewAutomaticDimension
        profileTableView.estimatedRowHeight = 300
        

        NotificationCenter.default.addObserver(self, selector: #selector(otherProfileUpdateNotification), name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateOtherProfile), object: nil)
        self.initialSetUp()
        self.callListAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        let RightBarButton = UIBarButtonItem.init(image: UIImage.init(named: "three_dots"), style: .done, target: self, action: #selector(popUpViewButtonAction(_:)))
        self.navigationItem.rightBarButtonItem = RightBarButton
        
        if self.avPlayer != nil && self.avPlayerViewController != nil
        {
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                print(error)
            }
            
            self.avPlayer.play()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.removeAVPlayer()
    }
    
    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            if arrFeeds.count == 0
            {
                self.showActivity(text: "")
            }
            self.performSelector(inBackground: (self.gridButton.isSelected == true) ? #selector(self.self.getMediaFeedsListApi) : ((self.listButton.isSelected == true) ? #selector(self.getFeedsListApi) : #selector(self.self.getTaggedFeedsListApi)), with: nil)
//            self.performSelector(inBackground: #selector(self.getMediaFeedsListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }
    
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.methodRefreshAllData()
        })
    }
    
    @objc func methodRefreshAllData()
    {
        self.currentPageNo = 1
        self.maxPage = 0
        self.currentPageNoMedia = 1
        self.maxPageMedia = 0
        self.currentPageNoTagged = 1
        self.maxPageTagged = 0
        self.callListAPI()
    }

    //MARK:- Helper Method
    func initialSetUp()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(updateFeedsData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialUpdateFeeds), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addNewFeedsData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialAddNewFeed), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCommentData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialDeleteComment), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addNewCommentData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialAddNewComment), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodRefreshAllData), name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)

        self.gridButton.isSelected = true
        
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            if strUserID != "" && self.userObj == nil
            {
                self.showActivity(text: "")
            }
            if strUserID != ""
            {
                self.performSelector(inBackground: #selector(self.getOtherUserProfileApi), with: nil)
            }
        }
        navigationBarWithBackButton(strTitle: self.userNameString, leftbuttonImageName: "back.png")
        self.profileTableView.reloadData()
    }
    
    //MARK:- Notification Observer Methods
    @objc func updateFeedsData(notiObj: Notification)
    {
        let feedObj = notiObj.object as! FeedModel
        self.replaceFeedObjectInArray(feedObj: feedObj)
        self.profileTableView.reloadData()
    }
    @objc func addNewFeedsData(notiObj: Notification)
    {
        let feedObj = notiObj.object as! FeedModel
        self.arrFeeds.insert(feedObj, at: 0)
        self.maxPage += 1
        self.profileTableView.reloadData()
        self.profileTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
    }
    @objc func deleteCommentData(notiObj: Notification)
    {
        let commentObj = notiObj.object as! CommentModel
        let feedObj = self.getFeedObjectInArray(feedId: commentObj.strFeedId)
        feedObj.strCommentsCount = "\((feedObj.strCommentsCount as NSString).integerValue - 1)"
        self.profileTableView.reloadData()
    }
    @objc func addNewCommentData(notiObj: Notification)
    {
        let commentObj = notiObj.object as! CommentModel
        let feedObj = self.getFeedObjectInArray(feedId: commentObj.strFeedId)
        feedObj.strCommentsCount = "\((feedObj.strCommentsCount as NSString).integerValue + 1)"
        self.profileTableView.reloadData()
    }
    @objc func otherProfileUpdateNotification(data : NSNotification)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            if strUserID != "" && self.userObj == nil
            {
                self.showActivity(text: "")
            }
            if strUserID != ""
            {
                self.performSelector(inBackground: #selector(self.getOtherUserProfileApi), with: nil)
            }
        }
    }
    


    //MARK:- UITableView DataSource And Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        } else {
            return self.headerView
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
            
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
            
        } else {
            if self.listButton.isSelected {
                return self.arrFeeds.count
                
            }
            else {
                return 1
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
            
        } else {
            if self.listButton.isSelected {
                return UITableViewAutomaticDimension

            } else {
                return (UIScreen.main.bounds.size.height - 246)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let indentifier:String = "OtherProfileCell"
            var cell : OtherProfileCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? OtherProfileCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("OtherProfileCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? OtherProfileCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }

            if strUserID != "" && self.userObj != nil
            {
                cell?.nameLabel.text = self.userObj.strName
                cell?.qutationLabel.text = self.userObj.strBio
                cell?.followedByLabel.text = self.userObj.strFollowedBy

                if self.userObj.strProfilepicImage != ""
                {
                    // \(Image_URL)
                    let url = URL.init(string: self.userObj.strProfilepicImage)
                    cell?.otherUserImageView.sd_addActivityIndicator()
                    cell?.otherUserImageView.sd_showActivityIndicatorView()
                    cell?.otherUserImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.otherUserImageView.sd_removeActivityIndicator()
                    })
                }
                else
                {
                    cell?.otherUserImageView.image = UIImage(named:"reg_USER")
                }
                cell?.lblFeedsCount.text = self.userObj.strFeedsCount
                cell?.lblFollowersCount.text = self.userObj.strFollowersCount
                cell?.lblFollowingsCount.text = self.userObj.strFollowingsCount
                cell?.followButton.isSelected = (self.userObj.strIsFollowed == "1" || self.userObj.strIsFollowed == "true") ? true : false
          
                cell?.followersButton.tag = 0
                cell?.followingsButton.tag = 1
                cell?.followersButton.addTarget(self, action: #selector(followlistButtonAction(_:)), for: .touchUpInside)
                cell?.followingsButton.addTarget(self, action: #selector(followlistButtonAction(_:)), for: .touchUpInside)
                cell?.followButton.addTarget(self, action: #selector(followButtonAction(_:)), for: .touchUpInside)
            }
            else
            {
                cell?.nameLabel.text = "William Rose"
                cell?.qutationLabel.text = "My dream is to fly... Over a RAINBOW... so high"
                cell?.followedByLabel.text = "Followed by alfred_smile & 2 others"
            }


            return cell!
            
        }
        else
        {
            if self.listButton.isSelected
            {
                let indentifier:String = "FeedVideoCell"
                var cell : FeedVideoCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? FeedVideoCell
                
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("FeedVideoCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? FeedVideoCell
                    cell?.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                
                cell?.btnUser.tag = indexPath.row
                cell?.btnLikes.tag = indexPath.row
                cell?.btnDislikes.tag = indexPath.row
                cell?.btnLikeFeed.tag = indexPath.row
                cell?.btnDislikeFeed.tag = indexPath.row
                cell?.btnSavedFeed.tag = indexPath.row
                cell?.cllctnViewMedia.tag = indexPath.row
                cell?.pageControll.tag = indexPath.row
                cell?.btnComments.tag = indexPath.row
                cell?.btnTag.tag = indexPath.row
                cell?.btnUser.addTarget(self, action: #selector(methodUserProfileClicked(_:)), for: .touchUpInside)
                cell?.btnLikes.addTarget(self, action: #selector(methodLikesUsersListClicked(_:)), for: .touchUpInside)
                cell?.btnDislikes.addTarget(self, action: #selector(methodDislikesUsersListClicked(_:)), for: .touchUpInside)
                cell?.btnLikeFeed.addTarget(self, action: #selector(methodLikeFeedClicked(_:)), for: .touchUpInside)
                cell?.btnDislikeFeed.addTarget(self, action: #selector(methodDislikeFeedClicked(_:)), for: .touchUpInside)
                cell?.btnSavedFeed.addTarget(self, action: #selector(methodSaveFeedClicked(_:)), for: .touchUpInside)
                cell?.btnComments.addTarget(self, action: #selector(methodCommentsClicked(_:)), for: .touchUpInside)
                cell?.btnTag.addTarget(self, action: #selector(methodTagPeopleClicked(_:)), for: .touchUpInside)

                let nib = UINib(nibName: "FeedMediaCell", bundle: nil)
                cell?.cllctnViewMedia.register(nib, forCellWithReuseIdentifier: "FeedMediaCell")
                
                cell?.cllctnViewMedia.delegate = self
                cell?.cllctnViewMedia.dataSource = self
                cell?.cllctnViewMedia.pageControll = cell?.pageControll
                
                let feedData = self.arrFeeds.object(at: indexPath.row) as! FeedModel
                
                cell?.lblTitle.text = feedData.UserInfo.strName
                cell?.lblCaption.text = feedData.strCaption
                cell?.lblTime.text = self.MethodGetShowDateAndTime(strDateTimeStamp: feedData.strCreatedAtTimeStamp)
                cell?.btnLikes.setTitle(feedData.strLikeCount + ((feedData.strLikeCount as NSString).integerValue == 1 ? " Like" : " Likes"), for: .normal)
                cell?.btnDislikes.setTitle(feedData.strDislikeCount + ((feedData.strDislikeCount as NSString).integerValue == 1 ? " Dislike" : " Dislikes"), for: .normal)
                cell?.btnComments.setTitle(feedData.strCommentsCount + ((feedData.strCommentsCount as NSString).integerValue == 1 ? " Comment" : " Comments"), for: .normal)
                
                cell?.btnLikeFeed.isSelected = (feedData.strIsLiked as NSString).boolValue
                cell?.btnDislikeFeed.isSelected = (feedData.strIsDisliked as NSString).boolValue
                cell?.btnSavedFeed.isSelected = (feedData.strIsSaved as NSString).boolValue
                cell?.heightConstaintCllctnView.constant = feedData.arrMedia.count == 0 ? 0 : cell!.cllctnViewMedia.frame.size.width
                cell?.pageControll.numberOfPages = feedData.arrMedia.count
                cell?.btnSavedFeed.isHidden = feedData.arrMedia.count == 0
                cell?.btnTag.isHidden = (feedData.strTaggedPeople == "" || feedData.arrMedia.count == 0)

                if feedData.UserInfo.strProfilepicImage != ""
                {
                    let url = URL.init(string: Profile_Thumb_Pic_URL + feedData.UserInfo.strUserId)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
                else
                {
                    cell?.imgUser.image = UIImage(named:"reg_USER")
                }
                
                return cell!
                
            }
            else if self.postButton.isSelected
            {
                let indentifier:String = "GridCell"
                var cell : GridCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? GridCell
                
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("GridCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? GridCell
                    cell?.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                let nib = UINib(nibName: "ImageCollectionCell", bundle: nil)
                cell?.gridCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCollectionCell")
                
                cell?.gridCollectionView.tag = 102
                cell?.gridCollectionView.delegate = self
                cell?.gridCollectionView.dataSource = self
                return cell!
                
            }
            else
            {
                let indentifier:String = "GridCell"
                var cell : GridCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? GridCell
                
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("GridCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? GridCell
                    cell?.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                let nib = UINib(nibName: "ImageCollectionCell", bundle: nil)
                cell?.gridCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCollectionCell")
                
                cell?.gridCollectionView.tag = 101
                cell?.gridCollectionView.delegate = self
                cell?.gridCollectionView.dataSource = self
                
                return cell!
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            if self.listButton.isSelected
            {
                let feedData = self.arrFeeds.object(at: indexPath.row) as! FeedModel
                let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
                feedDetails.feedData = feedData
                feedDetails.selectedRow = 0
                self.navigationController?.pushViewController(feedDetails, animated: true)
            }
            else if self.postButton.isSelected
            {
            }
            else
            {
            }
        }
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.avPlayer != nil && self.avPlayerViewController != nil && (self.avPlayerViewController.view.superview?.tag ?? 0) == indexPath.row
        {
            removeAVPlayer()
        }
    }

    //MARK:- UICollectionView DataSource And Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 101
        {
            return self.arrFeedsMedia.count
        }
        else if collectionView.tag == 102
        {
            return self.arrFeedsTagged.count
        }
        else
        {
            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            return feedData.arrMedia.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView.tag == 101 || collectionView.tag == 102
        {
            return CGSize(width: ((collectionView.frame.width/3)-5), height: (collectionView.frame.width/3)-5)
        }
        else
        {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.width)
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView.tag == 101
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
            
            cell.feedImageView.image = UIImage(named:"slider-img-placeholder")
            cell.tagImageView.isHidden = true
            
            let feedData = self.arrFeedsMedia.object(at: indexPath.item) as! FeedModel
            if feedData.arrMedia.count > 0
            {
                let feedMediaData = feedData.arrMedia.object(at: 0) as! FeedMediaModel
                
                if feedMediaData.strMediaURL != ""
                {
                    if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                    {
                        let url = URL.init(string: feedMediaData.strMediaURL)
                        cell.feedImageView.sd_addActivityIndicator()
                        cell.feedImageView.sd_showActivityIndicatorView()
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                            cell.feedImageView.sd_removeActivityIndicator()
                        })
                        cell.feedImageView.isHidden = false
                    }
                    else
                    {
                        let url = URL.init(string: feedMediaData.strMediaThumbURL)
                        cell.feedImageView.sd_addActivityIndicator()
                        cell.feedImageView.sd_showActivityIndicatorView()
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                            cell.feedImageView.sd_removeActivityIndicator()
                        })
                        cell.feedImageView.isHidden = false
                    }
                }
            }
            
            return cell
        }
        else if collectionView.tag == 102
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
            
            cell.feedImageView.image = UIImage(named:"slider-img-placeholder")
            cell.tagImageView.isHidden = true
            
            let feedData = self.arrFeedsTagged.object(at: indexPath.item) as! FeedModel
            if feedData.arrMedia.count > 0
            {
                let feedMediaData = feedData.arrMedia.object(at: 0) as! FeedMediaModel
                
                if feedMediaData.strMediaURL != ""
                {
                    if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                    {
                        let url = URL.init(string: feedMediaData.strMediaURL)
                        cell.feedImageView.sd_addActivityIndicator()
                        cell.feedImageView.sd_showActivityIndicatorView()
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                            cell.feedImageView.sd_removeActivityIndicator()
                        })
                        cell.feedImageView.isHidden = false
                    }
                    else
                    {
                        let url = URL.init(string: feedMediaData.strMediaThumbURL)
                        cell.feedImageView.sd_addActivityIndicator()
                        cell.feedImageView.sd_showActivityIndicatorView()
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                            cell.feedImageView.sd_removeActivityIndicator()
                        })
                        cell.feedImageView.isHidden = false
                    }
                }
            }
            
            return cell
        }
        else
        {
            let identifier: String = "FeedMediaCell"
            
            let cell: FeedMediaCell? = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)as? FeedMediaCell
            
            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            let feedMediaData = feedData.arrMedia.object(at: indexPath.item) as! FeedMediaModel
            
            if feedMediaData.strMediaURL != ""
            {
                // \(Image_URL)
                
                if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                {
                    let url = URL.init(string: feedMediaData.strMediaURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                    cell?.imgUser.isHidden = false
                }
                else
                {
                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
            }
            else
            {
                cell?.imgUser.image = UIImage(named:"slider-img-placeholder")
            }
            
            
            cell?.backgroundColor = UIColor.clear
            
            //            let strImage = (appDelegate.LearnerUserInfo.teacherDetails.arrTeacherMedia.object(at: indexPath.item) as! NSDictionary).object(forKey: "image") as! String
            //
            //            let url = URL.init(string: "\(Image_URL)\(strImage)")
            //            cell?.imgUser.sd_addActivityIndicator()
            //
            //            cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "gallery-img-default"), options: .refreshCached, completed: { (img, error, cacheType, url) in
            //                cell?.imgUser.sd_removeActivityIndicator()
            //            })
            
            return cell!
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if collectionView.tag != 101 &&  collectionView.tag != 102
        {
            let cell1: FeedMediaCell? = cell as? FeedMediaCell
            
            cell1!.imgUser.isHidden = false
            
            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            let feedMediaData = feedData.arrMedia.object(at: indexPath.item) as! FeedMediaModel
            
            if feedMediaData.strMediaURL != ""
            {
                if (feedMediaData.strMediaType != kKeySocialMediaTypeImage)
                {
                    if feedMediaData.strMediaURL.count > 0
                    {
                        if (feedMediaData.strMediaThumbURL != "" && self.avPlayer == nil && self.avPlayerViewController == nil) || self.urlOfCurrentlyPlayingInPlayer() != URL(string: feedMediaData.strMediaURL)!
                        {
                            self.removeAVPlayer()
                            let urlMainPost = URL.init(string: feedMediaData.strMediaURL)
                            
                            let item = AVPlayerItem(url: urlMainPost as! URL)
                            
                            do {
                                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                            } catch {
                                print(error)
                            }
                            self.avPlayer = AVPlayer(playerItem: item);
                            self.avPlayer.actionAtItemEnd = .none
                            print("SecvideoUrl",urlMainPost)
                            print("item",item)
                            
                            self.avPlayerViewController = AVPlayerViewController()
                            self.avPlayerViewController.player = self.avPlayer
                            self.avPlayerViewController.view.frame = cell1!.imgUser!.frame
                            self.avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                            
                            //self.avPlayerLayer = AVPlayerLayer(player:self.avPlayer);
                            //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                            //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
                            //self.avPlayerLayer.frame = cell!.imgProfileVideo!.frame
                            //cell?.contentView.layer.addSublayer(self.avPlayerLayer)
                            
                            cell1!.contentView.addSubview(self.avPlayerViewController.view)
                            self.addChildViewController(self.avPlayerViewController)
                            self.avPlayer.play()
                            
                            
                            NotificationCenter.default.addObserver(self,
                                                                   selector: #selector(self.playerItemDidReachEnd),
                                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                                   object: self.avPlayer.currentItem!)
                            cell1!.avPlayer = self.avPlayer
                            cell1!.avPlayerViewController = self.avPlayerViewController
                            cell1!.imgUser.isHidden = true
                            cell1!.contentView.tag = collectionView.tag
                            cell1!.imgUser.tag = indexPath.item
                            self.avPlayerViewController.view.tag = indexPath.item
                        }
                        
                        //                        if self.avPlayer != nil && self.avPlayerViewController != nil && cell?.avPlayer == nil && cell?.avPlayerViewController == nil
                        //                        {
                        //                            cell?.avPlayer = self.avPlayer
                        //                            cell?.avPlayerViewController = self.avPlayerViewController
                        //                            cell?.contentView.addSubview(self.avPlayerViewController.view)
                        //                            self.addChildViewController(self.avPlayerViewController)
                        //                            self.avPlayer.play()
                        //
                        //                        }
                        //                        else
                        //                        {
                        //                            cell?.imgUser.isHidden = false
                        //                        }
                        
                    }
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        if self.avPlayer != nil && self.avPlayerViewController != nil && self.avPlayerViewController.view.tag == indexPath.row
        {
            removeAVPlayer()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView.tag == 101
        {
            let feedData = self.arrFeedsMedia.object(at: indexPath.item) as! FeedModel
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedData
            feedDetails.selectedRow = 0
            self.navigationController?.pushViewController(feedDetails, animated: true)
        }
        else if collectionView.tag == 102
        {
            let feedData = self.arrFeedsTagged.object(at: indexPath.item) as! FeedModel
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedData
            feedDetails.selectedRow = 0
            self.navigationController?.pushViewController(feedDetails, animated: true)
        }
        else
        {
            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedData
            feedDetails.selectedRow = indexPath.item
            self.navigationController?.pushViewController(feedDetails, animated: true)
        }
    }
    func urlOfCurrentlyPlayingInPlayer() -> URL?
    {
        return ((self.avPlayer.currentItem?.asset) as? AVURLAsset)?.url
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification)
    {
        self.removeAVPlayer()
    }
    
    func removeAVPlayer()  {
        
        if (self.avPlayer != nil && self.avPlayerViewController != nil)
        {
            if self.avPlayerViewController.view.superview != nil
            {
                if self.avPlayer.isPlaying == true
                {
                    self.avPlayer.pause()
                }
                let idxPath = IndexPath(row: (self.avPlayerViewController.view.superview?.tag ?? 0), section: 1)
                if let feedTBLVideoCell = profileTableView.cellForRow(at: idxPath) as? FeedVideoCell
                {
                    let idxCllctnPath = IndexPath(row: self.avPlayerViewController.view.tag, section: 0)
                    let cell = feedTBLVideoCell.cllctnViewMedia.cellForItem(at: idxCllctnPath) as? FeedMediaCell
                    
                    cell?.imgUser.isHidden = false
                    
                    let feedData = self.arrFeeds.object(at: idxPath.row) as! FeedModel
                    let feedMediaData = feedData.arrMedia.object(at: idxCllctnPath.item) as! FeedMediaModel
                    
                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
            }
            
            self.avPlayer = nil
            self.avPlayerViewController.view.removeFromSuperview()
            self.avPlayerViewController = nil
        }
    }
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView.tag != 101 && scrollView.tag != 102
        {
            if let customCllctnView = scrollView as? customCollectionView
            {
                if customCllctnView is customCollectionView
                {
                    customCllctnView.pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                    
                    customCllctnView.pageControll.currentPage = Int(customCllctnView.pageNumber)
                }
            }
        }
        if scrollView == profileTableView && self.gridButton.isSelected == false && self.arrFeeds.count > 0
        {
            if  self.arrFeeds.count < self.maxPage && !boolGetData  //self.currentPageNo < self.totalPage
            {
                if let lastVisibleIndexPath = self.profileTableView.indexPathsForVisibleRows?.last {
                    let dataCount = self.arrFeeds.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.row)
                    
                    if dif == 0
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
            
            //        print("VisableVideoCellPlay*******scrollViewDidEndDecelerating")
        }
        else if scrollView is UICollectionView && self.gridButton.isSelected == true && self.arrFeedsMedia.count >= 0
        {
            if  self.arrFeedsMedia.count < self.maxPageMedia && !boolGetDataMedia  //self.currentPageNo < self.totalPage
            {
                let arrIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems
                let lastVisibleIndexPath =  IndexPath(row: self.arrFeedsMedia.count-1, section: 0)
                //                if let lastVisibleIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems.last {
                if lastVisibleIndexPath != nil && arrIndexPath.contains(lastVisibleIndexPath)
                {
                    let dataCount = self.arrFeedsMedia.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.item)
                    
                    if dif == 0
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNoMedia += 1
                            boolGetDataMedia = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
        }
        else if self.postButton.isSelected == true && self.postButton.isSelected == true && self.arrFeedsTagged.count >= 0
        {
            if  self.arrFeedsTagged.count < self.maxPageTagged && !boolGetDataTagged  //self.currentPageNo < self.totalPage
            {
                if let cllctn = scrollView.viewWithTag(102) as? UICollectionView{
                    let arrIndexPath = cllctn.indexPathsForVisibleItems
                    let lastVisibleIndexPath =  IndexPath(row: self.arrFeedsMedia.count-1, section: 0)
                    //                if let lastVisibleIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems.last {
                    if lastVisibleIndexPath != nil && arrIndexPath.contains(lastVisibleIndexPath)
                    {
                        let dataCount = self.arrFeedsTagged.count - 1
                        let dif = abs(dataCount - lastVisibleIndexPath.item)
                        
                        if dif == 0
                        {
                            if appDelegate.isInternetAvailable() == true
                            {
                                currentPageNoTagged += 1
                                boolGetDataTagged = true
                                self.callListAPI()
                            }
                            else
                            {
                                self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                            }
                        }
                    }
                }
            }
        }
    }

    //MARK:- UIButton ACtion Methods
    
    @objc func methodUserProfileClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let userObj = feedObj.UserInfo
        if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
        {
            let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
            other.userObj = userObj
            other.strUserID = userObj.strUserId
            other.userNameString = userObj.strName
            self.navigationController?.pushViewController(other, animated: true)
        }
        else
        {
            let register = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
        }
        
    }
    @objc func methodLikeFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.LikeFeedApi(feedObj:)), with: feedObj)
        }
    }
    @objc func methodDislikeFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.DislikeFeedApi(feedObj:)), with: feedObj)
        }
    }
    @objc func methodSaveFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.BookmarkFeedApi(feedObj:)), with: feedObj)
        }
    }
    @objc func methodLikesUsersListClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
        likesUser.strFeedId = feedObj.strFeedId
        likesUser.strType = "LIKES"
        self.navigationController?.pushViewController(likesUser, animated: true)
    }
    @objc func methodDislikesUsersListClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
        likesUser.strFeedId = feedObj.strFeedId
        likesUser.strType = "DISLIKES"
        self.navigationController?.pushViewController(likesUser, animated: true)
    }
    @objc func methodCommentsClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let CommentVCObjc = PostCommentScreen.init(nibName: "PostCommentScreen", bundle: nil)
        CommentVCObjc.feedData = feedObj
        self.navigationController?.pushViewController(CommentVCObjc, animated: true)
    }
    @objc func methodTagPeopleClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let taggedPeople = TaggedPeopleScreen.init(nibName: "TaggedPeopleScreen", bundle: nil)
        taggedPeople.strFeedId = feedObj.strFeedId
        taggedPeople.arrSelectedTaggedPeople = NSMutableArray.init(array: feedObj.strTaggedPeople.components(separatedBy: ","))
        self.navigationController?.pushViewController(taggedPeople, animated: true)
    }
    


    //MARK:- UIButton Action Methods
    
    @objc func popUpViewButtonAction(_ sender: Any) {
        let report = ReportPopUpVC(nibName: "ReportPopUpVC", bundle: nil)
        report.userObj = userObj
        report.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(report, animated: false, completion: nil)
    }
    @IBAction func toggleButtonAction(_ sender: UIButton!) {
        if sender.isSelected
        {
            return
        }
        sender.isSelected = !sender.isSelected
        
        self.gridButton.isSelected = false
        self.listButton.isSelected = false
        self.postButton.isSelected = false
       
        switch sender.tag {
        case 100:
            self.gridButton.isSelected = true
            self.currentPageNo = 1
            self.maxPage = 0
            self.currentPageNoMedia = 1
            self.maxPageMedia = 0
            self.currentPageNoTagged = 1
            self.maxPageTagged = 0
            self.callListAPI()

            break
            
        case 200:
            self.listButton.isSelected = true
            self.currentPageNo = 1
            self.maxPage = 0
            self.currentPageNoMedia = 1
            self.maxPageMedia = 0
            self.currentPageNoTagged = 1
            self.maxPageTagged = 0
            self.callListAPI()

            break
            
        case 300:
            self.postButton.isSelected = true
            self.currentPageNo = 1
            self.maxPage = 0
            self.currentPageNoMedia = 1
            self.maxPageMedia = 0
            self.currentPageNoTagged = 1
            self.maxPageTagged = 0
            self.callListAPI()
            break
            
        default:
            break
        }
        
        self.profileTableView.reloadData()
    }
    @IBAction func followlistButtonAction(_ sender: UIButton!) {
        let followersVC = FollowersVC.init(nibName: "FollowersVC", bundle: nil)
        followersVC.strUserID = self.strUserID
        followersVC.isFollowings = sender.tag == 0 ? false : true
        self.navigationController?.pushViewController(followersVC, animated: true)
    }
    @IBAction func followButtonAction(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.followOtherUserApi(strStatus:)), with: (self.userObj.strIsFollowed == "1" || self.userObj.strIsFollowed == "true") ? "0" : "1")
        }
    }

    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getOtherUserProfileApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(strUserID.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getOtherUserProfileApi
    @objc func getOtherUserProfileApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialGetOrherUserProfile, Details: getOtherUserProfileApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            self.userObj = UserModel.init(dict: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func followOtherUserApiKeys(strStatus: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(strUserID.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        dicrequest.setObject(strStatus.getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - followOtherUserApi
    @objc func followOtherUserApi(strStatus: String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialFollowUser, Details: followOtherUserApiKeys(strStatus: strStatus)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
           
                        self.performSelector(inBackground: #selector(self.getOtherUserProfileApi), with: nil)

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)

                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func getFeedsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.strUserID.getEncryptedString() ?? "", forKey: kUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getFeedsListApi
    @objc func getFeedsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialGetAllPost, Details: getFeedsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.arrFeeds.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kKeySocialFeedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrFeeds.insert((getFeedsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getFeedsObjectArray(arrResult: arr).count - 1))
                                self.arrFeeds.addObjects(from: (getFeedsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func getMediaFeedsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNoMedia)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.strUserID.getEncryptedString() ?? "", forKey: kUserID as NSCopying)
        dicrequest.setObject("1".getEncryptedString() ?? "", forKey: kKeySocialIsMediaFeeds as NSCopying)
        return dicrequest
    }
    //MARK : - getMediaFeedsListApi
    @objc func getMediaFeedsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialGetAllPost, Details: getMediaFeedsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNoMedia == 1 {
                            self.arrFeedsMedia.removeAllObjects()
                        }
                        self.boolGetDataMedia = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kKeySocialFeedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrFeedsMedia.insert((getFeedsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getFeedsObjectArray(arrResult: arr).count - 1))
                                self.arrFeedsMedia.addObjects(from: (getFeedsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPageMedia = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    

    func LikeFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsLiked == "1" || feedObj.strIsLiked == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - LikeFeedApi
    @objc func LikeFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialLikePost, Details: LikeFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            let feedObj = FeedModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            self.replaceFeedObjectInArray(feedObj: feedObj)
                            
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func DislikeFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsDisliked == "1" || feedObj.strIsDisliked == "true") ? "0" : "-1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - DislikeFeedApi
    @objc func DislikeFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialLikePost, Details: DislikeFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            let feedObj = FeedModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            self.replaceFeedObjectInArray(feedObj: feedObj)
                            
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func BookmarkFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsSaved == "1" || feedObj.strIsSaved == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - BookmarkFeedApi
    @objc func BookmarkFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialSavePost, Details: BookmarkFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            let feedObj = FeedModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            self.replaceFeedObjectInArray(feedObj: feedObj)
                            
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func replaceFeedObjectInArray(feedObj: FeedModel)
    {
        self.arrFeeds.enumerateObjects({(object, idx, stop) -> Void in
            print("idx",idx)
            let oldFeedObj = object as! FeedModel
            if oldFeedObj.strFeedId == feedObj.strFeedId
            {
                self.arrFeeds.replaceObject(at: idx, with: feedObj)
                return
            }
        })
    }
    func getFeedObjectInArray(feedId: String) -> FeedModel
    {
        var feedObj = FeedModel()
        
        self.arrFeeds.enumerateObjects({(object, idx, stop) -> Void in
            print("idx",idx)
            let oldFeedObj = object as! FeedModel
            if oldFeedObj.strFeedId == feedId
            {
                feedObj = oldFeedObj
            }
        })
        
        return feedObj
    }
    
    func getTaggedFeedsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNoTagged)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.strUserID.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getTaggedFeedsListApi
    @objc func getTaggedFeedsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialTaggedPostsList, Details: getTaggedFeedsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNoTagged == 1 {
                            self.arrFeedsTagged.removeAllObjects()
                        }
                        self.boolGetDataTagged = false
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kKeySocialFeedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
                                //                                self.arrFeedsMedia.insert((getFeedsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getFeedsObjectArray(arrResult: arr).count - 1))
                                self.arrFeedsTagged.addObjects(from: (getFeedsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPageTagged = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                            
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.profileTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

}
