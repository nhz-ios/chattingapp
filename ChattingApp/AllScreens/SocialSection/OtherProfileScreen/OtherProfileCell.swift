//
//  OtherProfileCell.swift
//  ChattingApp
//
//  Created by Shreya-ios on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class OtherProfileCell: UITableViewCell {

    @IBOutlet weak var followingsButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var feedsButton: UIButton!
    @IBOutlet weak var qutationLabel: UILabel!
    @IBOutlet weak var followedByLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var picBgView: UIView!
    @IBOutlet weak var otherUserImageView: UIImageView!
    @IBOutlet var lblFeedsCount: UILabel!
    @IBOutlet var lblFollowersCount: UILabel!
    @IBOutlet var lblFollowingsCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.otherUserImageView.layer.cornerRadius = self.otherUserImageView.frame.width/2
        self.picBgView.layer.cornerRadius = self.picBgView.frame.width/2
        self.picBgView.layer.borderWidth = 1
        self.picBgView.layer.borderColor = UIColor.white.cgColor
        self.picBgView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
