//
//  FollowersVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class FollowersVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var buttonBgView: UIView!
    @IBOutlet weak var followingsButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet var headerBGView: UIView!
    @IBOutlet weak var followTableView: UITableView!
    var strUserID = ""
    var isFollowings = true
    var followersArray = NSMutableArray()

    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
 
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        followTableView.addSubview(refreshControl)
        
        navigationBarWithBackButton(strTitle: isFollowings == false ? "FOLLOWERS" : "FOLLOWINGS", leftbuttonImageName: "back.png")
        self.callListAPI()
        
    }
    
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.methodRefreshAllData()
        })
    }
    @objc func methodRefreshAllData()
    {
        self.currentPageNo = 1
        self.maxPage = 0
        self.callListAPI()
    }


    //MARK:- Helper Method
    func initialSetUp() {
        self.buttonBgView.layer.borderWidth = 1
        self.buttonBgView.layer.borderColor = UIColor.init(red: 126.0/255, green: 139.0/255, blue: 153.0/255, alpha: 1).cgColor
        
        self.followersButton.backgroundColor = UIColor.white
        self.followingsButton.backgroundColor = UIColor.white
        
        if !isFollowings {
            self.followersButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
        } else {
            self.followingsButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
        }
        followersButton.isSelected = !isFollowings
        followingsButton.isSelected = isFollowings
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodRefreshAllData), name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)
   }
    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.getFollowListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }
    //MARK:- UITableView DatSource And Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerBGView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if  self.followersArray.count != 0
        {
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            let font = UIFont(name: "Lato-Bold", size: 17)
            noDataLabel.font = font
            noDataLabel.text = "No user found."
            noDataLabel.textColor = UIColor.colorAppTheam
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        
        return self.followersArray.count < self.maxPage ? self.followersArray.count+1 : self.followersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == self.followersArray.count
        {
            let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+60, 0.0, 0.0)
            cell.backgroundColor = UIColor.clear
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            spinner.frame = CGRect(x: (UIScreen .main.bounds.size.width - 20)/2, y: 12, width: 20, height: 20)
            cell.addSubview(spinner)
            spinner.startAnimating()
            return cell
        }
        else
        {
            
            let indentifier:String = "FollowersCell"
            var cell : FollowersCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? FollowersCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FollowersCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FollowersCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.userImageView.layer.cornerRadius = (cell?.userImageView.frame.size.height)!/2
            cell?.userImageView.layer.masksToBounds = true
            
            let userObj = self.followersArray.object(at: indexPath.row) as! UserModel
            
            //        cell?.followingButton.setTitle(isFollowings == false ? "Following" : "Unfollow", for: .normal)
            cell?.followingButton.isHidden = userObj.strUserId == self.appDelegate.currentLoginUser.strUserId
            cell?.followingButton.setTitle((userObj.strIsFollowed == "1" || userObj.strIsFollowed == "true") ? "Unfollow" : "Follow", for: .normal)
            cell?.followingButton.tag = indexPath.row
            cell?.followingButton.addTarget(self, action: #selector(followButtonAction(_:)), for: .touchUpInside)
            
            cell?.nameLabel.text = userObj.strName
            cell?.descriptionLabel.text = userObj.strEmail
            //        cell?.lblMessage.sizeToFit()
            //        cell?.lblMessage2.sizeToFit()
            if userObj.strProfilepicImage != ""
            {
                // \(Image_URL)
                let url = URL.init(string: userObj.strProfilepicImage)
                cell?.userImageView.sd_addActivityIndicator()
                cell?.userImageView.sd_showActivityIndicatorView()
                cell?.userImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.userImageView.sd_removeActivityIndicator()
                })
            }
            else
            {
                cell?.userImageView.image = UIImage(named:"reg_USER")
            }
            
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.followersArray.count
        {
            return
        }
        let userObj = self.followersArray.object(at: indexPath.row) as! UserModel
        if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
        {
            let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
            other.userObj = userObj
            other.strUserID = userObj.strUserId
            other.userNameString = userObj.strName
            self.navigationController?.pushViewController(other, animated: true)
        }
    }

    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == followTableView && self.followersArray.count > 0
        {
            if  self.followersArray.count < self.maxPage && !boolGetData  //self.currentPageNo < self.totalPage
            {
                if let lastVisibleIndexPath = self.followTableView.indexPathsForVisibleRows?.last {
                    let dataCount = self.followersArray.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.row)
                    
                    if dif == 1
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
            //        print("VisableVideoCellPlay*******scrollViewDidEndDecelerating")
        }
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func toggleButtonAction(_ sender: UIButton!) {
        
        if sender.isSelected
        {
            return
        }
        if self.appDelegate.isInternetAvailable() == true
        {
            self.followersButton.isSelected = false
            self.followingsButton.isSelected = false
            self.followersButton.backgroundColor = UIColor.white
            self.followingsButton.backgroundColor = UIColor.white
            
            if sender.tag == 100
            {
                self.followersButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
            }
            else
            {
                self.followingsButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
            }
            sender.isSelected = !sender.isSelected
            isFollowings = self.followingsButton.isSelected
            self.navigationItem.title = isFollowings == false ? "FOLLOWERS" : "FOLLOWINGS"
//            self.followTableView.reloadData()

            self.currentPageNo = 1
            self.maxPage = 0
            self.performSelector(inBackground: #selector(self.callListAPI), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }

    }
    @IBAction func followButtonAction(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let userObj = self.followersArray.object(at: sender.tag) as! UserModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.followOtherUserApi(userObj:)), with: userObj)
        }
    }
    

    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func getFollowListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject((isFollowings == true ? "following" : "follower").getEncryptedString() ?? "", forKey: kKeySocialType as NSCopying)
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(strUserID.getEncryptedString() ?? "", forKey: kUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getFollowListApi
    @objc func getFollowListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialFollowList, Details: getFollowListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.followersArray.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kData) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.followersArray.insert((getUsersObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getUsersObjectArray(arrResult: arr).count - 1))
                                self.followersArray.addObjects(from: (getUsersObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.followTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    func followOtherUserApiKeys(userObj: UserModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(userObj.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        dicrequest.setObject(((userObj.strIsFollowed == "1" || userObj.strIsFollowed == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - followOtherUserApi
    @objc func followOtherUserApi(userObj: UserModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialFollowUser, Details: followOtherUserApiKeys(userObj: userObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.currentPageNo = 1
                        self.maxPage = 0
                        self.performSelector(inBackground: #selector(self.callListAPI), with: nil)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)

                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.followTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

}


