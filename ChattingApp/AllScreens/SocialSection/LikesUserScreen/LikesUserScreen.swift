//
//  LikesUserScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class LikesUserScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblContacts:  UITableView!

    var strFeedId: String = ""
    var strType: String = "LIKES"
   var arrLikesUsersList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblContacts.tableFooterView = UIView.init(frame: CGRect.zero)
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        tblContacts.addSubview(refreshControl)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodRefreshAllData), name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)
        self.callListAPI()
   }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationBarWithBackButton(strTitle: strType, leftbuttonImageName: "back.png")
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }

    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.methodRefreshAllData()
        })
    }
    @objc func methodRefreshAllData()
    {
        self.currentPageNo = 1
        self.maxPage = 0
        self.callListAPI()
    }

    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.getLikesUsersListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if  self.arrLikesUsersList.count != 0
        {
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            let font = UIFont(name: "Lato-Bold", size: 17)
            noDataLabel.font = font
            noDataLabel.text = "No user found."
            noDataLabel.textColor = UIColor.colorAppTheam
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }

        return self.arrLikesUsersList.count < self.maxPage ? self.arrLikesUsersList.count+1 : self.arrLikesUsersList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return indexPath.row == self.arrLikesUsersList.count ? 48 : 70
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row == self.arrLikesUsersList.count
        {
            let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+60, 0.0, 0.0)
            cell.backgroundColor = UIColor.clear
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            spinner.frame = CGRect(x: (UIScreen .main.bounds.size.width - 20)/2, y: 12, width: 20, height: 20)
            cell.addSubview(spinner)
            spinner.startAnimating()
            return cell
        }
        else
        {
            //        if indexPath.row == arrSearchUser.count
            //        {
            //            let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            //            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+60, 0.0, 0.0)
            //            cell.backgroundColor = UIColor.clear
            //            let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            //            spinner.frame = CGRect(x: (UIScreen .main.bounds.size.width - 20)/2, y: 12, width: 20, height: 20)
            //            cell.addSubview(spinner)
            //            spinner.startAnimating()
            //            return cell
            //        }
            //        else
            //        {
            
            let indentifier:String = "FollowersCell"
            var cell : FollowersCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? FollowersCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FollowersCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FollowersCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            cell?.userImageView.layer.cornerRadius = (cell?.userImageView.frame.size.height)!/2
            cell?.userImageView.layer.masksToBounds = true
            
            let userObj = self.arrLikesUsersList.object(at: indexPath.row) as! UserModel
            
            //        cell?.followingButton.setTitle(isFollowings == false ? "Following" : "Unfollow", for: .normal)
            cell?.followingButton.isHidden = userObj.strUserId == self.appDelegate.currentLoginUser.strUserId
            if (strType != "BLOCKED")
            {
                cell?.followingButton.setTitle((userObj.strIsFollowed == "1" || userObj.strIsFollowed == "true") ? "Unfollow" : "Follow", for: .normal)
            }
            else{
                cell?.followingButton.setTitle((userObj.strIsBlockedByMe == "1" || userObj.strIsBlockedByMe == "true") ? "Unblock" : "Block", for: .normal)
            }
            
            cell?.followingButton.tag = indexPath.row
            cell?.followingButton.addTarget(self, action: #selector(followButtonAction(_:)), for: .touchUpInside)
            
            cell?.nameLabel.text = userObj.strName
            cell?.descriptionLabel.text = userObj.strEmail
            //        cell?.lblMessage.sizeToFit()
            //        cell?.lblMessage2.sizeToFit()
            if userObj.strProfilepicImage != ""
            {
                // \(Image_URL)
                let url = URL.init(string: userObj.strProfilepicImage)
                cell?.userImageView.sd_addActivityIndicator()
                cell?.userImageView.sd_showActivityIndicatorView()
                cell?.userImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.userImageView.sd_removeActivityIndicator()
                })
            }
            else
            {
                cell?.userImageView.image = UIImage(named:"reg_USER")
            }
            
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.arrLikesUsersList.count || (strType == "BLOCKED")
        {
            return
        }
        let userObj = self.arrLikesUsersList.object(at: indexPath.row) as! UserModel
        let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
        other.userObj = userObj
        other.strUserID = userObj.strUserId
        other.userNameString = userObj.strName
        self.navigationController?.pushViewController(other, animated: true)
    }
    
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == tblContacts && self.arrLikesUsersList.count > 0
        {
            if  self.arrLikesUsersList.count < self.maxPage && !boolGetData //self.currentPageNo < self.totalPage
            {
                if let lastVisibleIndexPath = self.tblContacts.indexPathsForVisibleRows?.last {
                    let dataCount = self.arrLikesUsersList.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.row)
                    
                    if dif == 1
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
            //        print("VisableVideoCellPlay*******scrollViewDidEndDecelerating")
        }
    }

    
    //MARK:- UIButton Action Methods
    @IBAction func followButtonAction(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let userObj = self.arrLikesUsersList.object(at: sender.tag) as! UserModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.followOtherUserApi(userObj:)), with: userObj)
        }
    }
    

    func getLikesUsersListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        if (strType != "BLOCKED")
        {
            dicrequest.setObject(strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
            dicrequest.setObject((strType == "LIKES" ? "1" : "-1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        }
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strUserId, forKey: kUserID as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strName, forKey: kName as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strEmail, forKey: kEmail as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strGender , forKey: kGender as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strBio, forKey: kBio as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strDob, forKey: kDOB as NSCopying)
        return dicrequest
    }
    //MARK : - getLikesUsersListApi
    @objc func getLikesUsersListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: (strType != "BLOCKED") ? kAPISocialDislikesUserList : kAPISocialBlockedUsersList, Details: getLikesUsersListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.arrLikesUsersList.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kData) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrLikesUsersList.insert((getUsersObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getUsersObjectArray(arrResult: arr).count - 1))
                                self.arrLikesUsersList.addObjects(from: (getUsersObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.tblContacts.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func followOtherUserApiKeys(userObj: UserModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(userObj.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        
        if (strType != "BLOCKED")
        {
            dicrequest.setObject(((userObj.strIsFollowed == "1" || userObj.strIsFollowed == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        }
        else
        {
            dicrequest.setObject(((userObj.strIsBlockedByMe == "1" || userObj.strIsBlockedByMe == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        }

        return dicrequest
    }
    //MARK : - followOtherUserApi
    @objc func followOtherUserApi(userObj: UserModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: strType != "BLOCKED" ? kAPISocialFollowUser : kAPISocialBlockUser, Details: followOtherUserApiKeys(userObj: userObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.performSelector(inBackground: #selector(self.getLikesUsersListApi), with: nil)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)

                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.tblContacts.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

