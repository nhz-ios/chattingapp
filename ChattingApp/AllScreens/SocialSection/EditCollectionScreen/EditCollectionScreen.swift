//
//  EditCollectionScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 09/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class EditCollectionScreen: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var nameTextFeild: UITextField!
    @IBOutlet var toolBar: UIToolbar!
    
    @IBOutlet weak var contectScrollView: UIScrollView!
    @IBOutlet weak var savedCollectionView: UICollectionView!
    var userImageData = Data()
    @IBOutlet weak var imgPhoto : UIImageView!
    var collectionData = CollectionModel()
    var arrFeeds = NSMutableArray()
    var arrSelectedFeeds = NSMutableArray()

    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!

    
    //MARK:- UIviewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        savedCollectionView.addSubview(refreshControl)
        if #available(iOS 11.0, *) {
//            contectScrollView.contentInsetAdjustmentBehavior = .never
            //print("contentInsetAdjustmentBehavior")
        } else {
//            automaticallyAdjustsScrollViewInsets = false
        }

        nameTextFeild.inputAccessoryView = toolBar
        self.initialSetUp()
        self.callListAPI()
    }
    //MARK:- Helper Method
    func initialSetUp() {
        //self.contectScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-90)
        let nib = UINib(nibName: "ImageCollectionCell", bundle: nil)
        
        self.savedCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCollectionCell")
        
        nameTextFeild.text =  collectionData.strCollectionName
        
        if collectionData.strCollectionImage != ""
        {
            let url = URL.init(string: collectionData.strCollectionImage)
            imgPhoto.sd_addActivityIndicator()
            imgPhoto.sd_showActivityIndicatorView()
            imgPhoto.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                self.imgPhoto.sd_removeActivityIndicator()
            })
        }
        else
            {
                self.imgPhoto.image = UIImage.init(named: "slider-img-placeholder")
        }
        self.getSelectedFeeds()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
//        navigationBarWithBackButton(strTitle: "CREATE COLLECTION", leftbuttonImageName: "back.png")
        self.navigationItem.title = "EDIT COLLECTION"

        let leftButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "social_col_cross")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(leftButtonAction(_:)))
        
        self.navigationItem.leftBarButtonItem = leftButton

        let rightButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "social_col_chk")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(rightButtonAction(_:)))
    
        self.navigationItem.rightBarButtonItem = rightButton
        savedCollectionView.reloadData()
    }
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.currentPageNo = 1
            self.maxPage = 0
            self.callListAPI()
        })
    }
    
    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            if arrFeeds.count == 0
            {
                self.showActivity(text: "")
            }
            
            self.performSelector(inBackground: #selector(self.getSavedFeedsListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }
    @objc func rightButtonAction(_ sender: Any) {
        
        if self.CheckValidation()
        {
            if self.appDelegate.isInternetAvailable() == true
            {
                self.showActivity(text: "")
                self.performSelector(inBackground: #selector(self.editCollectionApi), with: nil)
            }
            else
            {
                self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
            }
        }
    }
    @objc func leftButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITextFeild Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }

    @IBAction func deleteButtonAction(_ sender: Any) {

        let alert  =   UIAlertController(title: kAppName, message: kMessageSocialDeleteCollection, preferredStyle: .alert)
        
        let cancelAction  = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in
        }
        
        let okAction  = UIAlertAction(title: "Delete", style: .default) { (alert: UIAlertAction!) in
            
            if self.appDelegate.isInternetAvailable() == true
            {
                self.showActivity(text: "")
                self.performSelector(inBackground: #selector(self.deleteCollectionApi), with: nil)
            }
            else
            {
                self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
            }
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
   
    @IBAction func cameraButtonAction(_ sender: UIButton!)
    {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Open Camera")
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Open Gallery")
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        var image: UIImage!
        
        // fetch the selected image
        
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        self.userImageData = UIImageJPEGRepresentation(image, 0.75)!
        self.imgPhoto.image = image
    }
    //MARK:- UICollectionView DataSource And Delegate Methods
    
    //MARK:- UICollectionView DataSource And Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFeeds.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((collectionView.frame.width/3) - 5), height: (collectionView.frame.width/3) - 5)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
        
        let feedData = self.arrFeeds.object(at: indexPath.item) as! FeedModel
        if feedData.arrMedia.count > 0
        {
            let feedMediaData = feedData.arrMedia.object(at: 0) as! FeedMediaModel
            
            if feedMediaData.strMediaURL != ""
            {
                if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                {
                    let url = URL.init(string: feedMediaData.strMediaURL)
                    cell.feedImageView.sd_addActivityIndicator()
                    cell.feedImageView.sd_showActivityIndicatorView()
                    cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell.feedImageView.sd_removeActivityIndicator()
                    })
                    cell.feedImageView.isHidden = false
                }
                else
                {
                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell.feedImageView.sd_addActivityIndicator()
                    cell.feedImageView.sd_showActivityIndicatorView()
                    cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell.feedImageView.sd_removeActivityIndicator()
                    })
                    cell.feedImageView.isHidden = false
                }
            }
        }
        
        cell.btnEditCollection.isHidden = false
        cell.btnEditCollection.isSelected = self.arrSelectedFeeds.contains(feedData.strFeedId)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let feedData = self.arrFeeds.object(at: indexPath.item) as! FeedModel
        if self.arrSelectedFeeds.contains(feedData.strFeedId)
        {
            self.arrSelectedFeeds.remove(feedData.strFeedId)
        }
        else
        {
            self.arrSelectedFeeds.add(feedData.strFeedId)
        }
        self.savedCollectionView.reloadData()
    }

    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.arrFeeds.count > 0
        {
            if  self.arrFeeds.count < self.maxPage && !boolGetData 
            {
                let arrIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems
                let lastVisibleIndexPath =  IndexPath(row: self.arrFeeds.count-1, section: 0)
                //                if let lastVisibleIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems.last {
                if lastVisibleIndexPath != nil && arrIndexPath.contains(lastVisibleIndexPath)
                {
                    let dataCount = self.arrFeeds.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.item)
                    
                    if dif == 0
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
        }
    }
    

    
    //MARK: - CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if nameTextFeild.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" == "" && self.appDelegate.arrPostUrls.count == 0
        {
            isGo = false
            errorMessage = kMessageSocialEnterCollection
        }
        else if self.arrSelectedFeeds.count == 0
        {
            isGo = false
            errorMessage = kMessageSocialSelectCollectionFeed
        }

        if !isGo {
            
            self.showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        return isGo
    }
    
    func editCollectionApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.collectionData.strCollectionId.getEncryptedString() ?? "", forKey: kKeySocialCollectionId as NSCopying)
        dicrequest.setObject((self.nameTextFeild.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "").getEncryptedString() ?? "", forKey: kKeySocialCollectionName as NSCopying)
        dicrequest.setObject(self.arrSelectedFeeds.componentsJoined(by: ",").getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        return dicrequest
    }
    //MARK : - editCollectionApi
    @objc func editCollectionApi()
    {
        getAllSocialApiResultWithImagePostMethod(strMethodname: kAPISocialEditCollection, imgData: userImageData, strImgKey: kKeySocialCollectionImage, Details: editCollectionApiKeys()) { (responseData, error) in

            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let collectionData = CollectionModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            NotificationCenter.default.post(name: NSNotification.Name.init(kNotificationSocialUpdateCollections), object: collectionData)

                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.savedCollectionView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func deleteCollectionApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(self.collectionData.strCollectionId.getEncryptedString() ?? "", forKey: kKeySocialCollectionId as NSCopying)
        return dicrequest
    }
    //MARK : - deleteCollectionApi
    @objc func deleteCollectionApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialDeleteCollection, Details: deleteCollectionApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            var cntrollr : UIViewController!
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: SavedVC.self)
                                {
                                    cntrollr = controller
                                    break
                                }
                            }
                            if cntrollr != nil
                            {
                                self.navigationController!.popToViewController(cntrollr, animated: true)
                            }
                            else
                            {
                                self.navigationController!.popViewController(animated: true)
                            }
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.savedCollectionView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func getSavedFeedsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.appDelegate.currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getSavedFeedsListApi
    @objc func getSavedFeedsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialSavedPostList, Details: getSavedFeedsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.arrFeeds.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kKeySocialFeedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrFeeds.insert((getFeedsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getFeedsObjectArray(arrResult: arr).count - 1))
                                self.arrFeeds.addObjects(from: (getFeedsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.savedCollectionView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    func getSelectedFeeds()
    {
        self.collectionData.arrFeeds.enumerateObjects({(object, idx, stop) -> Void in
            print("idx",idx)
            let oldFeedObj = object as! FeedModel
            if self.arrSelectedFeeds.contains(oldFeedObj.strFeedId) == false
            {
                self.arrSelectedFeeds.add(oldFeedObj.strFeedId)
            }
        })
    }


    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

