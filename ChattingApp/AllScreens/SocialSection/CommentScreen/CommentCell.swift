//
//  CommentCell.swift
//  ChattingApp
//
//  Created by Shreya-ios on 07/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var sepratorLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var otherUserImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.otherUserImageView.layer.cornerRadius = self.otherUserImageView.frame.size.width/2
        self.otherUserImageView.layer.borderWidth = 1
        self.otherUserImageView.layer.borderColor = UIColor.white.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
