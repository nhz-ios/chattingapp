//
//  CommentVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 07/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class CommentVC: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var commentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear

       self.navigationItem.titleView = self.titleView
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "back.png"), style: .done, target: self, action: #selector(backButtonAction(_:)))
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //MARK:- Helper Method
    func initialSetUp() {
        self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2
        self.userImageView.layer.borderWidth = 1
        self.userImageView.layer.borderColor = UIColor.white.cgColor
    }
    
    //MARK:- UITableView Delegate And DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let indentifier:String = "CommentCell"
        var cell : CommentCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? CommentCell
        
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("CommentCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? CommentCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        if indexPath.row == 2 {
            cell?.sepratorLabel.isHidden = true
            
        } else {
            cell?.sepratorLabel.isHidden = false

        }
        return cell!
    }
    
    //MARK:- UIButton Action Methods
    @objc func backButtonAction(_ sender: UIButton!) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func liveButtonAction(_ sender: Any) {
        
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
