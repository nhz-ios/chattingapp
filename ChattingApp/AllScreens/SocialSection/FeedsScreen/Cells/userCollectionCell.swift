//
//  userCollectionCell.swift
//  ChattingApp
//
//  Created by Shreya-ios on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class userCollectionCell: UICollectionViewCell {
    @IBOutlet weak var userBgView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addUserButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userBgView.layer.masksToBounds = true
        self.userBgView.layer.cornerRadius = self.userBgView.frame.size.width/2
        self.userBgView.layer.borderWidth = 1
        self.userBgView.layer.borderColor = UIColor.init(red: 179.0/255, green: 179.0/255, blue: 179.0/255, alpha: 1).cgColor
        
        self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2
//        self.userImageView.layer.borderWidth = 1
//        self.userImageView.layer.borderColor = UIColor.init(red: 179.0/255, green: 179.0/255, blue: 179.0/255, alpha: 1).cgColor
    }

}
