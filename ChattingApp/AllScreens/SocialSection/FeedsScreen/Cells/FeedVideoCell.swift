//
//  FeedVideoCell.swift
//  ChattingApp
//
//  Created by Shreya-ios on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class customCollectionView: UICollectionView
{
    var pageNumber = CGFloat()
    var pageControll: UIPageControl!
}
class FeedVideoCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblCaption: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var btnComments: UIButton!
    @IBOutlet weak var btnLikes: UIButton!
    @IBOutlet weak var btnDislikes: UIButton!
    @IBOutlet weak var btnLikeFeed: UIButton!
    @IBOutlet weak var btnDislikeFeed: UIButton!
    @IBOutlet weak var btnSavedFeed: UIButton!
    @IBOutlet var btnTag: UIButton!
    @IBOutlet var cllctnViewMedia: customCollectionView!
    @IBOutlet weak var pageControll: UIPageControl!

    @IBOutlet var heightConstaintCllctnView: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
