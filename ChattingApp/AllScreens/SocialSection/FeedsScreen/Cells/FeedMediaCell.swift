//
//  GalleryWorkCollCell.swift
//  Learner
//
//  Created by RamPrasad-IOS on 25/06/18.
//  Copyright © 2018 Parikshit. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class FeedMediaCell: UICollectionViewCell {

    @IBOutlet  var btnCross: UIButton!
    @IBOutlet  var imgUser: UIImageView!
    var avPlayer : AVPlayer!
    //    var avPlayerLayer : AVPlayerLayer!
    var avPlayerViewController: AVPlayerViewController!
    @IBOutlet var videoView : YPVideoView!
    @IBOutlet weak var assetZoomableView: YPAssetZoomableView!
    @IBOutlet weak var assetViewContainer: YPAssetViewContainer!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
