//
//  FeedsVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import ASExtendedCircularMenu
import AVKit
import AVFoundation

class FeedsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, ASCircularButtonDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var addMenuBGView: UIView!
    @IBOutlet weak var colourPickerButton: ASCircularMenuButton!
    @IBOutlet weak var feedTableView: UITableView!
    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!

    var isBgView = true
    var arrFeeds = NSMutableArray()
     let colourArray: NSMutableArray = ["new_chat", "favorite" , "like" , "user 3" , "live" ]
    
    var avPlayer: AVPlayer!
    var avPlayerViewController: AVPlayerViewController!

    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        feedTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        feedTableView.addSubview(refreshControl)
        
        feedTableView.rowHeight = UITableViewAutomaticDimension
        feedTableView.estimatedRowHeight = 300
        self.initialSetUp()
        self.callListAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.isNavigationBarHidden = false
        self.appDelegate.TabLogin.tabBar.isHidden = false
        self.navigationBarWithBackButton(strTitle: "FEEDS", leftbuttonImageName: "menu.png")
        
       // self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage.init(named:"social_profile.png")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodMyProfile(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1, menuButton2]

        
        // Set 26px of fixed space between the two UIBarButtonItems
        var fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpace.width = -26.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        var negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        self.navigationItem.rightBarButtonItems = [negativeSpace, menuButton1, fixedSpace, menuButton2]
        
        if self.avPlayer != nil && self.avPlayerViewController != nil
        {
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                print(error)
            }
            
            self.avPlayer.play()
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.appDelegate.TabLogin.tabBar.isHidden = true
        self.removeAVPlayer()
    }

    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            if arrFeeds.count == 0
            {
                self.showActivity(text: "")
            }
            self.performSelector(inBackground: #selector(self.getFeedsListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }
    
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.methodRefreshAllData()
        })
    }
    

   @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }
    @objc func methodMyProfile(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    @objc func methodRefreshAllData()
    {
        self.currentPageNo = 1
        self.maxPage = 0
        self.callListAPI()
    }

    //MARK:- Helper Method
    func initialSetUp()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(updateFeedsData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialUpdateFeeds), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addNewFeedsData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialAddNewFeed), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCommentData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialDeleteComment), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addNewCommentData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialAddNewComment), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodRefreshAllData), name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)

        configureCircularMenuButton(button: colourPickerButton , numberOfMenuItems:5, menuRedius: 70, postion: .centerRight)
        colourPickerButton.menuButtonSize = .medium
        colourPickerButton.sholudMenuButtonAnimate = false
        
        
        self.addMenuBGView.isHidden = true
    }
    
    //MARK:- Notification Observer Methods
    @objc func updateFeedsData(notiObj: Notification)
    {
        let feedObj = notiObj.object as! FeedModel
        self.replaceFeedObjectInArray(feedObj: feedObj)
        self.feedTableView.reloadData()
    }
    @objc func addNewFeedsData(notiObj: Notification)
    {
        let feedObj = notiObj.object as! FeedModel
        self.arrFeeds.insert(feedObj, at: 0)
        self.maxPage += 1
        self.feedTableView.reloadData()
        self.feedTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
    }
    @objc func deleteCommentData(notiObj: Notification)
    {
        let commentObj = notiObj.object as! CommentModel
        let feedObj = self.getFeedObjectInArray(feedId: commentObj.strFeedId)
        feedObj.strCommentsCount = "\((feedObj.strCommentsCount as NSString).integerValue - 1)"
        self.feedTableView.reloadData()
    }
    @objc func addNewCommentData(notiObj: Notification)
    {
        let commentObj = notiObj.object as! CommentModel
        let feedObj = self.getFeedObjectInArray(feedId: commentObj.strFeedId)
        feedObj.strCommentsCount = "\((feedObj.strCommentsCount as NSString).integerValue + 1)"
        self.feedTableView.reloadData()
    }

   //MARK:- UITableView Delegate And DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return section == 0 ? 1 : self.arrFeeds.count
        
        if section == 0
        {
            return 1
        }
        else
        {
            if  self.arrFeeds.count != 0
            {
                tableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                let font = UIFont(name: "Lato-Bold", size: 17)
                noDataLabel.font = font
                noDataLabel.text = "No feeds yet."
                noDataLabel.textColor = UIColor.colorAppTheam
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
                tableView.separatorStyle = .none
            }
            return (self.arrFeeds.count < self.maxPage ? self.arrFeeds.count+1 : self.arrFeeds.count)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0
        {
            return 100
        }
        else
        {
//            return UITableViewAutomaticDimension
            return indexPath.row == self.arrFeeds.count ? 48 : UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let indentifier:String = "FeedUserCell"
            var cell : FeedUserCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? FeedUserCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FeedUserCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FeedUserCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            }
            
            let nib = UINib(nibName: "userCollectionCell", bundle: nil)
            cell?.userCollectionView.register(nib, forCellWithReuseIdentifier: "userCollectionCell")
            
            cell?.userCollectionView.delegate = self
            cell?.userCollectionView.dataSource = self
            cell?.userCollectionView.tag = 100
            return cell!

        default:
            
            if indexPath.row == self.arrFeeds.count
            {
                let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
                cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+60, 0.0, 0.0)
                cell.backgroundColor = UIColor.clear
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
                spinner.frame = CGRect(x: (UIScreen .main.bounds.size.width - 20)/2, y: 12, width: 20, height: 20)
                cell.addSubview(spinner)
                spinner.startAnimating()
                return cell
            }
            else
            {
                let indentifier:String = "FeedVideoCell"
                var cell : FeedVideoCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? FeedVideoCell
                
                if (cell == nil)
                {
                    let nib:Array = Bundle.main.loadNibNamed("FeedVideoCell", owner: nil, options: nil)! as [Any]
                    cell = nib[0] as? FeedVideoCell
                    cell?.selectionStyle = UITableViewCellSelectionStyle.none
                    cell?.backgroundColor = (UIColor.clear)
                }
                
                cell?.btnUser.tag = indexPath.row
                cell?.btnLikes.tag = indexPath.row
                cell?.btnDislikes.tag = indexPath.row
                cell?.btnLikeFeed.tag = indexPath.row
                cell?.btnDislikeFeed.tag = indexPath.row
                cell?.btnSavedFeed.tag = indexPath.row
                cell?.cllctnViewMedia.tag = indexPath.row
                cell?.btnComments.tag = indexPath.row
                cell?.btnTag.tag = indexPath.row
                cell?.pageControll.tag = indexPath.row
                cell?.btnUser.addTarget(self, action: #selector(methodUserProfileClicked(_:)), for: .touchUpInside)
                cell?.btnLikes.addTarget(self, action: #selector(methodLikesUsersListClicked(_:)), for: .touchUpInside)
                cell?.btnDislikes.addTarget(self, action: #selector(methodDislikesUsersListClicked(_:)), for: .touchUpInside)
                cell?.btnLikeFeed.addTarget(self, action: #selector(methodLikeFeedClicked(_:)), for: .touchUpInside)
                cell?.btnDislikeFeed.addTarget(self, action: #selector(methodDislikeFeedClicked(_:)), for: .touchUpInside)
                cell?.btnSavedFeed.addTarget(self, action: #selector(methodSaveFeedClicked(_:)), for: .touchUpInside)
                cell?.btnComments.addTarget(self, action: #selector(methodCommentsClicked(_:)), for: .touchUpInside)
                cell?.btnTag.addTarget(self, action: #selector(methodTagPeopleClicked(_:)), for: .touchUpInside)

                let nib = UINib(nibName: "FeedMediaCell", bundle: nil)
                cell?.cllctnViewMedia.register(nib, forCellWithReuseIdentifier: "FeedMediaCell")
                
                cell?.cllctnViewMedia.delegate = self
                cell?.cllctnViewMedia.dataSource = self
                cell?.cllctnViewMedia.pageControll = cell?.pageControll
                
                let feedData = self.arrFeeds.object(at: indexPath.row) as! FeedModel
                
                cell?.lblTitle.text = feedData.UserInfo.strName
                cell?.lblCaption.text = feedData.strCaption
                cell?.lblTime.text = self.MethodGetShowDateAndTime(strDateTimeStamp: feedData.strCreatedAtTimeStamp)
                cell?.btnLikes.setTitle(feedData.strLikeCount + ((feedData.strLikeCount as NSString).integerValue == 1 ? " Like" : " Likes"), for: .normal)
                cell?.btnDislikes.setTitle(feedData.strDislikeCount + ((feedData.strDislikeCount as NSString).integerValue == 1 ? " Dislike" : " Dislikes"), for: .normal)
                cell?.btnComments.setTitle(feedData.strCommentsCount + ((feedData.strCommentsCount as NSString).integerValue == 1 ? " Comment" : " Comments"), for: .normal)
                
                cell?.btnLikeFeed.isSelected = (feedData.strIsLiked as NSString).boolValue
                cell?.btnDislikeFeed.isSelected = (feedData.strIsDisliked as NSString).boolValue
                cell?.btnSavedFeed.isSelected = (feedData.strIsSaved as NSString).boolValue
                cell?.heightConstaintCllctnView.constant = feedData.arrMedia.count == 0 ? 0 : cell!.cllctnViewMedia.frame.size.width
                cell?.pageControll.numberOfPages = feedData.arrMedia.count
                cell?.btnSavedFeed.isHidden = feedData.arrMedia.count == 0
                cell?.btnTag.isHidden = (feedData.strTaggedPeople == "" || feedData.arrMedia.count == 0)
                cell?.btnTag.isHidden = (feedData.strTaggedPeople == "" || feedData.arrMedia.count == 0)

                if feedData.UserInfo.strProfilepicImage != ""
                {
                    let url = URL.init(string: Profile_Thumb_Pic_URL + feedData.UserInfo.strUserId)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
                else
                {
                    cell?.imgUser.image = UIImage(named:"reg_USER")
                }
                
                return cell!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch indexPath.section {
        case 0:
            break
        default:
            if indexPath.row == self.arrFeeds.count
            {
                return
            }

            let feedData = self.arrFeeds.object(at: indexPath.row) as! FeedModel
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedData
            feedDetails.selectedRow = 0
            self.navigationController?.pushViewController(feedDetails, animated: true)
            break
        }

    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if self.avPlayer != nil && self.avPlayerViewController != nil && (self.avPlayerViewController.view.superview?.tag ?? 0) == indexPath.row
        {
            removeAVPlayer()
        }
    }

    
    
    //MARK:- UICollectionView DataSource And Delegate Methods
    

    // MARK: - UICollectionView Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100
        {
            return 5
        }
        else
        {
            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            return feedData.arrMedia.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView.tag == 100
        {
            return CGSize(width: 56, height: 100)
        }
        else
        {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.width)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView.tag == 100
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for: indexPath) as! userCollectionCell
            if indexPath.item == 0 {
                cell.addUserButton.isHidden = false
                cell.nameLabel.text = "You"
                cell.nameLabel.textColor = UIColor.init(red: 5.0/255, green: 152.0/255, blue: 214.0/255, alpha: 1)
                //   cell.userImageView.image = UIImage.init(named: "feed_user")
            } else {
                cell.addUserButton.isHidden = true
                cell.nameLabel.text = "Andrew"
                cell.nameLabel.textColor = UIColor.darkGray
                //   cell.userImageView.image = UIImage.init(named: "feed_userandrew")
                
            }
            return cell
        }
        else
        {
            let identifier: String = "FeedMediaCell"
            
            let cell: FeedMediaCell? = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)as? FeedMediaCell
            
            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            let feedMediaData = feedData.arrMedia.object(at: indexPath.item) as! FeedMediaModel

            if feedMediaData.strMediaURL != ""
            {
                if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                {
                    let url = URL.init(string: feedMediaData.strMediaURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                    cell?.imgUser.isHidden = false
                }
                else
                {
                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
            }
            else
            {
                cell?.imgUser.image = UIImage(named:"slider-img-placeholder")
            }

            
            cell?.backgroundColor = UIColor.clear
            
//            let strImage = (appDelegate.LearnerUserInfo.teacherDetails.arrTeacherMedia.object(at: indexPath.item) as! NSDictionary).object(forKey: "image") as! String
//
//            let url = URL.init(string: "\(Image_URL)\(strImage)")
//            cell?.imgUser.sd_addActivityIndicator()
//
//            cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "gallery-img-default"), options: .refreshCached, completed: { (img, error, cacheType, url) in
//                cell?.imgUser.sd_removeActivityIndicator()
//            })

            return cell!
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if collectionView.tag != 100
        {
            let cell1: FeedMediaCell? = cell as? FeedMediaCell
            
            cell1!.imgUser.isHidden = false

            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            let feedMediaData = feedData.arrMedia.object(at: indexPath.item) as! FeedMediaModel
            
            if feedMediaData.strMediaURL != ""
            {
                if (feedMediaData.strMediaType != kKeySocialMediaTypeImage)
                {
                    if feedMediaData.strMediaURL.count > 0
                    {
                        if (feedMediaData.strMediaThumbURL != "" && self.avPlayer == nil && self.avPlayerViewController == nil) || self.urlOfCurrentlyPlayingInPlayer() != URL(string: feedMediaData.strMediaURL)!
                        {
                            self.removeAVPlayer()
                            let urlMainPost = URL.init(string: feedMediaData.strMediaURL)
                            
                            let item = AVPlayerItem(url: urlMainPost as! URL)
                            
                            do {
                                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                            } catch {
                                print(error)
                            }
                            self.avPlayer = AVPlayer(playerItem: item);
                            self.avPlayer.actionAtItemEnd = .none
                            print("SecvideoUrl",urlMainPost)
                            print("item",item)
                            
                            self.avPlayerViewController = AVPlayerViewController()
                            self.avPlayerViewController.player = self.avPlayer
                            self.avPlayerViewController.view.frame = cell1!.imgUser!.frame
                            self.avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                            
                            //self.avPlayerLayer = AVPlayerLayer(player:self.avPlayer);
                            //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                            //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
                            //self.avPlayerLayer.frame = cell!.imgProfileVideo!.frame
                            //cell?.contentView.layer.addSublayer(self.avPlayerLayer)
                            
                            cell1!.contentView.addSubview(self.avPlayerViewController.view)
                            self.addChildViewController(self.avPlayerViewController)
                            self.avPlayer.play()

                            
                            NotificationCenter.default.addObserver(self,
                                                                   selector: #selector(self.playerItemDidReachEnd),
                                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                                   object: self.avPlayer.currentItem!)
                            cell1!.avPlayer = self.avPlayer
                            cell1!.avPlayerViewController = self.avPlayerViewController
                            cell1!.imgUser.isHidden = true
                            cell1!.contentView.tag = collectionView.tag
                            cell1!.imgUser.tag = indexPath.item
                            self.avPlayerViewController.view.tag = indexPath.item
                        }
                        
                        //                        if self.avPlayer != nil && self.avPlayerViewController != nil && cell?.avPlayer == nil && cell?.avPlayerViewController == nil
                        //                        {
                        //                            cell?.avPlayer = self.avPlayer
                        //                            cell?.avPlayerViewController = self.avPlayerViewController
                        //                            cell?.contentView.addSubview(self.avPlayerViewController.view)
                        //                            self.addChildViewController(self.avPlayerViewController)
                        //                            self.avPlayer.play()
                        //
                        //                        }
                        //                        else
                        //                        {
                        //                            cell?.imgUser.isHidden = false
                        //                        }
                        
                    }
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        if self.avPlayer != nil && self.avPlayerViewController != nil && self.avPlayerViewController.view.tag == indexPath.row
        {
            removeAVPlayer()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView.tag != 100
        {
            let feedData = self.arrFeeds.object(at: collectionView.tag) as! FeedModel
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedData
            feedDetails.selectedRow = indexPath.item
            self.navigationController?.pushViewController(feedDetails, animated: true)
        }
    }
    func urlOfCurrentlyPlayingInPlayer() -> URL?
    {
        return ((self.avPlayer.currentItem?.asset) as? AVURLAsset)?.url
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification)
    {
        self.removeAVPlayer()
    }
    
    func removeAVPlayer()  {
        
        if (self.avPlayer != nil && self.avPlayerViewController != nil)
        {
            if self.avPlayerViewController.view.superview != nil
            {
                if self.avPlayer.isPlaying == true
                {
                    self.avPlayer.pause()
                }
                let idxPath = IndexPath(row: (self.avPlayerViewController.view.superview?.tag ?? 0), section: 1)
                if let feedTBLVideoCell = feedTableView.cellForRow(at: idxPath) as? FeedVideoCell
                {
                    let idxCllctnPath = IndexPath(row: self.avPlayerViewController.view.tag, section: 0)
                    let cell = feedTBLVideoCell.cllctnViewMedia.cellForItem(at: idxCllctnPath) as? FeedMediaCell
                    
                    cell?.imgUser.isHidden = false
                  
                    let feedData = self.arrFeeds.object(at: idxPath.row) as! FeedModel
                    let feedMediaData = feedData.arrMedia.object(at: idxCllctnPath.item) as! FeedMediaModel

                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
            }
            
            self.avPlayer = nil
            self.avPlayerViewController.view.removeFromSuperview()
            self.avPlayerViewController = nil
        }
    }
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView.tag != 100 && scrollView.tag != 101
        {
            if let customCllctnView = scrollView as? customCollectionView
            {
                if customCllctnView is customCollectionView
                {
                    customCllctnView.pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                    
                    customCllctnView.pageControll.currentPage = Int(customCllctnView.pageNumber)
                }
            }
        }
        

        //print("scrollViewDidEndDecelerating")
        
        if scrollView.tag == 101 && self.arrFeeds.count > 0
        {
        if  self.arrFeeds.count < self.maxPage && !boolGetData //self.currentPageNo < self.totalPage
        {
            if let lastVisibleIndexPath = self.feedTableView.indexPathsForVisibleRows?.last {
                let dataCount = self.arrFeeds.count - 1
                let dif = abs(dataCount - lastVisibleIndexPath.row)
                
                if dif == 1
                {
                    if appDelegate.isInternetAvailable() == true
                    {
                        currentPageNo += 1
                        boolGetData = true
                        self.callListAPI()
                    }
                    else
                    {
                        self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                    }
                }
            }
        }
        
        //        print("VisableVideoCellPlay*******scrollViewDidEndDecelerating")
        }
        
    }
    //MARK:- UIButton ACtion Methods
    
    @objc func methodUserProfileClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let userObj = feedObj.UserInfo
        if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
        {
            let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
            other.userObj = userObj
            other.strUserID = userObj.strUserId
            other.userNameString = userObj.strName
            self.navigationController?.pushViewController(other, animated: true)
        }
        else
        {
            let register = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            self.navigationController?.pushViewController(register, animated: true)
        }

    }
    @objc func methodLikeFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.LikeFeedApi(feedObj:)), with: feedObj)
        }
    }
    @objc func methodDislikeFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.DislikeFeedApi(feedObj:)), with: feedObj)
        }
    }
    @objc func methodSaveFeedClicked(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.BookmarkFeedApi(feedObj:)), with: feedObj)
        }
    }
    @objc func methodLikesUsersListClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
        likesUser.strFeedId = feedObj.strFeedId
        likesUser.strType = "LIKES"
        self.navigationController?.pushViewController(likesUser, animated: true)
    }
    @objc func methodDislikesUsersListClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
        likesUser.strFeedId = feedObj.strFeedId
        likesUser.strType = "DISLIKES"
        self.navigationController?.pushViewController(likesUser, animated: true)
    }
    @objc func methodCommentsClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let CommentVCObjc = PostCommentScreen.init(nibName: "PostCommentScreen", bundle: nil)
        CommentVCObjc.feedData = feedObj
        self.navigationController?.pushViewController(CommentVCObjc, animated: true)
    }
    @objc func methodTagPeopleClicked(_ sender: UIButton)
    {
        let feedObj = self.arrFeeds.object(at: sender.tag) as! FeedModel
        let taggedPeople = TaggedPeopleScreen.init(nibName: "TaggedPeopleScreen", bundle: nil)
        taggedPeople.strFeedId = feedObj.strFeedId
        taggedPeople.arrSelectedTaggedPeople = NSMutableArray.init(array: feedObj.strTaggedPeople.components(separatedBy: ","))
        self.navigationController?.pushViewController(taggedPeople, animated: true)
    }

    @IBAction func clickedMenuButtonAction(_ sender: UIButton) {
        if sender.isSelected {
            self.addMenuBGView.isHidden = true
            
        } else {
            self.addMenuBGView.isHidden = false
        }
    }
    
    func buttonForIndexAt(_ menuButton: ASCircularMenuButton, indexForButton: Int) -> UIButton {
        let button: UIButton = UIButton()
        
        if menuButton == colourPickerButton
        {
            button.setBackgroundImage(UIImage.init(named: self.colourArray.object(at: indexForButton) as! String), for: .normal)
        }

        return button
    }
    
    //MARK:- ASExtendedCircularMenu Delegate Method
    func didClickOnMainMenuButton(_ menuButton: ASCircularMenuButton) {
       
//        if self.isBgView {
//            self.addMenuBGView.isHidden = false
//            self.isBgView = false
//        } else {
//            self.addMenuBGView.isHidden = true
//            self.isBgView = true
//
//        }
        self.addMenuBGView.isHidden = !menuButton.isSelected
       
    }
    func didClickOnCircularMenuButton(_ menuButton: ASCircularMenuButton, indexForButton: Int, button: UIButton) {
        if menuButton == colourPickerButton {
            //            self.dismiss(animated: false, completion: nil)
            if menuButton.isSelected {
                self.addMenuBGView.isHidden = true

            } else {
                self.addMenuBGView.isHidden = false
            }
            
            switch(indexForButton)
            {
            case 0:
                
                let register = WriteFeedVC.init(nibName: "WriteFeedVC", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)
                
                break
            case 1:
                let register = SavedVC.init(nibName: "SavedVC", bundle: nil)
                self.navigationController?.pushViewController(register, animated: true)
                break
            case 2:
                let likes = LikesVC.init(nibName: "LikesVC", bundle: nil)
                likes.strUserId = self.appDelegate.currentLoginUser.strUserId
                self.navigationController?.pushViewController(likes, animated: true)
                break
            case 3:
                let followersVC = FollowersVC.init(nibName: "FollowersVC", bundle: nil)
                followersVC.strUserID = self.appDelegate.currentLoginUser.strUserId
                followersVC.isFollowings = false
                self.navigationController?.pushViewController(followersVC, animated: true)

                break
            case 4:
                let CameraVCObjc = CameraVC.init(nibName: "CameraVC", bundle: nil)
                self.navigationController?.pushViewController(CameraVCObjc, animated: false)
                break
            default:
                break
            }
        }
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getFeedsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        return dicrequest
    }
    //MARK : - getFeedsListApi
    @objc func getFeedsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialGetAllPost, Details: getFeedsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        
                        if self.currentPageNo == 1 {
                            self.arrFeeds.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kKeySocialFeedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrFeeds.insert((getFeedsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getFeedsObjectArray(arrResult: arr).count - 1))
                                self.arrFeeds.addObjects(from: (getFeedsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    func LikeFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsLiked == "1" || feedObj.strIsLiked == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - LikeFeedApi
    @objc func LikeFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialLikePost, Details: LikeFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            let feedObj = FeedModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            self.replaceFeedObjectInArray(feedObj: feedObj)
                            
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func DislikeFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsDisliked == "1" || feedObj.strIsDisliked == "true") ? "0" : "-1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - DislikeFeedApi
    @objc func DislikeFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialLikePost, Details: DislikeFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            let feedObj = FeedModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            self.replaceFeedObjectInArray(feedObj: feedObj)
                            
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func BookmarkFeedApiKeys(feedObj: FeedModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(feedObj.strFeedId.getEncryptedString() ?? "", forKey: kKeySocialFeedId as NSCopying)
        dicrequest.setObject(((feedObj.strIsSaved == "1" || feedObj.strIsSaved == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - BookmarkFeedApi
    @objc func BookmarkFeedApi(feedObj: FeedModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialSavePost, Details: BookmarkFeedApiKeys(feedObj: feedObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            let feedObj = FeedModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            self.replaceFeedObjectInArray(feedObj: feedObj)
                            
//                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.feedTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

    func replaceFeedObjectInArray(feedObj: FeedModel)
    {
        self.arrFeeds.enumerateObjects({(object, idx, stop) -> Void in
            print("idx",idx)
            let oldFeedObj = object as! FeedModel
            if oldFeedObj.strFeedId == feedObj.strFeedId
            {
                self.arrFeeds.replaceObject(at: idx, with: feedObj)
                return
            }
        })
    }
    func getFeedObjectInArray(feedId: String) -> FeedModel
    {
        var feedObj = FeedModel()

        self.arrFeeds.enumerateObjects({(object, idx, stop) -> Void in
            print("idx",idx)
            let oldFeedObj = object as! FeedModel
            if oldFeedObj.strFeedId == feedId
            {
                feedObj = oldFeedObj
            }
        })

        return feedObj
    }

}
