//
//  SavedVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 07/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SavedVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var buttonBgView: UIView!
    @IBOutlet weak var collectionsButton: UIButton!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var savedScrollView: UIScrollView!
    @IBOutlet weak var savedCollectionView: UICollectionView!
    @IBOutlet weak var viewBG: UIView!

    var arrFeeds = NSMutableArray()
    var arrCollections = NSMutableArray()

    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!
    var currentPageNoCollection = 1
    var maxPageCollection = 0
    var boolGetDataCollection:Bool!

    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        savedCollectionView.addSubview(refreshControl)
        
        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationBarWithBackButton(strTitle: "SAVED", leftbuttonImageName: "back.png")
        
        let rightButton = UIBarButtonItem.init(image: UIImage.init(named: "plus_white 2"), style: .done, target: self, action: #selector(addButtonAction(_:)))
        
        self.navigationItem.rightBarButtonItem = rightButton
        self.tabBarController?.tabBar.isHidden = true
 
//        let hegitValue = ScreenSize.SCREEN_HEIGHT - self.safeAreaTop
//        if ScreenSize.SCREEN_HEIGHT > 736
//        {
//            self.savedScrollView.frame = CGRect(x: 0, y: self.safeAreaTop, width: ScreenSize.SCREEN_WIDTH, height: CGFloat(hegitValue))
//        }
        self.callListAPI()
    }

    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.currentPageNo = 1
            self.maxPage = 0
            self.currentPageNoCollection = 1
            self.maxPageCollection = 0
            self.callListAPI()
        })
    }
    
    //MARK:- Helper Method
    func initialSetUp()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(updateCollectionsData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialUpdateCollections), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addNewCollectionData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialAddNewCollection), object: nil)

        let nib = UINib(nibName: "ImageCollectionCell", bundle: nil)
        
        self.savedCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCollectionCell")
//        self.savedScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.savedCollectionView.frame.origin.y + self.savedCollectionView.frame.size.height + 30)
        self.buttonBgView.layer.borderWidth = 1
        self.buttonBgView.layer.borderColor = UIColor.init(red: 126.0/255, green: 139.0/255, blue: 153.0/255, alpha: 1).cgColor
        
        
    }
    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            if (self.collectionsButton.isSelected == true)
            {
                if arrCollections.count == 0
                {
                    self.showActivity(text: "")
                }
                
                self.performSelector(inBackground: #selector(self.getCollectionsListApi), with: nil)
            }
            else
            {
                if arrFeeds.count == 0
                {
                    self.showActivity(text: "")
                }
                
                self.performSelector(inBackground: #selector(self.getSavedFeedsListApi), with: nil)
            }

        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }

    //MARK:- Notification Observer Methods
    @objc func updateCollectionsData(notiObj: Notification)
    {
        let collectionObj = notiObj.object as! CollectionModel
        self.replaceCollectionObjectInArray(collectionObj: collectionObj)
        self.savedCollectionView.reloadData()
    }
    @objc func addNewCollectionData(notiObj: Notification)
    {
        let collectionObj = notiObj.object as! CollectionModel
        self.arrCollections.insert(collectionObj, at: 0)
        self.maxPageCollection += 1
        self.savedCollectionView.reloadData()
        self.savedCollectionView.scrollToItem(at: IndexPath.init(row: savedCollectionView.numberOfItems(inSection: savedCollectionView.numberOfSections-1)-1, section: savedCollectionView.numberOfSections-1), at: .top, animated: false)
    }

    //MARK:- UICollectionView DataSource And Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.collectionsButton.isSelected == true)
        {
            return self.arrCollections.count
        }
        return self.arrFeeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        if (self.collectionsButton.isSelected == true)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
            
            cell.feedImageView.image = UIImage(named:"slider-img-placeholder")
            
            cell.lblName.isHidden = !self.collectionsButton.isSelected
            cell.imgBGName.isHidden = !self.collectionsButton.isSelected
            
            let collectionData = self.arrCollections.object(at: indexPath.item) as! CollectionModel
            cell.lblName.text = collectionData.strCollectionName
            if collectionData.strCollectionImage != ""
            {
                let url = URL.init(string: collectionData.strCollectionImage)
                cell.feedImageView.sd_addActivityIndicator()
                cell.feedImageView.sd_showActivityIndicatorView()
                cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell.feedImageView.sd_removeActivityIndicator()
                })
                cell.feedImageView.isHidden = false
            }
            return cell
       }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
            
            cell.feedImageView.image = UIImage(named:"slider-img-placeholder")
            
            cell.lblName.isHidden = !self.collectionsButton.isSelected
            cell.imgBGName.isHidden = !self.collectionsButton.isSelected
            
           let feedData = self.arrFeeds.object(at: indexPath.item) as! FeedModel
            if feedData.arrMedia.count > 0
            {
                let feedMediaData = feedData.arrMedia.object(at: 0) as! FeedMediaModel
                
                if feedMediaData.strMediaURL != ""
                {
                    if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                    {
                        let url = URL.init(string: feedMediaData.strMediaURL)
                        cell.feedImageView.sd_addActivityIndicator()
                        cell.feedImageView.sd_showActivityIndicatorView()
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                            cell.feedImageView.sd_removeActivityIndicator()
                        })
                        cell.feedImageView.isHidden = false
                    }
                    else
                    {
                        let url = URL.init(string: feedMediaData.strMediaThumbURL)
                        cell.feedImageView.sd_addActivityIndicator()
                        cell.feedImageView.sd_showActivityIndicatorView()
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                            cell.feedImageView.sd_removeActivityIndicator()
                        })
                        cell.feedImageView.isHidden = false
                   }
                }
            }

            return cell
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((collectionView.frame.width/3) - 5), height: (collectionView.frame.width/3) - 5)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if (self.collectionsButton.isSelected == true)
        {
            let collectionData = self.arrCollections.object(at: indexPath.item) as! CollectionModel
            let savedDetailScreen = SavedDetailScreen.init(nibName: "SavedDetailScreen", bundle: nil)
            savedDetailScreen.collectionData = collectionData
            self.navigationController?.pushViewController(savedDetailScreen, animated: true)
        }
        else
        {
            let feedData = self.arrFeeds.object(at: indexPath.item) as! FeedModel
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedData
            feedDetails.selectedRow = 0
            self.navigationController?.pushViewController(feedDetails, animated: true)
        }

    }
    
    
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.collectionsButton.isSelected == true && self.arrCollections.count > 0
        {
            if  self.arrCollections.count < self.maxPageCollection && !boolGetDataCollection
            {
                let arrIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems
                let lastVisibleIndexPath =  IndexPath(row: self.arrCollections.count-1, section: 0)
                //                if let lastVisibleIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems.last {
                if lastVisibleIndexPath != nil && arrIndexPath.contains(lastVisibleIndexPath)
                {
                    let dataCount = self.arrCollections.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.item)
                    
                    if dif == 0
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNoCollection += 1
                            boolGetDataCollection = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
        }
        else
        {
            if  self.arrFeeds.count < self.maxPage && !boolGetData 
            {
                let arrIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems
                let lastVisibleIndexPath =  IndexPath(row: self.arrFeeds.count-1, section: 0)
                //                if let lastVisibleIndexPath = (scrollView as! UICollectionView).indexPathsForVisibleItems.last {
                if lastVisibleIndexPath != nil && arrIndexPath.contains(lastVisibleIndexPath)
                {
                    let dataCount = self.arrFeeds.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.item)
                    
                    if dif == 0
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- UIButton Action Methods
    @objc func addButtonAction(_ sender: Any) {
        let createCollection = CreateCollectionVC.init(nibName: "CreateCollectionVC", bundle: nil)
        createCollection.strFrom = "Saved"
        self.navigationController?.pushViewController(createCollection, animated: true)
        
    }
    
    @IBAction func toggleButtonsAction(_ sender: UIButton!) {
        if (sender.isSelected == true)
        {
            return
        }
        self.allButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
        self.allButton.titleLabel?.textColor = UIColor.white
        
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 100 {
            self.collectionsButton.isSelected = false
            self.allButton.isSelected = true
            self.allButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
            self.collectionsButton.backgroundColor = UIColor.white
            self.allButton.setTitleColor(UIColor.white, for: .normal)
            
        } else {
            self.collectionsButton.isSelected = true
            self.collectionsButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
            self.allButton.isSelected = false
            self.allButton.setTitleColor(UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1), for: .normal)
            
            self.allButton.backgroundColor = UIColor.white
            
        }
        self.savedCollectionView.reloadData()
        self.callListAPI()
    }
    
    func getCollectionsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.appDelegate.currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getCollectionsListApi
    @objc func getCollectionsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialCollectionList, Details: getCollectionsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNoCollection == 1 {
                            self.arrCollections.removeAllObjects()
                        }
                        self.boolGetDataCollection = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kData) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrCollections.insert((getCollectionsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getCollectionsObjectArray(arrResult: arr).count - 1))
                                self.arrCollections.addObjects(from: (getCollectionsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPageCollection = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.savedCollectionView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func getSavedFeedsListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject(self.appDelegate.currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        return dicrequest
    }
    //MARK : - getSavedFeedsListApi
    @objc func getSavedFeedsListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialSavedPostList, Details: getSavedFeedsListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.arrFeeds.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kKeySocialFeedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrFeeds.insert((getFeedsObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getFeedsObjectArray(arrResult: arr).count - 1))
                                self.arrFeeds.addObjects(from: (getFeedsObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.savedCollectionView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

    func replaceCollectionObjectInArray(collectionObj: CollectionModel)
    {
        self.arrCollections.enumerateObjects({(object, idx, stop) -> Void in
            print("idx",idx)
            let oldCollectionObj = object as! CollectionModel
            if oldCollectionObj.strCollectionId == collectionObj.strCollectionId
            {
                self.arrCollections.replaceObject(at: idx, with: collectionObj)
                return
            }
        })
    }

    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
