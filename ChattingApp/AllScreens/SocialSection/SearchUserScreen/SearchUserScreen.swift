//
//  SearchUserScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SearchUserScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblContacts:  UITableView!
    @IBOutlet weak var viewNav:  UIView!
    @IBOutlet weak var btnBack:  UIButton!
    @IBOutlet weak var txtFieldSearch:  UITextField!
    @IBOutlet var toolBar: UIToolbar!
    var strSearchText = ""
    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!

    var arrSearchUsers = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFieldSearch.inputAccessoryView = toolBar
        btnBack.tintColor = UIColor.init(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
        tblContacts.tableFooterView = UIView.init(frame: CGRect.zero)
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        tblContacts.addSubview(refreshControl)

        NotificationCenter.default.addObserver(self, selector: #selector(self.methodRefreshAllData), name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)
       // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationItem.titleView = viewNav
        
        NotificationCenter.default.addObserver(self, selector: #selector(textFiledDidChangedNotification), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        self.methodRefreshAllData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
    }

    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.methodRefreshAllData()
       })
    }
    @objc func methodRefreshAllData()
    {
        self.currentPageNo = 1
        self.maxPage = 0
        if self.strSearchText != ""
        {
            self.perform(#selector(self.SearchKeywordreload), with: self.strSearchText, afterDelay: 0.3)
        }
    }


    // MARK: - UIButtonsMethod
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        txtFieldSearch.resignFirstResponder()
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        txtFieldSearch.resignFirstResponder()
    }
    @IBAction func methodBackClicked(_ sender: UIButton)
    {
        txtFieldSearch.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
        //   self.menuContainerViewController.toggleLeftSideMenuCompletion {
        
        //    }
    }
    //MARK:- Notification Observer Methods
    //MARK: - textFiledDidChangedNotification
    @objc func textFiledDidChangedNotification(notification:NSNotification)
    {
        //txtSearchlocation
        let textFiled = notification.object as! UITextField
        if textFiled.tag == 5001
        {
            let strKeyword = textFiled.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.hideActivity()
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.SearchKeywordreload), object: strSearchText)
            strSearchText = strKeyword
            self.currentPageNo = 1
            self.maxPage = 0
            if strKeyword != ""
            {
                self.perform(#selector(self.SearchKeywordreload), with: strKeyword, afterDelay: 0.3)
            }
            else
            {
                self.hideActivity()
                arrSearchUsers.removeAllObjects()
                tblContacts.reloadData()
            }
        }
        
    }
    @objc func SearchKeywordreload(strKeyword:String)
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.getSearchUserApi(strKeyword:)), with: strKeyword)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if  self.arrSearchUsers.count != 0
        {
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            let font = UIFont(name: "Lato-Bold", size: 17)
            noDataLabel.font = font
            noDataLabel.text = "No user found."
            noDataLabel.textColor = UIColor.colorAppTheam
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }

        return self.arrSearchUsers.count < self.maxPage ? self.arrSearchUsers.count+1 : self.arrSearchUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return indexPath.row == self.arrSearchUsers.count ? 48 : 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row == self.arrSearchUsers.count
        {
            let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+60, 0.0, 0.0)
            cell.backgroundColor = UIColor.clear
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            spinner.frame = CGRect(x: (UIScreen .main.bounds.size.width - 20)/2, y: 12, width: 20, height: 20)
            cell.addSubview(spinner)
            spinner.startAnimating()
            return cell
        }
        else
        {
            
            let cellIdentifier:String = "CallHistoryTableCell"
            var cell : CallHistoryTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CallHistoryTableCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("CallHistoryTableCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? CallHistoryTableCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
            cell?.imgViewUser.layer.masksToBounds = true
            
            let userObj = self.arrSearchUsers.object(at: indexPath.row) as! UserModel
            cell?.lblName.text = userObj.strName
            cell?.lblMessage2.text = userObj.strEmail
            cell?.lblMessage.text = ""
            //        cell?.lblMessage.sizeToFit()
            //        cell?.lblMessage2.sizeToFit()
            if userObj.strProfilepicImage != ""
            {
                // \(Image_URL)
                let url = URL.init(string: userObj.strProfilepicImage)
                cell?.imgViewUser.sd_addActivityIndicator()
                cell?.imgViewUser.sd_showActivityIndicatorView()
                cell?.imgViewUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.imgViewUser.sd_removeActivityIndicator()
                })
            }
            else
            {
                cell?.imgViewUser.image = UIImage(named:"reg_USER")
            }
            
            //        cell?.imgViewUser.image = UIImage(named: (arrSearchUsers.object(at: indexPath.row) as! NSDictionary).object(forKey: "image") as! String)
            //        cell?.lblName.text = (arrSearchUsers.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as! String
            //        cell?.lblMessage.text = (arrSearchUsers.object(at: indexPath.row) as! NSDictionary).object(forKey: "message") as! String
            //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
            //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
            
            cell?.imgStatus.isHidden = false
            cell?.lblMessage2.isHidden = false
            cell?.lblMessage.isHidden = true
            cell?.imgStatus.isHidden = true
            cell?.btnAudio.isHidden = true
            cell?.btnVideo.isHidden = true
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.arrSearchUsers.count
        {
            return
        }
        let userObj = self.arrSearchUsers.object(at: indexPath.row) as! UserModel
        let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
        other.userObj = userObj
        other.strUserID = userObj.strUserId
        other.userNameString = userObj.strName
        self.navigationController?.pushViewController(other, animated: true)
    }
    
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == tblContacts && self.arrSearchUsers.count > 0
        {
            if  self.arrSearchUsers.count < self.maxPage && !boolGetData //self.currentPageNo < self.totalPage
            {
                if let lastVisibleIndexPath = self.tblContacts.indexPathsForVisibleRows?.last {
                    let dataCount = self.arrSearchUsers.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.row)
                    
                    if dif == 1
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            if self.strSearchText != ""
                            {
                                self.perform(#selector(self.SearchKeywordreload), with: self.strSearchText, afterDelay: 0.3)
                            }
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
            //        print("VisableVideoCellPlay*******scrollViewDidEndDecelerating")
        }
    }
    

    func getSearchUserApiKeys(strKeyword: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(strKeyword.getEncryptedString() ?? "", forKey: kKeySocialKeyword as NSCopying)
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strUserId, forKey: kUserID as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strName, forKey: kName as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strEmail, forKey: kEmail as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strGender , forKey: kGender as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strBio, forKey: kBio as NSCopying)
//        dicrequest.setObject(appDelegate.currentLoginUser.strDob, forKey: kDOB as NSCopying)
        return dicrequest
    }
    //MARK : - getSearchUserApi
    @objc func getSearchUserApi(strKeyword : String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialSearchUser, Details: getSearchUserApiKeys(strKeyword: strKeyword)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.arrSearchUsers.removeAllObjects()
                        }
                        self.boolGetData = false

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kUserSavedDetails) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
//                                self.arrSearchUsers.insert((getUsersObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getUsersObjectArray(arrResult: arr).count - 1))
                                self.arrSearchUsers.addObjects(from: (getUsersObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0

                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.tblContacts.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

