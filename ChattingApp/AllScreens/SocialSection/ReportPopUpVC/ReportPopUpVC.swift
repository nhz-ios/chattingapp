//
//  ReportPopUpVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 14/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ReportPopUpVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var hideYourStoryButton: UIButton!
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var reportButton: UIButton!
    
    var userObj: UserModel!

    //MARK:- UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let userTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        userTapGesture.delegate = self
        self.view.addGestureRecognizer(userTapGesture)
        
        blockButton.setTitle((self.userObj.strIsBlockedByMe == "1" || self.userObj.strIsBlockedByMe == "true") ? "Unblock" : "Block", for: .normal)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK:- UIButton Action Method
    @IBAction func commonButtonAction(_ sender: UIButton!) {
        
        switch sender.tag {
        case 100:
            
            let alertContr  =   UIAlertController(title: kAppName, message: kMessageSocialReport, preferredStyle: .alert)
            
            alertContr.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Enter Message"
            })
            let cancelAction  = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in
            }
            
            let okAction  = UIAlertAction(title: "Report", style: .default) { (alert: UIAlertAction!) in
                
                let firstTextField = alertContr.textFields![0] as UITextField
                
                if (firstTextField.text == "")
                {
                    self.showToastOnViewController1(title: kError, message: kMessageSocialEnterReport, position: kToastTopPosition, controller: self, image: nil)
                    return
                }
                DispatchQueue.main.async
                    {
                        if self.appDelegate.isInternetAvailable() == false
                        {
                            self.showToastOnViewController1(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.view.endEditing(true)
                            self.showActivity(text: "")
                            self.performSelector(inBackground: #selector(self.ReportUserApi(strMessage:)), with: firstTextField.text)
                        }
                }
            }
            alertContr.addAction(cancelAction)
            alertContr.addAction(okAction)
            
            self.present(alertContr, animated: true, completion: nil)
            
            break
            
        case 200:
            
            let alert  =   UIAlertController(title: kAppName, message: (self.userObj.strIsBlockedByMe == "1" || self.userObj.strIsBlockedByMe == "true") ? kMessageSocialUnblock : kMessageSocialBlock, preferredStyle: .alert)
            
            let cancelAction  = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) in
            }
            
            let okAction  = UIAlertAction(title: (self.userObj.strIsBlockedByMe == "1" || self.userObj.strIsBlockedByMe == "true") ? "Unblock" : "Block", style: .default) { (alert: UIAlertAction!) in
                
                if self.appDelegate.isInternetAvailable() == false
                {
                    self.showToastOnViewController1(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    self.view.endEditing(true)
                    self.showActivity(text: "")
                    self.performSelector(inBackground: #selector(self.BlockUserApi(strStatus:)), with: (self.userObj.strIsBlockedByMe == "1" || self.userObj.strIsBlockedByMe == "true") ? "0" : "1")
                }
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)


            break
            
        case 300:
            self.dismiss(animated: false, completion: nil)
            break
            
        case 400:
            self.dismiss(animated: false, completion: nil)
           break
            
        default:
            break
        }
        
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func BlockUserApiKeys(strStatus: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(userObj.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        dicrequest.setObject(strStatus.getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        return dicrequest
    }
    //MARK : - BlockUserApi
    @objc func BlockUserApi(strStatus: String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialBlockUser, Details: BlockUserApiKeys(strStatus: strStatus)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateOtherProfile), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)
                            self.showToastOnViewController1(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                            self.dismiss(animated: false, completion: nil)
                        }
                        else
                        {
                            self.showToastOnViewController1(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController1(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController1(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

    func ReportUserApiKeys(strMessage: String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(userObj.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        dicrequest.setObject(strMessage.getEncryptedString() ?? "", forKey: kKeySocialReportMessage as NSCopying)
        return dicrequest
    }
    //MARK : - ReportUserApi
    @objc func ReportUserApi(strMessage: String)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialReportUser, Details: ReportUserApiKeys(strMessage: strMessage)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateOtherProfile), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)
                            self.showToastOnViewController1(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                            self.dismiss(animated: false, completion: nil)
                        }
                        else
                        {
                            self.showToastOnViewController1(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController1(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController1(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

}
