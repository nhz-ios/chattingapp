//
//  CameraVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 12/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import LLSimpleCamera
import AssetsLibrary
import Photos

class CameraVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var buttonHeaderView: UIView!
    @IBOutlet weak var rightArrowButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var buttonsBgView: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var galaryButton: UIButton!
    @IBOutlet weak var frontCameraButton: UIButton!
    @IBOutlet weak var emojiButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    
    @IBOutlet weak var liveButton: UIButton!
    @IBOutlet weak var typeButton: UIButton!
    @IBOutlet weak var normalButton: UIButton!
    var camera = LLSimpleCamera()
    var userImageData = Data()
      var images:[UIImage] = []
    
    //MARK:- UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    //MARK:- Helper Method
    func initialSetUp() {
        self.galaryButton.layer.borderWidth = 1
        self.galaryButton.layer.borderColor = UIColor.white.cgColor
        
        self.fetchPhotos()
        self.setupCamera()
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Fetch Last Photo From Library
    func fetchPhotos () {
        // Sort the images by descending creation date and fetch the first 3
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        fetchOptions.fetchLimit = 1
        
        // Fetch the image assets
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        
        // If the fetch result isn't empty,
        // proceed with the image request
        if fetchResult.count > 0 {
            let totalImageCountNeeded = 1 // <-- The number of images to fetch
            fetchPhotoAtIndex(0, totalImageCountNeeded, fetchResult)
        }
    }
    
    // Repeatedly call the following method while incrementing
    // the index until all the photos are fetched
    func fetchPhotoAtIndex(_ index:Int, _ totalImageCountNeeded: Int, _ fetchResult: PHFetchResult<PHAsset>) {
        
        // Note that if the request is not set to synchronous
        // the requestImageForAsset will return both the image
        // and thumbnail; by setting synchronous to true it
        // will return just the thumbnail
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        
        // Perform the image request
        PHImageManager.default().requestImage(for: fetchResult.object(at: index) as PHAsset, targetSize: view.frame.size, contentMode: PHImageContentMode.aspectFill, options: requestOptions, resultHandler: { (image, _) in
            if let image = image {
                // Add the returned image to your array
                self.images += [image]
            }
            // If you haven't already reached the first
            // index of the fetch result and if you haven't
            // already stored all of the images you need,
            // perform the fetch request again with an
            // incremented index
            if index + 1 < fetchResult.count && self.images.count < totalImageCountNeeded {
                self.fetchPhotoAtIndex(index + 1, totalImageCountNeeded, fetchResult)
            } else {
                // Else you have completed creating your array
                print("Completed array: \(self.images)")
                
                self.galaryButton.setImage(self.images.last, for: .normal)
            }
        })
    }

    
    //MARK:- Camera Methods
    func setupCamera()
    {
        DispatchQueue.main.async {
            UIApplication.shared.isStatusBarHidden = false
        }
        
        let screenRect = UIScreen.main.bounds
        
        self.camera = LLSimpleCamera(quality: AVCaptureSession.Preset.high.rawValue, position: LLCameraPositionRear, videoEnabled: false)
        
        self.camera.attach(to: self, withFrame: CGRect(x:0, y:0, width:screenRect.size.width, height:screenRect.size.height))
        
        
       // let point = self.camera.convertToPointOfInterest(fromViewCoordinates: self.view.center)
        
       // self.camera.focus(at: point)
        
        self.camera.fixOrientationAfterCapture =  true
        
        self.camera.onDeviceChange = {(camera, device) -> Void in
            
            if (camera?.isFlashAvailable())!
            {
                self.flashButton.isHidden = false
                if camera?.flash == LLCameraFlashOff
                {
                    self.flashButton.isSelected = false
                }
                else
                {
                    self.flashButton.isSelected = true
                }
            }
            else {
                self.flashButton.isHidden = true
            }
        }
        
        
        if(LLSimpleCamera.isFrontCameraAvailable() && LLSimpleCamera.isRearCameraAvailable()){
            
            self.camera.start()
            self.view.addSubview(self.buttonsBgView)
            self.view.addSubview(self.buttonHeaderView)
        }
            
        else{
            let label: UILabel = UILabel(frame: CGRect.zero)
            label.text = "You must have a camera to take video."
            label.numberOfLines = 2
            label.lineBreakMode = .byWordWrapping;
            label.backgroundColor = UIColor.clear
            label.font = UIFont(name: "Lato-Medium", size: 13.0)
            label.textColor = UIColor.white
            label.textAlignment = .center
            label.sizeToFit()
        }
        
    }
    
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        var image: UIImage!
        
        // fetch the selected image
        
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        self.userImageData = UIImageJPEGRepresentation(image, 0.6)!
        self.backgroundImageView.image = UIImage.init(data: self.userImageData)
        self.galaryButton.setImage(UIImage.init(data: self.userImageData), for: .normal)
    }
    
    //MARK:- UIButton Actions Methods
    @IBAction func gallaryButtonAction(_ sender: Any) {
        self.view.endEditing(true)

        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
       
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Open Gallery")
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    @IBAction func captureButtonAction(_ sender: Any) {
        self.camera.capture({(camera, image, metadata, error) -> Void in
            if (error == nil) {
                
                camera?.perform(#selector(LLSimpleCamera.stop), with: nil, afterDelay: 0.2)
                self.backgroundImageView.image = image!
            }
        }, exactSeenImage: true)
    }
    
    @IBAction func frontCameraButton(_ sender: Any) {
        if(self.camera.position == LLCameraPositionRear)
        {
            self.flashButton.isHidden = false
        }
        else
        {
            self.flashButton.isHidden = true
        }
        
        self.camera.togglePosition()
    }
    
    @IBAction func emojiButtonAction(_ sender: Any) {
    }
    
    @IBAction func flashOnOffButton(_ sender: Any) {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on)
                {
                    flashButton.isSelected = false
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                }
                else
                {
                    flashButton.isSelected = true
                    do {
                        try device?.setTorchModeOn(level: 1.0)
                    } catch {
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    func setInsets(_ sender: UIButton!)  
    {
        typeButton.isSelected = false
        normalButton.isSelected = false
        liveButton.isSelected = false
        
        sender.isSelected = true
        typeButton.titleEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        typeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        normalButton.titleEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        normalButton.imageEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        liveButton.titleEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        liveButton.imageEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)

        if (liveButton.isSelected == true)
        {
            liveButton.titleEdgeInsets = UIEdgeInsets(top: 0, left:-10, bottom: 13, right: 0)
            liveButton.imageEdgeInsets = UIEdgeInsets(top: 25, left:17, bottom: 0, right: 0)
        }
        else if (normalButton.isSelected == true)
        {
            normalButton.titleEdgeInsets = UIEdgeInsets(top: 0, left:-10, bottom: 13, right: 0)
            normalButton.imageEdgeInsets = UIEdgeInsets(top: 30, left:25, bottom: 0, right: 0)
        }
        else if (typeButton.isSelected == true)
        {
            typeButton.titleEdgeInsets = UIEdgeInsets(top: 0, left:-10, bottom: 13, right: 0)
            typeButton.imageEdgeInsets = UIEdgeInsets(top: 25, left:17, bottom: 0, right: 0)
        }
        
        
    }
    
    @IBAction func normalButtonAction(_ sender: UIButton!) {
        if(sender.isSelected == true)
        {
            return
        }
        self.setInsets(sender)
    }
    
    @IBAction func liveButtonAction(_ sender: UIButton!) {
        if(sender.isSelected == true)
        {
            return
        }
        self.setInsets(sender)
    }
    
    @IBAction func typeButtonAction(_ sender: UIButton!) {
        if(sender.isSelected == true)
        {
            return
        }
        self.setInsets(sender)
    }
    
    @IBAction func rightArrowButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
