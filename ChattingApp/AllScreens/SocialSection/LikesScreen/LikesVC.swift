//
//  LikesVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class LikesVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate {

    @IBOutlet weak var youButton: UIButton!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var borderButtonView: UIView!
    @IBOutlet weak var likeTableView: UITableView!
    
    var arrActivitiesList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var currentPageNo = 1
    var maxPage = 0
    var boolGetData:Bool!

    var strUserId = ""
    //MARK:- UIViewController Life Cycle Life Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(PoolToRefresh), for:UIControlEvents.valueChanged)
        likeTableView.addSubview(refreshControl)
        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationBarWithBackButton(strTitle: "ACTIVITIES", leftbuttonImageName: "back.png")
    }
    //PoolRefresh
    @objc func PoolToRefresh()
    {
        refreshControl.isHidden = false
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.refreshControl.endRefreshing()
            self.refreshControl.isHidden = true
            self.methodRefreshAllData()
        })
    }

    @objc func methodRefreshAllData()
    {
        likeTableView.setContentOffset(.zero, animated: true)
        self.currentPageNo = 1
        self.maxPage = 0
        self.callListAPI()
    }

    @objc func callListAPI()
    {
        if self.appDelegate.isInternetAvailable() == true
        {
            self.showActivity(text: "")
            self.performSelector(inBackground: #selector(self.getActivitiesListApi), with: nil)
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }
    }
    //MARK:- Helper Method
    func initialSetUp() {
        self.borderButtonView.layer.borderWidth = 1
        self.borderButtonView.layer.borderColor = UIColor.init(red: 126.0/255, green: 139.0/255, blue: 153.0/255, alpha: 1).cgColor
        
        self.youButton.backgroundColor = UIColor.white
        self.followingButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)

        youButton.isSelected = false
        followingButton.isSelected = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodRefreshAllData), name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)
        self.methodRefreshAllData()

    }

    //MARK:- UITableView DatSource And Delegate Methods
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  self.arrActivitiesList.count != 0
        {
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            let font = UIFont(name: "Lato-Bold", size: 17)
            noDataLabel.font = font
            noDataLabel.text = "No activities yet."
            noDataLabel.textColor = UIColor.colorAppTheam
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        
        return self.arrActivitiesList.count < self.maxPage ? self.arrActivitiesList.count+1 : self.arrActivitiesList.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.arrActivitiesList.count
        {
            let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width+60, 0.0, 0.0)
            cell.backgroundColor = UIColor.clear
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            spinner.frame = CGRect(x: (UIScreen .main.bounds.size.width - 20)/2, y: 12, width: 20, height: 20)
            cell.addSubview(spinner)
            spinner.startAnimating()
            return cell
        }
        else
        {
            
            let indentifier:String = "LikeCell"
            var cell : LikeCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? LikeCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("LikeCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? LikeCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }

            let activityObj = self.arrActivitiesList.object(at: indexPath.row) as! ActivityModel
            
            //        cell?.followingButton.setTitle(isFollowings == false ? "Following" : "Unfollow", for: .normal)
            cell?.userImageView.tag = indexPath.row
            cell?.postImageView.tag = indexPath.row
            cell?.dateLabel.text = ""
            cell?.descriptionLabel.isHidden = true
            cell?.postImageView.isHidden = true
            cell?.descriptionCommentLabel.isHidden = true
            cell?.lblMessage.isHidden = false
            cell?.postImageView.isHidden = activityObj.strActivityType == kKeySocialActivityTypeFollow
            cell?.lblMessage.text = activityObj.strActivityMessage
            
            let tapUser = UITapGestureRecognizer.init(target: self, action: #selector(userProfileClicked(gest: )))
            tapUser.delegate = self
            cell?.userImageView.addGestureRecognizer(tapUser)
            
//            if activityObj.arrUsersData.count > 0
//            {
//                let userObj = activityObj.arrUsersData.object(at: 0) as! UserModel
            
                let userObj = activityObj.userInfo
                if userObj.strProfilepicImage != ""
                {
                    // \(Image_URL)
                    let url = URL.init(string: userObj.strProfilepicImage)
                    cell?.userImageView.sd_addActivityIndicator()
                    cell?.userImageView.sd_showActivityIndicatorView()
                    cell?.userImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.userImageView.sd_removeActivityIndicator()
                    })
                }
                else
                {
                    cell?.userImageView.image = UIImage(named:"reg_USER")
                }
//            }
            let otherUserData = activityObj.otherUserData
            cell?.followingButton.isHidden = (otherUserData.strUserId == self.appDelegate.currentLoginUser.strUserId) || (activityObj.strActivityType != kKeySocialActivityTypeFollow)
            cell?.followingButton.setTitle((otherUserData.strIsFollowed == "1" || otherUserData.strIsFollowed == "true") ? "Unfollow" : "Follow", for: .normal)
            cell?.followingButton.tag = indexPath.row
            cell?.followingButton.addTarget(self, action: #selector(followButtonAction(_:)), for: .touchUpInside)

            if activityObj.strActivityType != kKeySocialActivityTypeFollow
            {
                let tapPost = UITapGestureRecognizer.init(target: self, action: #selector(postClicked(gest: )))
                tapPost.delegate = self
                cell?.postImageView.addGestureRecognizer(tapPost)

                let feedData = activityObj.feedData
                if feedData.arrMedia.count > 0
                {
                    let feedMediaData = feedData.arrMedia.object(at: 0) as! FeedMediaModel
                    
                    if feedMediaData.strMediaURL != ""
                    {
                        if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                        {
                            let url = URL.init(string: feedMediaData.strMediaURL)
                            cell?.postImageView.sd_addActivityIndicator()
                            cell?.postImageView.sd_showActivityIndicatorView()
                            cell?.postImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                                cell?.postImageView.sd_removeActivityIndicator()
                            })
                            cell?.postImageView.isHidden = false
                        }
                        else
                        {
                            let url = URL.init(string: feedMediaData.strMediaThumbURL)
                            cell?.postImageView.sd_addActivityIndicator()
                            cell?.postImageView.sd_showActivityIndicatorView()
                            cell?.postImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                                cell?.postImageView.sd_removeActivityIndicator()
                            })
                            cell?.postImageView.isHidden = false
                        }
                    }
                }
                else
                {
                    cell?.postImageView.isHidden = true
                }

            }
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.arrActivitiesList.count
        {
            return
        }
        let activityObj = self.arrActivitiesList.object(at: indexPath.row) as! ActivityModel
        let feedObj = activityObj.feedData
        
        if activityObj.strActivityType != kKeySocialActivityTypeFollow
        {
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedObj
            feedDetails.selectedRow = 0
            self.navigationController?.pushViewController(feedDetails, animated: true)
        }
        else
        {
            let otherUserObj = activityObj.otherUserData
            let userObj = activityObj.userInfo
            if otherUserObj.strUserId != ""
            {
                if otherUserObj.strUserId != self.appDelegate.currentLoginUser.strUserId
                {
                    let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
                    other.userObj = otherUserObj
                    other.strUserID = otherUserObj.strUserId
                    other.userNameString = otherUserObj.strName
                    self.navigationController?.pushViewController(other, animated: true)
                }
                else
                {
                    if userObj.strUserId != ""
                    {
                        if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
                        {
                            let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
                            other.userObj = userObj
                            other.strUserID = userObj.strUserId
                            other.userNameString = userObj.strName
                            self.navigationController?.pushViewController(other, animated: true)
                        }
                    }

                }
            }
        }
        
//        switch activityObj.strActivityType {
//        case kKeySocialActivityTypeLike:
//
//            let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
//            likesUser.strFeedId = feedObj.strFeedId
//            likesUser.strType = "LIKES"
//            self.navigationController?.pushViewController(likesUser, animated: true)
//
//            break
//        case kKeySocialActivityTypeDislike:
//            let likesUser = LikesUserScreen.init(nibName: "LikesUserScreen", bundle: nil)
//            likesUser.strFeedId = feedObj.strFeedId
//            likesUser.strType = "DISLIKES"
//            self.navigationController?.pushViewController(likesUser, animated: true)
//            break
//        case kKeySocialActivityTypeComment:
//
//            let CommentVCObjc = PostCommentScreen.init(nibName: "PostCommentScreen", bundle: nil)
//            CommentVCObjc.feedData = feedObj
//            self.navigationController?.pushViewController(CommentVCObjc, animated: true)
//
//            break
//        case kKeySocialActivityTypeFollow:
//
//            break
//        default:
//            break
//        }
    }

    //MARK:- UIButton Action Methods
    @IBAction func toggleButtonAction(_ sender: UIButton!)
    {
        if sender.isSelected
        {
            return
        }
        if self.appDelegate.isInternetAvailable() == true
        {
            self.youButton.isSelected = false
            self.followingButton.isSelected = false
            self.youButton.backgroundColor = UIColor.white
            self.followingButton.backgroundColor = UIColor.white
            
            if sender.tag == 200
            {
                self.youButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
            }
            else
            {
                self.followingButton.backgroundColor = UIColor.init(red: 125.0/255, green: 138.0/255, blue: 152.0/255, alpha: 1)
            }
            sender.isSelected = !sender.isSelected
//            isFollowings = self.followingButton.isSelected
//            self.navigationItem.title = isFollowings == false ? "FOLLOWERS" : "FOLLOWINGS"
            //            self.followTableView.reloadData()
            
            self.methodRefreshAllData()
        }
        else
        {
            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
        }

    }
    @objc func userProfileClicked(gest: UITapGestureRecognizer)
    {
        let activityObj = self.arrActivitiesList.object(at: gest.view?.tag ?? 0) as! ActivityModel
//        if activityObj.arrUsersData.count > 0
//        {
//            let userObj = activityObj.arrUsersData.object(at: 0) as! UserModel
            let userObj = activityObj.userInfo
            if userObj.strUserId != ""
            {
                if userObj.strUserId != self.appDelegate.currentLoginUser.strUserId
                {
                    let other = OtherProfileVC.init(nibName: "OtherProfileVC", bundle: nil)
                    other.userObj = userObj
                    other.strUserID = userObj.strUserId
                    other.userNameString = userObj.strName
                    self.navigationController?.pushViewController(other, animated: true)
                }
            }
//        }
    }
    @objc func postClicked(gest: UITapGestureRecognizer)
    {
        let activityObj = self.arrActivitiesList.object(at: gest.view?.tag ?? 0) as! ActivityModel
        let feedData = activityObj.feedData
        if activityObj.feedData.strFeedId != ""
        {
            let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
            feedDetails.feedData = feedData
            feedDetails.selectedRow = 0
            self.navigationController?.pushViewController(feedDetails, animated: true)
        }
    }

    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == likeTableView && self.arrActivitiesList.count > 0
        {
            if  self.arrActivitiesList.count < self.maxPage && !boolGetData //self.currentPageNo < self.totalPage
            {
                if let lastVisibleIndexPath = self.likeTableView.indexPathsForVisibleRows?.last {
                    let dataCount = self.arrActivitiesList.count - 1
                    let dif = abs(dataCount - lastVisibleIndexPath.row)
                    
                    if dif == 1
                    {
                        if appDelegate.isInternetAvailable() == true
                        {
                            currentPageNo += 1
                            boolGetData = true
                            self.callListAPI()
                        }
                        else
                        {
                            self.onShowAlertController(title: kInternetError , message: kInternetErrorMessage)
                        }
                    }
                }
            }
            //        print("VisableVideoCellPlay*******scrollViewDidEndDecelerating")
        }
    }
    
    
    //MARK:- UIButton Action Methods
    @IBAction func followButtonAction(_ sender: UIButton)
    {
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
            let activityObj = self.arrActivitiesList.object(at: sender.tag) as! ActivityModel
            let otherUserData = activityObj.otherUserData
            self.showActivity(text: "")
            
            self.performSelector(inBackground: #selector(self.followOtherUserApi(userObj:)), with: otherUserData)
        }
    }
    
    
    func getActivitiesListApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()

        dicrequest.setObject(strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        dicrequest.setObject("\(self.currentPageNo)".getEncryptedString() ?? "", forKey: kKeySocialPage as NSCopying)
        dicrequest.setObject((youButton.isSelected == true ? "my" : "followings").getEncryptedString() ?? "", forKey: kKeySocialType as NSCopying)
        return dicrequest
    }
    //MARK : - getActivitiesListApi
    @objc func getActivitiesListApi()
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialMyActivitiesList, Details: getActivitiesListApiKeys()) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if self.currentPageNo == 1 {
                            self.arrActivitiesList.removeAllObjects()
                        }
                        self.boolGetData = false
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            let arr = dicRes.object(forKey: kData) as? NSArray ?? NSArray()
                            if (arr.count > 0)
                            {
                                //                                self.arrLikesUsersList.insert((getUsersObjectArray(arrResult: arr).reversed()), at: IndexSet(0...getUsersObjectArray(arrResult: arr).count - 1))
                                self.arrActivitiesList.addObjects(from: (getActivitiesObjectArray(arrResult: arr) as! [Any]))
                            }
                            self.maxPage = Int(dicRes.decryptedValueForKey(key: kKeySocialTotalData)) ?? 0
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.likeTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    func followOtherUserApiKeys(userObj: UserModel) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(userObj.strUserId.getEncryptedString() ?? "", forKey: kKeySocialOtherUserID as NSCopying)
        dicrequest.setObject(((userObj.strIsFollowed == "1" || userObj.strIsFollowed == "true") ? "0" : "1").getEncryptedString() ?? "", forKey: kKeySocialStatus as NSCopying)
        
        return dicrequest
    }
    //MARK : - followOtherUserApi
    @objc func followOtherUserApi(userObj: UserModel)
    {
        getAllSocialApiResultwithPostMethod(strMethodname: kAPISocialFollowUser, Details: followOtherUserApiKeys(userObj: userObj)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.methodRefreshAllData()

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationSocialRefreshData), object: nil)

                            self.showToastOnViewController(title: kSuccess.capitalized, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        self.likeTableView.reloadData()
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                }
            }
        }
    }
    

    
}
