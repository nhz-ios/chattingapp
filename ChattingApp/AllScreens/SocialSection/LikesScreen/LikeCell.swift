//
//  LikeCell.swift
//  ChattingApp
//
//  Created by Shreya-ios on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class LikeCell: UITableViewCell {

    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var descriptionCommentLabel: UILabel!    
    @IBOutlet weak var followingButton: UIButton!
  
    @IBOutlet var lblMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.descriptionLabel.sizeToFit()
        self.descriptionCommentLabel.sizeToFit()
        self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
