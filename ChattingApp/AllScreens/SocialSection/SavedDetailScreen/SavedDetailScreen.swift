//
//  SavedDetailScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 07/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SavedDetailScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var savedCollectionView: UICollectionView!
    @IBOutlet var viewMore: UIView!
    @IBOutlet var viewInnerMore: UIView!
    var collectionData = CollectionModel()

    var imageArray = NSMutableArray()
    
    //MARK:- UIViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //        if #available(iOS 11.0, *) {
        //            savedScrollView.contentInsetAdjustmentBehavior = .never
        //            //print("contentInsetAdjustmentBehavior")
        //        } else {
        //            automaticallyAdjustsScrollViewInsets = false
        //        }
        
        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationBarWithBackButton(strTitle: self.collectionData.strCollectionName.uppercased(), leftbuttonImageName: "back.png")
        
        let rightButton = UIBarButtonItem.init(image: UIImage.init(named: "three_dots"), style: .done, target: self, action: #selector(moreButtonAction(_:)))
        
        self.navigationItem.rightBarButtonItem = rightButton
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- Helper Method
    func initialSetUp()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(updateCollectionsData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialUpdateCollections), object: nil)
        
        let nib = UINib(nibName: "ImageCollectionCell", bundle: nil)
        
        self.savedCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCollectionCell")
//        self.savedScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.savedCollectionView.frame.origin.y + self.savedCollectionView.frame.size.height + 30)
    }

    //MARK:- Notification Observer Methods
    @objc func updateCollectionsData(notiObj: Notification)
    {
        self.collectionData = notiObj.object as! CollectionModel
        self.savedCollectionView.reloadData()
    }

    //MARK:- UICollectionView DataSource And Delegate Methods

    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionData.arrFeeds.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((collectionView.frame.width/3) - 5), height: (collectionView.frame.width/3) - 5)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
        
        let feedData = self.collectionData.arrFeeds.object(at: indexPath.item) as! FeedModel
        if feedData.arrMedia.count > 0
        {
            let feedMediaData = feedData.arrMedia.object(at: 0) as! FeedMediaModel
            
            if feedMediaData.strMediaURL != ""
            {
                if (feedMediaData.strMediaType == kKeySocialMediaTypeImage)
                {
                    let url = URL.init(string: feedMediaData.strMediaURL)
                    cell.feedImageView.sd_addActivityIndicator()
                    cell.feedImageView.sd_showActivityIndicatorView()
                    cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell.feedImageView.sd_removeActivityIndicator()
                    })
                    cell.feedImageView.isHidden = false
                }
                else
                {
                    let url = URL.init(string: feedMediaData.strMediaThumbURL)
                    cell.feedImageView.sd_addActivityIndicator()
                    cell.feedImageView.sd_showActivityIndicatorView()
                    cell.feedImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell.feedImageView.sd_removeActivityIndicator()
                    })
                    cell.feedImageView.isHidden = false
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let feedData = self.collectionData.arrFeeds.object(at: indexPath.item) as! FeedModel
        let feedDetails = FeedDetailVC.init(nibName: "FeedDetailVC", bundle: nil)
        feedDetails.feedData = feedData
        feedDetails.selectedRow = 0
        self.navigationController?.pushViewController(feedDetails, animated: true)
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func editButtonsAction(_ sender: UIButton!)
    {
        viewMore.endEditing(true)
        viewMore.removeFromSuperview()
        let editCollection = EditCollectionScreen.init(nibName: "EditCollectionScreen", bundle: nil)
//        register.strFrom = "SavedDetail"
        editCollection.collectionData = collectionData
        self.navigationController?.pushViewController(editCollection, animated: true)
    }
    @IBAction func addButtonsAction(_ sender: UIButton!)
    {
        viewMore.endEditing(true)
        viewMore.removeFromSuperview()
       let register = CreateCollectionVC.init(nibName: "CreateCollectionVC", bundle: nil)
        register.strFrom = "SavedDetail"
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    @objc func moreButtonAction(_ sender: Any) {
//        let register = CreateCollectionVC.init(nibName: "CreateCollectionVC", bundle: nil)
//        self.navigationController?.pushViewController(register, animated: true)
        
        self.appDelegate.window!.addSubview(viewMore)
        viewMore.frame = self.appDelegate.window!.frame
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideView(rec:)))
        tap.delegate = self
        viewMore.addGestureRecognizer(tap)
    }
    
    @objc func hideView(rec: UITapGestureRecognizer)
    {
        viewMore.endEditing(true)
        rec.view?.removeFromSuperview()
    }
    //MARK:- UIGestureRecognizer Delegate Method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: viewInnerMore))! == true
        {
            return false
        }
        return true
    }
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

