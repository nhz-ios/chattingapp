//
//  WriteFeedVC.swift
//  ChattingApp
//
//  Created by Shreya-ios on 07/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class WriteFeedVC: UIViewController, UINavigationControllerDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var imgSelectedPhoto: UIImageView!
    @IBOutlet weak var ViewBGText: UIView!
    @IBOutlet weak var textViewWrite: UITextView!
    @IBOutlet weak var userCollectionView: UICollectionView!

    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet var toolBar: UIToolbar!
    let fusuma = FusumaViewController()

    var avPlayer: AVPlayer!
    var avPlayerViewController: AVPlayerViewController!
    
    var arrSelectedTaggedPeople = NSMutableArray()
    
    var arrMediaPhotos = [Media]()
    var arrMediaThumbs = [Media]()
    var selectedRow = 0

    //MARK:- UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.appDelegate.arrPostUrls.removeAllObjects()
        self.appDelegate.arrPostthumbUrls.removeAllObjects()
        self.appDelegate.dicFilterNameSave.removeAllObjects()
        self.RemoveDirectoryUrls(self.appDelegate.strVideoFolderPath)
        self.appDelegate.createFolders()

        self.initialSetUp()
        textViewWrite.inputAccessoryView = toolBar

    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationBarWithBackButton(strTitle: "WRITE FEED", leftbuttonImageName: "back.png")
        let rightBarButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonAction(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton

        print(" arrPostUrls",appDelegate.arrPostUrls)
        print(" arrPostthumbUrls",appDelegate.arrPostthumbUrls)
        self.userCollectionView.reloadData()
        if self.avPlayer != nil && self.avPlayerViewController != nil
        {
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                print(error)
            }
            
            self.avPlayer.play()
        }
   }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.appDelegate.TabLogin.tabBar.isHidden = true
    }

    //MARK:- Helepr Method
    func initialSetUp() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTaggedPeopleData(notiObj:)), name: NSNotification.Name(rawValue: kNotificationSocialUpdateTaggedPeople), object: nil)
        

        let nib = UINib(nibName: "FeedMediaCell", bundle: nil)
        self.userCollectionView.register(nib, forCellWithReuseIdentifier: "FeedMediaCell")

        self.userCollectionView.delegate = self
        self.userCollectionView.dataSource = self
        self.userCollectionView.tag = 100

        lblUsername.text = self.appDelegate.currentLoginUser.strName
      if appDelegate.currentLoginUser.strProfilepicImage != ""
        {
            // \(Image_URL)
            self.imgUser.sd_addActivityIndicator()
            self.imgUser.sd_showActivityIndicatorView()
            let url = URL.init(string: appDelegate.currentLoginUser.strProfilepicImage)
            self.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                self.imgUser.sd_removeActivityIndicator()
            })
        }
        else
        {
            self.imgUser.image = UIImage(named:"reg_USER")
        }

    }

    //MARK:- Notification Observer Methods
    @objc func updateTaggedPeopleData(notiObj: Notification)
    {
        self.arrSelectedTaggedPeople = (notiObj.object as! NSMutableArray)
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        textViewWrite.resignFirstResponder()
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        textViewWrite.resignFirstResponder()
    }

    @objc func doneButtonAction(_ sender: Any) {
        
        if self.CheckValidation()
        {
            self.view.endEditing(true)
            self.showActivity(text: "WINDOW")
            self.performSelector(inBackground: #selector(self.PostFeedApi(strMessage:)), with: textViewWrite.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        }
    }
    
   // @IBAction func addPhotoVidoeButtonAction(_ sender: Any) {
       // let post = PostCreateViewController.init(nibName: "PostCreateViewController", bundle: nil)
      //  self.navigationController?.pushViewController(post, animated: true)
        
//    self.fusuma.delegate = self
//
//    fusumaCameraRollTitle = "Library"
//    fusumaCameraTitle = "Photo" // Camera Title
//
//    self.fusuma.hasVideo = true //To allow for video capturing with .library and .camera available by default
//
//    self.fusuma.cropHeightRatio = 1 // Height-to-width ratio. The default value is 1, which means a squared-size photo.
//
//    self.appDelegate.NavigationController.isNavigationBarHidden = true
//    self.appDelegate.NavigationController.present(fusuma, animated: false, completion: {
//
//    })

    //}
    @IBAction func methodTagPeople(_ sender: Any)
    {
        if (self.appDelegate.arrPostthumbUrls.count == 0)
        {
            self.showToastOnViewController(title: kError, message: kMessageSocialSelectPicture, position: kToastTopPosition, controller: self, image: nil)
            return
        }
        
        let taggedPeople = TaggedPeopleScreen.init(nibName: "TaggedPeopleScreen", bundle: nil)
        taggedPeople.arrSelectedTaggedPeople = self.arrSelectedTaggedPeople.mutableCopy() as! NSMutableArray
        self.navigationController?.pushViewController(taggedPeople, animated: true)
    }

    @IBAction func methodOpenCamera(_ sender: Any)
    {
        let post = PostCreateViewController.init(nibName: "PostCreateViewController", bundle: nil)
        self.navigationController?.pushViewController(post, animated: true)
    }
    @objc func methodCrossClicked(_ sender: UIButton)
    {
        self.removeAVPlayer()
        self.appDelegate.arrPostUrls.removeObject(at: sender.tag)
        self.appDelegate.arrPostthumbUrls.removeObject(at: sender.tag)
        self.userCollectionView.reloadData()
    }
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - UICollectionView Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.appDelegate.arrPostUrls.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
            let identifier: String = "FeedMediaCell"
            
            let cell: FeedMediaCell? = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)as? FeedMediaCell
            
            
            let strMediaURL = self.appDelegate.arrPostUrls.object(at: indexPath.item) as? String ?? ""
            
            if strMediaURL != ""
            {
                // \(Image_URL)
                let url = URL.init(fileURLWithPath: strMediaURL)

                if (url.pathExtension == "jpg" || url.pathExtension == "png")
                {
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
                else
                {
                    let strMediaThumbURL = self.appDelegate.arrPostthumbUrls.object(at: indexPath.item) as? String ?? ""
                    let thumbURL = URL.init(fileURLWithPath: strMediaThumbURL)
                    cell?.imgUser.sd_addActivityIndicator()
                    cell?.imgUser.sd_showActivityIndicatorView()
                    cell?.imgUser.sd_setImage(with: thumbURL, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                        cell?.imgUser.sd_removeActivityIndicator()
                    })
                }
            }
            else
            {
                cell?.imgUser.image = UIImage(named:"slider-img-placeholder")
            }
            
            cell?.btnCross.isHidden = false
            cell?.btnCross.tag = indexPath.item
            cell?.btnCross.addTarget(self, action: #selector(methodCrossClicked(_:)), for: .touchUpInside)
            cell?.backgroundColor = UIColor.clear
            cell?.imgUser.contentMode = .scaleAspectFill
            //            let strImage = (appDelegate.LearnerUserInfo.teacherDetails.arrTeacherMedia.object(at: indexPath.item) as! NSDictionary).object(forKey: "image") as! String
            //
            //            let url = URL.init(string: "\(Image_URL)\(strImage)")
            //            cell?.imgUser.sd_addActivityIndicator()
            //
            //            cell?.imgUser.sd_setImage(with: url, placeholderImage: UIImage.init(named: "gallery-img-default"), options: .refreshCached, completed: { (img, error, cacheType, url) in
            //                cell?.imgUser.sd_removeActivityIndicator()
            //            })
            
            return cell!
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        let cell1: FeedMediaCell? = cell as? FeedMediaCell
        
        cell1!.imgUser.isHidden = false
        
        let strMediaURL = self.appDelegate.arrPostUrls.object(at: indexPath.item) as? String ?? ""
        
        if strMediaURL != ""
        {
            // \(Image_URL)
            let url = URL.init(fileURLWithPath: strMediaURL)
            
            if ((url.pathExtension == "jpg" || url.pathExtension == "png") == false)
            {
                if strMediaURL.count > 0
                {
                    if (strMediaURL != "" && self.avPlayer == nil && self.avPlayerViewController == nil) || self.urlOfCurrentlyPlayingInPlayer() != URL(string: strMediaURL)!
                    {
                        self.removeAVPlayer()
                        let urlMainPost = URL.init(fileURLWithPath: strMediaURL)
                        
                        let item = AVPlayerItem(url: urlMainPost as! URL)
                        
                        do {
                            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                        } catch {
                            print(error)
                        }
                        self.avPlayer = AVPlayer(playerItem: item);
                        self.avPlayer.actionAtItemEnd = .none
                        print("SecvideoUrl",urlMainPost)
                        print("item",item)
                        
                        self.avPlayerViewController = AVPlayerViewController()
                        self.avPlayerViewController.player = self.avPlayer
                        self.avPlayerViewController.view.frame = cell1!.imgUser!.frame
                        self.avPlayerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                        
                        //self.avPlayerLayer = AVPlayerLayer(player:self.avPlayer);
                        //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                        //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
                        //self.avPlayerLayer.frame = cell!.imgProfileVideo!.frame
                        //cell?.contentView.layer.addSublayer(self.avPlayerLayer)
                        
                        cell1!.contentView.addSubview(self.avPlayerViewController.view)
                        self.addChildViewController(self.avPlayerViewController)
                        self.avPlayer.play()
                        
                        
                        NotificationCenter.default.addObserver(self,
                                                               selector: #selector(self.playerItemDidReachEnd),
                                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                               object: self.avPlayer.currentItem!)
                        cell1!.avPlayer = self.avPlayer
                        cell1!.avPlayerViewController = self.avPlayerViewController
                        cell1!.imgUser.isHidden = true
                        cell1!.contentView.tag = collectionView.tag
                        cell1!.imgUser.tag = indexPath.item
                        self.avPlayerViewController.view.tag = indexPath.item
                        cell1!.contentView.bringSubview(toFront: cell1!.btnCross)
                    }
                    
                    //                        if self.avPlayer != nil && self.avPlayerViewController != nil && cell?.avPlayer == nil && cell?.avPlayerViewController == nil
                    //                        {
                    //                            cell?.avPlayer = self.avPlayer
                    //                            cell?.avPlayerViewController = self.avPlayerViewController
                    //                            cell?.contentView.addSubview(self.avPlayerViewController.view)
                    //                            self.addChildViewController(self.avPlayerViewController)
                    //                            self.avPlayer.play()
                    //
                    //                        }
                    //                        else
                    //                        {
                    //                            cell?.imgUser.isHidden = false
                    //                        }
                    
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        if self.avPlayer != nil && self.avPlayerViewController != nil && self.avPlayerViewController.view.tag == indexPath.row
        {
            removeAVPlayer()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.OpenFullImage(idx: indexPath.item)
    }
    func urlOfCurrentlyPlayingInPlayer() -> URL?
    {
        return ((self.avPlayer.currentItem?.asset) as? AVURLAsset)?.url
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification)
    {
        self.removeAVPlayer()
    }
    
    func removeAVPlayer()  {
        
        if (self.avPlayer != nil && self.avPlayerViewController != nil)
        {
            if self.avPlayerViewController.view.superview != nil
            {
                if self.avPlayer.isPlaying == true
                {
                    self.avPlayer.pause()
                }
                let idxCllctnPath = IndexPath(row: self.avPlayerViewController?.view.tag ?? 0, section: 0)
                let cell = userCollectionView.cellForItem(at: idxCllctnPath) as? FeedMediaCell
                
                cell?.imgUser.isHidden = false
                cell?.contentView.bringSubview(toFront: cell!.btnCross)

                let strMediaThumbURL = self.appDelegate.arrPostthumbUrls.object(at: idxCllctnPath.item) as? String ?? ""
                let thumbURL = URL.init(fileURLWithPath: strMediaThumbURL)
                cell?.imgUser.sd_addActivityIndicator()
                cell?.imgUser.sd_showActivityIndicatorView()
                cell?.imgUser.sd_setImage(with: thumbURL, placeholderImage: UIImage.init(named: "slider-img-placeholder"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.imgUser.sd_removeActivityIndicator()
                })
            }
            
            self.avPlayer = nil
            self.avPlayerViewController.view.removeFromSuperview()
            self.avPlayerViewController = nil
        }
    }

    
    // MARK: - ScrollingCollectionDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView.tag != 100 && scrollView.tag != 101
        {
            if let customCllctnView = scrollView as? customCollectionView
            {
                if customCllctnView is customCollectionView
                {
                    customCllctnView.pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                    
                    customCllctnView.pageControll.currentPage = Int(customCllctnView.pageNumber)
                }
            }
        }
        
    }
    
    //MARK:- MediaBrowser OpenFullImage Method
    func OpenFullImage(idx: Int)
    {
        var photos = [Media]()
        var thumbs = [Media]()
        var photo: Media!
        var thumb: Media!
        let displaySelectionButtons: Bool = false
        let displayMediaNavigationArrows: Bool = true
        let enableGrid: Bool = false
        let startOnGrid: Bool = false
        let displayActionButton: Bool = false
        let autoPlayOnAppear: Bool = true
        
        
        for io in 0..<self.appDelegate.arrPostUrls.count
        {
            let strMediaURL = self.appDelegate.arrPostUrls.object(at: io) as? String ?? ""
            let strThumbMediaURL = self.appDelegate.arrPostthumbUrls.object(at: io) as? String ?? ""

            if strMediaURL != ""
            {
                // \(Image_URL)
                let thumbURL = URL.init(fileURLWithPath: strThumbMediaURL)
                let mainURL = URL.init(fileURLWithPath: strMediaURL)

                if (mainURL.pathExtension == "jpg" || mainURL.pathExtension == "png")
                {
                    photo = Media(url: mainURL)
                    photo.isVideo = false
                    photos.append((photo! as Media))
                    
                    thumb = Media(url: thumbURL ?? mainURL)
                    thumb.isVideo = false
                    thumbs.append((thumb! as Media))
                }
                else
                {
                    photo = Media(videoURL: mainURL, previewImageURL: thumbURL)
                    photo.isVideo = true
                    photos.append((photo! as Media))
                    
                    thumb = Media(url: thumbURL)
                    thumb.isVideo = true
                    thumbs.append((thumb! as Media))
                }
            }
        }
        self.arrMediaPhotos = photos
        self.arrMediaThumbs = thumbs
        
        if  self.arrMediaPhotos.count > 0
        {
            let browser = MediaBrowser(delegate: self)
            browser.displayActionButton = displayActionButton
            browser.displayMediaNavigationArrows = displayMediaNavigationArrows
            browser.displaySelectionButtons = displaySelectionButtons
            browser.alwaysShowControls = displaySelectionButtons
            browser.zoomPhotosToFill = true
            browser.enableGrid = enableGrid
            browser.startOnGrid = startOnGrid
            browser.enableSwipeToDismiss = true
            browser.autoPlayOnAppear = autoPlayOnAppear
            //            browser.cachingImageCount = 2
            browser.setCurrentIndex(at:idx)
            
            navigationController?.pushViewController(browser, animated: true)
        }
    }
    //MARK: - CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if textViewWrite.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" == "" && self.appDelegate.arrPostUrls.count == 0
        {
            isGo = false
            errorMessage = kMessageSocialEnterFeed
        }
        if !isGo {
            
            self.showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        return isGo
    }
    
//    func getPostFeedApiKeys(strMessage : String, arrImgDataCount: Int, arrVideoCount: Int) -> NSMutableDictionary
//    {
//        let dicrequest = NSMutableDictionary()
//        dicrequest.setObject(strMessage.getEncryptedString() ?? "", forKey: kKeySocialCaption as NSCopying)
//        dicrequest.setObject("\(arrImgDataCount)".getEncryptedString() ?? "", forKey: kKeySocialImageCount as NSCopying)
//        dicrequest.setObject("\(arrVideoCount)".getEncryptedString() ?? "", forKey: kKeySocialVideoCount as NSCopying)
//        dicrequest.setObject(strMessage.getEncryptedString() ?? "", forKey: kKeySocialCaption as NSCopying)
//        return dicrequest
//    }
    func getPostFeedApiKeys(strMessage : String) -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(strMessage.getEncryptedString() ?? "", forKey: kKeySocialCaption as NSCopying)
        dicrequest.setObject("\(self.appDelegate.arrPostUrls.count)".getEncryptedString() ?? "", forKey: kKeySocialMediaCount as NSCopying)
        dicrequest.setObject(strMessage.getEncryptedString() ?? "", forKey: kKeySocialCaption as NSCopying)
        dicrequest.setObject((self.appDelegate.arrPostthumbUrls.count == 0) ? "" : (self.arrSelectedTaggedPeople.componentsJoined(by: ",").getEncryptedString() ?? ""), forKey: kKeySocialTaggedPeople as NSCopying)
        
        return dicrequest
    }    //MARK : - PostFeedApi
    @objc func PostFeedApi(strMessage : String)
    {
//        let arrImgData = NSMutableArray.init()
//        let arrImgKey = NSMutableArray.init(object: kKeySocialMultiImage + "1")
//        let arrVideoData = NSMutableArray.init()
//        let arrVideoKey = NSMutableArray.init(object: kKeySocialMultiVideo + "1")
//        getAllSocialApiResultWithMultipleImagesVideoesPostMethod(strMethodname: kAPISocialAddPost, imgDataArray: arrImgData, strImgKeyArray: arrImgKey, videoDataArray: arrVideoData, strVideoKeyArray: arrVideoKey, Details: dicrequest) { (responseData, error) in
        getSocialPostApiResultMethod(strMethodname: kAPISocialAddPost, mediaThumbArray: self.appDelegate.arrPostthumbUrls, mediaArray: self.appDelegate.arrPostUrls, Details: self.getPostFeedApiKeys(strMessage: strMessage)) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            self.removeAVPlayer()
                            self.appDelegate.arrPostUrls.removeAllObjects()
                            self.appDelegate.arrPostthumbUrls.removeAllObjects()
                            self.appDelegate.dicFilterNameSave.removeAllObjects()
                            self.RemoveDirectoryUrls(self.appDelegate.strVideoFolderPath)
                            self.appDelegate.createFolders()
                            let feedObj = FeedModel.init(dict: dicRes.object(forKey: kData) as! NSDictionary)
                            NotificationCenter.default.post(name: NSNotification.Name.init(kNotificationSocialAddNewFeed), object: feedObj)

                            print("PostFeed User data ****",responseData ?? "nil")
                            self.navigationController?.popToRootViewController(animated: true)
//                            self.appDelegate.saveCurrentUser(dictResult: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                            self.showToastOnViewController(title: kSuccess, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
}
//https://github.com/younatics/MediaBrowser
//MARK: MediaBrowserDelegate
extension WriteFeedVC: MediaBrowserDelegate
{
    func thumbnail(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < arrMediaThumbs.count {
            return arrMediaThumbs[index]
        }
        return  Media(image: UIImage.init(named: "slider-img-placeholder") ?? UIImage(), caption: "")
        
    }
    
    func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < arrMediaPhotos.count {
            return arrMediaPhotos[index]
        }
        return  Media(image: UIImage.init(named: "slider-img-placeholder") ?? UIImage(), caption: "")
    }
    
    func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
        return arrMediaPhotos.count
    }
    
    //    func isMediaSelected(at index: Int, in mediaBrowser: MediaBrowser) -> Bool {
    //        return Chatselections[index] as! Bool
    //
    //    }
    
    func didDisplayMedia(at index: Int, in mediaBrowser: MediaBrowser) {
        print("Did start viewing photo at index \(index)")
        
    }
    
    //    func mediaDid(selected: Bool, at index: Int, in mediaBrowser: MediaBrowser) {
    //        Chatselections[index] = selected
    //    }
}
