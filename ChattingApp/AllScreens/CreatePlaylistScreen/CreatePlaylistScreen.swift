//
//  CreatePlaylistScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class CreatePlaylistScreen: UIViewController {
 
    @IBOutlet var txtName: UITextField!
    @IBOutlet var ToolBar: UIToolbar!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtName .inputAccessoryView = ToolBar

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "CREATE PLAYLIST", leftbuttonImageName: "back.png")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool    
    {
     
        let newLength = textField.text!.count + string.count - range.length

        if (textField == txtName)
        {            
             if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        return true

    }
    // MARK: - UIButtonsMethod
    @IBAction func methodCancelKeyPad(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func methodKeyPadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodCreate(_ sender: Any)   
    {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
