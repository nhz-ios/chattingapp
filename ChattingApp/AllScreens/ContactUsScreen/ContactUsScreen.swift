//
//  ContactUsScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 09/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ContactUsScreen: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate
{
    @IBOutlet var tblContactUs: UITableView!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var btnSubmit:  UIButton!
    @IBOutlet var toolBar: UIToolbar!
    
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblContactUs.tableFooterView = UIView.init(frame: CGRect.zero)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "CONTACT US", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "CONTACT US", leftbuttonImageName: "back.png")
        }
   }
    
    //MARK:- Button Click Method
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func methodSubmitButton(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    

    //MARK:- UITableView Delegate Method
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return viewFooter
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 3
        {
            return 150
        }
        return 82
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "PaymentTableCell"
        var cell : PaymentTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("PaymentTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? PaymentTableCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        cell?.viewDouble.isHidden = true
        cell?.viewSingle.isHidden = true
        cell?.imgSingleName.isHidden = true
        cell?.txtDoubleMonth.isHidden = true
        cell?.txtDoubleYear.isHidden = true
        cell?.txtDoubleFirstName.isHidden = false
        cell?.txtSingleName.inputAccessoryView = toolBar
        cell?.txtDoubleFirstName.inputAccessoryView = toolBar
        cell?.txtDoubleLastName.inputAccessoryView = toolBar
        cell?.txtDoubleMonth.inputAccessoryView = toolBar
        cell?.txtDoubleYear.inputAccessoryView = toolBar
        cell?.txtViewReview.inputAccessoryView = toolBar
        cell?.txtSingleName.delegate = self
        cell?.txtDoubleFirstName.delegate = self
        cell?.txtDoubleLastName.delegate = self
        cell?.txtDoubleMonth.delegate = self
        cell?.txtDoubleYear.delegate = self
        cell?.txtSingleName.text = ""
        cell?.txtViewReview.isHidden = true
        cell?.txtSingleName.isHidden = false

        cell?.txtSingleName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleFirstName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleLastName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleMonth.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleYear.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        
        switch(indexPath.row)
        {
        case 0:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Name"
            cell?.txtSingleName.placeholder = "Enter Name"
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.tag = 0
            break
        case 1:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Email Address"
            cell?.txtSingleName.placeholder = "Enter Email Address"
            cell?.txtSingleName.keyboardType = .emailAddress
            cell?.txtSingleName.tag = 1
            break
        case 2:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Subject"
            cell?.txtSingleName.placeholder = "Enter Subject"
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.tag = 2
            break
        case 3:
            cell?.viewSingle.isHidden = false
            cell?.txtViewReview.isHidden = false
            cell?.txtSingleName.isHidden = true
            cell?.lblSingleName.text = "Message"
            cell?.txtViewReview.placeholder = "Enter Message"
            cell?.txtViewReview.keyboardType = .default
            cell?.txtViewReview.tag = 3
            break
        default:
            break

        }
        return cell!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //        if range.location == 0 && string == " "
        //        {
        //            return false
        //        }
        
        if string == " " && (textField.tag == 1)
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        
        if (textField.tag == 0)
        {
             if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        if (textField.tag == 1)
        {
            return newLength > kEmailLength ? false : true
        }
        if (textField.tag == 2)
        {
             if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        if (textField.tag == 3)
        {
            return newLength > kAddressLength ? false : true
        }
        
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

