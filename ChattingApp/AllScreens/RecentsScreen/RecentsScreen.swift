//
//  RecentsScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 08/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class RecentsScreen: UIViewController, UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var tblMessages:  UITableView!
    var arrMessages = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMessages.tableFooterView = UIView.init(frame: CGRect.zero)
        // Do any additional setup after loading the view.
        
        arrMessages = [["name": "Prisa Den",
                        "message": "Lorem Ipsum is simply dummy text...",
                        "image": "user.png",
                        "time": "15 Apr 2018",
                        "count": "3"], 
                       ["name": "Lisa Group",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "10 Apr 2018",
                        "count": ""],
                       ["name": "George Camp",
                        "message": "Where does it come from?...",
                        "image": "user.png",
                        "time": "15 Mar 2018",
                        "count": ""],
                       ["name": "William Rose",
                        "message": "Heyhowru?...",
                        "image": "user 4.png",
                        "time": "12 Mar 2018",
                        "count": ""],
                       ["name": "Alfred Waney",
                        "message": "Content here, content here...",
                        "image": "video_round.png",
                        "time": "10 Feb 2018",
                        "count": ""],
                       ["name": "Luke Thomas",
                        "message": "Where can I get some...",
                        "image": "user.png",
                        "time": "2 Feb 2018",
                        "count": ""],
                       ["name": "Rose Merry",
                        "message": "Hi...",
                        "image": "user 2.png",
                        "time": "5 Jan 2018",
                        "count": ""]]
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "CHATS", leftbuttonImageName: "menu.png")

        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage.init(named:"social_profile.png")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(methodMyProfile(_:)))
        let menuButton2 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"search.png"), style: .plain, target: self, action: #selector(methodSearch(_:)))
        let menuButton3 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"Call_history.png"), style: .plain, target: self, action: #selector(methodCallHistory(_:)))
//        self.navigationItem.rightBarButtonItems = [menuButton1, menuButton2,menuButton3]
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpace.width = -26.0
        
        let fixedSpace2:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpace2.width = -26.0

        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        self.navigationItem.rightBarButtonItems = [negativeSpace, menuButton1, fixedSpace, menuButton2, fixedSpace2,menuButton3]

        self.appDelegate.TabLogin.tabBar.isHidden = false

    }
    override func viewWillDisappear(_ animated: Bool)
    {
        self.appDelegate.TabLogin.tabBar.isHidden = true
    }

    @objc func methodMyProfile(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = ProfileScreen.init(nibName: "ProfileScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)

    }
    @objc func methodSearch(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let searchUsers = SearchUserScreen.init(nibName: "SearchUserScreen", bundle: nil)
        self.navigationController?.pushViewController(searchUsers, animated: true)
        
    }
    @objc func methodCallHistory(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let register = CallHistoryScreen.init(nibName: "CallHistoryScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)

    }
    
    
    // MARK : UITableViewDelegate
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMessages.count + 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 {
            return 100
        } 
        return 70
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch indexPath.row
        {
            
        case 0:
            let indentifier:String = "FeedUserCell"
            var cell : FeedUserCell? = tableView.dequeueReusableCell(withIdentifier: indentifier) as? FeedUserCell
            
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("FeedUserCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? FeedUserCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
            }
            
            let nib = UINib(nibName: "userCollectionCell", bundle: nil)
            cell?.userCollectionView.register(nib, forCellWithReuseIdentifier: "userCollectionCell")
            cell?.userCollectionView.backgroundColor = .clear
            cell?.userCollectionView.delegate = self
            cell?.userCollectionView.dataSource = self
            
            return cell!
            
        default:
            let cellIdentifier:String = "RecentsCell"
            var cell : RecentsCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? RecentsCell
            if (cell == nil)
            {
                let nib:Array = Bundle.main.loadNibNamed("RecentsCell", owner: nil, options: nil)! as [Any]
                cell = nib[0] as? RecentsCell
                cell!.selectionStyle = UITableViewCellSelectionStyle.none
                cell?.backgroundColor = (UIColor.clear)
                cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
            }
            cell?.lblCount.layer.cornerRadius = (cell?.lblCount.frame.size.height)!/2
            cell?.lblCount.layer.masksToBounds = true
            cell?.imgViewUser.layer.cornerRadius = (cell?.imgViewUser.frame.size.height)!/2
            cell?.imgViewUser.layer.masksToBounds = true
            cell?.lblMessage.sizeToFit()
            
            cell?.imgViewUser.image = UIImage(named: (arrMessages.object(at: indexPath.row - 1) as! NSDictionary).object(forKey: "image") as! String)
            cell?.lblName.text = (arrMessages.object(at: indexPath.row - 1) as! NSDictionary).object(forKey: "name") as! String
            cell?.lblMessage.text = (arrMessages.object(at: indexPath.row - 1) as! NSDictionary).object(forKey: "message") as! String 
            cell?.lblTime.text = (arrMessages.object(at: indexPath.row - 1) as! NSDictionary).object(forKey: "time") as! String
            cell?.lblCount.text = (arrMessages.object(at: indexPath.row - 1) as! NSDictionary).object(forKey: "count") as! String
            //        cell?.lbl_Name.text = (arrTittle.object(at: indexPath.row)) as? String
            //        cell?.img_icon.image = UIImage(named: arrImg.object(at: indexPath.row) as! String)
            
            if ((arrMessages.object(at: indexPath.row - 1) as! NSDictionary).object(forKey: "count") as! NSString).integerValue > 0
            {
                cell?.lblCount.isHidden = false
            }
            else
            {
                cell?.lblCount.isHidden = true
            }
            cell?.imgChatType.isHidden = true
            cell?.imgStatus.isHidden = true
            if (indexPath.row) == 2
            {
                cell?.imgChatType.image = UIImage(named: "groupIcon")
                cell?.imgChatType.isHidden = false
            }
            else if (indexPath.row) == 3
            {
                cell?.imgChatType.image = UIImage(named: "sound_icon")
                cell?.imgChatType.isHidden = false
            }
            else if (indexPath.row) == 5
            {
                cell?.imgChatType.image = UIImage(named: "user_blocked")
                cell?.imgChatType.isHidden = false
                cell?.imgStatus.image = UIImage(named: "read")
                cell?.imgStatus.isHidden = false
            }
            else if (indexPath.row) == 6
            {
                cell?.imgStatus.image = UIImage(named: "unread")
                cell?.imgStatus.isHidden = false
            }
            return cell!
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let chat = ChatScreen.init(nibName: "ChatScreen", bundle: nil)
        if (indexPath.row) == 2 || (indexPath.row) == 3
        {
            chat.isGroupChat = true
        }
        else
        {
            chat.isGroupChat = false
        }
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    
    //MARK:- UICollectionView DataSource And Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for: indexPath) as! userCollectionCell
        cell.addUserButton.isHidden = true
       if indexPath.item == 0 {
            cell.nameLabel.textColor = UIColor.init(red: 5.0/255, green: 152.0/255, blue: 214.0/255, alpha: 1)
            
        } else {
             cell.nameLabel.textColor = UIColor.darkGray
        }
        switch indexPath.row
        {
        case 0:
            cell.nameLabel.text = "All"
        case 1:
            cell.nameLabel.text = "Friends"
        case 2:
            cell.nameLabel.text = "Family"
        default:
            cell.nameLabel.text = "Relative"
        }

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let register = OtherGroupProfileViewController.init(nibName: "OtherGroupProfileViewController", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodMoreClicked(_ sender: UIButton)
    {
    }
    @IBAction func methodNewChatClicked(_ sender: UIButton)
    {
        let chat = ContactsScreen.init(nibName: "ContactsScreen", bundle: nil)
        self.navigationController?.pushViewController(chat, animated: true)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
