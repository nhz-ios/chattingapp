//
//  PhoneVerificationScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class PhoneVerificationScreen: UIViewController, UITextFieldDelegate {

    @IBOutlet var ToolBar: UIToolbar!
    
    @IBOutlet  var txt_first: UITextField!
    @IBOutlet  var txt_Sec: UITextField!
    @IBOutlet  var txt_third: UITextField!
    @IBOutlet  var txt_four: UITextField!
    @IBOutlet  var txt_fifth: UITextField!
    @IBOutlet  var txt_sixth: UITextField!
    
    @IBOutlet  var btnSubmit: UIButton!
    @IBOutlet  var btnResend: UIButton!

    var strOtp = ""
    var strPhone = ""
    var strCountryCode=""
    var strCountryId = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        txt_first .inputAccessoryView = ToolBar
        txt_Sec .inputAccessoryView = ToolBar
        txt_third .inputAccessoryView = ToolBar
        txt_four .inputAccessoryView = ToolBar
        txt_fifth .inputAccessoryView = ToolBar
        txt_sixth .inputAccessoryView = ToolBar
        
        txt_first.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_Sec.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_third.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_four.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_fifth.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_sixth.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.FillOTPTextField()
        // Do any additional setup after loading the view.
    }
    func FillOTPTextField()
    {
        let letters = strOtp.map{ String($0) }
        if letters.count>=6 {
            txt_first.text = letters[0]
            txt_Sec.text = letters[1]
            txt_third.text = letters[2]
            txt_four.text = letters[3]
            txt_fifth.text = letters[4]
            txt_sixth.text = letters[5]
        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "MOBILE VERIFICATION", leftbuttonImageName: "back.png")
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }

    @IBAction func methodSubmit(_ sender: Any)
    {
        if CheckValidation()
        {
            self.view.endEditing(true)
            let register = RegistrationScreen.init(nibName: "RegistrationScreen", bundle: nil)
            register.strCountryCode = strCountryCode
            register.strCountryId = strCountryId
            register.strPhone = strPhone
            self.navigationController?.pushViewController(register, animated: true)
        }
    }

    @IBAction func method_resend(_ sender: Any)
    {
        self.txt_first.text = ""
        self.txt_Sec.text = ""
        self.txt_third.text = ""
        self.txt_four.text = ""
        self.txt_fifth.text = ""
        self.txt_sixth.text = ""
        self.view.endEditing(true)
        self.showActivity(text: "WINDOW")
        self.performSelector(inBackground: #selector(UserCheckApi), with: getPhoneCheckApiKeys())

    }
    @IBAction func method_WrongNumber(_ sender: Any)
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: TextField Delegate
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("textFieldShouldClear",textFieldShouldClear)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        //        if textField.text == ""
        //        {
        //            textField.text = "-"
        //        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        //textField.text = textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        print("string",string)
        print("textField.text ",textField.text!)
        if string == " "
        {
            return false
        }
        
        //       else if string == "" && textField.text != "" && textField.text != " "
        //        {
        //            if  (textField.tag == txt_third.tag)
        //            {
        //                txt_third.text = " "
        //            }
        //            else if  (textField.tag == txt_four.tag)
        //            {
        //                txt_four.text = " "
        //            }
        //            else if (textField.tag == txt_Sec.tag)
        //            {
        //                txt_Sec.text = " "
        //            }
        //            else if (textField.tag == txt_first.tag)
        //            {
        //                txt_first.text = " "
        //            }
        //        }
        //       else  if string != "" && textField.text == " "
        //        {
        //            if  (textField.tag == txt_third.tag)
        //            {
        //                txt_Sec.becomeFirstResponder()
        //            }
        //            else if (textField.tag == txt_Sec.tag)
        //            {
        //                txt_first.becomeFirstResponder()
        //            }
        //            else if (textField.tag == txt_four.tag)
        //            {
        //                txt_third.becomeFirstResponder()
        //            }
        //        }
        /*
         else if string == " " && textField.text != "" && textField.text != " "
         {
         
         }
         */
        
        //        else
        //        {
        //            textField.text = string
        //            textField.text = textField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //
        //        }
        
        //        if (textField.tag == txt_first.tag && txt_first.text?.isEmpty == false) || (textField.tag == txt_third.tag && txt_third.text?.isEmpty == true)
        //        {
        //            txt_Sec.becomeFirstResponder()
        //        }
        //        else if (textField.tag == txt_Sec.tag && txt_Sec.text?.isEmpty == false) || (textField.tag == txt_four.tag && txt_four.text?.isEmpty == true)
        //        {
        //            txt_third.becomeFirstResponder()
        //        }
        //            
        //        else if (textField.tag == txt_third.tag && txt_third.text?.isEmpty == false)
        //        {
        //            txt_four.becomeFirstResponder()
        //        }
        //        else if (textField.tag == txt_Sec.tag && txt_Sec.text?.isEmpty == true)
        //        {
        //            txt_first.becomeFirstResponder()
        //        }
        if string != "" && textField.text == " "
        {
            textField.text = ""
        }
        
        let str = textField.text//.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        guard let text = str 
            else 
        { 
            return true 
            
        }
        let newLength = text.count + string.count - range.length
        if newLength > 1
        {
            let streing = (textField.text! + string).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)             
            if streing.characters.count == 2
            {
                return true
            }
            
            return false
        }
        
        if string == "" && textField.text != "" && textField.text != " "
        {
            if  (textField.tag == txt_sixth.tag)
            {
                txt_sixth.text = " \(txt_sixth.text!)"
            }
            else if  (textField.tag == txt_fifth.tag)
            {
                txt_fifth.text = " \(txt_fifth.text!)"
            }
            else if  (textField.tag == txt_four.tag)
            {
                txt_four.text = " \(txt_four.text!)"
            }
            else if  (textField.tag == txt_third.tag)
            {
                txt_third.text = " \(txt_third.text!)"
            }
            else if (textField.tag == txt_Sec.tag)
            {
                txt_Sec.text = " \(txt_Sec.text!)"
            }
            else if (textField.tag == txt_first.tag)
            {
                txt_first.text = " \(txt_first.text!)"
            }
        }
        /*
         else if string != "" && textField.text != " "
         {
         if  (textField.tag == txt_third.tag)
         {
         txt_four.becomeFirstResponder()
         }
         else if  (textField.tag == txt_third.tag)
         {
         txt_Sec.becomeFirstResponder()
         }
         else if (textField.tag == txt_first.tag)
         {
         txt_Sec.becomeFirstResponder()
         }
         }
         */
        
        
        //        let  char = string.cString(using: String.Encoding.utf8)!
        //        let isBackSpace = strcmp(char, "\\b")
        //        
        //        if (isBackSpace == -92)
        //        {
        //            print("Backspace was pressed")
        //        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        var streing = textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)             

        if streing.characters.count == 2 && streing.contains(" ") == false
        {
            streing.remove(at: streing.startIndex)
            textField.text = streing
        }
        if (textField.tag == txt_first.tag && txt_first.text?.isEmpty == false && txt_first.text != " ")
        {
            txt_Sec.becomeFirstResponder()
        }
        else if (textField.tag == txt_Sec.tag && txt_Sec.text?.isEmpty == false && txt_Sec.text != " ")
        {
            txt_third.becomeFirstResponder()
        }
        else if (textField.tag == txt_Sec.tag) && (txt_Sec.text?.isEmpty == true)
        {
            txt_Sec.text = " "
            txt_first.becomeFirstResponder()
        }
        else if (textField.tag == txt_Sec.tag && txt_Sec.text?.isEmpty == false && txt_Sec.text == " ")
        {
            txt_first.becomeFirstResponder()
        }  
        else if (textField.tag == txt_third.tag && txt_third.text?.isEmpty == false && txt_third.text != " ")
        {
            txt_four.becomeFirstResponder()
        }
        else if (textField.tag == txt_third.tag && txt_third.text?.isEmpty == true)
        {
            txt_third.text = " "
            txt_Sec.becomeFirstResponder()
        }
        else if (textField.tag == txt_third.tag && txt_third.text?.isEmpty == false && txt_third.text == " ")
        {
            txt_Sec.becomeFirstResponder()
        }  
        else if (textField.tag == txt_four.tag && txt_four.text?.isEmpty == false && txt_four.text != " ")
        {
            txt_fifth.becomeFirstResponder()
        }
        else if (textField.tag == txt_four.tag) && (txt_four.text?.isEmpty == true)
        {
            txt_four.text = " "
            txt_third.becomeFirstResponder()
        }
        else if (textField.tag == txt_four.tag && txt_four.text?.isEmpty == false && txt_four.text == " ")
        {
            txt_third.becomeFirstResponder()
        }  
        else if (textField.tag == txt_fifth.tag && txt_fifth.text?.isEmpty == false && txt_fifth.text != " ")
        {
            txt_sixth.becomeFirstResponder()
        }
        else if (textField.tag == txt_fifth.tag) && (txt_fifth.text?.isEmpty == true)
        {
            txt_fifth.text = " "
            txt_four.becomeFirstResponder()
        }
        else if (textField.tag == txt_fifth.tag && txt_fifth.text?.isEmpty == false && txt_fifth.text == " ")
        {
            txt_four.becomeFirstResponder()
        }  
        else if (textField.tag == txt_sixth.tag) && (txt_sixth.text?.isEmpty == true)
        {
            txt_sixth.text = " "
            txt_fifth.becomeFirstResponder()
        }
        else if (textField.tag == txt_sixth.tag && txt_sixth.text?.isEmpty == false && txt_sixth.text == " ")
        {
            txt_fifth.becomeFirstResponder()
        }  
        
    }
    //MARK:- CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if  txt_first.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || txt_Sec.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || txt_third.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || txt_four.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || txt_fifth.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || txt_sixth.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            isGo = false
            errorMessage = kEnterOTPError
        }
        else if "\(txt_first.text!.trimmingCharacters(in: .whitespacesAndNewlines) + txt_Sec.text!.trimmingCharacters(in: .whitespacesAndNewlines) + txt_third.text!.trimmingCharacters(in: .whitespacesAndNewlines) + txt_four.text!.trimmingCharacters(in: .whitespacesAndNewlines) + txt_fifth.text!.trimmingCharacters(in: .whitespacesAndNewlines) + txt_sixth.text!.trimmingCharacters(in: .whitespacesAndNewlines))" != "\(strOtp)"
        {
            isGo = false
            errorMessage = kEnterValidOTPError
       }
        else if appDelegate.isInternetAvailable() == false
        {
            isGo = false
            errorMessage = kInternetErrorMessage
            errorType = kInternetError
        }
        if !isGo {
            showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        
        return isGo
    }
    func getPhoneCheckApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(strPhone, forKey: kMobileNo as NSCopying)
        dicrequest.setObject(strCountryCode, forKey: kCountryCode as NSCopying)
        return dicrequest
    }
    //MARK : - UserCheckApi
    @objc func UserCheckApi(dicreq : NSMutableDictionary)
    {
        UserGetAllApiResultwithPostMethod(strMethodname: kCheckUserUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            self.showToastOnViewController(title: kSuccess, message: "OTP sent successfully", position: kToastTopPosition, controller: self, image: nil)
                            self.strOtp = dicRes.valueForNullableKey(key: kOtp)
                            self.FillOTPTextField()
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                    
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
