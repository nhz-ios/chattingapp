//
//  LoginScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import SwCrypt

class LoginScreen: UIViewController, UITextFieldDelegate, CountryListViewDelegate {
    ////TextField Outlets
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var txtCountryCode: UITextField!
    
    @IBOutlet var scrollingBar: UIScrollView!
    @IBOutlet var toolBar: UIToolbar!
    
    var strCountryId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txtMobileNo.keyboardType = .numberPad
        txtMobileNo.inputAccessoryView = toolBar
        
        if #available(iOS 11.0, *) {
            self.scrollingBar.contentInsetAdjustmentBehavior = .never
            ////print("contentInsetAdjustmentBehavior")
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        DispatchQueue.main.async {
            self.scrollingBar.contentSize = CGSize.init(width: self.scrollingBar.frame.size.width, height: 667)
        }
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    //    override func viewWillAppear(_ animated: Bool)
    //    {
    //        self.title = "CONTACT US"
    //        self.NavigationController.isNavigationBarHidden = false
    //        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Lato-Bold", size: 15)!]
    //        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    //        self.navigationController?.navigationBar.shadowImage = UIImage()
    //        self.navigationController?.navigationBar.isTranslucent = true
    //
    //        let textAttributes = [NSForegroundColorAttributeName:UIColor.white]
    //        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    //
    //        let menuButton : UIBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "selectfuel_menu.png")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(btnSideMenu(_:)))
    //        self.navigationItem.leftBarButtonItem = menuButton
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == 1
        {
            self.view.endEditing(true)
            let MainView = CountryListViewController(nibName: "CountryListViewController",delegate: self)
            self.present(MainView!, animated: true, completion: nil)
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if  string == " "
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        
        if (textField == txtMobileNo)
        {
            return newLength > kPhoneLength ? false : true
        }
        else if (textField == txtPassword)
        {
            return newLength > kPasswordLength ? false : true
        }
        return true
        
    }
    
    // MARK: - UIButtonsMethod
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodLogin(_ sender: Any)
    {
        if CheckValidation()
        {
            self.view.endEditing(true)
            self.showActivity(text: "WINDOW")
            self.performSelector(inBackground: #selector(self.UserloginApi), with: getloginApiKeys())
        }
    }
    @IBAction func methodRegister(_ sender: Any)
    {
        let register = RegisterScreen.init(nibName: "RegisterScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    @IBAction func methodForgotPassword(_ sender: Any)
    {
        let ForgotP = ForgotPassword.init(nibName: "ForgotPassword", bundle: nil)
        self.navigationController?.pushViewController(ForgotP, animated: true)
    }
    
    // MARK: - CountryPhoneCodePicker Delegate
    func didSelectCountry(_ country: [AnyHashable : Any]!)
    {
        let cou = country as NSDictionary
        txtCountryCode.text = cou.valueForNullableKey(key: kCountryCode)
        strCountryId = cou.valueForNullableKey(key: kCountryID)
    }
    
    //MARK: - CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if txtCountryCode.text == "" || txtCountryCode.text == "+00" || txtCountryCode.text == "00"
        {
            isGo = false
            errorMessage = kSelectCountryCodeError
        }
        else if  txtMobileNo.text == ""
        {
            isGo = false
            errorMessage = kEnterMobileNumber
        }
        else if  (txtMobileNo.text?.count)! < 7
        {
            isGo = false
            errorMessage = kMobileNumLengthError
        }
        else if  txtPassword.text == ""
        {
            isGo = false
            errorMessage = kEnterPasswordError
        }
        else if  (txtPassword.text?.count)! < 8
        {
            isGo = false
            errorMessage = kPasswordAcceptableLengthError
        }
        else if appDelegate.isInternetAvailable() == false
        {
            isGo = false
            errorMessage = kInternetErrorMessage
            errorType = kInternetError
        }
        if !isGo {
            
            self.showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        return isGo
    }

    func getloginApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(txtMobileNo.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kMobileNo as NSCopying)
        dicrequest.setObject(txtCountryCode.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kCountryCode as NSCopying)
        dicrequest.setObject(strCountryId, forKey: kCountryID as NSCopying)
        dicrequest.setObject(txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kPassword as NSCopying)
        dicrequest.setObject(kDeviceName, forKey: kDeviceType as NSCopying)
        var strDeviceToken = ""
        if UserDefaults.standard.object(forKey: kDeviceToken) != nil
        {
            strDeviceToken = UserDefaults.standard.object(forKey: kDeviceToken) as! String
        }
        else
        {
            strDeviceToken = "SIMULATOR"
        }
        dicrequest.setObject(strDeviceToken, forKey: kDeviceToken as NSCopying)
        return dicrequest
    }
    //MARK : - Login
    @objc func UserloginApi(dicreq : NSMutableDictionary)
    {
        UserGetAllApiResultwithPostMethod(strMethodname: kLoginUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)

                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            
                            if (dicRes.object(forKey: kServerTokenKey) != nil) && dicRes.object(forKey: kServerTokenKey) as! String != ""
                            {
                                UserDefaults.standard.set(dicRes.object(forKey: kServerTokenKey)! as! String, forKey: kServerTokenKey)
                                UserDefaults.standard.synchronize()
                                print(UserDefaults.standard.object(forKey: kServerTokenKey)!)

                                do {
//                                    let strClientPrivateKey = (UserDefaults.standard.object(forKey: kClientPrivateKey) as? String ?? "")
                                    let strDataClientPrivateKey = (UserDefaults.standard.object(forKey: kDataClientPrivateKey) as? Data ?? Data())

                                    let strEncodedServerPublicKey = (dicRes.object(forKey: kServerPublicKey)! as? String ?? "")

                                    let data = Data(base64Encoded: strEncodedServerPublicKey, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                                    
                                    let (dassta, count) = try CC.RSA.decrypt(data!, derKey: strDataClientPrivateKey, tag: Data(), padding: .pkcs1, digest: .none)
                                    let strDecodedServerPublicKey = String.init(data: dassta, encoding: .utf8)
                                    
                                    UserDefaults.standard.set(dassta, forKey: kDataServerPublicKey)
                                    UserDefaults.standard.set(strDecodedServerPublicKey, forKey: kServerPublicKey)
                                    UserDefaults.standard.synchronize()
                                    print(UserDefaults.standard.object(forKey: kServerPublicKey)!)
                                }
                                catch let error as NSError
                                {
                                    print("error",error)
                                }
                            }
                            self.appDelegate.saveCurrentUser(dictResult: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                            self.appDelegate.currentLoginUser = self.appDelegate.getloginUser()
                            UserDefaults.standard.set("YES", forKey: kLoginCheck)
                            UserDefaults.standard.synchronize()
                            self.appDelegate.showtabbar()
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }

                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)

//                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
}

