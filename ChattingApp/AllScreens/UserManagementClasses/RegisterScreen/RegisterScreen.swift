//
//  RegisterScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class RegisterScreen: UIViewController, UITextFieldDelegate, CountryListViewDelegate {

    @IBOutlet var txtMobileNo: UITextField!
    @IBOutlet var txtCountryCode: UITextField!
    @IBOutlet var toolBar: UIToolbar!
    var strCountryCode = "+00"
    var strCountryId = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        txtMobileNo.keyboardType = .numberPad
        txtMobileNo.inputAccessoryView = toolBar
        txtCountryCode.inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "MOBILE NUMBER", leftbuttonImageName: "back.png")

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == 1
        {
            self.view.endEditing(true)
            let MainView = CountryListViewController(nibName: "CountryListViewController",delegate: self)
            self.present(MainView!, animated: true, completion: nil)
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string == " "
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length

        if (textField == txtMobileNo)
        {
            return newLength > kPhoneLength ? false : true
        }
        return true
    }
    
    // MARK: - UIButtonsMethod
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodSubmit(_ sender: Any)
    {
        if CheckValidation()
        {
            self.view.endEditing(true)
            self.showActivity(text: "WINDOW")
            self.performSelector(inBackground: #selector(UserCheckApi), with: getPhoneCheckApiKeys())
        }
    }
    
    // MARK: - CountryPhoneCodePicker Delegate
    func didSelectCountry(_ country: [AnyHashable : Any]!)
    {
        let cou = country as NSDictionary
        txtCountryCode.text = cou.valueForNullableKey(key: kCountryName).capitalized
        strCountryCode = cou.valueForNullableKey(key: kCountryCode)
        strCountryId = cou.valueForNullableKey(key: kCountryID)
    }
    //MARK: - CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if txtCountryCode.text == ""
        {
            isGo = false
            errorMessage = kEnterCountryError
        }
        else if  txtMobileNo.text == ""
        {
            isGo = false
            errorMessage = kEnterMobileNumber
        }
        else if  (txtMobileNo.text?.count)! < 7
        {
            isGo = false
            errorMessage = kMobileNumLengthError
        }
        else if appDelegate.isInternetAvailable() == false
        {
            isGo = false
            errorMessage = kInternetErrorMessage
            errorType = kInternetError
        }
        if !isGo {
            
            self.showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        return isGo
    }
    
    func getPhoneCheckApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(txtMobileNo.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kMobileNo as NSCopying)
        dicrequest.setObject(strCountryId, forKey: kCountryID as NSCopying)
        dicrequest.setObject(strCountryCode, forKey: kCountryCode as NSCopying)
        return dicrequest
    }
    //MARK : - UserCheckApi
    @objc func UserCheckApi(dicreq : NSMutableDictionary)
    {
        UserGetAllApiResultwithPostMethod(strMethodname: kCheckUserUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            let register = PhoneVerificationScreen.init(nibName: "PhoneVerificationScreen", bundle: nil)
                            register.strOtp = dicRes.valueForNullableKey(key: kOtp)
                            register.strPhone = dicreq.valueForNullableKey(key: kMobileNo)
                            register.strCountryCode = self.strCountryCode
                            register.strCountryId = self.strCountryId
                            self.navigationController?.pushViewController(register, animated: true)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                    
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }

}
