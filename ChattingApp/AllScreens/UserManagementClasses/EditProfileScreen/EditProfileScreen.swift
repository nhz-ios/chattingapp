//
//  EditProfileScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 10/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class EditProfileScreen: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate, CountryListViewDelegate,UITextViewDelegate{
    @IBOutlet var tblEditProfile: TPKeyboardAvoidingTableView!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var btnSubmit:  UIButton!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var imgPhoto : UIImageView!
    @IBOutlet var pickerAllType: UIPickerView!
    @IBOutlet var datePickerView: UIDatePicker!

    
    var strFrom = ""
    var strGender = ""
    var strDOB = ""
    var strCountryCode = ""
    var strCountryId = ""
    var strName = ""
    var strEmail = ""
    var strBio = ""
    var strPhone = ""

    var isSelectedTxtF : Int = 0
    var userImageData = Data()
    /////All Array
    var arrGender = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrGender = ["Male", "Female" , "Other"]

        tblEditProfile.tableFooterView = UIView.init(frame: CGRect.zero)
        
        ////DOB Picker Method
//        datePickerView.maximumDate =  Date()
//        datePickerView.date = Date()
        let now : Date = Date()
        let date : Date = self.logicalOneYearAgo(from: now)
        datePickerView.maximumDate = date
        datePickerView.date = date

        self.setDefaultData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "EDIT PROFILE", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "EDIT PROFILE", leftbuttonImageName: "back.png")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(textViewDidChangedNotification), name: NSNotification.Name.UITextViewTextDidChange, object: nil)

    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextViewTextDidChange, object: nil)
        self.view.endEditing(true)

    }

    //setDefaultData
    func setDefaultData()
    {
        switch appDelegate.currentLoginUser.strGender {
        case "M":
            strGender = "Male"
        case "F":
            strGender = "Female"
        case "O":
            strGender = "Other"
        default:
            strGender = ""
        }
        strCountryCode = appDelegate.currentLoginUser.strCountryCode
        strCountryId = appDelegate.currentLoginUser.strCountryId
        strName = appDelegate.currentLoginUser.strName
        strEmail = appDelegate.currentLoginUser.strEmail
        strBio = appDelegate.currentLoginUser.strBio
        strPhone = appDelegate.currentLoginUser.strphone
        if appDelegate.currentLoginUser.strDob != ""
        {
            let dateFormate = DateFormatter()
            dateFormate.dateFormat = "yyyy/MM/dd"
            let getdate = dateFormate.date(from: appDelegate.currentLoginUser.strDob)
            if getdate != nil
            {
                dateFormate.dateFormat = "MM/dd/yyyy"
                strDOB = dateFormate.string(from: getdate!)
                datePickerView.date = getdate!
            }
        }
        else
        {
            strDOB = appDelegate.currentLoginUser.strDob
        }
        if appDelegate.currentLoginUser.strProfilepicImage != ""
        {
            // \(Image_URL)
            imgPhoto.sd_addActivityIndicator()
            imgPhoto.sd_showActivityIndicatorView()

            let url = URL.init(string: appDelegate.currentLoginUser.strProfilepicImage)
            imgPhoto.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                self.imgPhoto.sd_removeActivityIndicator()
            })
        }
        else
        {
            imgPhoto.image = UIImage(named:"reg_USER")
        }
        imgPhoto.layer.cornerRadius =  imgPhoto.frame.size.width / 2
        imgPhoto.contentMode = .scaleAspectFill
        imgPhoto.layer.masksToBounds = true
    }
    func logicalOneYearAgo(from: Date) -> Date
    {
        let gregorian : NSCalendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))!
        
        var offsetComponents = DateComponents()
        offsetComponents.year = -16
        
        return gregorian.date(byAdding: offsetComponents, to: from as Date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    
    //MARK:- UITableView Delegate Method
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 6
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 120
    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
//    {
//        return viewFooter
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
//    {
//        return 60
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 5
        {
            return 160
        }
        return 82
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "PaymentTableCell"
        var cell : PaymentTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("PaymentTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? PaymentTableCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        cell?.viewDouble.isHidden = true
        cell?.viewSingle.isHidden = true
        cell?.imgSingleName.isHidden = true
        cell?.txtDoubleMonth.isHidden = true
        cell?.txtDoubleYear.isHidden = true
        cell?.txtDoubleFirstName.isHidden = false
        cell?.txtSingleName.inputAccessoryView = toolBar
        cell?.txtDoubleFirstName.inputAccessoryView = toolBar
        cell?.txtDoubleLastName.inputAccessoryView = toolBar
        cell?.txtDoubleMonth.inputAccessoryView = toolBar
        cell?.txtDoubleYear.inputAccessoryView = toolBar
        cell?.txtViewReview.inputAccessoryView = toolBar
        cell?.txtSingleName.delegate = self
        cell?.txtDoubleFirstName.delegate = self
        cell?.txtDoubleLastName.delegate = self
        cell?.txtDoubleMonth.delegate = self
        cell?.txtDoubleYear.delegate = self
        cell?.txtSingleCountryCode.delegate = self
        cell?.txtSinglePhone.delegate = self
        cell?.txtSingleName.text = ""
        cell?.txtViewReview.isHidden = true
        cell?.txtSingleName.isHidden = false
        cell?.viewSinglePhone.isHidden = true
        
        cell?.txtSingleName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleFirstName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleLastName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleMonth.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleYear.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
     
        cell?.txtViewReview.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)

        cell?.txtSingleCountryCode.isUserInteractionEnabled = false
        cell?.txtSinglePhone.isUserInteractionEnabled = false
        switch(indexPath.row)
        {
        case 0:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Name"
            cell?.txtSingleName.placeholder = "Enter Name"
            cell?.txtSingleName.text = strName//"Orlando N Pena"
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.tag = 0
            break
        case 1:
            cell?.viewSingle.isHidden = false
            cell?.txtSingleName.isHidden = true
            cell?.viewSinglePhone.isHidden = false
            cell?.lblSingleName.text = "Phone Number"
            cell?.txtSingleCountryCode.placeholder = "+00"
            cell?.txtSingleCountryCode.text = strCountryCode
            cell?.txtSinglePhone.placeholder = "Enter Phone Number"
            cell?.txtSinglePhone.text = strPhone//"503-743-4251"
            cell?.txtSinglePhone.keyboardType = .numberPad
            cell?.txtSinglePhone.tag = 1
            cell?.txtSingleCountryCode.tag = 11
            break
        case 2:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Email Address"
            cell?.txtSingleName.placeholder = "Enter Email Address"
            cell?.txtSingleName.text = strEmail//"orlando.pena@gmail.com"
            cell?.txtSingleName.keyboardType = .emailAddress
            cell?.txtSingleName.tag = 2
            break
        case 3:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Gender"
            cell?.txtSingleName.placeholder = "Select Gender"
            cell?.txtSingleName.text = strGender
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.inputView = pickerAllType
            cell?.txtSingleName.tag = 3
            cell?.imgSingleName.isHidden = false
            cell?.imgSingleName.image = UIImage(named:"editp_dwon_arrow")
           break
        case 4:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Date of Birth"
            cell?.txtSingleName.placeholder = "Select Date of Birth"
            cell?.txtSingleName.text = strDOB
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.inputView = datePickerView
            cell?.txtSingleName.tag = 4
            cell?.imgSingleName.isHidden = false
            cell?.imgSingleName.image = UIImage(named:"editp_calender")
           break
       case 5:
            cell?.viewSingle.isHidden = false
            cell?.txtViewReview.isHidden = false
            cell?.txtSingleName.isHidden = true
            cell?.lblSingleName.text = "Bio"
            cell?.txtViewReview.placeholder = "Enter Bio"
            cell?.txtViewReview.text = strBio
            cell?.txtViewReview.keyboardType = .default
            cell?.txtViewReview.delegate = self
            cell?.txtViewReview.tag = 5
            break
        default:
            break
            
        }

        cell?.txtSingleName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        return cell!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - UITextFieldDelegate
    @objc func textViewDidChangedNotification(notification:NSNotification)
    {
        let textView = notification.object as! UITextView
        if textView.tag == 5
        {
            strBio = textView.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        self.tblEditProfile.scrollToRow(at: IndexPath.init(row: 5, section: 0), at: .bottom, animated: false)
    }


    @objc  private func textFieldDidChange(_ textField: UITextField)
    {
        let streing = textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if textField.tag == 0
        {
            strName = streing
        }
        else if textField.tag == 2
        {
            strEmail = streing
        }
        
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == 11
        {
            self.view.endEditing(true)
            let MainView = CountryListViewController(nibName: "CountryListViewController",delegate: self)
            self.present(MainView!, animated: true, completion: nil)
            return false
        }
        else if textField.tag == 3
        {
            isSelectedTxtF = 3
            // methodPickerReload()
        }
        else if textField.tag == 4
        {
            isSelectedTxtF = 4
            // methodPickerReload()
        }
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //        if range.location == 0 && string == " "
        //        {
        //            return false
        //        }
        
        if string == " " && (textField.tag != 0)
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        
        if (textField.tag == 0)
        {
            if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        if (textField.tag == 1)
        {
            return newLength > kPhoneLength ? false : true
        }
        if (textField.tag == 2)
        {
            return newLength > kEmailLength ? false : true
        }
        
        return true
    }
    //MARK:- Button Click Method
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
        
        let indexSltd = pickerAllType.selectedRow(inComponent: 0)
        
        if isSelectedTxtF == 3
        {
            strGender = arrGender.object(at: indexSltd) as! String
        }
        else if isSelectedTxtF == 4
        {
            let dateFormateSearch  = DateFormatter()
            dateFormateSearch.dateFormat = "MM/dd/yyyy"
            strDOB = dateFormateSearch.string(from: datePickerView.date) as String!
        }
        tblEditProfile.reloadData()
    }
    
    @IBAction func methodSubmitButton(_ sender: UIButton)
    {
        if CheckValidation()
        {
            self.view.endEditing(true)
            self.showActivity(text: "WINDOW")
            self.performSelector(inBackground: #selector(self.EditProfileApi(dicRequest:)), with: getEditProfileApiKeys())
        }
        
    }
    

    @IBAction func cameraButtonAction(_ sender: UIButton!) {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Open Camera")
            
            
            self.appDelegate.checkCameraPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
                    {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
                        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            })
            

            
        })
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Open Gallery")
            
            self.appDelegate.checkGalleryPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            })
            

        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        var image: UIImage!
        
        // fetch the selected image
        
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        self.userImageData = UIImageJPEGRepresentation(image, 0.75)!
        self.imgPhoto.image = image
    }

    // MARK: - UIPickerView Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        return arrGender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arrGender.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
    }
    
    // MARK: - CountryPhoneCodePicker Delegate
    func didSelectCountry(_ country: [AnyHashable : Any]!)
    {
        let cou = country as NSDictionary
        strCountryCode = cou.valueForNullableKey(key: kCountryCode)
        strCountryId = cou.valueForNullableKey(key: kCountryID)
        tblEditProfile.reloadData()
    }
    
    //MARK: - CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if strName == ""
        {
            isGo = false
            errorMessage = kEnternameError
        }
        else if strCountryCode == "" || strCountryCode == "+00" || strCountryCode == "00"
        {
            isGo = false
            errorMessage = kSelectCountryCodeError
        }
        else if  strPhone == ""
        {
            isGo = false
            errorMessage = kEnterMobileNumber
        }
        else if  (strPhone.count) < 7
        {
            isGo = false
            errorMessage = kMobileNumLengthError
        }
        else if  strEmail == ""
        {
            isGo = false
            errorMessage = kEnterEmailError
        }
        else if (isValidEmail(testStr: strEmail)) == false
        {
            isGo = false
            errorMessage = kEnterVaildEmailError
        }
        else if  strGender == ""
        {
            isGo = false
            errorMessage = kEnterGenderError
        }
        else if  strDOB == ""
        {
            isGo = false
            errorMessage = kEnterDOBError
        }
        else if appDelegate.isInternetAvailable() == false
        {
            isGo = false
            errorMessage = kInternetErrorMessage
            errorType = kInternetError
        }
        if !isGo {
            
            self.showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        return isGo
    }
    
    func getEditProfileApiKeys() -> NSMutableDictionary
    {
        let dicRequest = NSMutableDictionary()
//        dicRequest.setObject(appDelegate.currentLoginUser.strUserId, forKey: kUserID as NSCopying)

        dicRequest.setObject((strName.trimmingCharacters(in: .whitespacesAndNewlines)).getEncryptedString() ?? "", forKey: kName as NSCopying)
//        dicRequest.setObject(strPhone.trimmingCharacters(in: .whitespacesAndNewlines), forKey: kMobileNo as NSCopying)
//        dicRequest.setObject(strCountryCode.trimmingCharacters(in: .whitespacesAndNewlines), forKey: kCountryCode as NSCopying)
//        dicRequest.setObject(strCountryId.trimmingCharacters(in: .whitespacesAndNewlines), forKey: kCountryID as NSCopying)
        dicRequest.setObject((strEmail.trimmingCharacters(in: .whitespacesAndNewlines)).getEncryptedString() ?? "", forKey: kEmail as NSCopying)
        dicRequest.setObject("\(strGender.trimmingCharacters(in: .whitespacesAndNewlines).prefix(1))".getEncryptedString() ?? "" , forKey: kGender as NSCopying)
        dicRequest.setObject(strBio.getEncryptedString() ?? "", forKey: kBio as NSCopying)

        let dateFormateSearch  = DateFormatter()
        dateFormateSearch.dateFormat = "MM/dd/yyyy"
        let dateDob = dateFormateSearch.date(from: (strDOB.trimmingCharacters(in: .whitespacesAndNewlines)))
        dateFormateSearch.dateFormat = "yyyy-MM-dd"
        dicRequest.setObject((dateFormateSearch.string(from: dateDob!)).getEncryptedString() ?? "", forKey: kDOB as NSCopying)
        return dicRequest
    }
    //MARK : - EditProfileApi
    @objc func EditProfileApi(dicRequest: NSMutableDictionary)
    {
        UsergetallApiResultwithimagePostMethod(strMethodname: kEditProfileUrl, imgData: userImageData, strImgKey: kProfilePic, Details: dicRequest) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            self.appDelegate.saveCurrentUser(dictResult: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                            self.appDelegate.currentLoginUser = self.appDelegate.getloginUser()
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateProfile), object: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {

            self.tblEditProfile.scrollToRow(at: IndexPath.init(row: 5, section: 0), at: .bottom, animated: true)
        }
    }
}

