//
//  ForgotPassword.swift
//  ChattingApp
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtEmailAdd: UITextField!
    @IBOutlet var scrollingBar: UIScrollView!
    @IBOutlet var toolBar: UIToolbar!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        txtEmailAdd .inputAccessoryView = toolBar

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "FORGOT PASSWORD", leftbuttonImageName: "back.png")
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string == " "
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        if (textField == txtEmailAdd)
        {
            return newLength > kEmailLength ? false : true
        }
        return true
    }
    
    // MARK: - UIButtonsMethod
    @IBAction func methodCancelKeyPad(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func methodKeyPadDone(_ sender: Any)
    {
        
        self.view.endEditing(true)
    }
    @IBAction func methodSubmit(_ sender: Any)
    {
        if CheckValidation()
        {
            self.view.endEditing(true)
            self.showActivity(text: "WINDOW")
            self.performSelector(inBackground: #selector(self.ForgotPasswordApi), with: getForgotPasswordApiKeyData())
        }
    }
    //MARK:- ForgotPasswordApi
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError

        if  txtEmailAdd.text == ""
        {
            isGo = false
            errorMessage = kEnterEmailError
        }
        else if (isValidEmail(testStr: txtEmailAdd.text!)) == false
        {
            isGo = false
            errorMessage = kEnterVaildEmailError
        }
        else if appDelegate.isInternetAvailable() == false
        {
            isGo = false
            errorMessage = kInternetErrorMessage
            errorType = kInternetError
        }
        if !isGo {
            showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        
        return isGo
    }
    func getForgotPasswordApiKeyData() -> NSMutableDictionary
    {
        let dictReq = NSMutableDictionary()
        dictReq.setValue(txtEmailAdd.text?.trimmingCharacters(in: .whitespacesAndNewlines), forKey: kEmail)
        return dictReq
    }
    //MARK : - ForgotPasswordApi
    @objc func ForgotPasswordApi(dicreq : NSMutableDictionary)
    {
        UserGetAllApiResultwithPostMethod(strMethodname: kForgotPasswordUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                   }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                    
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
