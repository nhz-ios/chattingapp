//
//  RegistrationScreen.swift
//  ChattingApp
//
//  Created by ramprakash on 06/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import SwCrypt

class RegistrationScreen: UIViewController, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate, CountryListViewDelegate,UITextViewDelegate {

    ///Text Field IBoutlets
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmailAdd: UITextField!
    @IBOutlet var txtPhoneNo: UITextField!
    @IBOutlet var txtCountryCode: UITextField!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var txtDOB: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var txtViewBio: SZTextView!
    ///Iboutlet Button & Others
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var scrollingBar: UIScrollView!
    @IBOutlet var pickerAllType: UIPickerView!
    @IBOutlet var datePickerView: UIDatePicker!
    
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var imgUserProfile: UIImageView!
    
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    var imgData = Data()
    var isSelectedTxtF : Int = 0

    /////All Array
    var arrGender = NSArray()
    var strPhone = ""
    var strCountryCode = ""
    var strCountryId = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        imgUserProfile.layer.cornerRadius =  imgUserProfile.frame.size.width / 2
        imgUserProfile.contentMode = .scaleAspectFill
        imgUserProfile.layer.masksToBounds = true
        arrGender = ["Male", "Female", "Other"]
        
        txtCountryCode.text = strCountryCode
        txtPhoneNo.text = strPhone
        
        txtPhoneNo.isUserInteractionEnabled  = false
        txtCountryCode.isUserInteractionEnabled  = false
        
        txtName .inputAccessoryView = toolBar
        txtEmailAdd .inputAccessoryView = toolBar
        txtPhoneNo .inputAccessoryView = toolBar
        txtPassword .inputAccessoryView = toolBar
        txtConfirmPassword .inputAccessoryView = toolBar
        txtViewBio .inputAccessoryView = toolBar

        pickerOpenTextField()
        
        if #available(iOS 11.0, *) {
            self.scrollingBar.contentInsetAdjustmentBehavior = .never
            ////print("contentInsetAdjustmentBehavior")
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        DispatchQueue.main.async {
            self.scrollingBar.contentSize = CGSize.init(width: self.scrollingBar.frame.size.width, height: 880)
        }
        
        ////DOB Picker Method
//        datePickerView.maximumDate = Date()
//        datePickerView.date = Date()
        let now : Date = Date()
        let date : Date = self.logicalOneYearAgo(from: now)
        datePickerView.maximumDate = date
        datePickerView.date = date

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationBarWithBackButton(strTitle: "REGISTER", leftbuttonImageName: "back.png")
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    func logicalOneYearAgo(from: Date) -> Date
    {
        let gregorian : NSCalendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))!
        
        var offsetComponents = DateComponents()
        offsetComponents.year = -16
        
        return gregorian.date(byAdding: offsetComponents, to: from as Date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    func pickerOpenTextField()
    {
        txtGender.inputAccessoryView = toolBar
        txtGender.inputView = pickerAllType
        
        txtPhoneNo.keyboardType = .numberPad
        txtPhoneNo.inputAccessoryView = toolBar
        
        txtDOB.inputAccessoryView = toolBar
        txtDOB.inputView = datePickerView
    }

    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == 1
        {
            self.view.endEditing(true)
            let MainView = CountryListViewController(nibName: "CountryListViewController",delegate: self)
            self.present(MainView!, animated: true, completion: nil)
            return false
        }
        else if textField.tag == 6
        {
            isSelectedTxtF = 6
            // methodPickerReload()
        }
        else if textField.tag == 7
        {
            isSelectedTxtF = 7
            // methodPickerReload()
        }
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //        if range.location == 0 && string == " "
        //        {
        //            return false
        //        }
        
        if string == " " && (textField != txtName)
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        if (textField == txtName)
        {
             if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji"
            {
                return false
            }
            return newLength > kNameLength ? false : true
        }
        if (textField == txtEmailAdd)
        {
            return newLength > kEmailLength ? false : true
        }
        if (textField == txtPhoneNo)
        {
            return newLength > kPhoneLength ? false : true
        }
        if (textField == txtPassword || textField == txtConfirmPassword)
        {
            return newLength > kPasswordLength ? false : true
        }
        return true
    }
    
    // MARK: - UIButtonsMethod
    @IBAction func methodCancelKeyPad(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func methodKeyPadDone(_ sender: Any)
    {
        self.view.endEditing(true)
        
        let indexSltd = pickerAllType.selectedRow(inComponent: 0)
        
        if isSelectedTxtF == 6
        {
            txtGender.text = arrGender.object(at: indexSltd)as? String
        }
        else if isSelectedTxtF == 7
        {
            let dateFormateSearch  = DateFormatter()
            dateFormateSearch.dateFormat = "MM/dd/yyyy"
            txtDOB.text = dateFormateSearch.string(from: datePickerView.date) as String!
        }
    }
    @IBAction func methodRegister(_ sender: Any)
    {
        if CheckValidation()
        {
            self.view.endEditing(true)
            self.showActivity(text: "WINDOW")
            self.performSelector(inBackground: #selector(self.RegistrationApi), with: getRegistrationApiKeys())
        }
    }
    
    // MARK: - UIPickerView Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        return arrGender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arrGender.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
    }
    
    // MARK: - CountryPhoneCodePicker Delegate
    func didSelectCountry(_ country: [AnyHashable : Any]!)
    {
        let cou = country as NSDictionary
        txtCountryCode.text = cou.valueForNullableKey(key: kCountryCode)
        strCountryCode = cou.valueForNullableKey(key: kCountryCode)
        strCountryId = cou.valueForNullableKey(key: kCountryID)
    }
    
    @IBAction func methodOpenCamera(_ sender: Any)
    {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Select Image Option", message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default)
        { void in
            
            self.appDelegate.checkGalleryPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    self.openGallary()
                }
            })
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { void in
            
            self.appDelegate.checkCameraPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    self.openCamera()
                }
            })
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker?.allowsEditing = true
            imagePicker?.delegate = self
            
            self .present(imagePicker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    
    func openGallary()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker!.allowsEditing = true
        imagePicker!.delegate = self
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        var image: UIImage!
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        
        //         imgUserProfile.image = image
        
        imgData = UIImageJPEGRepresentation(image!, 0.75)!
        
        if imgData.count > 0
        {
            imgUserProfile.image = UIImage.init(data: imgData)
        }
//        else
//        {
//            self.imgUserProfile.sd_addActivityIndicator()
//            let url = URL.init(string: "\(Image_URL)\(self.appDelegate.EatAppUserInfo.strProfilePic)")
//            
//            self.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage.init(named: "user_place.png"), options: .refreshCached, completed: { (img, error, cacheType, url) in
//                self.imgUserProfile.sd_removeActivityIndicator()
//            })
//        }
        
        imagePicker!.dismiss(animated: true, completion: nil)
        
    }
    //MARK: - CheckValidation
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if txtName.text == ""
        {
            isGo = false
            errorMessage = kEnternameError
        }
       else if txtCountryCode.text == "" || txtCountryCode.text == "+00" || txtCountryCode.text == "00"
        {
            isGo = false
            errorMessage = kSelectCountryCodeError
        }
        else if  txtPhoneNo.text == ""
        {
            isGo = false
            errorMessage = kEnterMobileNumber
        }
        else if  (txtPhoneNo.text?.count)! < 7
        {
            isGo = false
            errorMessage = kMobileNumLengthError
        }
        else if  txtPassword.text == ""
        {
            isGo = false
            errorMessage = kEnterPasswordError
        }
        else if  (txtPassword.text?.count)! < 8
        {
            isGo = false
            errorMessage = kPasswordAcceptableLengthError
        }
        else if  txtConfirmPassword.text == ""
        {
            isGo = false
            errorMessage = kEnterConfirmPasswordError
        }
        else if  txtPassword.text != txtConfirmPassword.text
        {
            isGo = false
            errorMessage = kPasswordsMatchingError
        }
        else if  txtEmailAdd.text == ""
        {
            isGo = false
            errorMessage = kEnterEmailError
        }
        else if (isValidEmail(testStr: txtEmailAdd.text!)) == false
        {
             isGo = false
            errorMessage = kEnterVaildEmailError
        }
        else if  txtGender.text == ""
        {
            isGo = false
            errorMessage = kEnterGenderError
        }
        else if  txtDOB.text == ""
        {
            isGo = false
            errorMessage = kEnterDOBError
        }
        else if appDelegate.isInternetAvailable() == false
        {
            isGo = false
            errorMessage = kInternetErrorMessage
            errorType = kInternetError
        }
        if !isGo {
            
            self.showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        return isGo
    }
  
    func getRegistrationApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(txtName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kName as NSCopying)
        dicrequest.setObject(txtPhoneNo.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kMobileNo as NSCopying)
        dicrequest.setObject(txtCountryCode.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kCountryCode as NSCopying)
        dicrequest.setObject(strCountryId, forKey: kCountryID as NSCopying)
        dicrequest.setObject(txtEmailAdd.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kEmail as NSCopying)
        dicrequest.setObject(txtGender.text?.trimmingCharacters(in: .whitespacesAndNewlines).prefix(1) ?? "", forKey: kGender as NSCopying)
        dicrequest.setObject(txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kPassword as NSCopying)
        dicrequest.setObject(txtViewBio.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", forKey: kBio as NSCopying)
        dicrequest.setObject(kDeviceName, forKey: kDeviceType as NSCopying)
        var strDeviceToken = ""
        if UserDefaults.standard.object(forKey: kDeviceToken) != nil
        {
            strDeviceToken = UserDefaults.standard.object(forKey: kDeviceToken) as! String
        }
        else
        {
            strDeviceToken = "SIMULATOR"
        }
        dicrequest.setObject(strDeviceToken, forKey: kDeviceToken as NSCopying)

        let dateFormateSearch  = DateFormatter()
        dateFormateSearch.dateFormat = "MM/dd/yyyy"
        let dateDob = dateFormateSearch.date(from: (txtDOB.text!.trimmingCharacters(in: .whitespacesAndNewlines)))
        dateFormateSearch.dateFormat = "yyyy-MM-dd"
        dicrequest.setObject(dateFormateSearch.string(from: dateDob!), forKey: kDOB as NSCopying)

        return dicrequest
    }
    //MARK : - RegistrationApi
    @objc func RegistrationApi(dicreq : NSMutableDictionary)
    {
        UsergetallApiResultwithimagePostMethod(strMethodname: kSignupUrl, imgData: imgData, strImgKey: kProfilePic, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            print("Registration User data ****",responseData ?? "nil")
                            
                            if (dicRes.object(forKey: kServerTokenKey) != nil) && dicRes.object(forKey: kServerTokenKey) as! String != ""
                            {
                                UserDefaults.standard.set(dicRes.object(forKey: kServerTokenKey)! as! String, forKey: kServerTokenKey)
                                UserDefaults.standard.synchronize()
                                print(UserDefaults.standard.object(forKey: kServerTokenKey)!)
                                
                                do {
                                    //                                    let strClientPrivateKey = (UserDefaults.standard.object(forKey: kClientPrivateKey) as? String ?? "")
                                    let strDataClientPrivateKey = (UserDefaults.standard.object(forKey: kDataClientPrivateKey) as? Data ?? Data())
                                    
                                    let strEncodedServerPublicKey = (dicRes.object(forKey: kServerPublicKey)! as? String ?? "")
                                    
                                    let data = Data(base64Encoded: strEncodedServerPublicKey, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                                    
                                    let (dassta, count) = try CC.RSA.decrypt(data!, derKey: strDataClientPrivateKey, tag: Data(), padding: .pkcs1, digest: .none)
                                    let strDecodedServerPublicKey = String.init(data: dassta, encoding: .utf8)
                                    
                                    UserDefaults.standard.set(dassta, forKey: kDataServerPublicKey)
                                    UserDefaults.standard.set(strDecodedServerPublicKey, forKey: kServerPublicKey)
                                    UserDefaults.standard.synchronize()
                                    print(UserDefaults.standard.object(forKey: kServerPublicKey)!)
                                }
                                catch let error as NSError
                                {
                                    print("error",error)
                                }
                            }
                            self.appDelegate.saveCurrentUser(dictResult: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                            self.appDelegate.currentLoginUser = self.appDelegate.getloginUser()
                            UserDefaults.standard.set("YES", forKey: kLoginCheck)
                            UserDefaults.standard.synchronize()
                            self.appDelegate.showtabbar()
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
