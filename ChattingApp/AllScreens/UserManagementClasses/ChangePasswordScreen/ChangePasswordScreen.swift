//
//  ChangePasswordScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 09/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ChangePasswordScreen: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate
{
    @IBOutlet var tblContactUs: UITableView!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var btnUpdate:  UIButton!
    @IBOutlet var toolBar: UIToolbar!
    
    var strFrom = ""
    var strOldPass = ""
    var strNewPass = ""
    var strConfirmPass = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblContactUs.tableFooterView = UIView.init(frame: CGRect.zero)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "CHANGE PASSWORD", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "CHANGE PASSWORD", leftbuttonImageName: "back.png")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    //MARK:- Button Click Method
    @IBAction func methodKeypadCancel(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    @IBAction func methodKeypadDone(_ sender: Any)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func methodUpdateButton(_ sender: UIButton)
    {
        if CheckValidation()
        {
            self.view.endEditing(true)
            self.showActivity(text: "WINDOW")
            self.performSelector(inBackground: #selector(self.ChangePasswordApi), with: getChangePasswordApiKeyData())
        }
    }
    
    
    //MARK:- UITableView Delegate Method
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return viewFooter
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 82
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "PaymentTableCell"
        var cell : PaymentTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PaymentTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("PaymentTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? PaymentTableCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        cell?.viewDouble.isHidden = true
        cell?.viewSingle.isHidden = true
        cell?.imgSingleName.isHidden = true
        cell?.txtDoubleMonth.isHidden = true
        cell?.txtDoubleYear.isHidden = true
        cell?.txtDoubleFirstName.isHidden = false
        cell?.txtSingleName.inputAccessoryView = toolBar
        cell?.txtDoubleFirstName.inputAccessoryView = toolBar
        cell?.txtDoubleLastName.inputAccessoryView = toolBar
        cell?.txtDoubleMonth.inputAccessoryView = toolBar
        cell?.txtDoubleYear.inputAccessoryView = toolBar
        cell?.txtViewReview.inputAccessoryView = toolBar
        cell?.txtSingleName.delegate = self
        cell?.txtDoubleFirstName.delegate = self
        cell?.txtDoubleLastName.delegate = self
        cell?.txtDoubleMonth.delegate = self
        cell?.txtDoubleYear.delegate = self
        cell?.txtSingleName.text = ""
        cell?.txtViewReview.isHidden = true
        cell?.txtSingleName.isHidden = false
        cell?.txtSingleName.isSecureTextEntry = true

        cell?.txtSingleName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleFirstName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleLastName.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleMonth.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        cell?.txtDoubleYear.textColor = UIColor.init(red: 127.0/255.0, green: 139.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        
        switch(indexPath.row)
        {
        case 0:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Current Password"
            cell?.txtSingleName.placeholder = "Type Current Password"
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.tag = 0
            break
        case 1:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "New Password"
            cell?.txtSingleName.placeholder = "Type New Password"
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.tag = 1
            break
        case 2:
            cell?.viewSingle.isHidden = false
            cell?.lblSingleName.text = "Confirm New Password"
            cell?.txtSingleName.placeholder = "Type Confirm New Password"
            cell?.txtSingleName.keyboardType = .default
            cell?.txtSingleName.tag = 2
            break
        default:
            break
            
            
        }
        
        cell?.txtSingleName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        return cell!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //        if range.location == 0 && string == " "
        //        {
        //            return false
        //        }
        
        if string == " "
        {
            return false
        }
        let newLength = textField.text!.count + string.count - range.length
        return newLength > kPasswordLength ? false : true
    }
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        let streing = textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if textField.tag == 0
        {
            strOldPass = streing
        }
        else if textField.tag == 1
        {
            strNewPass = streing
        }
        else if textField.tag == 2
        {
            strConfirmPass = streing
        }
    }
    //MARK:- ChangePasswordApi
    func CheckValidation() -> Bool
    {
        var isGo = true
        var errorMessage = ""
        var errorType = kError
        if strOldPass == ""
        {
            isGo = false
            errorMessage = kEnterCurrentPassError
        }
        else if  (strOldPass.count) < 8
        {
            isGo = false
            errorMessage = kEnterVaildOldPasswordError
        }
        else  if strNewPass == ""
        {
            isGo = false
            errorMessage = kEnterNewPassError
        }
        else  if strConfirmPass == ""
        {
            isGo = false
            errorMessage = kEnterConfirmPassError
        }
        else if  (strNewPass.count) < 8
        {
            isGo = false
            errorMessage = kEnterVaildNewPasswordError
        }
        else  if strNewPass != strConfirmPass
        {
            isGo = false
            errorMessage = kSamePassError
        }
        else if appDelegate.isInternetAvailable() == false
        {
            isGo = false
            errorMessage = kInternetErrorMessage
            errorType = kInternetError
        }
        if !isGo {
            showToastOnViewController(title: errorType, message: errorMessage, position: kToastTopPosition, controller: self, image: nil)
        }
        
        return isGo
    }
    func getChangePasswordApiKeyData() -> NSMutableDictionary
    {
        let dictReq = NSMutableDictionary()
//         dictReq.setValue(appDelegate.currentLoginUser.strUserId, forKey: kUserID)
        dictReq.setValue(strOldPass.getEncryptedString() ?? "", forKey: kCurrentPassword)
        dictReq.setValue(strNewPass.getEncryptedString() ?? "", forKey: kNewPassword)
        return dictReq
    }
    //MARK : - ChangePasswordApi
    @objc func ChangePasswordApi(dicreq : NSMutableDictionary)
    {
        UserGetAllApiResultwithPostMethod(strMethodname: kChangePasswordUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                    
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


