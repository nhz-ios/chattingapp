//
//  SideMenuScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 05/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class SideMenuScreen: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet  var tblMenu:  UITableView!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var lblProfileName: UILabel!
    @IBOutlet var imgPhoto : UIImageView!
    @IBOutlet var lblPhone: UILabel!

    var arrMenu = NSMutableArray()
    var selectedMenu = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 11.0, *) {
            self.tblMenu.contentInsetAdjustmentBehavior = .never
            ////print("contentInsetAdjustmentBehavior")
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        tblMenu.tableFooterView = UIView.init(frame: CGRect.zero)
        
        arrMenu = [["name": "Home",
                             "image": "menu_home",
                             "active_image": "menu_home_active"],
                            ["name": "My Orders",
                             "image": "menu_myorders",
                             "active_image": "menu_myorders_active"],
                            ["name": "Wishlist",
                             "image": "menu_wishlist",
                             "active_image": "menu_wishlist_active"],
                            ["name": "Contacts",
                             "image": "menu_contacts",
                             "active_image": "menu_contacts_active"],
                            ["name": "Calls",
                             "image": "menu_call",
                             "active_image": "menu_call_active"],
                            ["name": "My Wallet",
                             "image": "menu_wallet",
                             "active_image": "menu_wallet_active"],
                            ["name": "Group Chat",
                             "image": "menu_gchat",
                             "active_image": "menu_gchat_active"],
                            ["name": "My Channels",
                             "image": "menu_channel",
                             "active_image": "menu_channel_active"],
                            ["name": "My Playlists",
                             "image": "menu_playlist",
                             "active_image": "menu_playlist_active"],
                            ["name": "My Business Card",
                             "image": "menu_bcard",
                             "active_image": "menu_bcard_active"],
                            ["name": "About Us",
                             "image": "menu_about",
                             "active_image": "menu_about_active"],
                            ["name": "Contact Us",
                             "image": "menu_contact",
                             "active_image": "menu_contact_active"],
                            ["name": "Invite Friends",
                             "image": "menu_invite",
                             "active_image": "menu_invite_active"],
                            ["name": "Settings",
                             "image": "menu_settings",
                             "active_image": "menu_settings_active"],
                            ["name": "Logout",
                             "image": "menu_logout",
                             "active_image": "menu_logout_active"]]

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileUpdateNotification), name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateProfile), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileUpdateNotification), name: NSNotification.Name(rawValue: MFSideMenuStateNotificationEvent), object: nil)
        
        imgPhoto.layer.cornerRadius =  imgPhoto.frame.size.width / 2
        imgPhoto.contentMode = .scaleAspectFill
        imgPhoto.layer.masksToBounds = true
        
         SetdefaultData()

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    func SetdefaultData()
    {
        lblProfileName.text = appDelegate.currentLoginUser.strName
        lblPhone.text = appDelegate.currentLoginUser.strCountryCode + "-" + appDelegate.currentLoginUser.strphone
        if appDelegate.currentLoginUser.strProfilepicImage != ""
        {
            // \(Image_URL)
            imgPhoto.sd_addActivityIndicator()
            imgPhoto.sd_showActivityIndicatorView()
            let url = URL.init(string: appDelegate.currentLoginUser.strProfilepicImage)
            imgPhoto.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                self.imgPhoto.sd_removeActivityIndicator()
            })
        }
        else
        {
            imgPhoto.image = UIImage(named:"reg_USER")
        }
    }
  
    //MARK:- Notification Observer Methods
    @objc func ProfileUpdateNotification(data : NSNotification)
    {
        SetdefaultData()
        
        tblMenu.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func methodMyProfile(_ sender : UIButton)
    {
        selectedMenu = -1
        let loginScreen  =  ProfileScreen(nibName:"ProfileScreen",bundle:nil)
        loginScreen.strFrom = "Menu"
        var navigationController = UINavigationController()
        
        navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
        navigationController.viewControllers = [loginScreen] as [UIViewController]
        self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
            
        })
        tblMenu.reloadData()
   }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMenu.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 150
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "SideMenuTableCell"
        var cell : SideMenuTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SideMenuTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("SideMenuTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? SideMenuTableCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        cell?.btnMenu.isSelected = indexPath.row == selectedMenu ? true : false
        cell?.btnMenu.setTitle("\((arrMenu.object(at: indexPath.row) as! NSDictionary).object(forKey: "name")!)", for: .normal)
        cell?.btnMenu.setImage(UIImage(named: "\((arrMenu.object(at: indexPath.row) as! NSDictionary).object(forKey: "image")!)"), for: .normal)
        cell?.btnMenu.setImage(UIImage(named: "\((arrMenu.object(at: indexPath.row) as! NSDictionary).object(forKey: "active_image")!)"), for: .selected)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if selectedMenu == indexPath.row
        {
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })

            return
        }

        
//            let loginScreen  =  SubMenuListScreen(nibName:"SubMenuListScreen",bundle:nil)
//            loginScreen.strTitle = "\((arrMenu.object(at: indexPath.section) as! NSDictionary).object(forKey: "name")!)" + "'s \(((arrMenu.object(at: indexPath.section) as! NSDictionary).object(forKey: "sub_name") as! NSArray).object(at: indexPath.row - 1))"
//            self.navigationController?.pushViewController(loginScreen, animated: true)
        
        switch indexPath.row {
        case 0:
            selectedMenu = indexPath.row
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            self.appDelegate.showtabbar()
         
            break
            
            
        case 1:
            selectedMenu = indexPath.row
            let loginScreen  =  MyOrdersScreen(nibName:"MyOrdersScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            
            break
        case 2:
            selectedMenu = indexPath.row
            let loginScreen  =  MyWishListScreen(nibName:"MyWishListScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            
            break
        case 3:
            selectedMenu = indexPath.row
            let loginScreen  =  ContactsScreen(nibName:"ContactsScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            
            break
        case 4:
            selectedMenu = indexPath.row
            let loginScreen  =  CallHistoryScreen(nibName:"CallHistoryScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            
            break
        case 5:
            
            selectedMenu = indexPath.row
            let loginScreen  =  MyWalletScreen(nibName:"MyWalletScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            break
        case 6:
            selectedMenu = indexPath.row
            let loginScreen  =  GroupChatsScreen(nibName:"GroupChatsScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()

            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in

            })
            break
        case 7:
//            selectedMenu = indexPath.row
//            let loginScreen  =  ColorThemeScreen(nibName:"ColorThemeScreen",bundle:nil)
//            loginScreen.strFrom = "Menu"
//            var navigationController = UINavigationController()
//
//            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
//            navigationController.viewControllers = [loginScreen] as [UIViewController]
//            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
//
//            })
            break
        case 8:
            selectedMenu = indexPath.row
            let loginScreen  =  MyPlaylistsScreen(nibName:"MyPlaylistsScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            break
        case 9:
            
//            selectedMenu = indexPath.row
//            let loginScreen  =  ChangePasswordScreen(nibName:"ChangePasswordScreen",bundle:nil)
//            loginScreen.strFrom = "Menu"
//            var navigationController = UINavigationController()
//
//            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
//            navigationController.viewControllers = [loginScreen] as [UIViewController]
//            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
//
//            })
           break
        case 10:
            selectedMenu = indexPath.row
            let loginScreen  =  AboutUsScreen(nibName:"AboutUsScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            break
        case 11:
            selectedMenu = indexPath.row
            let loginScreen  =  ContactUsScreen(nibName:"ContactUsScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            break
        case 12:
            selectedMenu = indexPath.row
            let loginScreen  =  InviteFriendsScreen(nibName:"InviteFriendsScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()

            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in

            })
            break
        case 13:
            selectedMenu = indexPath.row
            let loginScreen  =  MainSettingsScreen(nibName:"MainSettingsScreen",bundle:nil)
            loginScreen.strFrom = "Menu"
            var navigationController = UINavigationController()
            
            navigationController = self.menuContainerViewController.centerViewController as! UINavigationController
            navigationController.viewControllers = [loginScreen] as [UIViewController]
            self.menuContainerViewController .setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
                
            })
            break
            
        case 14:
            if appDelegate.isInternetAvailable() == false
            {
                self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                return
            }
            let alertVC = UIAlertController(title: "ChattingApp", message: "Are you sure you want to logout?", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Logout", style: .default, handler:{(action) in
                self.selectedMenu = 0
                self.showActivity(text: "WINDOW")
                self.performSelector(inBackground: #selector(self.UserlogoutApi), with: self.getUserlogoutApiKeyData())
            })
            alertVC.addAction(alertAction)
            let alertActionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertVC.addAction(alertActionCancel)
            self.present(alertVC, animated: true, completion: nil)
            break
        default:
            break
        }
        tblMenu.reloadData()
    }
        //MARK : - UserlogoutApi
    func getUserlogoutApiKeyData() -> NSMutableDictionary
    {
        let dictReq = NSMutableDictionary()
//        dictReq.setValue(appDelegate.currentLoginUser.strUserId, forKey: kUserID)
        dictReq.setObject(kDeviceName, forKey: kDeviceType as NSCopying)
        var strDeviceToken = ""
        if UserDefaults.standard.object(forKey: kDeviceToken) != nil
        {
            strDeviceToken = UserDefaults.standard.object(forKey: kDeviceToken) as! String
        }
        else
        {
            strDeviceToken = "SIMULATOR"
        }
        dictReq.setObject(strDeviceToken, forKey: kDeviceToken as NSCopying)
        return dictReq
    }

    @objc func UserlogoutApi(dicreq : NSMutableDictionary)
    {
        UserGetAllApiResultwithPostMethod(strMethodname: kLogoutUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        self.showToastOnViewController(title: "", message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            self.appDelegate.logoutAndClearDefaults()
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                    
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
