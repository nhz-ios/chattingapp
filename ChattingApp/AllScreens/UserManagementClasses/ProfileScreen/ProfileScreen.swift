//
//  ProfileScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 10/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import SDWebImage
class ProfileScreen: UIViewController, UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    @IBOutlet  var tblProfile:  UITableView!
    @IBOutlet var viewHeader:  UIView!
    @IBOutlet var btnEditProfilePic:  UIButton!
    
    @IBOutlet var lblProfileName: UILabel!
    @IBOutlet var imgPhoto : UIImageView!

    var userImageData : Data!
    
    var strFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetdefaultData()
        
        imgPhoto.layer.cornerRadius =  imgPhoto.frame.size.width / 2
        imgPhoto.contentMode = .scaleAspectFill
        imgPhoto.layer.masksToBounds = true
        
        tblProfile.tableFooterView = UIView.init(frame: CGRect.zero)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileUpdateNotification), name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateProfile), object: nil)
     }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "PROFILE", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "PROFILE", leftbuttonImageName: "back.png")
        }
        let menuButton1 : UIBarButtonItem =  UIBarButtonItem.init(image: UIImage(named:"profile_edit.png"), style: .plain, target: self, action: #selector(methodEditProfileClicked(_:)))
        self.navigationItem.rightBarButtonItems = [menuButton1]
    }

    @objc func methodEditProfileClicked(_ sender: UIButton)
    {
        let register = EditProfileScreen.init(nibName: "EditProfileScreen", bundle: nil)
        self.navigationController?.pushViewController(register, animated: true)
    }
    func SetdefaultData()
    {
        lblProfileName.text = appDelegate.currentLoginUser.strName
        
        if appDelegate.currentLoginUser.strProfilepicImage != ""
        {
            // \(Image_URL)
            imgPhoto.sd_addActivityIndicator()
            imgPhoto.sd_showActivityIndicatorView()
            let url = URL.init(string: appDelegate.currentLoginUser.strProfilepicImage)
            imgPhoto.sd_setImage(with: url, placeholderImage: UIImage.init(named: "reg_USER"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                self.imgPhoto.sd_removeActivityIndicator()
            })
        }
        else
        {
            imgPhoto.image = UIImage(named:"reg_USER")
        }
    }
    @objc func ProfileUpdateNotification(data : NSNotification)
    {
        SetdefaultData()
        tblProfile.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 9
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 180
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.row == 4
        {
            if appDelegate.currentLoginUser.strBio != ""
            {
               return 30
            }
            return 0
        }
        else if indexPath.row == 5
        {
            if appDelegate.currentLoginUser.strBio == ""
            {
                return 0
            }
            let size = Method_HeightCalculation(text: appDelegate.currentLoginUser.strBio, font: UIFont(name: "Lato-Semibold",size:11.0)!, width: ScreenSize.SCREEN_WIDTH - 30)
            return size + 20
//            return 130
        }
        else if indexPath.row == 7
        {
            return 70
        }
        else
        {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "MerchantCompanyProfileCell"
        var cell : MerchantCompanyProfileCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MerchantCompanyProfileCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("MerchantCompanyProfileCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? MerchantCompanyProfileCell
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        if (indexPath.row == 4 || indexPath.row == 8)
        {
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: ScreenSize.SCREEN_WIDTH+5, bottom: 0, right: 0)
            
        }
        else
        {
            cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        }
        cell?.layer.masksToBounds = true
        cell?.lblName.isHidden = false
        cell?.lblDescription.isHidden = false
        cell?.lblCompanyOverview.isHidden = true
        cell?.imgQRCode.isHidden = true
        cell?.lblName.font = UIFont.init(name: "Lato-SemiBold", size: 12.0)
        cell?.lblCompanyOverview.font = UIFont.init(name: "Lato-SemiBold", size: 12.0)
        cell?.lblName.textColor = .black
        cell?.lblCompanyOverview.textColor = .black
        cell?.lblDescription.textColor = .darkGray
        cell?.lblQRCode.isHidden = true

        switch(indexPath.row)
        {
        case 0:
            cell?.lblName.text = "Mobile Number"
            cell?.lblDescription.text = "\(appDelegate.currentLoginUser.strCountryCode)- \(appDelegate.currentLoginUser.strphone)"//"+01- 503-743-4251"
            break
        case 1:
            cell?.lblName.text = "Email Address"
            cell?.lblDescription.text = appDelegate.currentLoginUser.strEmail
            break
        case 2:
            cell?.lblName.text = "Gender"
            switch appDelegate.currentLoginUser.strGender {
            case "M":
                cell?.lblDescription.text = "Male"
            case "F":
                cell?.lblDescription.text = "Female"
            case "O":
                cell?.lblDescription.text = "Other"
            default:
                cell?.lblDescription.text = ""
            }
            break
        case 3:
            cell?.lblName.text = "Date of Birth"
            cell?.lblDescription.text =  changeDobFormate(strDate: appDelegate.currentLoginUser.strDob) //"July 05, 1982"
            break
        case 4:
            cell?.lblName.isHidden = true
            cell?.lblDescription.isHidden = true
            cell?.lblCompanyOverview.isHidden = false
            cell?.lblCompanyOverview.text = "Bio"
            break
        case 5:
            cell?.lblName.isHidden = false
            cell?.lblDescription.isHidden = true
            cell?.lblCompanyOverview.isHidden = true
            
            cell?.lblName.font = UIFont.init(name: "Lato-Regular", size: 12.0)
            cell?.lblName.textColor = .darkGray
            cell?.lblName.text = appDelegate.currentLoginUser.strBio
            break
        case 6:
            cell?.lblName.text = "Saved Address"
            cell?.lblDescription.text = appDelegate.currentLoginUser.strAddress //"3837 Illinois Avenue, Turner, Oregon, 97392"
            break
        case 7:
            cell?.lblQRCode.isHidden = false
            cell?.lblName.isHidden = true
            cell?.lblDescription.isHidden = true
            cell?.lblQRCode.text = "QR Code"
            cell?.imgQRCode.isHidden = false
            if appDelegate.currentLoginUser.strQRcodeUrl != ""
            {
                // \(Image_URL)
                let url = URL.init(string: appDelegate.currentLoginUser.strQRcodeUrl)
                cell?.imgQRCode.sd_addActivityIndicator()
                cell?.imgQRCode.sd_showActivityIndicatorView()
                cell?.imgQRCode.sd_setImage(with: url, placeholderImage: UIImage.init(named: "profile_QR"), options: .refreshCached, completed: { (img, error, cacheType, url) in
                    cell?.imgQRCode.sd_removeActivityIndicator()
                })
            }
            else
            {
                cell?.imgQRCode.image = UIImage(named:"profile_QR")
            }
           break
        case 8:
            cell?.lblName.isHidden = true
            cell?.lblDescription.isHidden = true
            cell?.lblCompanyOverview.isHidden = false
            cell?.lblCompanyOverview.text = "Business Card"
            break
        default:
            break
            
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    @IBAction func cameraButtonAction(_ sender: UIButton!)
    {
        self.view.endEditing(true)
        if appDelegate.isInternetAvailable() == false
        {
            self.showToastOnViewController(title: kInternetError, message: kInternetErrorMessage, position: kToastTopPosition, controller: self, image: nil)
            return
        }
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        // 2
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Open Camera")
            
            self.appDelegate.checkCameraPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
                    {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
                        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            })

            
        })
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Open Gallery")
            
            self.appDelegate.checkGalleryPermission(completionHandler: { (checkBool) in
                if checkBool == true
                {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            })

        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    // MARK: - UIImagePicker Delegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        var image: UIImage!
        
        // fetch the selected image
        
        if picker.allowsEditing
        {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else
        {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        self.userImageData = UIImageJPEGRepresentation(image, 0.75)!
        self.imgPhoto.image = image
        
        
        self.showActivity(text: "WINDOW")
        self.performSelector(inBackground: #selector(self.EditProfileApi), with: getEditProfileApiKeys())

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARk :- changeDobFormate
    func changeDobFormate(strDate:String) -> String
    {
        if strDate == ""
        {
            return strDate
        }
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy/MM/dd"
        let getdate = dateFormate.date(from: strDate)
        if getdate != nil
        {
            dateFormate.dateFormat = "MMMM dd, yyyy"
            let changeDate = dateFormate.string(from: getdate!)
            return changeDate
        }
        return strDate
    }
    func getEditProfileApiKeys() -> NSMutableDictionary
    {
        let dicrequest = NSMutableDictionary()
        dicrequest.setObject(appDelegate.currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kUserID as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strName.getEncryptedString() ?? "", forKey: kName as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strEmail.getEncryptedString() ?? "", forKey: kEmail as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strGender.getEncryptedString() ?? "" , forKey: kGender as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strBio.getEncryptedString() ?? "", forKey: kBio as NSCopying)
        dicrequest.setObject(appDelegate.currentLoginUser.strDob.getEncryptedString() ?? "", forKey: kDOB as NSCopying)
        return dicrequest
    }
    //MARK : - EditProfileApi
    @objc func EditProfileApi(dicreq : NSMutableDictionary)
    {
        var imgdata = Data()
        if userImageData != nil
        {
            imgdata = userImageData
        }
        UsergetallApiResultwithimagePostMethod(strMethodname: kEditProfileUrl, imgData: imgdata, strImgKey: kProfilePic, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            //print("Edit data ****",responseData ?? "nil")
                            self.appDelegate.saveCurrentUser(dictResult: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                            self.appDelegate.currentLoginUser = self.appDelegate.getloginUser()
                            
                            self.userImageData = nil
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateProfile), object: nil)
                            
                            
                            //                            self.profileTableView.reloadData()
                        }
                        else
                        {
                            self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                }
                else
                {
                    self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}





