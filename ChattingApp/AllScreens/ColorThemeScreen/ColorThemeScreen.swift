//
//  ColorThemeScreen.swift
//  ChattingApp
//
//  Created by NINEHERTZ on 09/07/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit

class ColorThemeScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tblThemes:  UITableView!
    @IBOutlet var viewFooter:  UIView!
    @IBOutlet var btnSubmit:  UIButton!
    
    var arrThemes = NSMutableArray()
    var selectedTheme = 0
    var strSelectedTheme = ""
    var strFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblThemes.tableFooterView = UIView.init(frame: CGRect.zero)
        
        arrThemes = ["Dark", "Light"]
        selectedTheme = arrThemes.index(of: strSelectedTheme)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        if (strFrom == "Menu")
        {
            self.navigationBarWithBackButton(strTitle: "COLOR THEME", leftbuttonImageName: "menu.png")
        }
        else
        {
            self.navigationBarWithBackButton(strTitle: "COLOR THEME", leftbuttonImageName: "back.png")
        }
    }
    
    @IBAction func methodSubmitButton(_ sender: UIButton)
    {
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "ThemeChanged"), object: "\(arrThemes.object(at: selectedTheme))")
        self.navigationController?.popViewController(animated: true)
    }
    

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrThemes.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return viewFooter
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 160
    }
    

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier:String = "FilterTableCell"
        var cell : FilterTableCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FilterTableCell
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("FilterTableCell", owner: nil, options: nil)! as [Any]
            cell = nib[0] as? FilterTableCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = (UIColor.clear)
        }
        
        cell?.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        
        //            cell?.lblSubTotal.textColor = UIColor.lightGray
        cell?.btnSelect.isHidden = true
        var font = UIFont.init(name: "Lato-Semibold", size: 15.0)
        
        cell?.btnSelect.isHidden = false
        cell?.lblFilter.text = "\(arrThemes.object(at: indexPath.row))"
        cell?.btnSelect.isSelected = selectedTheme == indexPath.row ? true : false
        font = UIFont.init(name: "Lato-Regular", size: 13.0)
        cell?.lblFilter.font = font
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedTheme = indexPath.row
        
        tblThemes.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}





