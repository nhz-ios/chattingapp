//
//  AppDelegate.swift
//  ChattingApp
//
//  Created by ramprakash on 05/06/18.
//  Copyright © 2018 NineHertzIndia. All rights reserved.
//

import UIKit
import SystemConfiguration
import UserNotifications
import AVFoundation
import Photos

import Foundation
import CoreFoundation
import Security
import SwCrypt
import CryptoSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UINavigationControllerDelegate,  UITabBarDelegate,UITabBarControllerDelegate {

    var NavigationController = UINavigationController()

    var window: UIWindow?

    var MediaData1 : NSData!
    var videoURL : NSURL!
    var isfront : Bool = false
    var isBackFromNext : Bool = false
    var TabLogin : UITabBarController! = nil
    var selectTab:NSInteger = 0
    var currentLoginUser : UserModel!
    @objc var arrCountriesList = NSArray()
    
    //Camera filter
    var strVideoFolderPath = ""
    var arrPostUrls = NSMutableArray()
    var arrPostthumbUrls = NSMutableArray()
    var dicFilterNameSave = NSMutableDictionary()

    var  backgroundTimer = Timer()
    var  backgroundTask : UIBackgroundTaskIdentifier! = nil
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.makeKeyAndVisible()
        
        self.createPublicPrivateKeys()

        UINavigationBar.appearance().barTintColor = UIColor(red: 43.0/255.0, green: 152.0/255.0, blue: 214.0/255.0, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white, NSAttributedStringKey.font: UIFont.init(name: "Lato-Regular", size: 20.0) as Any]
        
        // registerForPushNotifications
//        registerForPushNotifications()


        
//        if UserDefaults.standard.object(forKey: kCurrentUser) != nil && UserDefaults.standard.object(forKey: kLoginCheck) as! String == "Yes"
//        {
//            self.EatAppUserInfo = self.getloginUser()
//            self.showtabbar()
//            self.TabLogin.selectedIndex = 2
//        }
//        else
//        {
        
//        }
        
        if UserDefaults.standard.object(forKey: kLoginCheck) != nil && UserDefaults.standard.object(forKey: kCurrentUser) != nil
        {
            self.currentLoginUser = self.getloginUser()
            self.showtabbar()
        }
        else
        {
            showFirstPage()
        }
        
        
        
        
        // Override point for customization after application launch.
        return true
    }

    func createPublicPrivateKeys()
    {
        do
        {
            if UserDefaults.standard.object(forKey: kClientPrivateKey) == nil && UserDefaults.standard.object(forKey: kClientPublicKey) == nil
            {
                let (privateKey, publicKey) = try! CC.RSA.generateKeyPair(1024)
                let privateKeyPEM = SwKeyConvert.PrivateKey.derToPKCS1PEM(privateKey)
                let publicKeyPEM = SwKeyConvert.PublicKey.derToPKCS8PEM(publicKey)
                
                print("privateKeyPEM",privateKeyPEM)
                print("publicKeyPEM",publicKeyPEM)
                
//                let utf8strprivate = privateKeyPEM.data(using: String.Encoding.utf8)
//                if let base64Encodedprivate = utf8strprivate?.base64EncodedString()
//                {
//                    UserDefaults.standard.set(base64Encodedprivate, forKey: kClientPrivateKey)
//                    UserDefaults.standard.synchronize()
//                }
                UserDefaults.standard.set(privateKey, forKey: kDataClientPrivateKey)
                UserDefaults.standard.set(publicKey, forKey: kDataClientPublicKey)
                UserDefaults.standard.set(privateKeyPEM, forKey: kClientPrivateKey)
                UserDefaults.standard.synchronize()

                let utf8strpublic = publicKeyPEM.data(using: String.Encoding.utf8)
                if let base64Encodedpublic = utf8strpublic?.base64EncodedString()
                {
                    UserDefaults.standard.set(base64Encodedpublic, forKey: kClientPublicKey)
                    UserDefaults.standard.synchronize()
                }
            }
            
            //            self.performSelector(inBackground: #selector(self.authCheckingApi(strPublicKeys:)), with: publicKeyPEM)
        }
        catch let error as NSError
        {
            print("error",error)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        self.backgroundTask = application.beginBackgroundTask(expirationHandler: {
            DispatchQueue.main.async(execute: {
                if self.backgroundTask != nil
                {
                    self.backgroundTimer.invalidate()
                    // self.backgroundTimer = nil
                }
                application.endBackgroundTask(self.backgroundTask)
                self.backgroundTask = UIBackgroundTaskInvalid
            })
        })
        

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        //        print("isBackgroundChatApp",self.isBackgroundChatApp)
        if self.backgroundTask != nil
        {
            self.backgroundTimer.invalidate()
        }
        self.backgroundTask = UIBackgroundTaskInvalid

        self.performSelector(inBackground: #selector(self.getCountriesListApi), with: nil)
        if UserDefaults.standard.object(forKey: kLoginCheck) != nil && UserDefaults.standard.object(forKey: kCurrentUser) != nil
        {
            self.performSelector(inBackground: #selector(self.userProfileApi), with: nil)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        self.TabLogin.tabBar.isHidden = false

    }
    func showFirstPage()  {
        
//        let loginScreen : SideMenuScreen =  SideMenuScreen(nibName:"SideMenuScreen",bundle:nil)
        let loginScreen : LoginScreen =  LoginScreen(nibName:"LoginScreen",bundle:nil)
        NavigationController = UINavigationController(rootViewController: loginScreen)
        NavigationController.isNavigationBarHidden = false
        self.window?.rootViewController = NavigationController
        
        
//        let request = NSMutableURLRequest(url: NSURL(string: "http://employeetracker.us/cookin/services/ws-booking.php?type=CHECKBOOKINGPAYMENT&data=%255B%257B%2522userid%2522%253A%252244%2522%257D%255D")! as URL,
//                                          cachePolicy: .useProtocolCachePolicy,
//                                          timeoutInterval: 10.0)
//        request.httpMethod = "GET"
////        request.allHTTPHeaderFields = headers
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//
//        let session = URLSession.shared
//        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//            if (error != nil) {
//
//                print(data)
//                // let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : Any]
//
//
//                print(error)
//            } else {
//                let httpResponse = response as? HTTPURLResponse
//                print(httpResponse)
//                var backToString = String(data:  data!, encoding: String.Encoding.utf8) as String!
//
//                print(backToString)
//                let jsonData = (backToString!.data(using: String.Encoding.utf8)!) as Data
//                do {
//                    let dicRes = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSDictionary
//                    print("dicRes",dicRes)
//                }
//                catch let error as NSError {
//                }
//            }
//        })
//
//        dataTask.resume()

    }
    
    //MARK - TabBar
    func showtabbar()
    {
        let recentsScreen : RecentsScreen =  RecentsScreen(nibName:"RecentsScreen",bundle:nil)
        
        let NearByFeeds = FeedsVC(nibName: "FeedsVC", bundle:nil)
        
        let Ecommerce = EcomHomeScreen(nibName: "EcomHomeScreen", bundle:nil)
        
        let ChatInbox = FeedsVC(nibName: "FeedsVC", bundle:nil)
        
        TabLogin = UITabBarController()
        
        let nav_Home = UINavigationController(rootViewController: recentsScreen)
        nav_Home.isNavigationBarHidden = true
        
        let nav_Features = UINavigationController(rootViewController: NearByFeeds)
        nav_Features.isNavigationBarHidden = true
        
        let nav_Ecom = UINavigationController(rootViewController: Ecommerce)
        nav_Ecom.isNavigationBarHidden = true
        
        let nav_MyAccounts = UINavigationController(rootViewController: ChatInbox)
        nav_MyAccounts.isNavigationBarHidden = true
        
        
        let count = [nav_Home,nav_Features,nav_Ecom,nav_MyAccounts]
        
        TabLogin.viewControllers = count
        TabLogin.delegate = self
        
        let  tabItem1 : UITabBarItem = self.TabLogin.tabBar.items![0]
        tabItem1.image = UIImage (named: "chat_icn")?.withRenderingMode(.alwaysOriginal)
        tabItem1.selectedImage = UIImage (named: "chat_icon_active")?.withRenderingMode(.alwaysOriginal)
        tabItem1.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        tabItem1.title = "Chats"
        
        let  tabItem2 : UITabBarItem = self.TabLogin.tabBar.items![1]
        tabItem2.image = UIImage (named: "feed_icon")?.withRenderingMode(.alwaysOriginal)
        tabItem2.selectedImage = UIImage (named: "feed_active")?.withRenderingMode(.alwaysOriginal)
        tabItem2.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        tabItem2.title = "Feeds"
        
        let  tabItem3 : UITabBarItem = self.TabLogin.tabBar.items![2]
        tabItem3.image = UIImage (named: "shopping")?.withRenderingMode(.alwaysOriginal)
        tabItem3.selectedImage = UIImage (named: "camera_post.png")?.withRenderingMode(.alwaysOriginal)
        tabItem3.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        tabItem3.title = "Shopping"
        
        let  tabItem4 : UITabBarItem = self.TabLogin.tabBar.items![3]
        tabItem4.image = UIImage (named: "miniprograms_icon")?.withRenderingMode(.alwaysOriginal)
        tabItem4.selectedImage = UIImage (named: "recent_on.png")?.withRenderingMode(.alwaysOriginal)
        tabItem4.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        tabItem4.title = "Mini Programs"
        
        
        
        UITabBar.appearance().layer.borderWidth = 0.0
        //TabLogin.tabBar.shadowImage = UIImage(named: "dark_header")
        // TabLogin.tabBar.setValue((true), forKeyPath: "_hidesShadow")
        UITabBar.appearance().backgroundColor = UIColor.white
        UITabBar.appearance().barTintColor = UIColor.init(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
        UITabBar.appearance().barTintColor = UIColor.white

        NavigationController = UINavigationController(rootViewController: TabLogin)
        NavigationController.isNavigationBarHidden = true
//        self.window?.rootViewController = NavigationController
        
        self.window?.makeKeyAndVisible()
        TabLogin.selectedIndex = 0
        
        
        let LeftMenuViewController:SideMenuScreen = SideMenuScreen(nibName:"SideMenuScreen",bundle: nil)
        let container: MFSideMenuContainerViewController = MFSideMenuContainerViewController .container(withCenter: NavigationController, leftMenuViewController: LeftMenuViewController, rightMenuViewController: nil)
        container.panMode = MFSideMenuPanModeNone
        container.leftMenuWidth = UIScreen.main.bounds.size.width-70
        
        container.shadow.enabled = true
        
        self.window!.rootViewController = container
    }
    //MARK : - userProfileApi
    @objc func userProfileApi()
    {
        let dicreq = NSMutableDictionary()
        dicreq.setObject(currentLoginUser.strUserId.getEncryptedString() ?? "", forKey: kUserID as NSCopying)
        UserGetAllApiResultwithPostMethod(strMethodname: kgetProfileUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
//                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            self.saveCurrentUser(dictResult: dicRes.object(forKey: kUserSavedDetails) as! NSDictionary)
                            self.currentLoginUser = self.getloginUser()
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNotificationUserManagementUpdateProfile), object: nil)

                        }
                        else if dicRes.object(forKey: kMsg) != nil && dicRes.object(forKey: kMsg) is String == true && dicRes.valueForNullableKey(key: kMsg) == kAppLogoutUnauthorizedMessage
                        {
                            self.logoutAndClearDefaults()
                        }
                        else
                        {
                            //self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else if (responseData != nil) && responseData?.object(forKey: kMsg) != nil && responseData?.object(forKey: kMsg) is String == true && responseData?.valueForNullableKey(key: kMsg) == kAppLogoutUnauthorizedMessage
                    {
                        self.logoutAndClearDefaults()
                    }

                    else
                    {
                        //s//elf.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                    
                }
                else
                {
                    //self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
    
    //MARK : - getCountriesListApi
    @objc func getCountriesListApi()
    {
        let dicreq = NSMutableDictionary()
//        dicreq.setObject(currentLoginUser.strUserId, forKey: kUserID as NSCopying)
        UserGetAllApiResultwithGetMethod(strMethodname: kCountriesListUrl, Details: dicreq) { (responseData, error) in
            DispatchQueue.main.async {
                //                self.hideWindowActivity()
                if error == nil
                {
                    if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
                    {
                        let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
                        
                        if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
                        {
                            // user_details
                            
                            self.arrCountriesList = dicRes.object(forKey: kData) as! NSArray
                        }
                        else
                        {
                            //self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
                        }
                    }
                    else
                    {
                        //s//elf.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    }
                    
                }
                else
                {
                    //self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
                    
                    //                    print("Api Fail ********",(error as! NSError).localizedDescription)
                    
                }
            }
        }
    }
//    //MARK : - authCheckingApi
//    @objc func authCheckingApi(strPublicKeys: String)
//    {
//        let utf8str = strPublicKeys.data(using: String.Encoding.utf8)
//
//        if let base64Encoded = utf8str?.base64EncodedString()
//        {
//            let dicreq = NSMutableDictionary()
//            dicreq.setObject(base64Encoded, forKey: kClientPublicKey as NSCopying)
//            UserGetAllApiResultwithPostMethod(strMethodname: "auth-checking.json", Details: dicreq) { (responseData, error) in
//                DispatchQueue.main.async {
//                    //                self.hideWindowActivity()
//                    if error == nil
//                    {
//                        if (responseData != nil) && responseData?.object(forKey: kResponse) != nil && (responseData?.object(forKey: kResponse)  is NSDictionary) && (responseData?.object(forKey: kResponse) as! NSDictionary).count > 0
//                        {
//                            let dicRes = (responseData?.object(forKey: kResponse) as! NSDictionary)
//                            
//                            if (dicRes.object(forKey: kSuccess) != nil && dicRes.object(forKey: kSuccess) is Bool &&  dicRes.object(forKey: kSuccess) as! Bool) == true
//                            {
//                                // user_details
//                                let dicData = dicRes.object(forKey: "data") as! NSDictionary
//                                let dicServerPublicKey = dicData.object(forKey: "serverPublicKey") as! NSDictionary
//                                let dicServerPrivateKey = dicData.object(forKey: "serverPrivateKey") as! NSDictionary
//                                
//                                let arrServerPublicKey = dicServerPublicKey.allKeys
//                                let arrServerPrivateKey = dicServerPrivateKey.allKeys
//                                
//                                var strServerPublicKey = ""
//                                var strServerPrivateKey = ""
//
//                                for  i in 0..<arrServerPublicKey.count
//                                {
//                                    do {
//                                        let base64Encoded = dicServerPublicKey.valueForNullableKey(key: "str_" + "\(i)")
//                                        
//                                        let data = Data(base64Encoded: base64Encoded, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
//                                        
//                                        let (dassta, count) = try CC.RSA.decrypt(data!, derKey: self.privateKey, tag: Data(), padding: .pkcs1, digest: .none)
//                                        let str = String.init(data: dassta, encoding: .utf8)
//                                        strServerPublicKey = strServerPublicKey + str!
//                                    }
//                                    catch let error as NSError
//                                    {
//                                        print("error",error)
//                                    }
//                                }
//                                for i in 0..<arrServerPrivateKey.count
//                                {
//                                    do {
//                                        let base64Encoded = dicServerPrivateKey.valueForNullableKey(key: "str_" + "\(i)")
//                                        
//                                        let data = Data(base64Encoded: base64Encoded, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
//                                        
//                                        let (dassta, count) = try CC.RSA.decrypt(data!, derKey: self.privateKey, tag: Data(), padding: .pkcs1, digest: .none)
//                                        let str = String.init(data: dassta, encoding: .utf8)
//                                        strServerPrivateKey = strServerPrivateKey + str!
//                                    }
//                                    catch let error as NSError
//                                    {
//                                        print("error",error)
//                                    }
//                                }
//                                print("strServerPublicKey ****",strServerPublicKey)
//                                print("strServerPrivateKey ****",strServerPrivateKey)
//                            }
//                            else
//                            {
//                                //self.showToastOnViewController(title: kError, message: dicRes.valueForNullableKey(key : kMsg), position: kToastTopPosition, controller: self, image: nil)
//                            }
//                        }
//                        else
//                        {
//                            //s//elf.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
//                        }
//                        
//                    }
//                    else
//                    {
//                        //self.showToastOnViewController(title: kError, message: kOtherIssueErrorMessage, position: kToastTopPosition, controller: self, image: nil)
//                        
//                        //                    print("Api Fail ********",(error as! NSError).localizedDescription)
//                        
//                    }
//                }
//            }
//        }
//        
//    }
    
    // MARK: Check Internet Connection
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    func checkCameraPermission(completionHandler:@escaping (Bool) -> ())
    {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            completionHandler(true)
            // Already Authorized
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    completionHandler(true)
                    // User granted
                } else {
                    //                    self.TabLogin.selectedIndex = self.selectTab
                    
                    completionHandler(false)
                    let alert:UIAlertController=UIAlertController(title: "Permissions", message: "App doesn't have camera access permissions. Please go to settings and allow \(kAppName) for camera access permissions.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    }
                    alert.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
                    {
                        UIAlertAction in
                        
                    }
                    // Add the actions
                    alert.addAction(cancelAction)
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    // User Rejected
                }
            })
        }
    }
    
    func checkGalleryPermission(completionHandler:@escaping (Bool) -> ())
    {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        // check the status for PHAuthorizationStatusAuthorized or ALAuthorizationStatusDenied e.g
        if status == PHAuthorizationStatus.notDetermined
        {
            PHPhotoLibrary.requestAuthorization({ (statusinner) in
                if statusinner != PHAuthorizationStatus.authorized {
                    //show alert for asking the user to give permission
                    completionHandler(false)
                    let alert:UIAlertController=UIAlertController(title: "Permissions", message: "App doesn't have gallery access permissions. Please go to settings and allow \(kAppName) for gallery access permissions.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    }
                    alert.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
                    {
                        UIAlertAction in
                    }
                    // Add the actions
                    alert.addAction(cancelAction)
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
                else
                {
                    completionHandler(true)
                }
            })
        }
        else if status != PHAuthorizationStatus.authorized {
            //show alert for asking the user to give permission
            completionHandler(false)
            let alert:UIAlertController=UIAlertController(title: "Permissions", message: "App doesn't have gallery access permissions. Please go to settings and allow \(kAppName) for gallery access permissions.", preferredStyle: UIAlertControllerStyle.alert)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            alert.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            // Add the actions
            alert.addAction(cancelAction)
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        else
        {
            completionHandler(true)
        }
        
    }
    /*
    //MARK: Notification Method
    func registerForPushNotifications()
    {
        if #available(iOS 10.0, *)
        {
            UNUserNotificationCenter.current().delegate = self
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                
                if (granted)
                {
                    DispatchQueue.main.async
                        {
                            UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                }
                else
                {
                    // Do stuff if unsuccessful...
                    UserDefaults.standard.set("SIMULATOR", forKey: kDeviceToken)
                    UserDefaults.standard.synchronize()
                }
            })
        }
            
        else
        {
            //If user is not on iOS 10 use the old methods we've been using
            
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil);
            UIApplication.shared.registerUserNotificationSettings(setting);
            UIApplication.shared.registerForRemoteNotifications();
            
        }

        
    }
    
    @available(iOS 10.0, *)
    private func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        //Handle the notification
    }
    
    @available(iOS 10.0, *)
    private func userNotificationCenter(center: UNUserNotificationCenter, didReceiveNotificationResponse response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void)
    {
        let dicUserInfo = response.notification.request.content.userInfo as NSDictionary
        
        if dicUserInfo.allKeys.count > 0
        {
            print("Notification Data",dicUserInfo)
        }
        
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification)
    {
        
        
    }
    //MARK: - didReceiveRemoteNotification
    func application(_ application: UIApplication,didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        
    }
    //MARK: - Register device for device token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        UserDefaults.standard.set(token, forKey: kDeviceToken)
        print("kDeviceToken",token)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Failed to register:", error)
        
        UserDefaults.standard.set("SIMULATOR", forKey: kDeviceToken)
        UserDefaults.standard.synchronize()
        
    }
    */
    
    ////MARK : saveCurrentUser Methods
    
    func saveCurrentUser(dictResult : NSDictionary)  {
        
        let userFeed = UserModel.init(dict: dictResult)
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userFeed)
        userDefaults.set(encodedData, forKey: kCurrentUser)
        userDefaults.synchronize()
    }
    
    func getloginUser() -> UserModel
    {
        let userDefaults = UserDefaults.standard
        let decoded  = userDefaults.object(forKey: kCurrentUser) as! Data
        let fUser : UserModel = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserModel
        return fUser
    }
    //MARK :- logoutAndClearDefaults
    func logoutAndClearDefaults()
    {
        print("**++//+/+/+")
        self.showFirstPage()
        self.currentLoginUser = nil
        UserDefaults.standard.removeObject(forKey: kCurrentUser)
        UserDefaults.standard.removeObject(forKey: kLoginCheck)
        UserDefaults.standard.synchronize()
    }
    //VideocreateFolders
    func createFolders()
    {
        var path  = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        path = (path as NSString).appending("/")
        strVideoFolderPath = URL.init(string: path)!.appendingPathComponent("Video").absoluteString
        
        
        
        if !FileManager.default.fileExists(atPath: strVideoFolderPath) {
            try? FileManager.default.createDirectory(atPath: strVideoFolderPath, withIntermediateDirectories: false, attributes: nil)
        }
    }
}
extension NSDictionary
{
    func valueForNullableKey(key : String) -> String
    {
        if self.value(forKey: key) == nil
        {
            return ""
        }
        if self.value(forKey: key) is NSNull
        {
            return ""
        }
        if self.value(forKey: key) is NSNumber
        {
            return String(describing: self.value(forKey: key) as! NSNumber)
        }
        else if self.value(forKey: key) is String
        {
            return self.value(forKey: key) as! String
        }
        else
        {
            return ""
        }
    }
    
    func decryptedValueForKey(key : String) -> String
    {
        var strDecodedValue = ""
        if self.value(forKey: key) == nil
        {
            strDecodedValue = ""
        }
        if self.value(forKey: key) is NSNull
        {
            strDecodedValue = ""
        }
        if self.value(forKey: key) is NSNumber
        {
            strDecodedValue = String(describing: self.value(forKey: key) as! NSNumber)
        }
        else if self.value(forKey: key) is String
        {
            strDecodedValue = self.value(forKey: key) as! String
        }
        else
        {
            strDecodedValue = ""
        }
//        print("*****strDecodedValue", key,strDecodedValue)
        
        
//        do {
//            //                                    let strClientPrivateKey = (UserDefaults.standard.object(forKey: kClientPrivateKey) as? String ?? "")
//            let strDataServerPublicKey = (UserDefaults.standard.object(forKey: kDataServerPublicKey) as? Data ?? Data())
//
//            let data = Data(base64Encoded: strDecodedValue, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
//
//
////            let (dassta, count) = try CC.RSA.decrypt(data!, derKey: strDataServerPublicKey, tag: Data(), padding: .pkcs1, digest: .none)
//
//            let dassta = try CC.crypt(.decrypt, blockMode: .ecb, algorithm: .blowfish, padding: .pkcs7Padding, data: data!, key: strDataServerPublicKey, iv: Data())
//
//            strDecodedValue = String.init(data: dassta, encoding: .utf8)!
//
//            print("*****strDecodedValue", key, strDecodedValue)
//        }
//        catch let error as NSError
//        {
//            print("error",error)
//        }
        strDecodedValue = strDecodedValue.getDecryptedString() ?? ""

        return strDecodedValue
    }
}

extension String {
    
    func getEncryptedString() -> String? {
        
        do {
            let strDataServerPublicKey = (UserDefaults.standard.object(forKey: kDataServerPublicKey) as? Data ?? Data())
            
//            let data = Data(base64Encoded: self.base64Encoded() ?? "", options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
//            let data = (self.base64Encoded() ?? "").data(using: .utf8)
//            var mutableValue = self
//            let data = withUnsafeBytes(of: &mutableValue) { Data.init($0) }
            var decodedBytes: Data = self.data(using: .utf8) as! Data
            if (decodedBytes.count % 8 != 0){ //not a multiple of 8
                print("decodedBytes is not padded properly in 8 bits");
                //create a new array with a size which is a multiple of 8
                var ddBytes =  NSMutableData.init(data: decodedBytes)
                let ptr = UnsafeMutablePointer<UInt8>.allocate(capacity: 8 - (decodedBytes.count % 8))
                ddBytes.append(ptr, length: 8 - (decodedBytes.count % 8))
                decodedBytes = ddBytes as Data
            }
            let iv = CC.generateRandom(8)

            let dassta = try CC.crypt(.encrypt, blockMode: .ecb, algorithm: .blowfish, padding: .noPadding, data: decodedBytes, key: strDataServerPublicKey, iv: iv)

            var strEncodedValue = String.init(data: dassta, encoding: .isoLatin1) ?? ""
            
            let daxssta = strEncodedValue.data(using: .isoLatin1)
            strEncodedValue = daxssta!.base64EncodedString()
//            print("*****strEncodedValue", strEncodedValue)

           return strEncodedValue
        }
        catch let error as NSError
        {
            print("error",error)
            return nil
        }
    }
    
    func getDecryptedString() -> String? {
        
        do {
            let strDataServerPublicKey = (UserDefaults.standard.object(forKey: kDataServerPublicKey) as? Data ?? Data())
            
            let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
            
            let iv = CC.generateRandom(8)

            let dassta = try CC.crypt(.decrypt, blockMode: .ecb, algorithm: .blowfish, padding: .noPadding, data: data!, key: strDataServerPublicKey, iv: iv)

//            let strDecodedValue = String.init(data: dassta, encoding: .utf8) ?? ""
//            print("*****strDecodedValue", strDecodedValue)
//            // safe way, provided data is \0-terminated
//            let newStr1 = String(data: dassta.subdata(in: 0 ..< dassta.count - 1), encoding: .utf8)
            // unsafe way, provided data is \0-terminated
            var strDecodedValue = dassta.withUnsafeBytes(String.init(utf8String:)) ?? ""
//            print("*****strDecodedValue", strDecodedValue)
            if (strDecodedValue == "")
            {
                strDecodedValue = String.init(data: dassta, encoding: .utf8) ?? ""
//                print("*****strDecodedValue", strDecodedValue)
            }
            strDecodedValue = strDecodedValue.replacingOccurrences(of: "\0", with: "", options: NSString.CompareOptions.literal, range:nil)

            return strDecodedValue
            
        }
        catch let error as NSError
        {
            print("error",error)
            return nil
        }
    }
    
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    
    //: ### Base64 decoding a string
    func base64Decoded() -> String? {
//        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
//            return String(data: data, encoding: .utf8)

        
        let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
        
        let base64Decoded = String(data: data!, encoding: .utf16)
        
        return base64Decoded
//            let array : [UInt8] = Array(self.utf8)
//
//            //Convert [UInt8] to NSData
//            let data = NSData(bytes: array, length: array.count)
//
//            //Encode to base64
//            let base64Data = data.base64EncodedData(options: .lineLength64Characters)
//
//            //Decode base64
//            let newData = Data(base64Encoded: base64Data, options: .ignoreUnknownCharacters)!
//
//            //Convert NSData to NSString
//            let newNSString = String(data: data as Data, encoding: .utf8)!
//
//            print(newNSString)
//
//            return newNSString
//        }
//        return nil
    }
}

